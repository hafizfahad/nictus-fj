import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:multi_vendor_customer/generated/translations.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/onBoardModel.dart';
import 'package:multi_vendor_customer/src/models/splashModel.dart';
import 'package:multi_vendor_customer/src/models/themeModel.dart';
import 'package:multi_vendor_customer/src/utils/theme.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();

  //-----load-configurations-from-local-json
  await GlobalConfiguration().loadFromAsset("configurations");

  //----get-theme-settings
  configThemeData = ThemeModel.fromJson(GlobalConfiguration().get('theme'));

  //----get-splash-configurations
  configSplashData =
      SplashModel.fromJson(GlobalConfiguration().get('splashData'));

  //----get-onBoard-configurations
  configOnBoardData =
      OnBoardModel.fromJson(GlobalConfiguration().get('onBoardData'));
  onBoardDataList = configOnBoardData.onBoardData;

  print("---->> base_url: ${GlobalConfiguration().get('base_url')}");
  print("---->> theme: ${configThemeData.displayText}");
  print("---->> splashData: ${configSplashData.title}");
  print("---->> onBoardData: ${configOnBoardData.length}");
  print("---->> onBoardData: ${onBoardDataList[0].title}");

  runApp(InitClass());
}

class InitClass extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _InitClassState createState() => _InitClassState();
}

class _InitClassState extends State<InitClass> {
  @override
  Widget build(BuildContext context) {
    Get.put(AppController());
    Get.put(HomeController());

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,

      translations: TranslationClass(), // your translations
      locale: Locale('en', 'US'),
      fallbackLocale:
          Locale('en', 'US'), // translations will be displayed in that locale

      initialRoute: PageRoutes.splash,
      getPages: routes(),

      themeMode:
          box.read('themeValue') == "dark" ? ThemeMode.dark : ThemeMode.light,

      theme: lightTheme(),
      darkTheme: darkTheme(),
    );
  }
}
