import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/pages/cartPage.dart';
import 'package:multi_vendor_customer/src/pages/chatsPage.dart';
import 'package:multi_vendor_customer/src/pages/chattingPage.dart';
import 'package:multi_vendor_customer/src/pages/contactUsPage.dart';
import 'package:multi_vendor_customer/src/pages/editUserProfilePage.dart';
import 'package:multi_vendor_customer/src/pages/orderHistoryDetailPage.dart';
import 'package:multi_vendor_customer/src/pages/orderHistoryPage.dart';
import 'package:multi_vendor_customer/src/pages/paymentMethodsPage.dart';
import 'package:multi_vendor_customer/src/pages/reviewWritePage.dart';
import 'package:multi_vendor_customer/src/pages/reviewsPage.dart';
import 'package:multi_vendor_customer/src/pages/checkoutPage.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/homePage1.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/bottomNavigationBarPage.dart';
import 'package:multi_vendor_customer/src/pages/loginPage.dart';
import 'package:multi_vendor_customer/src/pages/maintenanceEnvironmentPage.dart';
import 'package:multi_vendor_customer/src/pages/onBoardPage.dart';
import 'package:multi_vendor_customer/src/pages/orderConfirmationScreen.dart';
import 'package:multi_vendor_customer/src/pages/paymentPage.dart';
import 'package:multi_vendor_customer/src/pages/productDetailPage.dart';
import 'package:multi_vendor_customer/src/pages/resetPasswordSelectionPage.dart';
import 'package:multi_vendor_customer/src/pages/serverErrorPage.dart';
import 'package:multi_vendor_customer/src/pages/settingsPage.dart';
import 'package:multi_vendor_customer/src/pages/shopFiltersPage.dart';
import 'package:multi_vendor_customer/src/pages/shopPage.dart';
import 'package:multi_vendor_customer/src/pages/signUpPage.dart';
import 'package:multi_vendor_customer/src/pages/splashPage.dart';
import 'package:multi_vendor_customer/src/pages/userProfilePage.dart';
import 'package:multi_vendor_customer/src/pages/vendorStorePage.dart';
import 'package:multi_vendor_customer/src/pages/walletPage.dart';
import 'package:multi_vendor_customer/src/pages/wishListPage.dart';

routes() => [

  GetPage(name: "/", page: () => SplashPage()),
  GetPage(
    name: "/onBoard",
    page: () => OnBoardPage(),
    transitionDuration: Duration(milliseconds: 2000),
    transition: Transition.fadeIn
  ),
  GetPage(name: "/maintenance", page: () => EnvironmentMaintenancePage()),
  GetPage(name: "/bottomNavBar", page: () => BottomNavigationBarPage()),
  GetPage(name: "/home", page: () => HomePage()),
  GetPage(name: "/settings", page: () => SettingsPage()),
  GetPage(name: "/serverError", page: () => ServerErrorPage()),
  GetPage(name: "/login", page: () => LoginPage()),
  GetPage(name: "/signUp", page: () => SignUpPage()),
  GetPage(name: "/resetSelection", page: () => ResetPasswordSelectionPage()),
  GetPage(name: "/productDetail", page: () => ProductDetailPage()),
  GetPage(name: "/cart", page: () => CartPage()),
  GetPage(name: "/checkout", page: () => CheckoutPage()),
  GetPage(name: "/payment", page: () => PaymentScreen()),
  GetPage(name: "/orderConfirmation", page: () => OrderConfirmationScreen()),
  GetPage(name: "/vendorStore", page: () => VendorStorePage()),
  GetPage(name: "/shop", page: () => ShopPage()),
  GetPage(name: "/shopFilters", page: () => ShopFiltersPage()),
  GetPage(name: "/userProfile", page: () => UserProfilePage()),
  GetPage(name: "/wallet", page: () => MyWalletPage()),
  GetPage(name: "/orderHistory", page: () => OrderHistoryPage()),
  GetPage(name: "/orderHistoryDetail", page: () => OrderHistoryDetailPage()),
  GetPage(name: "/reviews", page: () => ReviewsPage()),
  GetPage(name: "/reviewWrite", page: () => ReviewWritePage()),
  GetPage(name: "/paymentMethods", page: () => PaymentMethodsPage()),
  GetPage(name: "/editUserProfile", page: () => EditUserProfilePage()),
  GetPage(name: "/contactUs", page: () => ContactUsPage()),
  GetPage(name: "/chats", page: () => ChatsPage()),
  GetPage(name: "/chatting", page: () => ChattingPage()),
  GetPage(name: "/wishList", page: () => WishListPage()),
];

class PageRoutes {
  static const String splash = '/';
  static const String onBoard = '/onBoard';
  static const String environmentMaintenance = '/maintenance';
  static const String serverError = '/serverError';
  static const String signUp = '/signUp';
  static const String login = '/login';
  static const String resetSelection = '/resetSelection';
  static const String bottomNavBar = '/bottomNavBar';
  static const String home = '/home';
  static const String settings = '/settings';
  static const String productDetail = '/productDetail';
  static const String cart = '/cart';
  static const String checkout = '/checkout';
  static const String payment = '/payment';
  static const String orderConfirmation = '/orderConfirmation';
  static const String vendorStore = '/vendorStore';
  static const String shop = '/shop';
  static const String shopFilters = '/shopFilters';
  static const String userProfile = '/userProfile';
  static const String wallet = '/wallet';
  static const String orderHistory = '/orderHistory';
  static const String orderHistoryDetail = '/orderHistoryDetail';
  static const String reviews = '/reviews';
  static const String reviewWrite = '/reviewWrite';
  static const String paymentMethods = '/paymentMethods';
  static const String editUserProfile = '/editUserProfile';
  static const String contactUs = '/contactUs';
  static const String chats = '/chats';
  static const String chatting = '/chatting';
  static const String wishList = '/wishList';

  Map<String, WidgetBuilder> routes() {
    return {
      splash: (context) => SplashPage(),
      onBoard: (context) => OnBoardPage(),
      environmentMaintenance: (context) => EnvironmentMaintenancePage(),
      serverError: (context) => ServerErrorPage(),
      signUp: (context) => SignUpPage(),
      login: (context) => LoginPage(),
      resetSelection: (context) => ResetPasswordSelectionPage(),
      bottomNavBar: (context) => BottomNavigationBarPage(),
      home: (context) => HomePage(),
      settings: (context) => SettingsPage(),
      productDetail: (context) => ProductDetailPage(),
      cart: (context) => CartPage(),
      checkout: (context) => CheckoutPage(),
      payment: (context) => PaymentScreen(),
      orderConfirmation: (context) => OrderConfirmationScreen(),
      vendorStore: (context) => VendorStorePage(),
      shop: (context) => ShopPage(),
      shopFilters: (context) => ShopFiltersPage(),
      userProfile: (context) => UserProfilePage(),
      wallet: (context) => MyWalletPage(),
      orderHistory: (context) => OrderHistoryPage(),
      orderHistoryDetail: (context) => OrderHistoryDetailPage(),
      reviews: (context) => ReviewsPage(),
      reviewWrite: (context) => ReviewWritePage(),
      paymentMethods: (context) => PaymentMethodsPage(),
      editUserProfile: (context) => EditUserProfilePage(),
      contactUs: (context) => ContactUsPage(),
      chats: (context) => ChatsPage(),
      chatting: (context) => ChattingPage(),
      wishList: (context) => WishListPage(),
    };
  }
}
