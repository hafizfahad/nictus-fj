import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/pages/productDetailPage.dart';

class HomeController extends GetxController{


  ///-------------------------------categories-list-check
  bool getAllCategoriesCheck = false;
  changeGetAllCategoriesCheck(bool value){
    getAllCategoriesCheck = value;
    update();
  }

  ///-------------------------------generic-app-data-check
  bool getAllGenericAppDataCheck = false;
  changeGetAllGenericAppDataCheck(bool value){
    getAllGenericAppDataCheck = value;
    update();
  }

  ///-------------------------------flash-sale-data-check
  bool getAFlashSaleDataCheck = false;
  changeGetAFlashSaleDataCheck(bool value){
    getAFlashSaleDataCheck = value;
    update();
  }

  ///-------------------------------new-arrival-data-check
  bool getNewArrivalDataCheck = false;
  changeGetNewArrivalDataCheck(bool value){
    getNewArrivalDataCheck = value;
    update();
  }

  ///------------------------------- featured-products-data-check
  bool getFeaturedProductsDataCheck = false;
  changeGetFeaturedProductsDataCheck(bool value){
    getFeaturedProductsDataCheck = value;
    update();
  }

  ///------------------------------- top-selling-products-data-check
  bool getTopSellingProductsDataCheck = false;
  changeGetTopSellingProductsDataCheck(bool value){
    getTopSellingProductsDataCheck = value;
    update();
  }

  ///------------------------------- trending-products-data-check
  bool getTrendingProductsDataCheck = false;
  changeGetTrendingProductsDataCheck(bool value){
    getTrendingProductsDataCheck = value;
    update();
  }

  ///------------------------------- top-reviewed-products-data-check
  bool getTopReviewedProductsDataCheck = false;
  changeGetTopReviewedProductsDataCheck(bool value){
    getTopReviewedProductsDataCheck = value;
    update();
  }

  ///------------------------------- featured-vendors-products-data-check
  bool getFeaturedVendorsProductsDataCheck = false;
  changeGetFeaturedVendorsProductsDataCheck(bool value){
    getFeaturedVendorsProductsDataCheck = value;
    update();
  }

  ///------------------------------- category-wise-products-data-check
  bool getCategoryWiseProductsDataCheck = false;
  changeGetCategoryWiseProductsDataCheck(bool value){
    getCategoryWiseProductsDataCheck = value;
    update();
  }

  ///------------------------------- product-detail-data-check
  bool getProductDetailDataCheck = false;
  changeGetProductDetailDataCheck(bool value){
    getProductDetailDataCheck = value;
    update();
  }
  int getProductDetailTabsDataCount = 0;
  changeGetProductDetailTabsDataCount(int updatedValue){
    getProductDetailTabsDataCount = updatedValue;
    update();
  }

  ///------------------------------- shop-page-products-data-check
  bool getShopPageProductsDataCheck = false;
  changeGetShopPageProductsDataCheck(bool value){
    getShopPageProductsDataCheck = value;
    update();
  }

  ///------------------------------- shop-page-data-check
  bool getShopPageDataCheck = false;
  changeGetShopPageDataCheck(bool value){
    getShopPageDataCheck = value;
    update();
  }

  ///------------------------------- products-of-vendor-data-check
  bool getProductsOfVendorDataCheck = false;
  changeGetProductsOfVendorDataCheck(bool value){
    getProductsOfVendorDataCheck = value;
    update();
  }

  ///------------------------------- vendor-store-data-check
  bool getVendorStoreDetailDataCheck = false;
  changeGetVendorStoreDetailDataCheck(bool value){
    getVendorStoreDetailDataCheck = value;
    update();
  }

  ///------------------------------- wish-list-data-check
  bool getWishListDataCheck = false;
  changeGetWishListDataCheckDataCheck(bool value){
    getWishListDataCheck = value;
    update();
  }



  int quantity = 1;
  updateQuantity(int value){
    quantity = value;
    update();
  }

  List cartList = [];
  updateCartList(List value){
    cartList = value;
    update();
  }
  calculateTotalCart(){
    int totalPayable = 0;
    cartList.forEach((element) {
      totalPayable = totalPayable + (element['subTotal']);
    });
    return totalPayable;
  }
}