import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppController extends GetxController{

  ///-------------------------------internet-check
  bool internetChecker = true;
  changeInternetCheckerState(bool value){
    internetChecker = value;
    update();
  }

  ///-------------------------------server-check
  bool serverErrorCheck = true;
  changeServerErrorCheck(bool value){
    serverErrorCheck = value;
    update();
  }

  ///-------------------------------grid-list-view-for-shop-page
  bool gridViewForShopPageCheck = true;
  changeGridViewForShopPageCheck(bool value){
    gridViewForShopPageCheck = value;
    update();
  }

  ///------------------------------- pagination-check
  bool getPaginationProgressCheck = false;
  changeGetPaginationProgressCheck(bool value){
    getPaginationProgressCheck = value;
    update();
  }

  ///------------------------------- loader-check
  bool loaderCheck = false;
  changeLoaderCheck(bool value){
    loaderCheck = value;
    update();
  }

}