
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class GetShopPageDataModel {
  GetShopPageDataModelData _data;
  bool _success;
  String _message;
  List<dynamic> _errors;

  GetShopPageDataModelData get data => _data;
  bool get success => _success;
  String get message => _message;
  List<dynamic> get errors => _errors;

  GetShopPageDataModel({
    GetShopPageDataModelData data,
      bool success, 
      String message, 
      List<dynamic> errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
}

  GetShopPageDataModel.fromJson(dynamic json) {
    _data = json["data"] != null ? GetShopPageDataModelData.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetShopPageDataModelData {
  List<GetShopPageDataModelCategories> _categories;
  List<Brand> _brands;
  List<GetShopPageDataModelAttributes> _attributes;

  List<GetShopPageDataModelCategories> get categories => _categories;
  List<Brand> get brands => _brands;
  List<GetShopPageDataModelAttributes> get attributes => _attributes;

  GetShopPageDataModelData({
      List<GetShopPageDataModelCategories> categories,
      List<Brand> brands,
      List<GetShopPageDataModelAttributes> attributes}){
    _categories = categories;
    _brands = brands;
    _attributes = attributes;
}

  GetShopPageDataModelData.fromJson(dynamic json) {
    if (json["categories"] != null) {
      _categories = [];
      json["categories"].forEach((v) {
        _categories.add(GetShopPageDataModelCategories.fromJson(v));
      });
    }
    if (json["brands"] != null) {
      _brands = [];
      json["brands"].forEach((v) {
        _brands.add(Brand.fromJson(v));
      });
    }
    if (json["attributes"] != null) {
      _attributes = [];
      json["attributes"].forEach((v) {
        _attributes.add(GetShopPageDataModelAttributes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_categories != null) {
      map["categories"] = _categories.map((v) => v.toJson()).toList();
    }
    if (_brands != null) {
      map["brands"] = _brands.map((v) => v.toJson()).toList();
    }
    if (_attributes != null) {
      map["attributes"] = _attributes.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetShopPageDataModelAttributes {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  List<GetShopPageDataModelValues> _values;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  List<GetShopPageDataModelValues> get values => _values;

  GetShopPageDataModelAttributes({
      int id, 
      String name, 
      String description, 
      String descriptionWeb, 
      List<GetShopPageDataModelValues> values}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _values = values;
}

  GetShopPageDataModelAttributes.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    if (json["values"] != null) {
      _values = [];
      json["values"].forEach((v) {
        _values.add(GetShopPageDataModelValues.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_values != null) {
      map["values"] = _values.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetShopPageDataModelValues {
  int _id;
  String _name;
  int _attributeId;
  String _slug;
  bool _isSelected;

  int get id => _id;
  String get name => _name;
  int get attributeId => _attributeId;
  String get slug => _slug;
  bool get isSelected => _isSelected;

  set isSelected (bool name) {
    this._isSelected = name;
  }

  GetShopPageDataModelValues({
      int id, 
      String name, 
      int attributeId, 
      String slug, 
      bool isSelected}){
    _id = id;
    _name = name;
    _attributeId = attributeId;
    _slug = slug;
    _isSelected = isSelected;
}

  GetShopPageDataModelValues.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _attributeId = json["attribute_id"];
    _slug = json["slug"];
    _isSelected = json["is_selected"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["attribute_id"] = _attributeId;
    map["slug"] = _slug;
    map["is_selected"] = _isSelected;
    return map;
  }

}

class GetShopPageDataModelCategories {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  ImageModal _image;
  ImageModal _icon;
  String _slug;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  ImageModal get image => _image;
  ImageModal get icon => _icon;
  String get slug => _slug;

  GetShopPageDataModelCategories({
      int id, 
      String name, 
      String description, 
      String descriptionWeb,
    ImageModal image,
    ImageModal icon,
      String slug}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _image = image;
    _icon = icon;
    _slug = slug;
}

  GetShopPageDataModelCategories.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _image = json["image"] != null ? ImageModal.fromJson(json["image"]) : null;
    _icon = json["icon"] != null ? ImageModal.fromJson(json["icon"]) : null;
    _slug = json["slug"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    if (_icon != null) {
      map["icon"] = _icon.toJson();
    }
    map["slug"] = _slug;
    return map;
  }

}
