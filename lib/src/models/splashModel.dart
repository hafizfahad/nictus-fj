
class SplashModel {
  String title;
  String subTitle;
  String splashLogo;
  String splashBackground;
  String poweredByLogo;

  SplashModel({
    this.title,
    this.subTitle,
    this.splashLogo,
    this.splashBackground,
    this.poweredByLogo,
  });

  factory SplashModel.fromJson(Map<String, dynamic> json) => SplashModel(
    title : json["title"].toString(),
    subTitle : json["subTitle"].toString(),
    splashLogo : json["splashLogo"].toString(),
    splashBackground : json["splashBackground"].toString(),
    poweredByLogo : json["poweredByLogo"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "title" : title,
    "subTitle" : subTitle,
    "splashLogo" : splashLogo,
    "splashBackground" : splashBackground,
    "poweredByLogo" : poweredByLogo,
  };

  @override
  String toString() {
    return 'Splash: {title: $title}';
  }
}