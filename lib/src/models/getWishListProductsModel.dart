import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getShopPageProductsModel.dart';

class GetWishListProductsModel {
  GetWishListProductsModelData _data;
  bool _success;
  String _message;
  List<dynamic> _errors;

  GetWishListProductsModelData get data => _data;
  bool get success => _success;
  String get message => _message;
  List<dynamic> get errors => _errors;

  GetWishListProductsModel({
    GetWishListProductsModelData data,
    bool success,
    String message,
    List<dynamic> errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetWishListProductsModel.fromJson(dynamic json) {
    _data = json["data"] != null ? GetWishListProductsModelData.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetWishListProductsModelData {
  List<ProductModel> _data;
  LinksPagination _links;
  MetaPagination _meta;

  List<ProductModel> get data => _data;
  LinksPagination get links => _links;
  MetaPagination get meta => _meta;

  GetWishListProductsModelData({
    List<ProductModel> data,
    LinksPagination links,
    MetaPagination meta}){
    _data = data;
    _links = links;
    _meta = meta;
  }

  GetWishListProductsModelData.fromJson(dynamic json) {
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(ProductModel.fromJson(v));
      });
    }
    _links = json["links"] != null ? LinksPagination.fromJson(json["links"]) : null;
    _meta = json["meta"] != null ? MetaPagination.fromJson(json["meta"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    if (_links != null) {
      map["links"] = _links.toJson();
    }
    if (_meta != null) {
      map["meta"] = _meta.toJson();
    }
    return map;
  }

}