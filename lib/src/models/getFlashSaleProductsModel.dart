
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';

class GetFlashSaleProductsModel {
  List<ProductModel> _data;
  // Links _links;
  // Meta _meta;
  bool _success;
  String _message;
  List _errors;

  List<ProductModel> get data => _data;
  // Links get links => _links;
  // Meta get meta => _meta;
  bool get success => _success;
  String get message => _message;
  List get errors => _errors;

  GetFlashSaleProductsModel({
      List<ProductModel> data,
      // Links links,
      // Meta meta,
      bool success,
      String message,
      List errors}){
    _data = data;
    // _links = links;
    // _meta = meta;
    _success = success;
    _message = message;
    _errors = errors;
}

  GetFlashSaleProductsModel.fromJson(dynamic json) {
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(ProductModel.fromJson(v));
      });
    }
    // _links = json["links"] != null ? Links.fromJson(json["links"]) : null;
    // _meta = json["meta"] != null ? Meta.fromJson(json["meta"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    // if (_links != null) {
    //   map["links"] = _links.toJson();
    // }
    // if (_meta != null) {
    //   map["meta"] = _meta.toJson();
    // }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class ProductModel {
  int _id;
  List<Attributes> _attributes;
  List<Variants> _variants;
  String _name;
  List<String> _tags;
  String _shortDescription;
  String _description;
  String _refundPolicy;
  String _shortDescriptionWeb;
  String _descriptionWeb;
  String _refundPolicyWeb;
  int _productType;
  String _sku;
  Brand _brand;
  String _modal;
  String _displayPrice;
  String _price;
  String _currency;
  String _defaultCurrency;
  String _defaultPrice;
  int _conversionRate;
  int _stock;
  String _availableDate;
  String _weight;
  String _unit;
  List<ImageModal> _media;
  Manufacturer _manufacturer;
  TaxClass _taxClass;
  VendorModel _vendor;
  int _productOrdered;
  int _productLiked;
  String _slug;
  dynamic _externalLink;
  int _maxOrder;
  int _minOrder;
  List<FAQModel> _faq;
  FlashSaleModal _flashSale;
  FlashSaleModal _specialSale;
  List<Categories> _categories;
  Seo _seo;
  List<ReviewsModal> _reviews;
  int _totalReviews;
  double _reviewsAverageRating;

  int get id => _id;
  List<Attributes> get attributes => _attributes;
  List<Variants> get variants => _variants;
  String get name => _name;
  List<String> get tags => _tags;
  String get shortDescription => _shortDescription;
  String get description => _description;
  String get refundPolicy => _refundPolicy;
  String get shortDescriptionWeb => _shortDescriptionWeb;
  String get descriptionWeb => _descriptionWeb;
  String get refundPolicyWeb => _refundPolicyWeb;
  int get productType => _productType;
  String get sku => _sku;
  Brand get brand => _brand;
  String get modal => _modal;
  String get displayPrice => _displayPrice;
  String get price => _price;
  String get currency => _currency;
  String get defaultCurrency => _defaultCurrency;
  String get defaultPrice => _defaultPrice;
  int get conversionRate => _conversionRate;
  int get stock => _stock;
  String get availableDate => _availableDate;
  String get weight => _weight;
  String get unit => _unit;
  List<ImageModal> get media => _media;
  Manufacturer get manufacturer => _manufacturer;
  TaxClass get taxClass => _taxClass;
  VendorModel get vendor => _vendor;
  int get productOrdered => _productOrdered;
  int get productLiked => _productLiked;
  String get slug => _slug;
  dynamic get externalLink => _externalLink;
  int get maxOrder => _maxOrder;
  int get minOrder => _minOrder;
  List<FAQModel> get faq => _faq;
  FlashSaleModal get flashSale => _flashSale;
  FlashSaleModal get specialSale => _specialSale;
  List<Categories> get categories => _categories;
  Seo get seo => _seo;
  List<ReviewsModal> get reviews => _reviews;
  int get totalReviews => _totalReviews;
  double get reviewsAverageRating => _reviewsAverageRating;

  ProductModel({
    int id,
    List<Attributes> attributes,
    List<Variants> variants,
    String name,
    List<String> tags,
    String shortDescription,
    String description,
    String refundPolicy,
    String shortDescriptionWeb,
    String descriptionWeb,
    String refundPolicyWeb,
    int productType,
    String sku,
    Brand brand,
    String modal,
    String displayPrice,
    String price,
    String currency,
    String defaultCurrency,
    String defaultPrice,
    int conversionRate,
    int stock,
    String availableDate,
    String weight,
    String unit,
    List<ImageModal> media,
    Manufacturer manufacturer,
    TaxClass taxClass,
    VendorModel vendor,
    int productOrdered,
    int productLiked,
    String slug,
    dynamic externalLink,
    int maxOrder,
    int minOrder,
    List<FAQModel> faq,
    FlashSaleModal flashSale,
    FlashSaleModal specialSale,
    List<Categories> categories,
    Seo seo,
    List<ReviewsModal> reviews,
    int totalReviews,
    double reviewsAverageRating}){
    _id = id;
    _attributes = attributes;
    _variants = variants;
    _name = name;
    _tags = tags;
    _shortDescription = shortDescription;
    _description = description;
    _refundPolicy = refundPolicy;
    _shortDescriptionWeb = shortDescriptionWeb;
    _descriptionWeb = descriptionWeb;
    _refundPolicyWeb = refundPolicyWeb;
    _productType = productType;
    _sku = sku;
    _brand = brand;
    _modal = modal;
    _displayPrice = displayPrice;
    _price = price;
    _currency = currency;
    _defaultCurrency = defaultCurrency;
    _defaultPrice = defaultPrice;
    _conversionRate = conversionRate;
    _stock = stock;
    _availableDate = availableDate;
    _weight = weight;
    _unit = unit;
    _media = media;
    _manufacturer = manufacturer;
    _taxClass = taxClass;
    _vendor = vendor;
    _productOrdered = productOrdered;
    _productLiked = productLiked;
    _slug = slug;
    _externalLink = externalLink;
    _maxOrder = maxOrder;
    _minOrder = minOrder;
    _faq = faq;
    _flashSale = flashSale;
    _specialSale = specialSale;
    _categories = categories;
    _seo = seo;
    _reviews = reviews;
    _totalReviews = totalReviews;
    _reviewsAverageRating = reviewsAverageRating;
  }

  ProductModel.fromJson(dynamic json) {
    _id = json["id"];
    if (json["attributes"] != null) {
      _attributes = [];
      json["attributes"].forEach((v) {
        _attributes.add(Attributes.fromJson(v));
      });
    }
    if (json["variants"] != null) {
      _variants = [];
      json["variants"].forEach((v) {
        _variants.add(Variants.fromJson(v));
      });
    }
    _name = json["name"];
    _tags = json["tags"] != null ? json["tags"].cast<String>() : [];
    _shortDescription = json["short_description"];
    _description = json["description"];
    _refundPolicy = json["refund_policy"];
    _shortDescriptionWeb = json["short_description_web"];
    _descriptionWeb = json["description_web"];
    _refundPolicyWeb = json["refund_policy_web"];
    _productType = json["product_type"];
    _sku = json["sku"];
    _brand = json["brand"] != null ? Brand.fromJson(json["brand"]) : null;
    _modal = json["modal"];
    _displayPrice = json["display_price"];
    _price = json["price"];
    _currency = json["currency"];
    _defaultCurrency = json["default_currency"];
    _defaultPrice = json["default_price"];
    _conversionRate = json["conversion_rate"];
    _stock = json["stock"];
    _availableDate = json["available_date"];
    _weight = json["weight"];
    _unit = json["unit"];
    if (json["media"] != null) {
      _media = [];
      json["media"].forEach((v) {
        _media.add(ImageModal.fromJson(v));
      });
    }
    _manufacturer = json["manufacturer"] != null ? Manufacturer.fromJson(json["manufacturer"]) : null;
    _taxClass = json["tax_class"] != null ? TaxClass.fromJson(json["tax_class"]) : null;
    _vendor = json["vendor"] != null ? VendorModel.fromJson(json["vendor"]) : null;
    _productOrdered = json["product_ordered"];
    _productLiked = json["product_liked"];
    _slug = json["slug"];
    _externalLink = json["external_link"];
    _maxOrder = json["max_order"];
    _minOrder = json["min_order"];
    if (json["faq"] != null) {
      _faq = [];
      json["faq"].forEach((v) {
        _faq.add(FAQModel.fromJson(v));
      });
    }
    _flashSale = json["flash_sale"] != null ? FlashSaleModal.fromJson(json["flash_sale"]) : null;
    _specialSale = json["special_sale"]  != null ? FlashSaleModal.fromJson(json["special_sale"]) : null;
    if (json["categories"] != null) {
      _categories = [];
      json["categories"].forEach((v) {
        _categories.add(Categories.fromJson(v));
      });
    }
    _seo = json["seo"] != null ? Seo.fromJson(json["seo"]) : null;
    if (json["reviews"] != null) {
      _reviews = [];
      json["reviews"].forEach((v) {
        _reviews.add(ReviewsModal.fromJson(v));
      });
    }
    _totalReviews = json["total_reviews"];
    _reviewsAverageRating = double.parse(json["reviews_average_rating"].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    if (_attributes != null) {
      map["attributes"] = _attributes.map((v) => v.toJson()).toList();
    }
    if (_variants != null) {
      map["variants"] = _variants.map((v) => v.toJson()).toList();
    }
    map["name"] = _name;
    map["short_description"] = _shortDescription;
    map["description"] = _description;
    map["refund_policy"] = _refundPolicy;
    map["short_description_web"] = _shortDescriptionWeb;
    map["description_web"] = _descriptionWeb;
    map["refund_policy_web"] = _refundPolicyWeb;
    map["product_type"] = _productType;
    map["sku"] = _sku;
    if (_brand != null) {
      map["brand"] = _brand.toJson();
    }
    map["modal"] = _modal;
    map["display_price"] = _displayPrice;
    map["price"] = _price;
    map["currency"] = _currency;
    map["default_currency"] = _defaultCurrency;
    map["default_price"] = _defaultPrice;
    map["conversion_rate"] = _conversionRate;
    map["stock"] = _stock;
    map["available_date"] = _availableDate;
    map["weight"] = _weight;
    map["unit"] = _unit;
    if (_media != null) {
      map["media"] = _media.map((v) => v.toJson()).toList();
    }
    if (_manufacturer != null) {
      map["manufacturer"] = _manufacturer.toJson();
    }
    if (_taxClass != null) {
      map["tax_class"] = _taxClass.toJson();
    }
    if (_vendor != null) {
      map["vendor"] = _vendor.toJson();
    }
    map["product_ordered"] = _productOrdered;
    map["product_liked"] = _productLiked;
    map["slug"] = _slug;
    map["external_link"] = _externalLink;
    map["max_order"] = _maxOrder;
    map["min_order"] = _minOrder;
    if (_faq != null) {
      map["faq"] = _faq.map((v) => v.toJson()).toList();
    }
    if (_flashSale != null) {
      map["flash_sale"] = _flashSale.toJson();
    }
    map["special_sale"] = _specialSale;
    if (_categories != null) {
      map["categories"] = _categories.map((v) => v.toJson()).toList();
    }
    if (_seo != null) {
      map["seo"] = _seo.toJson();
    }
    if (_reviews != null) {
      map["reviews"] = _reviews.map((v) => v.toJson()).toList();
    }
    map["total_reviews"] = _totalReviews;
    map["reviews_average_rating"] = _reviewsAverageRating;
    return map;
  }

}

class Variants {
  int _id;
  String _price;
  String _displayPrice;
  String _currency;
  String _defaultCurrency;
  String _defaultPrice;
  String _variant;
  dynamic _sku;

  int get id => _id;
  String get price => _price;
  String get displayPrice => _displayPrice;
  String get currency => _currency;
  String get defaultCurrency => _defaultCurrency;
  String get defaultPrice => _defaultPrice;
  String get variant => _variant;
  dynamic get sku => _sku;

  Variants({
    int id,
    String price,
    String displayPrice,
    String currency,
    String defaultCurrency,
    String defaultPrice,
    String variant,
    dynamic sku}){
    _id = id;
    _price = price;
    _displayPrice = displayPrice;
    _currency = currency;
    _defaultCurrency = defaultCurrency;
    _defaultPrice = defaultPrice;
    _variant = variant;
    _sku = sku;
  }

  Variants.fromJson(dynamic json) {
    _id = json["id"];
    _price = json["price"];
    _displayPrice = json["display_price"];
    _currency = json["currency"];
    _defaultCurrency = json["default_currency"];
    _defaultPrice = json["default_price"];
    _variant = json["variant"];
    _sku = json["sku"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["price"] = _price;
    map["display_price"] = _displayPrice;
    map["currency"] = _currency;
    map["default_currency"] = _defaultCurrency;
    map["default_price"] = _defaultPrice;
    map["variant"] = _variant;
    map["sku"] = _sku;
    return map;
  }

}

class Attributes {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  List<Values> _values;
  int _isActive;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  List<Values> get values => _values;
  int get isActive => _isActive;

  Attributes({
    int id,
    String name,
    String description,
    String descriptionWeb,
    List<Values> values,
    int isActive}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _values = values;
    _isActive = isActive;
  }

  Attributes.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    if (json["values"] != null) {
      _values = [];
      json["values"].forEach((v) {
        _values.add(Values.fromJson(v));
      });
    }
    _isActive = json["is_active"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_values != null) {
      map["values"] = _values.map((v) => v.toJson()).toList();
    }
    map["is_active"] = _isActive;
    return map;
  }

}

class Values {
  int _id;
  String _name;
  int _attributeId;
  int _productId;
  String _slug;
  bool _isSelected;

  int get id => _id;
  String get name => _name;
  int get attributeId => _attributeId;
  int get productId => _productId;
  String get slug => _slug;
  bool get isSelected => _isSelected;

  set isSelected (bool name) {
    this._isSelected = name;
  }

  Values({
    int id,
    String name,
    int attributeId,
    int productId,
    String slug,
    bool isSelected}){
    _id = id;
    _name = name;
    _attributeId = attributeId;
    _productId = productId;
    _slug = slug;
    _isSelected = isSelected;
  }

  Values.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _attributeId = json["attribute_id"];
    _productId = json["product_id"];
    _slug = json["slug"];
    _isSelected = json["is_selected"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["attribute_id"] = _attributeId;
    map["product_id"] = _productId;
    map["slug"] = _slug;
    map["is_selected"] = _isSelected;
    return map;
  }

}

class Brand {
  int _id;
  String _name;
  ImageModal _image;
  String _description;
  String _websiteUrl;
  int _isActive;

  int get id => _id;
  String get name => _name;
  ImageModal get image => _image;
  String get description => _description;
  String get websiteUrl => _websiteUrl;
  int get isActive => _isActive;

  Brand({
    int id,
    String name,
    ImageModal image,
    String description,
    String websiteUrl,
    int isActive}){
    _id = id;
    _name = name;
    _image = image;
    _description = description;
    _websiteUrl = websiteUrl;
    _isActive = isActive;
  }

  Brand.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _image = json["image"] != null ? ImageModal.fromJson(json["image"]) : null;
    _description = json["description"];
    _websiteUrl = json["website_url"];
    _isActive = json["is_active"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    map["description"] = _description;
    map["website_url"] = _websiteUrl;
    map["is_active"] = _isActive;
    return map;
  }

}

class ReviewsModal {
  int _id;
  Customer _customer;
  String _description;
  double _rating;

  int get id => _id;
  Customer get customer => _customer;
  String get description => _description;
  double get rating => _rating;

  ReviewsModal({
    int id,
    Customer customer,
    String description,
    double rating}){
    _id = id;
    _customer = customer;
    _description = description;
    _rating = rating;
  }

  ReviewsModal.fromJson(dynamic json) {
    _id = json["id"];
    _customer = json["customer"] != null ? Customer.fromJson(json["customer"]) : null;
    _description = json["description"];
    _rating = double.parse(json["rating"].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    if (_customer != null) {
      map["customer"] = _customer.toJson();
    }
    map["description"] = _description;
    map["rating"] = _rating;
    return map;
  }

}

class Customer {
  int _id;
  String _name;
  String _firstName;
  String _lastName;
  String _email;
  String _gender;
  String _dob;
  String _phone;
  int _isActive;
  String _createdAt;

  int get id => _id;
  String get name => _name;
  String get firstName => _firstName;
  String get lastName => _lastName;
  String get email => _email;
  String get gender => _gender;
  String get dob => _dob;
  String get phone => _phone;
  int get isActive => _isActive;
  String get createdAt => _createdAt;

  Customer({
    int id,
    String name,
    String firstName,
    String lastName,
    String email,
    String gender,
    String dob,
    String phone,
    int isActive,
    String createdAt}){
    _id = id;
    _name = name;
    _firstName = firstName;
    _lastName = lastName;
    _email = email;
    _gender = gender;
    _dob = dob;
    _phone = phone;
    _isActive = isActive;
    _createdAt = createdAt;
  }

  Customer.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _firstName = json["first_name"];
    _lastName = json["last_name"];
    _email = json["email"];
    _gender = json["gender"];
    _dob = json["dob"];
    _phone = json["phone"];
    _isActive = json["is_active"];
    _createdAt = json["created_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["first_name"] = _firstName;
    map["last_name"] = _lastName;
    map["email"] = _email;
    map["gender"] = _gender;
    map["dob"] = _dob;
    map["phone"] = _phone;
    map["is_active"] = _isActive;
    map["created_at"] = _createdAt;
    return map;
  }

}

class Seo {
  int _id;
  String _title;
  String _description;
  String _keywords;
  List<MetaTags> _metaTags;

  int get id => _id;
  String get title => _title;
  String get description => _description;
  String get keywords => _keywords;
  List<MetaTags> get metaTags => _metaTags;

  Seo({
      int id,
      String title,
      String description,
      String keywords,
      List<MetaTags> metaTags,}){
    _id = id;
    _title = title;
    _description = description;
    _keywords = keywords;
    _metaTags = metaTags;
}

  Seo.fromJson(dynamic json) {
    _id = json["id"];
    _title = json["title"];
    _description = json["description"];
    _keywords = json["keywords"];
    if (json["meta_tags"] != null) {
      _metaTags = [];
      json["meta_tags"].forEach((v) {
        _metaTags.add(MetaTags.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["title"] = _title;
    map["description"] = _description;
    map["keywords"] = _keywords;
    if (_metaTags != null) {
      map["meta_tags"] = _metaTags.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class MetaTags {
  String _name;
  String _check;

  String get name => _name;
  String get check => _check;

  MetaTags({
      String name,
      String check}){
    _name = name;
    _check = check;
}

  MetaTags.fromJson(dynamic json) {
    _name = json["name"];
    _check = json["check"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = _name;
    map["check"] = _check;
    return map;
  }

}

class Categories {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  ImageModal _image;
  ImageModal _icon;
  List<Childrens> _childrens;
  String _slug;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  ImageModal get image => _image;
  ImageModal get icon => _icon;
  List<Childrens> get childrens => _childrens;
  String get slug => _slug;

  Categories({
      int id,
      String name,
      String description,
      String descriptionWeb,
    ImageModal image,
    ImageModal icon,
      List<Childrens> childrens,
      String slug}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _image = image;
    _icon = icon;
    _childrens = childrens;
    _slug = slug;
}

  Categories.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _image = json["image"] != null
        ?ImageModal.fromJson(json['image'])
        :null;
    _icon = json["icon"] != null
        ?ImageModal.fromJson(json['icon'])
        :null;
    if (json["childrens"] != null) {
      _childrens = [];
      json["childrens"].forEach((v) {
        _childrens.add(Childrens.fromJson(v));
      });
    }
    _slug = json["slug"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    if(_icon != null){
      map["icon"] = _icon.toJson();
    }
    if (_childrens != null) {
      map["childrens"] = _childrens.map((v) => v.toJson()).toList();
    }
    map["slug"] = _slug;
    return map;
  }

}

class Childrens {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  ImageModal _image;
  ImageModal _icon;
  List<Childrens> _childrens;
  String _slug;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  ImageModal get image => _image;
  ImageModal get icon => _icon;
  List<Childrens> get childrens => _childrens;
  String get slug => _slug;

  Childrens({
      int id,
      String name,
      String description,
      String descriptionWeb,
    ImageModal image,
    ImageModal icon,
      List<Childrens> childrens,
      String slug}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _image = image;
    _icon = icon;
    _childrens = childrens;
    _slug = slug;
}

  Childrens.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _image = json["image"] != null
        ?ImageModal.fromJson(json['image'])
        :null;
    _icon = json["icon"] != null
        ?ImageModal.fromJson(json['icon'])
        :null;
    if (json["childrens"] != null) {
      _childrens = [];
      json["childrens"].forEach((v) {
        _childrens.add(Childrens.fromJson(v));
      });
    }
    _slug = json["slug"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    if(_icon != null){
      map["icon"] = _icon.toJson();
    }
    if (_childrens != null) {
      map["childrens"] = _childrens.map((v) => v.toJson()).toList();
    }
    map["slug"] = _slug;
    return map;
  }

}

class FlashSaleModal {
  int _id;
  String _displayPrice;
  String _price;
  String _currency;
  String _defaultCurrency;
  String _defaultCurrencyPrice;
  int _conversionRate;
  String _startDate;
  String _expireDate;

  int get id => _id;
  String get displayPrice => _displayPrice;
  String get price => _price;
  String get currency => _currency;
  String get defaultCurrency => _defaultCurrency;
  String get defaultCurrencyPrice => _defaultCurrencyPrice;
  int get conversionRate => _conversionRate;
  String get startDate => _startDate;
  String get expireDate => _expireDate;

  FlashSaleModal({
    int id,
    String displayPrice,
    String price,
    String currency,
    String defaultCurrency,
    String defaultCurrencyPrice,
    int conversionRate,
    String startDate,
    String expireDate}){
    _id = id;
    _displayPrice = displayPrice;
    _price = price;
    _currency = currency;
    _defaultCurrency = defaultCurrency;
    _defaultCurrencyPrice = defaultCurrencyPrice;
    _conversionRate = conversionRate;
    _startDate = startDate;
    _expireDate = expireDate;
  }

  FlashSaleModal.fromJson(dynamic json) {
    _id = json["id"];
    _displayPrice = json["display_price"];
    _price = json["price"];
    _currency = json["currency"];
    _defaultCurrency = json["default_currency"];
    _defaultCurrencyPrice = json["default_currency_price"];
    _conversionRate = json["conversion_rate"];
    _startDate = json["start_date"];
    _expireDate = json["expire_date"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["display_price"] = _displayPrice;
    map["price"] = _price;
    map["currency"] = _currency;
    map["default_currency"] = _defaultCurrency;
    map["default_currency_price"] = _defaultCurrencyPrice;
    map["conversion_rate"] = _conversionRate;
    map["start_date"] = _startDate;
    map["expire_date"] = _expireDate;
    return map;
  }

}

class VendorModel {
  int _id;
  String _email;
  String _name;
  String _slug;
  String _contactPhone;
  ImageModal _profileImage;
  VendorStoreModel _store;
  int _isActive;
  String _updatedAt;

  int get id => _id;
  String get email => _email;
  String get name => _name;
  String get slug => _slug;
  String get contactPhone => _contactPhone;
  ImageModal get profileImage => _profileImage;
  VendorStoreModel get store => _store;
  int get isActive => _isActive;
  String get updatedAt => _updatedAt;

  VendorModel({
    int id,
    String email,
    String name,
    String slug,
    String contactPhone,
    ImageModal profileImage,
    VendorStoreModel store,
    int isActive,
    String updatedAt}){
    _id = id;
    _email = email;
    _name = name;
    _slug = slug;
    _contactPhone = contactPhone;
    _profileImage = profileImage;
    _store = store;
    _isActive = isActive;
    _updatedAt = updatedAt;
  }

  VendorModel.fromJson(dynamic json) {
    _id = json["id"];
    _email = json["email"];
    _name = json["name"];
    _slug = json["slug"];
    _contactPhone = json["contact_phone"];
    _profileImage = json["profile_image"] != null ? ImageModal.fromJson(json["profile_image"]) : null;
    _store = json["store"] != null ? VendorStoreModel.fromJson(json["store"]) : null;
    _isActive = json["is_active"];
    _updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["email"] = _email;
    map["name"] = _name;
    map["slug"] = _slug;
    map["contact_phone"] = _contactPhone;
    if (_profileImage != null) {
      map["profile_image"] = _profileImage.toJson();
    }
    if (_store != null) {
      map["store"] = _store.toJson();
    }
    map["is_active"] = _isActive;
    map["updated_at"] = _updatedAt;
    return map;
  }

}

class VendorStoreModel {
  int _id;
  String _name;
  String _address;
  ImageModal _coverImage;
  ImageModal _storeLogo;
  String _city;
  String _country;
  String _state;
  String _phone;
  String _postalCode;
  String _headline;
  String _description;
  int _isActive;
  String _updatedAt;

  int get id => _id;
  String get name => _name;
  String get address => _address;
  ImageModal get coverImage => _coverImage;
  ImageModal get storeLogo => _storeLogo;
  String get city => _city;
  String get country => _country;
  String get state => _state;
  String get phone => _phone;
  String get postalCode => _postalCode;
  String get headline => _headline;
  String get description => _description;
  int get isActive => _isActive;
  String get updatedAt => _updatedAt;

  VendorStoreModel({
    int id,
    String name,
    String address,
    ImageModal coverImage,
    ImageModal storeLogo,
    String city,
    String country,
    String state,
    String phone,
    String postalCode,
    String headline,
    String description,
    int isActive,
    String updatedAt}){
    _id = id;
    _name = name;
    _address = address;
    _coverImage = coverImage;
    _storeLogo = storeLogo;
    _city = city;
    _country = country;
    _state = state;
    _phone = phone;
    _postalCode = postalCode;
    _headline = headline;
    _description = description;
    _isActive = isActive;
    _updatedAt = updatedAt;
  }

  VendorStoreModel.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _address = json["address"];
    _coverImage = json["cover_image"] != null ? ImageModal.fromJson(json["cover_image"]) : null;
    _storeLogo = json["store_logo"] != null ? ImageModal.fromJson(json["store_logo"]) : null;
    _city = json["city"];
    _country = json["country"];
    _state = json["state"];
    _phone = json["phone"];
    _postalCode = json["postal_code"];
    _headline = json["headline"];
    _description = json["description"];
    _isActive = json["is_active"];
    _updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["address"] = _address;
    if (_coverImage != null) {
      map["cover_image"] = _coverImage.toJson();
    }
    if (_storeLogo != null) {
      map["store_logo"] = _storeLogo.toJson();
    }
    map["city"] = _city;
    map["country"] = _country;
    map["state"] = _state;
    map["phone"] = _phone;
    map["postal_code"] = _postalCode;
    map["headline"] = _headline;
    map["description"] = _description;
    map["is_active"] = _isActive;
    map["updated_at"] = _updatedAt;
    return map;
  }

}

class TaxClass {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;

  TaxClass({
      int id,
      String name,
      String description,
      String descriptionWeb}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
}

  TaxClass.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    return map;
  }

}

class Manufacturer {
  int _id;
  String _name;
  ImageModal _image;
  String _slug;
  String _description;
  String _descriptionWeb;
  String _websiteUrl;

  int get id => _id;
  String get name => _name;
  ImageModal get image => _image;
  String get slug => _slug;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  String get websiteUrl => _websiteUrl;

  Manufacturer({
      int id,
      String name,
    ImageModal image,
      String slug,
      String description,
      String descriptionWeb,
      String websiteUrl}){
    _id = id;
    _name = name;
    _image = image;
    _slug = slug;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _websiteUrl = websiteUrl;
}

  Manufacturer.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _image = json["image"] != null
        ?ImageModal.fromJson(json['image'])
        :null;
    _slug = json["slug"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _websiteUrl = json["website_url"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    map["slug"] = _slug;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    map["website_url"] = _websiteUrl;
    return map;
  }

}

class FAQModel {
  int _id;
  String _question;
  QuestionTranslations _questionTranslations;
  String _answer;
  AnswerTranslations _answerTranslations;
  int _isActive;
  String _createdAt;

  int get id => _id;
  String get question => _question;
  QuestionTranslations get questionTranslations => _questionTranslations;
  String get answer => _answer;
  AnswerTranslations get answerTranslations => _answerTranslations;
  int get isActive => _isActive;
  String get createdAt => _createdAt;

  FAQModel({
    int id,
    String question,
    QuestionTranslations questionTranslations,
    String answer,
    AnswerTranslations answerTranslations,
    int isActive,
    String createdAt}){
    _id = id;
    _question = question;
    _questionTranslations = questionTranslations;
    _answer = answer;
    _answerTranslations = answerTranslations;
    _isActive = isActive;
    _createdAt = createdAt;
  }

  FAQModel.fromJson(dynamic json) {
    _id = json["id"];
    _question = json["question"];
    _questionTranslations = json["questionTranslations"] != null ? QuestionTranslations.fromJson(json["questionTranslations"]) : null;
    _answer = json["answer"];
    _answerTranslations = json["answerTranslations"] != null ? AnswerTranslations.fromJson(json["answerTranslations"]) : null;
    _isActive = json["is_active"];
    _createdAt = json["created_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["question"] = _question;
    if (_questionTranslations != null) {
      map["questionTranslations"] = _questionTranslations.toJson();
    }
    map["answer"] = _answer;
    if (_answerTranslations != null) {
      map["answerTranslations"] = _answerTranslations.toJson();
    }
    map["is_active"] = _isActive;
    map["created_at"] = _createdAt;
    return map;
  }

}

class AnswerTranslations {
  String _pa;

  String get pa => _pa;

  AnswerTranslations({
    String pa}){
    _pa = pa;
  }

  AnswerTranslations.fromJson(dynamic json) {
    _pa = json["pa"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["pa"] = _pa;
    return map;
  }

}

class QuestionTranslations {
  String _pa;

  String get pa => _pa;

  QuestionTranslations({
    String pa}){
    _pa = pa;
  }

  QuestionTranslations.fromJson(dynamic json) {
    _pa = json["pa"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["pa"] = _pa;
    return map;
  }

}