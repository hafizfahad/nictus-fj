import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class GetCategoryWiseProductsModel {
  Data _data;
  bool _success;
  String _message;
  List _errors;

  Data get data => _data;
  bool get success => _success;
  String get message => _message;
  List get errors => _errors;

  GetCategoryWiseProductsModel({
    Data data,
    bool success,
    String message,
    List errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetCategoryWiseProductsModel.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}
class Data {
  List<CategoryWiseProductsModel> _otherCats;
  List<ProductModel> _allCategories;

  List<CategoryWiseProductsModel> get otherCats => _otherCats;
  List<ProductModel> get allCategories => _allCategories;

  Data({
    List<CategoryWiseProductsModel> otherCats,
    List<ProductModel> allCategories}){
    _otherCats = otherCats;
    _allCategories = allCategories;
  }

  Data.fromJson(dynamic json) {
    if (json["other_cats"] != null) {
      _otherCats = [];
      json["other_cats"].forEach((v) {
        _otherCats.add(CategoryWiseProductsModel.fromJson(v));
      });
    }
    if (json["all_categories"] != null) {
      _allCategories = [];
      json["all_categories"].forEach((v) {
        _allCategories.add(ProductModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_otherCats != null) {
      map["other_cats"] = _otherCats.map((v) => v.toJson()).toList();
    }
    if (_allCategories != null) {
      map["all_categories"] = _allCategories.map((v) => v.toJson()).toList();
    }
    return map;
  }

}
class CategoryWiseProductsModel {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  ImageModal _image;
  ImageModal _icon;
  List<ProductModel> _products;
  String _slug;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  ImageModal get image => _image;
  ImageModal get icon => _icon;
  List<ProductModel> get products => _products;
  String get slug => _slug;

  CategoryWiseProductsModel({
    int id,
    String name,
    String description,
    String descriptionWeb,
    ImageModal image,
    ImageModal icon,
    List<ProductModel> products,
    String slug}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _image = image;
    _icon = icon;
    _products = products;
    _slug = slug;
  }

  CategoryWiseProductsModel.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _image = json["image"] != null ? ImageModal.fromJson(json["image"]) : null;
    _icon = json["icon"] != null ? ImageModal.fromJson(json["icon"]) : null;
    if (json["products"] != null) {
      _products = [];
      json["products"].forEach((v) {
        _products.add(ProductModel.fromJson(v));
      });
    }
    _slug = json["slug"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    if (_icon != null) {
      map["icon"] = _icon.toJson();
    }
    if (_products != null) {
      map["products"] = _products.map((v) => v.toJson()).toList();
    }
    map["slug"] = _slug;
    return map;
  }

}
