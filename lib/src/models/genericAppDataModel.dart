// import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';
//
// class GetGenericAppDataModel {
//   bool success;
//   String message;
//   List errors;
//   GetGenericAppDataDetailModel data;
//
//
//   GetGenericAppDataModel({
//     this.success,
//     this.message,
//     this.errors,
//     this.data,
//   });
//
//   factory GetGenericAppDataModel.fromJson(Map<String, dynamic> json) => GetGenericAppDataModel(
//     success : json["success"],
//     message : json["message"],
//     errors : json["errors"],
//     data : json['data'] != null
//         ?GetGenericAppDataDetailModel.fromJson(json['data'])
//         : null,
//   );
//
//   Map<String, dynamic> toJson() => {
//     "success" : success,
//     "message" : message,
//     "errors" : errors,
//     "data" : data,
//   };
//
//   @override
//   String toString() {
//     return 'GetGenericAppDataModel: {success: $success}';
//   }
// }
//
// class GetGenericAppDataDetailModel {
//   List<GetSliderModel> sliderImages;
//   List<GetSliderModel> staticBanners;
//   List<GetSliderModel> parallaxBanners;
//   List<GetContentPagesModel> contentPages;
//   List<GetPostsModel> posts;
//   List reviews;
//
//
//   GetGenericAppDataDetailModel({
//     this.sliderImages,
//     this.staticBanners,
//     this.parallaxBanners,
//     this.contentPages,
//     this.posts,
//     this.reviews,
//   });
//
//   factory GetGenericAppDataDetailModel.fromJson(Map<String, dynamic> json) => GetGenericAppDataDetailModel(
//     sliderImages : json['slider_images'] != null
//         ? List.from(json['slider_images']).map((element) => GetSliderModel.fromJson(element)).toList()
//         : [],
//     staticBanners : json['static_banners'] != null
//         ? List.from(json['static_banners']).map((element) => GetSliderModel.fromJson(element)).toList()
//         : [],
//     parallaxBanners : json['parallax_banners'] != null
//         ? List.from(json['parallax_banners']).map((element) => GetSliderModel.fromJson(element)).toList()
//         : [],
//     contentPages : json['content_pages'] != null
//         ? List.from(json['content_pages']).map((element) => GetContentPagesModel.fromJson(element)).toList()
//         : [],
//     posts : json['posts'] != null
//         ? List.from(json['posts']).map((element) => GetPostsModel.fromJson(element)).toList()
//         : [],
//     reviews : json['reviews'] != null
//         ? List.from(json['reviews']).map((element) => GetReviewsModel.fromJson(element)).toList()
//         : [],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "slider_images" : sliderImages,
//     "static_banners" : staticBanners,
//     "parallax_banners" : parallaxBanners,
//     "content_pages" : contentPages,
//     "posts" : posts,
//     "reviews" : reviews,
//   };
//
//   @override
//   String toString() {
//     return 'GetGenericAppDataDetailModel: {sliderImages: $sliderImages}';
//   }
// }
//
// class GetSliderModel {
//   int id;
//   GetImageModel image;
//   String name;
//   String description;
//   String descriptionWeb;
//   String websiteUrl;
//   String sliderType;
//   String expiryDate;
//
//
//   GetSliderModel({
//     this.id,
//     this.image,
//     this.name,
//     this.description,
//     this.descriptionWeb,
//     this.websiteUrl,
//     this.sliderType,
//     this.expiryDate,
//   });
//
//   factory GetSliderModel.fromJson(Map<String, dynamic> json) => GetSliderModel(
//     id : json["id"],
//     image : json['image'] != null
//         ?GetImageModel.fromJson(json['image'])
//         : null,
//     name : json["name"],
//     description : json["description"],
//     descriptionWeb : json["description_web"],
//     websiteUrl : json["website_url"],
//     sliderType : json["slider_type"],
//     expiryDate : json["expiry_date"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id" : id,
//     "image" : image,
//     "name" : name,
//     "description" : description,
//     "description_web" : descriptionWeb,
//     "website_url" : websiteUrl,
//     "slider_type" : sliderType,
//     "expiry_date" : expiryDate,
//   };
//
//   @override
//   String toString() {
//     return 'GetSliderModel: {slider_type: $sliderType}';
//   }
// }
//
// class GetImageModel {
//   String name;
//   String originalMediaPath;
//   String type;
//   String mimeType;
//   GetImageThumbnailModel thumbnails;
//
//
//   GetImageModel({
//     this.name,
//     this.originalMediaPath,
//     this.type,
//     this.mimeType,
//     this.thumbnails,
//   });
//
//   factory GetImageModel.fromJson(Map<String, dynamic> json) => GetImageModel(
//     name : json["name"],
//     originalMediaPath : json["original_media_path"],
//     type : json["type"],
//     mimeType : json["mime_type"],
//     thumbnails : json['thumbnails'] != null
//         ?GetImageThumbnailModel.fromJson(json['thumbnails'])
//         : null,
//   );
//
//   Map<String, dynamic> toJson() => {
//     "name" : name,
//     "original_media_path" : originalMediaPath,
//     "type" : type,
//     "mime_type" : mimeType,
//     "thumbnails" : thumbnails,
//   };
//
//   @override
//   String toString() {
//     return 'GetImageModel: {name: $name}';
//   }
// }
//
// class GetImageThumbnailModel {
//   GetImageThumbnailSizeModel small;
//   GetImageThumbnailSizeModel medium;
//   GetImageThumbnailSizeModel large;
//
//
//   GetImageThumbnailModel({
//     this.small,
//     this.medium,
//     this.large,
//   });
//
//   factory GetImageThumbnailModel.fromJson(Map<String, dynamic> json) => GetImageThumbnailModel(
//     small : json['small'] != null
//         ?GetImageThumbnailSizeModel.fromJson(json['small'])
//         : null,
//     medium : json['medium'] != null
//         ?GetImageThumbnailSizeModel.fromJson(json['medium'])
//         : null,
//     large : json['large'] != null
//         ?GetImageThumbnailSizeModel.fromJson(json['large'])
//         : null,
//   );
//
//   Map<String, dynamic> toJson() => {
//     "small" : small,
//     "medium" : medium,
//     "large" : large,
//   };
//
//   @override
//   String toString() {
//     return 'GetImageThumbnailModel: {small: $small}';
//   }
// }
//
// class GetImageThumbnailSizeModel {
//   String thumbnail;
//   String thumbnailType;
//   double height;
//   double width;
//
//
//   GetImageThumbnailSizeModel({
//     this.thumbnail,
//     this.thumbnailType,
//     this.height,
//     this.width,
//   });
//
//   factory GetImageThumbnailSizeModel.fromJson(Map<String, dynamic> json) => GetImageThumbnailSizeModel(
//     thumbnail : json["thumbnail"],
//     thumbnailType : json["thumbnail_type"],
//     height : double.parse(json["height"].toString()),
//     width : double.parse(json["width"].toString()),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "thumbnail" : thumbnail,
//     "thumbnail_type" : thumbnailType,
//     "height" : height,
//     "width" : width,
//   };
//
//   @override
//   String toString() {
//     return 'GetImageThumbnailSizeModel: {thumbnail: $thumbnail}';
//   }
// }
//
// class GetContentPagesModel {
//   int id;
//   String name;
//   String slug;
//   String description;
//   String descriptionWeb;
//
//
//   GetContentPagesModel({
//     this.id,
//     this.name,
//     this.slug,
//     this.description,
//     this.descriptionWeb,
//   });
//
//   factory GetContentPagesModel.fromJson(Map<String, dynamic> json) => GetContentPagesModel(
//     id : json["id"],
//     name : json["name"],
//     slug : json["slug"],
//     description : json["description"],
//     descriptionWeb : json["description_web"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id" : id,
//     "name" : name,
//     "slug" : slug,
//     "description" : description,
//     "description_web" : descriptionWeb,
//   };
//
//   @override
//   String toString() {
//     return 'GetContentPagesModel: {name: $name}';
//   }
// }
//
// class GetPostsModel {
//   int id;
//   String name;
//   String slug;
//   GetNewCategoryForPostsModel newCategory;
//   String description;
//   String descriptionWeb;
//   int isFeatured;
//   String createdAt;
//
//
//   GetPostsModel({
//     this.id,
//     this.name,
//     this.slug,
//     this.newCategory,
//     this.description,
//     this.descriptionWeb,
//     this.isFeatured,
//     this.createdAt,
//   });
//
//   factory GetPostsModel.fromJson(Map<String, dynamic> json) => GetPostsModel(
//     id : json["id"],
//     name : json["name"],
//     slug : json["slug"],
//     newCategory : json["news_category"] != null
//         ?GetNewCategoryForPostsModel.fromJson(json['news_category'])
//         :null,
//     description : json["description"],
//     descriptionWeb : json["description_web"],
//     isFeatured : json["is_featured"],
//     createdAt : json["created_at"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id" : id,
//     "name" : name,
//     "slug" : slug,
//     "news_category" : newCategory,
//     "description" : description,
//     "description_web" : descriptionWeb,
//     "is_featured" : isFeatured,
//     "created_at" : createdAt,
//   };
//
//   @override
//   String toString() {
//     return 'GetPostsModel: {name: $name}';
//   }
// }
//
// class GetNewCategoryForPostsModel {
//   int id;
//   String name;
//   String slug;
//   String description;
//   String descriptionWeb;
//   String createdAt;
//
//
//   GetNewCategoryForPostsModel({
//     this.id,
//     this.name,
//     this.slug,
//     this.description,
//     this.descriptionWeb,
//     this.createdAt,
//   });
//
//   factory GetNewCategoryForPostsModel.fromJson(Map<String, dynamic> json) => GetNewCategoryForPostsModel(
//     id : json["id"],
//     name : json["name"],
//     slug : json["slug"],
//     description : json["description"],
//     descriptionWeb : json["description_web"],
//     createdAt : json["created_at"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id" : id,
//     "name" : name,
//     "slug" : slug,
//     "description" : description,
//     "description_web" : descriptionWeb,
//     "created_at" : createdAt,
//   };
//
//   @override
//   String toString() {
//     return 'GetNewCategoryForPostsModel: {name: $name}';
//   }
// }
//
// class GetReviewsModel {
//   int id;
//   String name;
//   String customer;
//   String product;
//   String description;
//   double rating;
//
//
//   GetReviewsModel({
//     this.id,
//     this.name,
//     this.customer,
//     this.product,
//     this.description,
//     this.rating,
//   });
//
//   factory GetReviewsModel.fromJson(Map<String, dynamic> json) => GetReviewsModel(
//     id : json["id"],
//     name : json["name"],
//     customer : json["customer"],
//     product : json["product"],
//     description : json["description"],
//     rating : double.parse(json["rating"].toString()),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id" : id,
//     "name" : name,
//     "customer" : customer,
//     "product" : product,
//     "description" : description,
//     "rating" : rating,
//   };
//
//   @override
//   String toString() {
//     return 'GetReviewsModel: {name: $name}';
//   }
// }


class GetGenericAppDataModel {
  Data _data;
  bool _success;
  String _message;
  List _errors;

  Data get data => _data;
  bool get success => _success;
  String get message => _message;
  List get errors => _errors;

  GetGenericAppDataModel({
    Data data,
    bool success,
    String message,
    List errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetGenericAppDataModel.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Data {
  List<SliderImages> _sliderImages;
  List<BannerModal> _staticBanners;
  List<SliderImages> _parallaxBanners;
  List<Posts> _posts;
  List<Reviews> _reviews;

  List<SliderImages> get sliderImages => _sliderImages;
  List<BannerModal> get staticBanners => _staticBanners;
  List<SliderImages> get parallaxBanners => _parallaxBanners;
  List<Posts> get posts => _posts;
  List<Reviews> get reviews => _reviews;

  Data({
    List<SliderImages> sliderImages,
    List<BannerModal> staticBanners,
    List<SliderImages> parallaxBanners,
    List<Posts> posts,
    List<Reviews> reviews}){
    _sliderImages = sliderImages;
    _staticBanners = staticBanners;
    _parallaxBanners = parallaxBanners;
    _posts = posts;
    _reviews = reviews;
  }

  Data.fromJson(dynamic json) {
    if (json["slider_images"] != null) {
      _sliderImages = [];
      json["slider_images"].forEach((v) {
        _sliderImages.add(SliderImages.fromJson(v));
      });
    }
    if (json["static_banners"] != null) {
      _staticBanners = [];
      json["static_banners"].forEach((v) {
        _staticBanners.add(BannerModal.fromJson(v));
      });
    }
    if (json["parallax_banners"] != null) {
      _parallaxBanners = [];
      json["parallax_banners"].forEach((v) {
        _parallaxBanners.add(SliderImages.fromJson(v));
      });
    }
    if (json["posts"] != null) {
      _posts = [];
      json["posts"].forEach((v) {
        _posts.add(Posts.fromJson(v));
      });
    }
    if (json["reviews"] != null) {
      _reviews = [];
      json["reviews"].forEach((v) {
        _reviews.add(Reviews.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_sliderImages != null) {
      map["slider_images"] = _sliderImages.map((v) => v.toJson()).toList();
    }
    if (_staticBanners != null) {
      map["static_banners"] = _staticBanners.map((v) => v.toJson()).toList();
    }
    if (_parallaxBanners != null) {
      map["parallax_banners"] = _parallaxBanners.map((v) => v.toJson()).toList();
    }
    if (_posts != null) {
      map["posts"] = _posts.map((v) => v.toJson()).toList();
    }
    if (_reviews != null) {
      map["reviews"] = _reviews.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Reviews {
  int _id;
  dynamic _name;
  dynamic _customer;
  dynamic _product;
  String _description;
  double _rating;

  int get id => _id;
  dynamic get name => _name;
  dynamic get customer => _customer;
  dynamic get product => _product;
  String get description => _description;
  double get rating => _rating;

  Reviews({
    int id,
    dynamic name,
    dynamic customer,
    dynamic product,
    String description,
    double rating}){
    _id = id;
    _name = name;
    _customer = customer;
    _product = product;
    _description = description;
    _rating = rating;
  }

  Reviews.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _customer = json["customer"];
    _product = json["product"];
    _description = json["description"];
    _rating = double.parse(json["rating"].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["customer"] = _customer;
    map["product"] = _product;
    map["description"] = _description;
    map["rating"] = _rating;
    return map;
  }

}

class Posts {
  int _id;
  String _name;
  dynamic _image;
  String _slug;
  NewsCategory _newsCategory;
  String _description;
  String _descriptionWeb;
  int _isFeatured;
  String _createdAt;

  int get id => _id;
  String get name => _name;
  dynamic get image => _image;
  String get slug => _slug;
  NewsCategory get newsCategory => _newsCategory;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  int get isFeatured => _isFeatured;
  String get createdAt => _createdAt;

  Posts({
    int id,
    String name,
    dynamic image,
    String slug,
    NewsCategory newsCategory,
    String description,
    String descriptionWeb,
    int isFeatured,
    String createdAt}){
    _id = id;
    _name = name;
    _image = image;
    _slug = slug;
    _newsCategory = newsCategory;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _isFeatured = isFeatured;
    _createdAt = createdAt;
  }

  Posts.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _image = json["image"];
    _slug = json["slug"];
    _newsCategory = json["news_category"] != null ? NewsCategory.fromJson(json["news_category"]) : null;
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _isFeatured = json["is_featured"];
    _createdAt = json["created_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["image"] = _image;
    map["slug"] = _slug;
    if (_newsCategory != null) {
      map["news_category"] = _newsCategory.toJson();
    }
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    map["is_featured"] = _isFeatured;
    map["created_at"] = _createdAt;
    return map;
  }

}

class NewsCategory {
  int _id;
  String _name;
  String _slug;
  String _description;
  String _descriptionWeb;
  String _createdAt;

  int get id => _id;
  String get name => _name;
  String get slug => _slug;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  String get createdAt => _createdAt;

  NewsCategory({
    int id,
    String name,
    String slug,
    String description,
    String descriptionWeb,
    String createdAt}){
    _id = id;
    _name = name;
    _slug = slug;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _createdAt = createdAt;
  }

  NewsCategory.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _slug = json["slug"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _createdAt = json["created_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["slug"] = _slug;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    map["created_at"] = _createdAt;
    return map;
  }

}

class ContentPages {
  int _id;
  String _name;
  String _slug;
  String _description;
  String _descriptionWeb;

  int get id => _id;
  String get name => _name;
  String get slug => _slug;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;

  ContentPages({
    int id,
    String name,
    String slug,
    String description,
    String descriptionWeb}){
    _id = id;
    _name = name;
    _slug = slug;
    _description = description;
    _descriptionWeb = descriptionWeb;
  }

  ContentPages.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _slug = json["slug"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["slug"] = _slug;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    return map;
  }

}

class SliderImages {
  int _id;
  ImageModal _image;
  String _name;
  String _description;
  String _descriptionWeb;
  String _websiteUrl;
  int _sliderType;
  String _expiryDate;

  int get id => _id;
  ImageModal get image => _image;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  String get websiteUrl => _websiteUrl;
  int get sliderType => _sliderType;
  String get expiryDate => _expiryDate;

  SliderImages({
    int id,
    ImageModal image,
    String name,
    String description,
    String descriptionWeb,
    String websiteUrl,
    int sliderType,
    String expiryDate}){
    _id = id;
    _image = image;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _websiteUrl = websiteUrl;
    _sliderType = sliderType;
    _expiryDate = expiryDate;
  }

  SliderImages.fromJson(dynamic json) {
    _id = json["id"];
    _image = json["image"] != null ? ImageModal.fromJson(json["image"]) : null;
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _websiteUrl = json["website_url"];
    _sliderType = json["slider_type"];
    _expiryDate = json["expiry_date"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    map["website_url"] = _websiteUrl;
    map["slider_type"] = _sliderType;
    map["expiry_date"] = _expiryDate;
    return map;
  }

}

class BannerModal {
  int _id;
  ImageModal _image;
  String _name;
  String _description;
  String _descriptionWeb;
  String _websiteUrl;
  String _expiryDate;

  int get id => _id;
  ImageModal get image => _image;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  String get websiteUrl => _websiteUrl;
  String get expiryDate => _expiryDate;

  BannerModal({
    int id,
    ImageModal image,
    String name,
    String description,
    String descriptionWeb,
    String websiteUrl,
    String expiryDate}){
    _id = id;
    _image = image;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _websiteUrl = websiteUrl;
    _expiryDate = expiryDate;
  }

  BannerModal.fromJson(dynamic json) {
    _id = json["id"];
    _image = json["image"] != null ? ImageModal.fromJson(json["image"]) : null;
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _websiteUrl = json["website_url"];
    _expiryDate = json["expiry_date"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    map["website_url"] = _websiteUrl;
    map["expiry_date"] = _expiryDate;
    return map;
  }

}

class ImageModal {
  String _name;
  String _originalMediaPath;
  String _type;
  String _mimeType;
  Thumbnails _thumbnails;

  String get name => _name;
  String get originalMediaPath => _originalMediaPath;
  String get type => _type;
  String get mimeType => _mimeType;
  Thumbnails get thumbnails => _thumbnails;

  ImageModal({
    String name,
    String originalMediaPath,
    String type,
    String mimeType,
    Thumbnails thumbnails}){
    _name = name;
    _originalMediaPath = originalMediaPath;
    _type = type;
    _mimeType = mimeType;
    _thumbnails = thumbnails;
  }

  ImageModal.fromJson(dynamic json) {
    _name = json["name"];
    _originalMediaPath = json["original_media_path"];
    _type = json["type"];
    _mimeType = json["mime_type"];
    _thumbnails = json["thumbnails"] != null ? Thumbnails.fromJson(json["thumbnails"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = _name;
    map["original_media_path"] = _originalMediaPath;
    map["type"] = _type;
    map["mime_type"] = _mimeType;
    if (_thumbnails != null) {
      map["thumbnails"] = _thumbnails.toJson();
    }
    return map;
  }

}

class Thumbnails {
  ThumbnailSize _small;
  ThumbnailSize _medium;
  ThumbnailSize _large;

  ThumbnailSize get small => _small;
  ThumbnailSize get medium => _medium;
  ThumbnailSize get large => _large;

  Thumbnails({
    ThumbnailSize small,
    ThumbnailSize medium,
    ThumbnailSize large}){
    _small = small;
    _medium = medium;
    _large = large;
  }

  Thumbnails.fromJson(dynamic json) {
    _small = json["small"] != null ? ThumbnailSize.fromJson(json["small"]) : null;
    _medium = json["medium"] != null ? ThumbnailSize.fromJson(json["medium"]) : null;
    _large = json["large"] != null ? ThumbnailSize.fromJson(json["large"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_small != null) {
      map["small"] = _small.toJson();
    }
    if (_medium != null) {
      map["medium"] = _medium.toJson();
    }
    if (_large != null) {
      map["large"] = _large.toJson();
    }
    return map;
  }

}

class ThumbnailSize {
  String _thumbnail;
  String _thumbnailType;
  double _height;
  double _width;

  String get thumbnail => _thumbnail;
  String get thumbnailType => _thumbnailType;
  double get height => _height;
  double get width => _width;

  ThumbnailSize({
    String thumbnail,
    String thumbnailType,
    double height,
    double width}){
    _thumbnail = thumbnail;
    _thumbnailType = thumbnailType;
    _height = height;
    _width = width;
  }

  ThumbnailSize.fromJson(dynamic json) {
    _thumbnail = json["thumbnail"];
    _thumbnailType = json["thumbnail_type"];
    _height = double.parse(json["height"].toString());
    _width = double.parse(json["width"].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["thumbnail"] = _thumbnail;
    map["thumbnail_type"] = _thumbnailType;
    map["height"] = _height;
    map["width"] = _width;
    return map;
  }

}
