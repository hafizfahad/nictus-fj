
class GetLoginDataModel {
  GetLoginDataModelData _data;
  bool _success;
  String _message;
  List<dynamic> _errors;

  GetLoginDataModelData get data => _data;
  bool get success => _success;
  String get message => _message;
  List<dynamic> get errors => _errors;

  GetLoginDataModel({
    GetLoginDataModelData data,
      bool success, 
      String message, 
      List<dynamic> errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
}

  GetLoginDataModel.fromJson(dynamic json) {
    _data = json["data"] != null ? GetLoginDataModelData.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetLoginDataModelData {
  UserModel _user;
  String _token;

  UserModel get user => _user;
  String get token => _token;

  GetLoginDataModelData({
    UserModel user,
      String token}){
    _user = user;
    _token = token;
}

  GetLoginDataModelData.fromJson(dynamic json) {
    _user = json["user"] != null ? UserModel.fromJson(json["user"]) : null;
    _token = json["token"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_user != null) {
      map["user"] = _user.toJson();
    }
    map["token"] = _token;
    return map;
  }

}

class UserModel {
  int _id;
  String _name;
  String _email;
  String _profileImagePath;
  bool _verified;
  String _createdAt;
  List<String> _roles;
  // List<String> _permissions;

  int get id => _id;
  String get name => _name;
  String get email => _email;
  String get profileImagePath => _profileImagePath;
  bool get verified => _verified;
  String get createdAt => _createdAt;
  List<String> get roles => _roles;
  // List<String> get permissions => _permissions;

  UserModel({
      int id, 
      String name, 
      String email, 
      String profileImagePath, 
      bool verified, 
      String createdAt, 
      List<String> roles, 
      // List<String> permissions
  }){
    _id = id;
    _name = name;
    _email = email;
    _profileImagePath = profileImagePath;
    _verified = verified;
    _createdAt = createdAt;
    _roles = roles;
    // _permissions = permissions;
}

  UserModel.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _email = json["email"];
    _profileImagePath = json["profile_image_path"];
    _verified = json["verified"];
    _createdAt = json["created_at"];
    _roles = json["roles"] != null ? json["roles"].cast<String>() : [];
    // _permissions = json["permissions"] != null ? json["permissions"].cast<String>() : [];
    // if (json["permissions"] != null) {
    //   _permissions = [];
    //   json["permissions"].forEach((v) {
    //     _permissions.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["email"] = _email;
    map["profile_image_path"] = _profileImagePath;
    map["verified"] = _verified;
    map["created_at"] = _createdAt;
    map["roles"] = _roles;
    // map["permissions"] = _permissions;
    // if (_permissions != null) {
    //   map["permissions"] = _permissions.map((v) => v.toJson()).toList();
    // }
    return map;
  }

}