import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';

class GetAllLanguageModel {
  bool success;
  String message;
  List errors;
  List<Language> data;


  GetAllLanguageModel({
    this.success,
    this.message,
    this.errors,
    this.data,
  });

  factory GetAllLanguageModel.fromJson(Map<String, dynamic> json) => GetAllLanguageModel(
    success : json["success"],
    message : json["message"],
    errors : json["errors"],
    data : json['data'] != null
        ? List.from(json['data']).map((element) => Language.fromJson(element)).toList()
        : [],
  );

  Map<String, dynamic> toJson() => {
    "success" : success,
    "message" : message,
    "errors" : errors,
    "data" : data,
  };

  @override
  String toString() {
    return 'GetAllLanguageModel: {success: $success}';
  }
}

