import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class GetTrendingProductsModel {
  List<ProductModel> _data;
  bool _success;
  String _message;
  List _errors;

  List<ProductModel> get data => _data;
  bool get success => _success;
  String get message => _message;
  List get errors => _errors;

  GetTrendingProductsModel({
    List<ProductModel> data,
    bool success,
    String message,
    List errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetTrendingProductsModel.fromJson(dynamic json) {
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(ProductModel.fromJson(v));
      });
    }
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}
