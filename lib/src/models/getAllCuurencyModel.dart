import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';

class GetAllCurrencyModel {
  bool success;
  String message;
  List errors;
  List<Currency> data;


  GetAllCurrencyModel({
    this.success,
    this.message,
    this.errors,
    this.data,
  });

  factory GetAllCurrencyModel.fromJson(Map<String, dynamic> json) => GetAllCurrencyModel(
    success : json["success"],
    message : json["message"],
    errors : json["errors"],
    data : json['data'] != null
        ? List.from(json['data']).map((element) => Currency.fromJson(element)).toList()
        : [],
  );

  Map<String, dynamic> toJson() => {
    "success" : success,
    "message" : message,
    "errors" : errors,
    "data" : data,
  };

  @override
  String toString() {
    return 'GetAllCurrencyModel: {success: $success}';
  }
}

