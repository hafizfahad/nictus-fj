import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class GetShopPageProductsModel {
  GetShopPageProductsModelData _data;
  bool _success;
  String _message;
  List<dynamic> _errors;

  GetShopPageProductsModelData get data => _data;
  bool get success => _success;
  String get message => _message;
  List<dynamic> get errors => _errors;

  GetShopPageProductsModel({
    GetShopPageProductsModelData data,
    bool success,
    String message,
    List<dynamic> errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetShopPageProductsModel.fromJson(dynamic json) {
    _data = json["data"] != null ? GetShopPageProductsModelData.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetShopPageProductsModelData {
  List<ProductModel> _data;
  LinksPagination _links;
  MetaPagination _meta;

  List<ProductModel> get data => _data;
  LinksPagination get links => _links;
  MetaPagination get meta => _meta;

  GetShopPageProductsModelData({
    List<ProductModel> data,
    LinksPagination links,
    MetaPagination meta}){
    _data = data;
    _links = links;
    _meta = meta;
  }

  GetShopPageProductsModelData.fromJson(dynamic json) {
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(ProductModel.fromJson(v));
      });
    }
    _links = json["links"] != null ? LinksPagination.fromJson(json["links"]) : null;
    _meta = json["meta"] != null ? MetaPagination.fromJson(json["meta"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    if (_links != null) {
      map["links"] = _links.toJson();
    }
    if (_meta != null) {
      map["meta"] = _meta.toJson();
    }
    return map;
  }

}

class MetaPagination {
  int _currentPage;
  int _from;
  int _lastPage;
  List<MetaLinks> _links;
  String _path;
  int _perPage;
  int _to;
  int _total;

  int get currentPage => _currentPage;
  int get from => _from;
  int get lastPage => _lastPage;
  List<MetaLinks> get links => _links;
  String get path => _path;
  int get perPage => _perPage;
  int get to => _to;
  int get total => _total;

  MetaPagination({
    int currentPage,
    int from,
    int lastPage,
    List<MetaLinks> links,
    String path,
    int perPage,
    int to,
    int total}){
    _currentPage = currentPage;
    _from = from;
    _lastPage = lastPage;
    _links = links;
    _path = path;
    _perPage = perPage;
    _to = to;
    _total = total;
  }

  MetaPagination.fromJson(dynamic json) {
    _currentPage = json["current_page"];
    _from = json["from"];
    _lastPage = json["last_page"];
    if (json["links"] != null) {
      _links = [];
      json["links"].forEach((v) {
        _links.add(MetaLinks.fromJson(v));
      });
    }
    _path = json["path"];
    _perPage = int.parse(json["per_page"].toString());
    _to = json["to"];
    _total = json["total"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["current_page"] = _currentPage;
    map["from"] = _from;
    map["last_page"] = _lastPage;
    if (_links != null) {
      map["links"] = _links.map((v) => v.toJson()).toList();
    }
    map["path"] = _path;
    map["per_page"] = _perPage;
    map["to"] = _to;
    map["total"] = _total;
    return map;
  }

}

class MetaLinks {
  dynamic _url;
  String _label;
  bool _active;

  dynamic get url => _url;
  String get label => _label;
  bool get active => _active;

  MetaLinks({
    dynamic url,
    String label,
    bool active}){
    _url = url;
    _label = label;
    _active = active;
  }

  MetaLinks.fromJson(dynamic json) {
    _url = json["url"];
    _label = json["label"].toString();
    _active = json["active"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["url"] = _url;
    map["label"] = _label;
    map["active"] = _active;
    return map;
  }

}

class LinksPagination {
  String _first;
  String _last;
  dynamic _prev;
  String _next;

  String get first => _first;
  String get last => _last;
  dynamic get prev => _prev;
  String get next => _next;

  LinksPagination({
    String first,
    String last,
    dynamic prev,
    String next}){
    _first = first;
    _last = last;
    _prev = prev;
    _next = next;
  }

  LinksPagination.fromJson(dynamic json) {
    _first = json["first"];
    _last = json["last"];
    _prev = json["prev"];
    _next = json["next"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["first"] = _first;
    map["last"] = _last;
    map["prev"] = _prev;
    map["next"] = _next;
    return map;
  }

}