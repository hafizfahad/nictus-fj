
class DefaultSettingsModel {
  bool success;
  String message;
  List errors;
  Data data;


  DefaultSettingsModel({
    this.success,
    this.message,
    this.errors,
    this.data,
  });

  factory DefaultSettingsModel.fromJson(Map<String, dynamic> json) => DefaultSettingsModel(
    success : json["success"],
    message : json["message"],
    errors : json["errors"],
    data : json['data'] != null
        ? Data.fromJson(json['data'])
        : null,
  );

  Map<String, dynamic> toJson() => {
    "success" : success,
    "message" : message,
    "errors" : errors,
    "data" : data,
  };

  @override
  String toString() {
    return 'DefaultSettingsModel: {success: $success}';
  }
}


class Data {
  Language language;
  Currency currency;
  DefaultThemeModel theme;
  GeneralSettings generalSettings;

  Data({
    this.language,
    this.currency,
    this.theme,
    this.generalSettings,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    language : json["language"] != null
        ? Language.fromJson(json['language'])
        : null,
    currency : json["currency"] != null
        ? Currency.fromJson(json['currency'])
        : null,
    theme : json["theme"] != null
        ? DefaultThemeModel.fromJson(json['theme'])
        : null,
    generalSettings : json["general_settings"] != null
        ? GeneralSettings.fromJson(json['general_settings'])
        : null,
  );

  Map<String, dynamic> toJson() => {
    "language" : language,
    "currency" : currency,
    "theme" : theme,
    "general_settings" : generalSettings,
  };

  @override
  String toString() {
    return 'Data: {language: ${language.code}';
  }
}
class Language {
  int id;
  String name;
  String code;
  String direction;
  int isDefault;
  int imageId;


  Language({
    this.id,
    this.name,
    this.code,
    this.direction,
    this.isDefault,
    this.imageId,
  });

  factory Language.fromJson(Map<String, dynamic> json) => Language(
    id : json["id"],
    name : json["name"],
    code : json["code"],
    direction : json["direction"],
    isDefault : json["is_default"],
    imageId : json["image_id"],
  );

  Map<String, dynamic> toJson() => {
    "id" : id,
    "name" : name,
    "code" : code,
    "direction" : direction,
    "is_default" : isDefault,
    "image_id" : imageId,
  };

  @override
  String toString() {
    return 'currency: {code: $code}';
  }
}
class Currency {
  int id;
  String name;
  String code;
  String symbol;
  String direction;
  int decimalPlaces;
  int value;


  Currency({
    this.id,
    this.name,
    this.code,
    this.symbol,
    this.direction,
    this.decimalPlaces,
    this.value,
  });

  factory Currency.fromJson(Map<String, dynamic> json) => Currency(
    id : json["id"],
    name : json["name"],
    code : json["code"],
    symbol : json["symbol"],
    direction : json["direction"],
    decimalPlaces : int.parse(json["decimal_places"].toString()),
    value : json["value"],
  );

  Map<String, dynamic> toJson() => {
    "id" : id,
    "name" : name,
    "code" : code,
    "symbol" : symbol,
    "direction" : direction,
    "decimal_places" : decimalPlaces,
    "value" : value,
  };

  @override
  String toString() {
    return 'currency: {code: $code}';
  }
}
class DefaultThemeModel {
  int homePage;
  int shopPage;
  int productPage;
  int checkoutPage;


  DefaultThemeModel({
    this.homePage,
    this.shopPage,
    this.productPage,
    this.checkoutPage,
  });

  factory DefaultThemeModel.fromJson(Map<String, dynamic> json) => DefaultThemeModel(
    homePage : json["home_page"],
    shopPage : json["shop_page"],
    productPage : json["product_page"],
    checkoutPage : json["checkout_page"],
  );

  Map<String, dynamic> toJson() => {
    "home_page" : homePage,
    "shop_page" : shopPage,
    "product_page" : productPage,
    "checkout_page" : checkoutPage,
  };

  @override
  String toString() {
    return 'Theme: {homePage: $homePage}';
  }
}
class GeneralSettings {
  int isMultiVendor;

  GeneralSettings({
    this.isMultiVendor,
  });

  factory GeneralSettings.fromJson(Map<String, dynamic> json) => GeneralSettings(
    isMultiVendor : json["is_multi_vendor"],
  );

  Map<String, dynamic> toJson() => {
    "is_multi_vendor" : isMultiVendor,
  };

  @override
  String toString() {
    return 'GeneralSettings: {is_multi_vendor: $isMultiVendor}';
  }
}
