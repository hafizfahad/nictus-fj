class ProductFilterModel {
  ProductFilterModel(
      {this.selectedCategorySlug,
      this.selectedBrandList,
      this.selectedAttributesList,
      this.selectedSorting,
      this.selectedPage,
      this.minPrice,
      this.maxPrice,
      this.searchName});

  String selectedCategorySlug;
  List<int> selectedBrandList;
  List<Map<String, dynamic>> selectedAttributesList = [];
  ProductFilterModelForSort selectedSorting;
  int selectedPage;
  double minPrice;
  double maxPrice;
  String searchName;
}

class ProductFilterModelForSort {
  ProductFilterModelForSort({this.sortField, this.sortType});
  String sortField;
  String sortType;
}
