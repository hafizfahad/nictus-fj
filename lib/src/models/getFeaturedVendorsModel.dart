import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class GetFeaturedVendorsModel {
  List<VendorsModelWithProducts> _data;
  bool _success;
  String _message;
  List _errors;

  List<VendorsModelWithProducts> get data => _data;
  bool get success => _success;
  String get message => _message;
  List get errors => _errors;

  GetFeaturedVendorsModel({
    List<VendorsModelWithProducts> data,
    bool success,
    String message,
    List errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetFeaturedVendorsModel.fromJson(dynamic json) {
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(VendorsModelWithProducts.fromJson(v));
      });
    }
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class VendorsModelWithProducts {
  int _id;
  List<ProductModel> _products;
  String _email;
  String _name;
  String _slug;
  String _contactPhone;
  ImageModal _profileImage;
  VendorStoreModel _store;
  int _isActive;
  String _updatedAt;

  int get id => _id;
  List<ProductModel> get products => _products;
  String get email => _email;
  String get name => _name;
  String get slug => _slug;
  String get contactPhone => _contactPhone;
  ImageModal get profileImage => _profileImage;
  VendorStoreModel get store => _store;
  int get isActive => _isActive;
  String get updatedAt => _updatedAt;

  VendorsModelWithProducts({
    int id,
    List<ProductModel> products,
    String email,
    String name,
    String slug,
    String contactPhone,
    ImageModal profileImage,
    VendorStoreModel store,
    int isActive,
    String updatedAt}){
    _id = id;
    _products = products;
    _email = email;
    _name = name;
    _slug = slug;
    _contactPhone = contactPhone;
    _profileImage = profileImage;
    _store = store;
    _isActive = isActive;
    _updatedAt = updatedAt;
  }

  VendorsModelWithProducts.fromJson(dynamic json) {
    _id = json["id"];
    if (json["products"] != null) {
      _products = [];
      json["products"].forEach((v) {
        _products.add(ProductModel.fromJson(v));
      });
    }
    _email = json["email"];
    _name = json["name"];
    _slug = json["slug"];
    _contactPhone = json["contact_phone"];
    _profileImage = json["profile_image"] != null ? ImageModal.fromJson(json["profile_image"]) : null;
    _store = json["store"] != null ? VendorStoreModel.fromJson(json["store"]) : null;
    _isActive = json["is_active"];
    _updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    if (_products != null) {
      map["products"] = _products.map((v) => v.toJson()).toList();
    }
    map["email"] = _email;
    map["name"] = _name;
    map["slug"] = _slug;
    map["contact_phone"] = _contactPhone;
    if (_profileImage != null) {
      map["profile_image"] = _profileImage.toJson();
    }
    if (_store != null) {
      map["store"] = _store.toJson();
    }
    map["is_active"] = _isActive;
    map["updated_at"] = _updatedAt;
    return map;
  }

}
