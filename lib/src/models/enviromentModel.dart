
class EnvironmentModel {
  String environment;

  EnvironmentModel({
    this.environment
  });

  factory EnvironmentModel.fromJson(Map<String, dynamic> json) => EnvironmentModel(
    environment : json["environment"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "environment" : environment,
  };

  @override
  String toString() {
    return 'Environment: {environment: $environment}';
  }
}