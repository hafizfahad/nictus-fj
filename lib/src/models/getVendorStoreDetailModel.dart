
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getShopPageDataModel.dart';

class GetVendorStoreDetailModel {
  GetVendorStoreDetailModelData _data;
  bool _success;
  String _message;
  List<dynamic> _errors;

  GetVendorStoreDetailModelData get data => _data;
  bool get success => _success;
  String get message => _message;
  List<dynamic> get errors => _errors;

  GetVendorStoreDetailModel({
    GetVendorStoreDetailModelData data,
    bool success,
    String message,
    List<dynamic> errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
  }

  GetVendorStoreDetailModel.fromJson(dynamic json) {
    _data = json["data"] != null ? GetVendorStoreDetailModelData.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json["errors"];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetVendorStoreDetailModelData {
  VendorModel _vendor;
  List<GetShopPageDataModelCategories> _categories;
  List<ProductModel> _newArrivalProducts;
  List<ProductModel> _trendingProducts;
  List<ProductModel> _upcomingProducts;

  VendorModel get vendor => _vendor;
  List<GetShopPageDataModelCategories> get categories => _categories;
  List<ProductModel> get newArrivalProducts => _newArrivalProducts;
  List<ProductModel> get trendingProducts => _trendingProducts;
  List<ProductModel> get upcomingProducts => _upcomingProducts;

  GetVendorStoreDetailModelData({
    VendorModel vendor,
    List<GetShopPageDataModelCategories> categories,
    List<ProductModel> newArrivalProducts,
    List<ProductModel> trendingProducts,
    List<ProductModel> upcomingProducts}){
    _vendor = vendor;
    _categories = categories;
    _newArrivalProducts = newArrivalProducts;
    _trendingProducts = trendingProducts;
    _upcomingProducts = upcomingProducts;
  }

  GetVendorStoreDetailModelData.fromJson(dynamic json) {
    _vendor = json["vendor"] != null ? VendorModel.fromJson(json["vendor"]) : null;
    if (json["categories"] != null) {
      _categories = [];
      json["categories"].forEach((v) {
        _categories.add(GetShopPageDataModelCategories.fromJson(v));
      });
    }
    if (json["new_arrival_products"] != null) {
      _newArrivalProducts = [];
      json["new_arrival_products"].forEach((v) {
        _newArrivalProducts.add(ProductModel.fromJson(v));
      });
    }
    if (json["trending_products"] != null) {
      _trendingProducts = [];
      json["trending_products"].forEach((v) {
        _trendingProducts.add(ProductModel.fromJson(v));
      });
    }
    if (json["upcoming_products"] != null) {
      _upcomingProducts = [];
      json["upcoming_products"].forEach((v) {
        _upcomingProducts.add(ProductModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_vendor != null) {
      map["vendor"] = _vendor.toJson();
    }
    if (_categories != null) {
      map["categories"] = _categories.map((v) => v.toJson()).toList();
    }
    if (_newArrivalProducts != null) {
      map["new_arrival_products"] = _newArrivalProducts.map((v) => v.toJson()).toList();
    }
    if (_trendingProducts != null) {
      map["trending_products"] = _trendingProducts.map((v) => v.toJson()).toList();
    }
    if (_upcomingProducts != null) {
      map["upcoming_products"] = _upcomingProducts.map((v) => v.toJson()).toList();
    }
    return map;
  }

}