import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';

class GetAllCategoriesModel {
  bool success;
  String message;
  List errors;
  List<GetAllCategoriesDataModel> data;


  GetAllCategoriesModel({
    this.success,
    this.message,
    this.errors,
    this.data,
  });

  factory GetAllCategoriesModel.fromJson(Map<String, dynamic> json) => GetAllCategoriesModel(
    success : json["success"],
    message : json["message"],
    errors : json["errors"],
    data : json['data'] != null
        ? List.from(json['data']).map((element) => GetAllCategoriesDataModel.fromJson(element)).toList()
        : [],
  );

  Map<String, dynamic> toJson() => {
    "success" : success,
    "message" : message,
    "errors" : errors,
    "data" : data,
  };

  @override
  String toString() {
    return 'GetAllCategoriesModel: {success: $success}';
  }
}

class GetAllCategoriesDataModel {

  int id;
  String name;
  String description;
  String descriptionWeb;
  ImageModal image;
  ImageModal icon;
  String slug;
  List<GetAllCategoriesDataModel> childrens;
  CategoriesParentDataModel parent;



  GetAllCategoriesDataModel({
    this.id,
    this.name,
    this.description,
    this.descriptionWeb,
    this.image,
    this.icon,
    this.slug,
    this.childrens,
    this.parent,
  });

  factory GetAllCategoriesDataModel.fromJson(Map<String, dynamic> json) => GetAllCategoriesDataModel(
    id : json["id"],
    name : json["name"],
    description : json["description"],
    descriptionWeb : json["description_web"],
    image : json["image"] != null
        ?ImageModal.fromJson(json["image"])
        :null,
    icon : json["icon"] != null
      ?ImageModal.fromJson(json["icon"])
      :null,
    slug : json["slug"],
    childrens : json['childrens'] != null
      ? List.from(json['childrens']).map((element) => GetAllCategoriesDataModel.fromJson(element)).toList()
      : [],
    parent : json["parent"] != null
        ?CategoriesParentDataModel.fromJson(json["parent"])
        :null,
  );

  Map<String, dynamic> toJson() => {
    "id" : id ,
    "name" : name,
    "description" : description,
    "description_web" : descriptionWeb,
    "image" : image,
    "icon" : icon,
    "slug" : slug,
    "childrens" : childrens,
    "parent" : parent,
  };

  @override
  String toString() {
    return 'GetAllCategoriesDataModel: {name: $name}';
  }
}

class CategoriesParentDataModel {

  int id;
  String name;
  String description;
  String descriptionWeb;
  ImageModal image;
  ImageModal icon;
  String slug;

  CategoriesParentDataModel({
    this.id,
    this.name,
    this.description,
    this.descriptionWeb,
    this.image,
    this.icon,
    this.slug,
  });

  factory CategoriesParentDataModel.fromJson(Map<String, dynamic> json) => CategoriesParentDataModel(
    id : json["id"],
    name : json["name"],
    description : json["description"],
    descriptionWeb : json["description_web"],
    image : json["image"] != null
        ?ImageModal.fromJson(json["image"])
        :null,
    icon : json["icon"] != null
        ?ImageModal.fromJson(json["icon"])
        :null,
    slug : json["slug"],
  );

  Map<String, dynamic> toJson() => {
    "id" : id ,
    "name" : name,
    "description" : description,
    "description_web" : descriptionWeb,
    "image" : image,
    "icon" : icon,
    "slug" : slug,
  };

  @override
  String toString() {
    return 'CategoriesParentDataModel: {name: $name}';
  }
}


