
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';
import 'package:multi_vendor_customer/src/models/getFeaturedVendorsModel.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class GetProductDetailModel {
  GetProductDetailModelData _data;
  bool _success;
  String _message;
  List<dynamic> _errors;

  GetProductDetailModelData get data => _data;
  bool get success => _success;
  String get message => _message;
  List<dynamic> get errors => _errors;

  GetProductDetailModel({
    GetProductDetailModelData data,
      bool success, 
      String message, 
      List<dynamic> errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
}

  GetProductDetailModel.fromJson(dynamic json) {
    _data = json["data"] != null ? GetProductDetailModelData.fromJson(json["data"]) : null;
    _success = json["success"];
    _message = json["message"];
    _errors = json['errors'];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class GetProductDetailModelData {
  ProductModelForProductDetail _product;
  List<ProductModel> _relatedProducts;

  ProductModelForProductDetail get product => _product;
  List<dynamic> get relatedProducts => _relatedProducts;

  GetProductDetailModelData({
    ProductModelForProductDetail product,
      List<ProductModel> relatedProducts}){
    _product = product;
    _relatedProducts = relatedProducts;
}

  GetProductDetailModelData.fromJson(dynamic json) {
    _product = json["product"] != null ? ProductModelForProductDetail.fromJson(json["product"]) : null;
    if (json["related_products"] != null) {
      _relatedProducts = [];
      json["related_products"].forEach((v) {
        _relatedProducts.add(ProductModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_product != null) {
      map["product"] = _product.toJson();
    }
    if (_relatedProducts != null) {
      map["related_products"] = _relatedProducts.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class ProductModelForProductDetail {
  int _id;
  List<Attributes> _attributes;
  List<Variants> _variants;
  String _name;
  List<String> _tags;
  String _shortDescription;
  String _description;
  String _refundPolicy;
  String _shortDescriptionWeb;
  String _descriptionWeb;
  String _refundPolicyWeb;
  int _productType;
  String _sku;
  Brand _brand;
  String _modal;
  String _displayPrice;
  String _price;
  String _currency;
  String _defaultCurrency;
  String _defaultPrice;
  int _conversionRate;
  int _stock;
  String _availableDate;
  String _weight;
  String _unit;
  List<ImageModal> _media;
  Manufacturer _manufacturer;
  TaxClass _taxClass;
  VendorsModelWithProducts _vendor;
  int _productOrdered;
  int _productLiked;
  String _slug;
  dynamic _externalLink;
  int _maxOrder;
  int _minOrder;
  List<FAQModel> _faq;
  FlashSaleModal _flashSale;
  FlashSaleModal _specialSale;
  List<Categories> _categories;
  Seo _seo;
  List<ReviewsModal> _reviews;
  int _totalReviews;
  double _reviewsAverageRating;

  int get id => _id;
  List<Attributes> get attributes => _attributes;
  List<Variants> get variants => _variants;
  String get name => _name;
  List<String> get tags => _tags;
  String get shortDescription => _shortDescription;
  String get description => _description;
  String get refundPolicy => _refundPolicy;
  String get shortDescriptionWeb => _shortDescriptionWeb;
  String get descriptionWeb => _descriptionWeb;
  String get refundPolicyWeb => _refundPolicyWeb;
  int get productType => _productType;
  String get sku => _sku;
  Brand get brand => _brand;
  String get modal => _modal;
  String get displayPrice => _displayPrice;
  String get price => _price;
  String get currency => _currency;
  String get defaultCurrency => _defaultCurrency;
  String get defaultPrice => _defaultPrice;
  int get conversionRate => _conversionRate;
  int get stock => _stock;
  String get availableDate => _availableDate;
  String get weight => _weight;
  String get unit => _unit;
  List<ImageModal> get media => _media;
  Manufacturer get manufacturer => _manufacturer;
  TaxClass get taxClass => _taxClass;
  VendorsModelWithProducts get vendor => _vendor;
  int get productOrdered => _productOrdered;
  int get productLiked => _productLiked;
  String get slug => _slug;
  dynamic get externalLink => _externalLink;
  int get maxOrder => _maxOrder;
  int get minOrder => _minOrder;
  List<FAQModel> get faq => _faq;
  FlashSaleModal get flashSale => _flashSale;
  FlashSaleModal get specialSale => _specialSale;
  List<Categories> get categories => _categories;
  Seo get seo => _seo;
  List<ReviewsModal> get reviews => _reviews;
  int get totalReviews => _totalReviews;
  double get reviewsAverageRating => _reviewsAverageRating;

  ProductModelForProductDetail({
    int id,
    List<Attributes> attributes,
    List<Variants> variants,
    String name,
    List<String> tags,
    String shortDescription,
    String description,
    String refundPolicy,
    String shortDescriptionWeb,
    String descriptionWeb,
    String refundPolicyWeb,
    int productType,
    String sku,
    Brand brand,
    String modal,
    String displayPrice,
    String price,
    String currency,
    String defaultCurrency,
    String defaultPrice,
    int conversionRate,
    int stock,
    String availableDate,
    String weight,
    String unit,
    List<ImageModal> media,
    Manufacturer manufacturer,
    TaxClass taxClass,
    VendorsModelWithProducts vendor,
    int productOrdered,
    int productLiked,
    String slug,
    dynamic externalLink,
    int maxOrder,
    int minOrder,
    List<FAQModel> faq,
    FlashSaleModal flashSale,
    FlashSaleModal specialSale,
    List<Categories> categories,
    Seo seo,
    List<ReviewsModal> reviews,
    int totalReviews,
    double reviewsAverageRating}){
    _id = id;
    _attributes = attributes;
    _variants = variants;
    _name = name;
    _tags = tags;
    _shortDescription = shortDescription;
    _description = description;
    _refundPolicy = refundPolicy;
    _shortDescriptionWeb = shortDescriptionWeb;
    _descriptionWeb = descriptionWeb;
    _refundPolicyWeb = refundPolicyWeb;
    _productType = productType;
    _sku = sku;
    _brand = brand;
    _modal = modal;
    _displayPrice = displayPrice;
    _price = price;
    _currency = currency;
    _defaultCurrency = defaultCurrency;
    _defaultPrice = defaultPrice;
    _conversionRate = conversionRate;
    _stock = stock;
    _availableDate = availableDate;
    _weight = weight;
    _unit = unit;
    _media = media;
    _manufacturer = manufacturer;
    _taxClass = taxClass;
    _vendor = vendor;
    _productOrdered = productOrdered;
    _productLiked = productLiked;
    _slug = slug;
    _externalLink = externalLink;
    _maxOrder = maxOrder;
    _minOrder = minOrder;
    _faq = faq;
    _flashSale = flashSale;
    _specialSale = specialSale;
    _categories = categories;
    _seo = seo;
    _reviews = reviews;
    _totalReviews = totalReviews;
    _reviewsAverageRating = reviewsAverageRating;
  }

  ProductModelForProductDetail.fromJson(dynamic json) {
    _id = json["id"];
    if (json["attributes"] != null) {
      _attributes = [];
      json["attributes"].forEach((v) {
        _attributes.add(Attributes.fromJson(v));
      });
    }
    if (json["variants"] != null) {
      _variants = [];
      json["variants"].forEach((v) {
        _variants.add(Variants.fromJson(v));
      });
    }
    _name = json["name"];
    _tags = json["tags"] != null ? json["tags"].cast<String>() : [];
    _shortDescription = json["short_description"];
    _description = json["description"];
    _refundPolicy = json["refund_policy"];
    _shortDescriptionWeb = json["short_description_web"];
    _descriptionWeb = json["description_web"];
    _refundPolicyWeb = json["refund_policy_web"];
    _productType = json["product_type"];
    _sku = json["sku"];
    _brand = json["brand"] != null ? Brand.fromJson(json["brand"]) : null;
    _modal = json["modal"];
    _displayPrice = json["display_price"];
    _price = json["price"];
    _currency = json["currency"];
    _defaultCurrency = json["default_currency"];
    _defaultPrice = json["default_price"];
    _conversionRate = json["conversion_rate"];
    _stock = json["stock"];
    _availableDate = json["available_date"];
    _weight = json["weight"];
    _unit = json["unit"];
    if (json["media"] != null) {
      _media = [];
      json["media"].forEach((v) {
        _media.add(ImageModal.fromJson(v));
      });
    }
    _manufacturer = json["manufacturer"] != null ? Manufacturer.fromJson(json["manufacturer"]) : null;
    _taxClass = json["tax_class"] != null ? TaxClass.fromJson(json["tax_class"]) : null;
    _vendor = json["vendor"] != null ? VendorsModelWithProducts.fromJson(json["vendor"]) : null;
    _productOrdered = json["product_ordered"];
    _productLiked = json["product_liked"];
    _slug = json["slug"];
    _externalLink = json["external_link"];
    _maxOrder = json["max_order"];
    _minOrder = json["min_order"];
    if (json["faq"] != null) {
      _faq = [];
      json["faq"].forEach((v) {
        _faq.add(FAQModel.fromJson(v));
      });
    }
    _flashSale = json["flash_sale"] != null ? FlashSaleModal.fromJson(json["flash_sale"]) : null;
    _specialSale = json["special_sale"]  != null ? FlashSaleModal.fromJson(json["special_sale"]) : null;
    if (json["categories"] != null) {
      _categories = [];
      json["categories"].forEach((v) {
        _categories.add(Categories.fromJson(v));
      });
    }
    _seo = json["seo"] != null ? Seo.fromJson(json["seo"]) : null;
    if (json["reviews"] != null) {
      _reviews = [];
      json["reviews"].forEach((v) {
        _reviews.add(ReviewsModal.fromJson(v));
      });
    }
    _totalReviews = json["total_reviews"];
    _reviewsAverageRating = double.parse(json["reviews_average_rating"].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    if (_attributes != null) {
      map["attributes"] = _attributes.map((v) => v.toJson()).toList();
    }
    if (_variants != null) {
      map["variants"] = _variants.map((v) => v.toJson()).toList();
    }
    map["name"] = _name;
    map["short_description"] = _shortDescription;
    map["description"] = _description;
    map["refund_policy"] = _refundPolicy;
    map["short_description_web"] = _shortDescriptionWeb;
    map["description_web"] = _descriptionWeb;
    map["refund_policy_web"] = _refundPolicyWeb;
    map["product_type"] = _productType;
    map["sku"] = _sku;
    if (_brand != null) {
      map["brand"] = _brand.toJson();
    }
    map["modal"] = _modal;
    map["display_price"] = _displayPrice;
    map["price"] = _price;
    map["currency"] = _currency;
    map["default_currency"] = _defaultCurrency;
    map["default_price"] = _defaultPrice;
    map["conversion_rate"] = _conversionRate;
    map["stock"] = _stock;
    map["available_date"] = _availableDate;
    map["weight"] = _weight;
    map["unit"] = _unit;
    if (_media != null) {
      map["media"] = _media.map((v) => v.toJson()).toList();
    }
    if (_manufacturer != null) {
      map["manufacturer"] = _manufacturer.toJson();
    }
    if (_taxClass != null) {
      map["tax_class"] = _taxClass.toJson();
    }
    if (_vendor != null) {
      map["vendor"] = _vendor.toJson();
    }
    map["product_ordered"] = _productOrdered;
    map["product_liked"] = _productLiked;
    map["slug"] = _slug;
    map["external_link"] = _externalLink;
    map["max_order"] = _maxOrder;
    map["min_order"] = _minOrder;
    if (_faq != null) {
      map["faq"] = _faq.map((v) => v.toJson()).toList();
    }
    if (_flashSale != null) {
      map["flash_sale"] = _flashSale.toJson();
    }
    map["special_sale"] = _specialSale;
    if (_categories != null) {
      map["categories"] = _categories.map((v) => v.toJson()).toList();
    }
    if (_seo != null) {
      map["seo"] = _seo.toJson();
    }
    if (_reviews != null) {
      map["reviews"] = _reviews.map((v) => v.toJson()).toList();
    }
    map["total_reviews"] = _totalReviews;
    map["reviews_average_rating"] = _reviewsAverageRating;
    return map;
  }

}
