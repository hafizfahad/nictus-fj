
class ThemeModel {
  int screenBackgroundColor;
  String displayText;
  int buttonColor;
  int primaryColor;
  String logoPath;

  ThemeModel({
    this.screenBackgroundColor,
    this.displayText,
    this.buttonColor,
    this.primaryColor,
    this.logoPath
  });

  factory ThemeModel.fromJson(Map<String, dynamic> json) => ThemeModel(
    displayText : json["displayText"].toString(),
    screenBackgroundColor : int.parse(json["screenBackgroundColor"]),
    buttonColor : int.parse(json["buttonColor"]),
    primaryColor : int.parse(json["primaryColor"]),
    logoPath : json["logo"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "displayText" : displayText,
    "screenBackgroundColor" : screenBackgroundColor,
    "buttonColor" : buttonColor,
    "primaryColor" : primaryColor,
    "logo" : logoPath,
  };
  @override
  String toString() {
    return 'Theme: {name: $displayText, logoPath: $logoPath}';
  }
}