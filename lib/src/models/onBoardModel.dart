
class OnBoardModel {
  int length;
  List<OnBoardDetailDataModel> onBoardData;

  OnBoardModel({
    this.length,
    this.onBoardData,
  });

  factory OnBoardModel.fromJson(Map<String, dynamic> json) => OnBoardModel(
    length : json["length"],
    onBoardData : json['data'] != null
        ? List.from(json['data']).map((element) => OnBoardDetailDataModel.fromJson(element)).toList()
        : [],
  );

  Map<String, dynamic> toJson() => {
    "length" : length,
    "data" : onBoardData,
  };

  @override
  String toString() {
    return 'OnBoard: {Length: $length}';
  }
}


class OnBoardDetailDataModel {
  String title;
  String subTitle;
  String logo;

  OnBoardDetailDataModel({
    this.title,
    this.subTitle,
    this.logo,
  });

  factory OnBoardDetailDataModel.fromJson(Map<String, dynamic> json) => OnBoardDetailDataModel(
    title : json["title"],
    subTitle : json["subTitle"],
    logo : json["logo"],
  );

  Map<String, dynamic> toJson() => {
    "title" : title,
    "subTitle" : subTitle,
    "logo" : logo,
  };

  @override
  String toString() {
    return 'OnBoardDetailData: {title: $title}';
  }
}