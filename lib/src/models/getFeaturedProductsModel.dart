
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';

class FeaturedProductsModel {
  List<Data> _data;
  bool _success;
  String _message;
  List _errors;

  List<Data> get data => _data;
  bool get success => _success;
  String get message => _message;
  List get errors => _errors;

  FeaturedProductsModel({
      List<Data> data, 
      bool success, 
      String message, 
      List errors}){
    _data = data;
    _success = success;
    _message = message;
    _errors = errors;
}

  FeaturedProductsModel.fromJson(dynamic json) {
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
    _success = json["success"];
    _message = json["message"];
    _errors = json['serrors'];
    // if (json["errors"] != null) {
    //   _errors = [];
    //   json["errors"].forEach((v) {
    //     _errors.add(dynamic.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    map["success"] = _success;
    map["message"] = _message;
    if (_errors != null) {
      map["errors"] = _errors.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Data {
  int _id;
  String _name;
  String _description;
  String _descriptionWeb;
  ImageModal _image;
  ImageModal _icon;
  List<ProductModel> _products;
  String _slug;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get descriptionWeb => _descriptionWeb;
  ImageModal get image => _image;
  ImageModal get icon => _icon;
  List<ProductModel> get products => _products;
  String get slug => _slug;

  Data({
      int id, 
      String name, 
      String description, 
      String descriptionWeb,
      ImageModal image,
      ImageModal icon,
      List<ProductModel> products,
      String slug}){
    _id = id;
    _name = name;
    _description = description;
    _descriptionWeb = descriptionWeb;
    _image = image;
    _icon = icon;
    _products = products;
    _slug = slug;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _descriptionWeb = json["description_web"];
    _image = json["image"] != null
        ?ImageModal.fromJson(json["image"])
        :null;
    _icon = json["icon"] != null
        ?ImageModal.fromJson(json["icon"])
        :null;
    if (json["products"] != null) {
      _products = [];
      json["products"].forEach((v) {
        _products.add(ProductModel.fromJson(v));
      });
    }
    _slug = json["slug"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["description_web"] = _descriptionWeb;
    if (_image != null) {
      map["image"] = _image.toJson();
    }
    if (_icon != null) {
      map["icon"] = _icon.toJson();
    }
    if (_products != null) {
      map["products"] = _products.map((v) => v.toJson()).toList();
    }
    map["slug"] = _slug;
    return map;
  }

}
