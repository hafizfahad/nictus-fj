import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:dio/dio.dart' as dio_instance;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/headersService.dart';

getMethod(
    BuildContext context,
    String apiUrl,
    dynamic queryData,
    bool addAuthHeader,
    Function executionMethod  // for performing functionalities
    ) async{

  dio_instance.Response response;
  dio_instance.Dio dio = new dio_instance.Dio();

  dio.options.connectTimeout = 10000;
  dio.options.receiveTimeout = 6000;

  setCustomHeader(dio, 'Consumer-Key', '889f9d80a5568e719e581f3b7f71b36d09455331edd128f9dddf9f4cee6ab903');
  setCustomHeader(dio, 'Consumer-Secret', 'f32a46c04c52760b6241046f8a6f85994270351b68e37f0c9433fb2be3b739fd');

  if(apiUrl != getEnvironmentApi && apiUrl != getDefaultSettingsApi){

    Language language = Language.fromJson(jsonDecode(box.read('customLanguageSettings')));
    Currency currency = Currency.fromJson(jsonDecode(box.read('customCurrencySettings')));
    print("header--i18n_redirected---->>${language.code}");
    print("header--currency---->>${currency.code}");

    setCustomHeader(dio, 'i18n_redirected', '${language.code}');
    setLanguageHeader(dio, 'Cookie', 'i18n_redirected=${language.code}');
    setCustomHeader(dio, 'currency', '${currency.code}');
  }

  if(addAuthHeader){
    setCustomHeader(dio, 'Authorization', 'Bearer ${box.read('authToken')}');
  }

  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('Internet Connected');
      Get.find<AppController>().changeInternetCheckerState(true);
      try{

        response = await dio.get(apiUrl, queryParameters: queryData);

        if(response.statusCode == 200){
          log('StatusCode------>> ${response.statusCode}');
          log('Response $apiUrl------>> ${response.data}');
          executionMethod(true,response.data);
        }else{
          executionMethod(false,null);
        }

      } on dio_instance.DioError catch (e){


        executionMethod(false,null);
        if(e.response.data != null){
          print('Dio Error From Get $apiUrl -->> ${e.response.data['message']}');
        }else{
          print('Dio Error From Get $apiUrl -->> ${e}');
        }

        // messageShowService(response.data,false);



      }
    }
  } on SocketException catch (_) {

    Get.find<AppController>().changeInternetCheckerState(false);
    print('Internet Not Connected');
  }


}