import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' as dio_instance;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/dialogWidget.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';
import 'package:multi_vendor_customer/src/services/headersService.dart';
import 'package:multi_vendor_customer/src/services/messageShowService.dart';

postMethod(
    BuildContext context,
    String apiUrl,
    dynamic postData,
    bool addAuthHeader,
    Function executionMethod   // for performing functionalities
    ) async{

  dio_instance.Response response;
  dio_instance.Dio dio = new dio_instance.Dio();

  dio.options.connectTimeout = 10000;
  dio.options.receiveTimeout = 6000;

  setCustomHeader(dio, 'Consumer-Key', '889f9d80a5568e719e581f3b7f71b36d09455331edd128f9dddf9f4cee6ab903');
  setCustomHeader(dio, 'Consumer-Secret', 'f32a46c04c52760b6241046f8a6f85994270351b68e37f0c9433fb2be3b739fd');

  Language language = Language.fromJson(jsonDecode(box.read('customLanguageSettings')));
  Currency currency = Currency.fromJson(jsonDecode(box.read('customCurrencySettings')));
  print("Post-header--i18n_redirected---->>${language.code}");
  print("Post-header--currency---->>${currency.code}");

  setCustomHeader(dio, 'i18n_redirected', '${language.code}');
  setLanguageHeader(dio, 'Cookie', 'i18n_redirected=${language.code}');
  // setLanguageHeader(dio, 'Cookie', 'i18n_redirected=${language.code},session_token=');
  setCustomHeader(dio, 'currency', '${currency.code}');

  //-- if API need headers then this if works and it based on bool value come from function calling
  if(addAuthHeader){
    setCustomHeader(dio, 'Authorization', 'Bearer ${box.read('authToken')}');
  }

  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('Internet Connected');
      Get.find<AppController>().changeInternetCheckerState(true);
      try{

        response = await dio.post(apiUrl, data: postData);

        if(response.statusCode == 200){

          executionMethod(true, response.data);

        }else{

          executionMethod(false, null);

        }
      } on dio_instance.DioError catch (e){

        print('Dio Error From Post $apiUrl -->> ${e.response.data['message']}');
        Get.snackbar(
          'Error',
          '${e.response.data['message']}',
          colorText: Colors.black
        );

        if(e.response.data != null){
          print('Dio Error From Post $apiUrl -->> ${e.response.data['message']}');
          Get.snackbar(
              'Error',
              '${e.response.data['message']}',
              colorText: Colors.black
          );
        }else{
          print('Dio Error From Post $apiUrl -->> ${e}');
        }


        executionMethod(false,null);

        // messageShowService(response.data,false);


      }
    }
  } on SocketException catch (_) {

    Get.find<AppController>().changeInternetCheckerState(false);
    print('Internet Not Connected');
  }


}