import 'package:multi_vendor_customer/src/components/dialogWidget.dart';

messageShowService(
    dynamic responseData,
    bool messageType  // if true, means success. if false, means error.
    ){

  if(messageType){

    successMessageBox('Success', '$responseData}');

  }else{

    errorMessageBox('Error','$responseData');

  }

}
