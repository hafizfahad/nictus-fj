import 'package:global_configuration/global_configuration.dart';

//---------------API-base url-------------
final String baseUrl = GlobalConfiguration().get('api_base_url');

final String getEnvironmentApi = baseUrl + "/api/web/environment";

final String getDefaultSettingsApi = baseUrl + "/api/web/application_default_settings";

final String getAllCurrencyApi = baseUrl + "/api/web/currencies";

final String getAllLanguageApi = baseUrl + "/api/web/languages";

final String getAllCategoriesApi = baseUrl + "/api/web/categories";

final String getAllGenericAppDataApi = baseUrl + "/api/web/generic_app_data";

final String getFlashSaleDataApi = baseUrl + "/api/web/flash_sale_products";

final String getNewArrivalDataApi = baseUrl + "/api/web/new_arrival_products";

final String getFeaturedProductsDataApi = baseUrl + "/api/web/featured_products";

final String getTopSellingProductsDataApi = baseUrl + "/api/web/top_selling_products";

final String getTrendingProductsDataApi = baseUrl + "/api/web/trending_products";

final String getTopReviewedProductsDataApi = baseUrl + "/api/web/top_reviewed_products";

final String getFeaturedVendorsDataApi = baseUrl + "/api/web/featured_vendors";

final String getVendorDetailDataApi = baseUrl + "/api/web/vendor";

final String getCategoryWiseProductsDataApi = baseUrl + "/api/web/category_wise_products";

final String getProductsDetailApi = baseUrl + "/api/web/product";

final String getFilteredProductsForShopPageApi = baseUrl + "/api/web/shopFilterProducts";

final String getShopPageDataApi = baseUrl + "/api/web/shopPageData";

final String loginApi = baseUrl + "/api/customer/auth/login";

final String logOutApi = baseUrl + "/api/customer/auth/logout";

final String addToWishListApi = baseUrl + "/api/web/addWishlistItem";

final String getWishListProductsApi = baseUrl + "/api/web/fetchWishlistItems";
