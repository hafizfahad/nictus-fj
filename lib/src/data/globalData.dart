import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';
import 'package:multi_vendor_customer/src/models/enviromentModel.dart';
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';
import 'package:multi_vendor_customer/src/models/getAllCategoriesModel.dart';
import 'package:multi_vendor_customer/src/models/getAllCuurencyModel.dart';
import 'package:multi_vendor_customer/src/models/getAllLanguageModel.dart';
import 'package:multi_vendor_customer/src/models/getCategoryWiseProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getFeaturedProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getFeaturedVendorsModel.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getLoginDataModel.dart';
import 'package:multi_vendor_customer/src/models/getNewArrivalProductsModal.dart';
import 'package:multi_vendor_customer/src/models/getProductDetailModel.dart';
import 'package:multi_vendor_customer/src/models/getProductsOfVendorModel.dart';
import 'package:multi_vendor_customer/src/models/getShopPageDataModel.dart';
import 'package:multi_vendor_customer/src/models/getTopReviewedProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getTopSellingProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getTrendingProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getShopPageProductsModel.dart';
import 'package:multi_vendor_customer/src/models/getVendorStoreDetailModel.dart';
import 'package:multi_vendor_customer/src/models/getWishListProductsModel.dart';
import 'package:multi_vendor_customer/src/models/onBoardModel.dart';
import 'package:multi_vendor_customer/src/models/splashModel.dart';
import 'package:multi_vendor_customer/src/models/themeModel.dart';
import 'package:multi_vendor_customer/src/pages/shopFiltersPage.dart';

///
GetStorage box = GetStorage();    // for get & set local storage
///
ThemeModel configThemeData;   // for saving theme configurations
///
SplashModel configSplashData;   // for saving splash configurations
///
OnBoardModel configOnBoardData;   // for saving onBoard configurations
List<OnBoardDetailDataModel> onBoardDataList; // for saving onBoard data list configurations
///
EnvironmentModel environmentModel;  // for saving environment configurations
///
DefaultSettingsModel defaultSettingsModel;   // for saving defaultSettings configurations
///
GetAllCurrencyModel getAllCurrencyModel;   // for saving all-currencies
///
GetAllLanguageModel getAllLanguageModel;   // for saving all-languages
///
GetAllCategoriesModel getAllCategoriesModel;   // for saving all-categories
///
GetGenericAppDataModel genericAppDataModel;     //  for saving all-generic-app-data
///
UserModel currentUserModel;     //  for saving all-generic-app-data
///
GetFlashSaleProductsModel getFlashSaleProductsModel;     //  for saving flash-sale-data
List<AnimationController> flashSaleTimerControllerList = [];
///
NewArrivalModal getNewArrivalModal;     //  for saving new-arrival-data
///
FeaturedProductsModel getFeaturedProductsModel;     //  for saving featured-products-data
///
GetTopSellingProductsModel getTopSellingProductsModel;     //  for saving top-selling-products-data
///
GetTrendingProductsModel getTrendingProductsModel;     //  for saving trending-products-data
///
GetTopReviewedProductsModel getTopReviewedProductsModel;     //  for saving top-reviewed-products-data
///
GetFeaturedVendorsModel getFeaturedVendorsModel;     //  for saving top-reviewed-products-data
///
GetCategoryWiseProductsModel getCategoryWiseProductsModel;     //  for saving category-wise-products-data
List<Widget> categoryWiseProductsTabList = [];
///
GetProductDetailModel getProductDetailModel;     //  for saving product-detail
///
GetShopPageProductsModel getShopPageProductsModel;     //  for saving shop-page-products
List<ProductModel> shopPageProductListForPagination = [];
///
GetShopPageDataModel getShopPageDataModel;     //  for saving shop-page-products
///
GetProductsOfVendorModel getProductsOfVendorModel;     //  for saving products of vendor
///
GetVendorStoreDetailModel getVendorStoreDetailModel;     //  for saving products of vendor
///
GetWishListProductsModel getWishListProductsModel;     //  for saving products of vendor
List<ProductModel> wishListProductListForPagination = [];
///
GetLoginDataModel getLoginDataModel;     //  for saving login-data
///

List<ColorChangeSelectorClass> categoryFilterColorSelectorList = [];
List<ColorChangeSelectorClass> brandFilterColorSelectorList = [];


final TextEditingController voucherController = TextEditingController();

TabController productDetailPageTabController;

final TextEditingController checkoutNameController = TextEditingController();
final TextEditingController checkoutEmailController = TextEditingController();
final TextEditingController checkoutPhoneController = TextEditingController();

///----internet-settings-------------
String connectionStatus = 'Unknown';
final Connectivity connectivity = Connectivity();
StreamSubscription<ConnectivityResult> connectivitySubscription;
bool internetCheck = true;