import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/homeDrawerComponent.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/repository/getWishListDataRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class WishListPage extends StatefulWidget {
  @override
  _WishListPageState createState() => _WishListPageState();
}

class _WishListPageState extends State<WishListPage> {

  Future _paginationDataLoad() async {
    // perform fetching data delay
    // await new Future.delayed(new Duration(seconds: 2));

    print("load more");
    // update data and loading status
    if(getWishListProductsModel.data.meta.lastPage > getWishListProductsModel.data.meta.currentPage){
      Get.find<AppController>()
          .changeGetPaginationProgressCheck(true);

      setState(() {});

      ///------get-wish-list-products-data-api-call
      getMethod(
          context,
          getWishListProductsApi,
          {
            'page': (getWishListProductsModel.data.meta.currentPage + 1),
            'perPage': getWishListProductsModel.data.meta.perPage
          },
          true,
          getWishListProductsDataForPagination
      );

    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(currentUserModel != null){
      ///------get-wish-list-products-data-api-call
      getMethod(
          context,
          getWishListProductsApi,
          null,
          true,
          getWishListProductsData
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (homeController) => GestureDetector(
        onTap: (){
          ///---keyboard-close
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          drawer: MyCustomDrawer(),
          appBar: MyCustomAppBar(
            drawerShow: true,
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GetBuilder<AppController>(
              init: AppController(),
              builder: (appController) =>  Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ///---search-bar
                    Container(
                      color: Color(0xffF9F9F9),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Theme(
                          data: ThemeData(
                              primaryColor: Color(0xff363636)
                          ),
                          child: TextFormField(
                            decoration: InputDecoration(
                                suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                                border: InputBorder.none,
                                hintText: 'search here',
                                hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                            ),
                          ),
                        ),
                      ),
                    ),
                    ///---title
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                      child: Text(
                        'WishList',
                        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 23),
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      color: customSmoothGreyColor,
                    ),
                    currentUserModel == null
                        ?Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Center(
                              child: Container(
                      height: 300,
                      width: 300,
                      decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(300),
                              image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/not-logged in.jpg'
                                ),
                                fit: BoxFit.fill
                              )
                      ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                              child: InkWell(
                                onTap: (){
                                  Get.toNamed(PageRoutes.login);
                                },
                                child: Container(
                                  height: 40,
                                  width: MediaQuery.of(context).size.width * .55,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: customThemeColor),
                                  child: Center(
                                    child: Text(
                                      'Lets Login',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2
                                          .copyWith(
                                          fontSize: 15, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                        :Expanded(
                      child: NotificationListener<ScrollNotification>(
                        onNotification: (ScrollNotification scrollInfo) {
                          if (!appController.getPaginationProgressCheck &&
                              scrollInfo.metrics.pixels ==
                                  scrollInfo.metrics.maxScrollExtent) {

                            print('ScrollNotification----->>');
                          }
                          return null;
                        },
                        child: SingleChildScrollView(
                          child: !homeController
                              .getWishListDataCheck
                              ? SkeletonLoader(
                            period: Duration(seconds: 2),
                            highlightColor: Colors.grey,
                            direction: SkeletonDirection.ltr,
                            builder: Center(
                              child: Wrap(
                                children: List.generate(6, (index) {
                                  return Container(
                                    width: MediaQuery.of(context)
                                        .size
                                        .width *
                                        .45,
                                    child: Padding(
                                      padding: index % 2 == 0
                                          ? const EdgeInsets.fromLTRB(
                                          0, 5, 5, 5)
                                          : const EdgeInsets.fromLTRB(
                                          5, 5, 0, 5),
                                      child: Container(
                                        height: 270,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                          BorderRadius.vertical(
                                            top: Radius.circular(10),
                                            bottom:
                                            Radius.circular(10),
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey
                                                  .withOpacity(0.2),
                                              spreadRadius: 2,
                                              blurRadius: 6,
                                              offset: Offset(0, 3),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ),
                          )
                              : Column(
                                children: [
                                  Center(
                            child: Wrap(
                                  children: List.generate(
                                      wishListProductListForPagination
                                          .length, (index) {
                                    return Container(
                                      width: MediaQuery.of(context)
                                          .size
                                          .width *
                                          .45,
                                      child: Padding(
                                        padding: index % 2 == 0
                                            ? const EdgeInsets.fromLTRB(
                                            0, 5, 5, 5)
                                            : const EdgeInsets.fromLTRB(
                                            5, 5, 0, 5),
                                        child: Stack(children: [
                                          InkWell(
                                            onTap: () {
                                              ///------get-product-detail-data-api-call
                                              getMethod(
                                                  context,
                                                  '$getProductsDetailApi/${wishListProductListForPagination[index].slug}',
                                                  null,
                                                  false,
                                                  getProductDetailData);
                                              Get.find<HomeController>()
                                                  .changeGetProductDetailDataCheck(
                                                  false);
                                              Get.toNamed(PageRoutes
                                                  .productDetail);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                BorderRadius.vertical(
                                                  top:
                                                  Radius.circular(10),
                                                  bottom:
                                                  Radius.circular(10),
                                                ),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.2),
                                                    spreadRadius: 2,
                                                    blurRadius: 6,
                                                    offset: Offset(0, 3),
                                                  ),
                                                ],
                                              ),
                                              child: Column(
                                                children: [
                                                  ///--------------product-image
                                                  Container(
                                                    height: 145,
                                                    width:
                                                    double.infinity,
                                                    child: ClipRRect(
                                                      borderRadius:
                                                      BorderRadius
                                                          .vertical(
                                                        top: Radius
                                                            .circular(10),
                                                        bottom: Radius
                                                            .circular(10),
                                                      ),
                                                      child: wishListProductListForPagination[
                                                      index]
                                                          .media
                                                          .length ==
                                                          0
                                                          ? Image.asset(
                                                        'assets/images/imagePlaceHolder.jpg',
                                                        fit: BoxFit
                                                            .fill,
                                                      )
                                                          : Image.network(
                                                        '${baseUrl + wishListProductListForPagination[index].media[0].originalMediaPath}',
                                                        fit: BoxFit
                                                            .fill,
                                                      ),
                                                    ),
                                                  ),

                                                  ///--------------product-detail
                                                  Container(
                                                    width:
                                                    double.infinity,
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .only(
                                                          top: 8.0,
                                                          left: 10.0),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          ///--------------product-name
                                                          Text(
                                                            '${wishListProductListForPagination[index].name}',
                                                            softWrap:
                                                            true,
                                                            overflow:
                                                            TextOverflow
                                                                .ellipsis,
                                                            maxLines: 2,
                                                            style: Theme.of(
                                                                context)
                                                                .textTheme
                                                                .headline1
                                                                .copyWith(
                                                                fontSize:
                                                                14,
                                                                color:
                                                                customDarkGreyColor),
                                                          ),
                                                          SizedBox(
                                                            height: 4.0,
                                                          ),

                                                          ///--------------product-rating-row
                                                          Row(
                                                            children: [
                                                              RatingBar(
                                                                ignoreGestures: true,
                                                                initialRating:
                                                                wishListProductListForPagination[index]
                                                                    .reviewsAverageRating,
                                                                direction:
                                                                Axis.horizontal,
                                                                allowHalfRating:
                                                                true,
                                                                itemCount:
                                                                5,
                                                                itemSize:
                                                                15,
                                                                ratingWidget:
                                                                RatingWidget(
                                                                  full:
                                                                  Icon(
                                                                    Icons
                                                                        .star,
                                                                    color:
                                                                    customRatingStartColor,
                                                                  ),
                                                                  half: Icon(
                                                                      Icons
                                                                          .star_half,
                                                                      color:
                                                                      customRatingStartColor),
                                                                  empty: Icon(
                                                                      Icons
                                                                          .star_border_purple500_sharp,
                                                                      color:
                                                                      customRatingStartColor),
                                                                ),
                                                                itemPadding:
                                                                EdgeInsets.symmetric(
                                                                    horizontal: 0.0),
                                                                onRatingUpdate:
                                                                    (rating) {
                                                                  print(
                                                                      rating);
                                                                },
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets
                                                                    .only(
                                                                    left:
                                                                    3),
                                                                child:
                                                                Text(
                                                                  '(${wishListProductListForPagination[index].totalReviews})',
                                                                  style: Theme.of(context)
                                                                      .textTheme
                                                                      .subtitle2
                                                                      .copyWith(fontSize: 10),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          SizedBox(
                                                            height: 10.0,
                                                          ),
                                                          (wishListProductListForPagination[index].flashSale !=
                                                              null ||
                                                              wishListProductListForPagination[index].specialSale !=
                                                                  null)
                                                              ? Text(
                                                            '${wishListProductListForPagination[index].displayPrice}',
                                                            style: Theme.of(context)
                                                                .textTheme
                                                                .subtitle2
                                                                .copyWith(
                                                              decoration: TextDecoration.lineThrough,
                                                            ),
                                                          )
                                                              : SizedBox(),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                            children: [
                                                              ///--------------product-price
                                                              Expanded(
                                                                child: (wishListProductListForPagination[index].flashSale != null &&
                                                                    wishListProductListForPagination[index].specialSale == null)
                                                                    ? Text(
                                                                  '${wishListProductListForPagination[index].flashSale.displayPrice}',
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(
                                                                      fontSize: 14,
                                                                      color: customThemeColor),
                                                                )
                                                                    : (wishListProductListForPagination[index].flashSale == null &&
                                                                    wishListProductListForPagination[index].specialSale != null)
                                                                    ? Text(
                                                                  '${wishListProductListForPagination[index].specialSale.displayPrice}',
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(
                                                                      fontSize: 14,
                                                                      color: customThemeColor),
                                                                )
                                                                    : Text(
                                                                  '${wishListProductListForPagination[index].displayPrice}',
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(
                                                                      fontSize: 14,
                                                                      color: customThemeColor),
                                                                ),
                                                              ),
                                                              InkWell(
                                                                onTap:
                                                                    () {},
                                                                child:
                                                                Padding(
                                                                  padding: const EdgeInsets.fromLTRB(
                                                                      5,
                                                                      5,
                                                                      10,
                                                                      5),
                                                                  child:
                                                                  Icon(
                                                                    Icons
                                                                        .add_box_rounded,
                                                                    size:
                                                                    20,
                                                                    color:
                                                                    customThemeColor,
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          SizedBox(
                                                            height: 8.0,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            right: 0.0,
                                            top: 0,
                                            child: InkWell(
                                              onTap: (){},
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                                child: Container(
                                                  height: 14,
                                                  width: 16,
                                                  child: Center(
                                                      child: SvgPicture.asset(
                                                        'assets/Icons/Product_page_icons/Group 366.svg',
                                                        color: customThemeColor,
                                                      )
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                              top: 10.0,
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: List.generate(
                                                    wishListProductListForPagination[index].tags.length, (tagIndex){
                                                  return Column(
                                                    children: [
                                                      Container(
                                                        height: 20,
                                                        decoration: BoxDecoration(
                                                            color: customThemeColor,
                                                            borderRadius: BorderRadius.only(
                                                              topRight: Radius.circular(6.0),
                                                              bottomRight: Radius.circular(6.0),
                                                            )
                                                        ),
                                                        child: Center(
                                                            child: Padding(
                                                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                              child: Text('${wishListProductListForPagination[index].tags[tagIndex]}',
                                                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                    fontSize: 12,
                                                                    color: Colors.white
                                                                ),
                                                              ),
                                                            )),
                                                      ),
                                                      SizedBox(height: 6,),
                                                    ],
                                                  );
                                                }),
                                              )
                                          ),
                                        ]),
                                      ),
                                    );
                                  }),
                            ),
                          ),
                                  getWishListProductsModel.data.meta.lastPage >
                                      getWishListProductsModel.data.meta.currentPage
                                      ? Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                                    child: appController.getPaginationProgressCheck
                                        ?SizedBox()
                                        :InkWell(
                                      onTap: (){
                                        _paginationDataLoad();
                                      },
                                      child: Container(
                                        height: 40,
                                        width: MediaQuery.of(context).size.width * .35,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            color: customThemeColor),
                                        child: Center(
                                          child: Text(
                                            'Load More',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle2
                                                .copyWith(
                                                fontSize: 15, color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                      : SizedBox(),
                                ],
                              ),
                        ),
                      ),
                    ),
                    Container(
                      height: appController.getPaginationProgressCheck ? 50.0 : 0,
                      color: Colors.transparent,
                      child: Center(
                        child: new CircularProgressIndicator(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
