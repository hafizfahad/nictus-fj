import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class MyWalletPage extends StatefulWidget {
  @override
  _MyWalletPageState createState() => _MyWalletPageState();
}

class _MyWalletPageState extends State<MyWalletPage> {

  DateTime selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Column(
          children: [
            ///---header
            Container(
              height: MediaQuery.of(context).size.height*.43,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(30)
                ),
                  image: DecorationImage(
                      image: AssetImage(
                          'assets/images/userProfileBackground.png'
                      ),
                      fit: BoxFit.fill
                  )
              ),
              child: Center(
                child: Column(
                  children: [
                    MyCustomAppBar(drawerShow: false,transparent: true,),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 0, 15, 30),
                          child: Container(
                            // height: MediaQuery.of(context).size.height*.2,
                            height: 170,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5)
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Available Balance',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                SizedBox(height: 10,),
                                Text(
                                  '\$200.00',
                                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                                      color: customGreenSaleFlagColor
                                  ),
                                ),
                                SizedBox(height: 10,),
                                Container(
                                  height: 40,
                                  width: 119,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(color: customRatingStartColor),
                                      color: Colors.white
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Top Up',
                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                          fontSize: 15,
                                          color: customRatingStartColor
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  ///-date-heading
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Transactions',
                          style: Theme.of(context).textTheme.headline1,
                        ),
                        InkWell(
                          onTap: (){
                            _selectDate(context);
                          },
                          child: Container(
                            height: 30,
                            width: 105,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: customSmoothGreyColor)
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                SvgPicture.asset(
                                  'assets/Icons/other_icons/fi-rr-calendar.svg',
                                  color: customThemeColor,
                                ),
                                Text(
                                  '${selectedDate.toString().substring(0,11)}',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  ///-transaction-list
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: ListView(
                        children: List.generate(6, (index){
                          return Padding(
                            padding: const EdgeInsets.fromLTRB(15, 0, 15, 13),
                            child: Column(
                              children: [
                                Container(
                                  height: 70,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 2,
                                        blurRadius: 6,
                                        offset: Offset(0,3),
                                      ),
                                    ],
                                  ),
                                  child: Center(
                                    child: ListTile(
                                      // contentPadding:EdgeInsets.all(0),
                                      leading: SvgPicture.asset(
                                        'assets/Icons/other_icons/Email.svg',
                                      ),
                                      title: Text(
                                        '\$150.0',
                                        style: Theme.of(context).textTheme.headline5.copyWith(
                                            color: customGreenSaleFlagColor,
                                            fontSize: 16
                                        ),
                                      ),
                                      subtitle: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Credit Card',
                                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                color: customDarkGreyColor,
                                                fontSize: 12
                                            ),
                                          ),
                                          Text(
                                            '25/06/2021',
                                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                color: customDarkGreyColor,
                                                fontSize: 12
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        })
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}
