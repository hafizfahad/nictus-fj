import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
class ResetPasswordSelectionPage extends StatefulWidget {
  @override
  _ResetPasswordSelectionPageState createState() => _ResetPasswordSelectionPageState();
}

class _ResetPasswordSelectionPageState extends State<ResetPasswordSelectionPage> {

  bool emailFieldActive = true;

  GlobalKey<FormState> _resetFromKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Form(
              key: _resetFromKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*.09,),
                  Text(
                    'RESET PASSWORD',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height*.01,),
                  SizedBox(
                    width: MediaQuery.of(context).size.width*.7,
                    child: Text(
                      'Reset your password using Email or phone number',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.subtitle2.copyWith(color: customLightBlackColor),
                    ),
                  ),
                  ///---logo
                  SizedBox(height: MediaQuery.of(context).size.height*.07,),
                  SvgPicture.asset('assets/Icons/resetPasswordPageImage.svg'),
                  ///---selection-tabs
                  SizedBox(height: MediaQuery.of(context).size.height*.07,),
                  Container(
                    width: MediaQuery.of(context).size.width*.7,
                    height: 40,
                    decoration: BoxDecoration(
                      border: Border.all(color: customThemeColor)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: (){
                              FocusScopeNode currentFocus =
                              FocusScope.of(context);
                              if (!currentFocus.hasPrimaryFocus) {
                                currentFocus.unfocus();
                              }
                              setState(() {
                                emailFieldActive = !emailFieldActive;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: emailFieldActive
                                    ?customThemeColor
                                    :Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  'Email',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 15,
                                    color: emailFieldActive
                                        ?Colors.white
                                        :customThemeColor
                                  ),
                                ),
                              ),
                            ),
                          )
                        ),
                        Expanded(
                            child: InkWell(
                              onTap: (){
                                FocusScopeNode currentFocus =
                                FocusScope.of(context);
                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                                setState(() {
                                  emailFieldActive = !emailFieldActive;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: emailFieldActive
                                      ?Colors.white
                                      :customThemeColor,
                                ),
                                child: Center(
                                  child: Text(
                                    'Phone',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                        fontSize: 15,
                                        color: emailFieldActive
                                            ?customThemeColor
                                            :Colors.white
                                    ),
                                  ),
                                ),
                              ),
                            )
                        ),
                      ],
                    ),
                  ),
                  ///---email-password-section
                  SizedBox(height: MediaQuery.of(context).size.height*.07,),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        emailFieldActive
                            ?'Email'
                            :'Password',
                        style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: TextFormField(
                      keyboardType: emailFieldActive
                          ?TextInputType.emailAddress
                          :TextInputType.name,
                      style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                      decoration: InputDecoration(
                        hintText: emailFieldActive
                            ?'Enter Email'
                            :'Enter Password',
                        hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(width: 1, color: customGreenSaleFlagColor),
                        ),
                        errorBorder: UnderlineInputBorder(
                          borderSide: BorderSide(width: 1, color: customRedColor),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                        ),
                      ),
                    ),
                  ),
                  ///---button
                  SizedBox(height: MediaQuery.of(context).size.height*.1,),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width*.75,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: customThemeColor
                      ),
                      child: Center(
                        child: Text(
                          'Reset Password',
                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                              fontSize: 15,
                              color: Colors.white
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
