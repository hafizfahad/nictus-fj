import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';

class ServerErrorPage extends StatefulWidget {
  @override
  _ServerErrorPageState createState() => _ServerErrorPageState();
}

class _ServerErrorPageState extends State<ServerErrorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Color(0xffF6F6F6),
        child: SafeArea(
          child: Column(
            children: [
              Expanded(
                  flex: 3,
                  child: Column(
                    children: [
                      SizedBox(height: MediaQuery.of(context).size.height*.1,),
                      Container(
                        width: MediaQuery.of(context).size.width*.7,
                        child: Image.asset('assets/images/serverErrorImage.png')
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*.05,),
                      Text(
                        'Server Error',
                        style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black,fontSize: 40),
                      )
                    ],
                  )
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: InkWell(
                  onTap: (){
                    Get.offAllNamed(PageRoutes.splash);
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width*.75,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Theme.of(context).buttonColor,
                        borderRadius: BorderRadius.circular(20)
                    ),
                    child: Center(
                      child: Text(
                        'Try Again',
                        style: Theme.of(context).textTheme.button.copyWith(color: Colors.white,fontSize: 25),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
