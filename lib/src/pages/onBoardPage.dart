import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/pages/serverErrorPage.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class OnBoardPage extends StatefulWidget {
  @override
  _OnBoardPageState createState() => _OnBoardPageState();
}

class _OnBoardPageState extends State<OnBoardPage> {


  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  int _currentPageForSlider = 0;
  int _pageLength = onBoardDataList.length;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SafeArea(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height*.7,
                  width: MediaQuery.of(context).size.width,
                  child: PageView(
                    physics: ClampingScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                        _currentPageForSlider = _currentPage;
                        print("currentPage --> $_currentPage");
                      });
                    },
                    children: List.generate(onBoardDataList.length, (index){
                      return  Column(
                        children: [
                          ///----------title,subtitle part
                          Expanded(
                            flex: 2,
                            child: Center(
                              child: Container(
                                width: MediaQuery.of(context).size.width*.7,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${onBoardDataList[index].title}',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.subtitle1,
                                    ),
                                    Text(
                                      '${onBoardDataList[index].subTitle}',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.subtitle2.copyWith(color: customLightBlackColor),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          ///----------onBoard-logo part
                          Expanded(
                              flex: 3,
                              child: Image.asset(
                                '${onBoardDataList[index].logo}',
                                fit: BoxFit.fill,
                                width: MediaQuery.of(context).size.width,
                              )
                          ),

                        ],
                      );
                    })
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: onBoardDataList.map((image) {       //these two lines
                    int index = onBoardDataList.indexOf(image); //are changed
                    return Container(
                      width: 7.0,
                      height: 7.0,
                      margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: _currentPageForSlider == index
                              ? Theme.of(context).buttonColor
                              : Color(0xff707070)),
                    );
                  },
                  ).toList(),
                ),
                ///----------buttons
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*.05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 39,
                          width: 120,
                          child: _currentPage == _pageLength-1
                              ?InkWell(
                            onTap: (){
                              var serverCheck = Get.find<AppController>().serverErrorCheck;
                              print("serverCheck---------->> $serverCheck");
                              if(serverCheck){
                                Get.offAllNamed(PageRoutes.serverError);
                              }else{
                                if(environmentModel.environment == "maintenance"){
                                  Get.offAllNamed(PageRoutes.environmentMaintenance);
                                }else{
                                  Get.offAllNamed(PageRoutes.bottomNavBar);
                                }
                              }
                            },
                            child: Container(
                              width: double.infinity,
                              height: double.infinity,
                              decoration: BoxDecoration(
                                  color: Theme.of(context).buttonColor,
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Center(
                                child: Text(
                                  'Done',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(color: Colors.white,fontSize: 15),
                                ),
                              ),
                            ),
                          )
                              :InkWell(
                            onTap:(){
                              _pageController.nextPage(
                                duration: Duration(milliseconds: 500),
                                curve: Curves.ease,
                              );
                              if(_currentPage == _pageLength-1){
                                print("ssssssss  ${_currentPage}");
                              }

                            },
                            child: Container(
                              width: 100,
                              height: double.infinity,
                              decoration: BoxDecoration(
                                  color: Theme.of(context).buttonColor,
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Center(
                                child: Text(
                                  'Next',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(color: Colors.white,fontSize: 15),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        _currentPage == _pageLength-1
                            ?SizedBox()
                            :InkWell(
                          onTap: (){
                            var serverCheck = Get.find<AppController>().serverErrorCheck;
                            print("serverCheck---------->> $serverCheck");
                            if(serverCheck){
                              Get.offAllNamed('/serverError');
                            }else{
                              if(environmentModel.environment == "maintenance"){
                                Get.offAllNamed("/maintenance");
                              }else{
                                Get.offAllNamed("/bottomNavBar");
                              }
                            }
                          },
                          child: Center(
                            child: Text(
                              'Skip',
                              style: Theme.of(context).textTheme.headline5.copyWith(
                                  color: customDarkBlackColor,
                                  decoration: TextDecoration.underline
                              ),
                            ),
                          ),
                        ),
                        // Container(
                        //   height: 35,
                        //   width: 138,
                        //   child: _currentPage == _pageLength-1
                        //       ?InkWell(
                        //     onTap: (){
                        //       var serverCheck = Get.find<AppController>().serverErrorCheck;
                        //       print("serverCheck---------->> $serverCheck");
                        //       if(serverCheck){
                        //         Get.offAllNamed('/serverError');
                        //       }else{
                        //         if(environmentModel.environment == "maintenance"){
                        //           Get.offAllNamed("/maintenance");
                        //         }else{
                        //           Get.offAllNamed("/bottomNavBar");
                        //         }
                        //       }
                        //     },
                        //     child: Container(
                        //       width: double.infinity,
                        //       height: double.infinity,
                        //       decoration: BoxDecoration(
                        //           color: Theme.of(context).buttonColor,
                        //           borderRadius: BorderRadius.circular(10)
                        //       ),
                        //       child: Center(
                        //         child: Text(
                        //           'Continue',
                        //           style: Theme.of(context).textTheme.button.copyWith(color: Colors.white),
                        //         ),
                        //       ),
                        //     ),
                        //   )
                        //       :Row(
                        //     children: [
                        //       InkWell(
                        //         onTap:(){
                        //           _pageController.nextPage(
                        //             duration: Duration(milliseconds: 500),
                        //             curve: Curves.ease,
                        //           );
                        //           if(_currentPage == _pageLength-1){
                        //             print("ssssssss  ${_currentPage}");
                        //           }
                        //
                        //         },
                        //         child: Container(
                        //           width: 100,
                        //           height: double.infinity,
                        //           decoration: BoxDecoration(
                        //               color: Theme.of(context).buttonColor,
                        //               borderRadius: BorderRadius.circular(10)
                        //           ),
                        //           child: Center(
                        //             child: Text(
                        //               'Next',
                        //               style: Theme.of(context).textTheme.button.copyWith(color: Colors.white),
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //       InkWell(
                        //         onTap: (){
                        //
                        //         },
                        //         child: Padding(
                        //           padding: const EdgeInsets.only(left: 11),
                        //           child: Center(
                        //             child: Text(
                        //               'Skip',
                        //               style: Theme.of(context).textTheme.button,
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //     ],
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
