import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker_gallery_camera/image_picker_gallery_camera.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class ReviewWritePage extends StatefulWidget {
  @override
  _ReviewWritePageState createState() => _ReviewWritePageState();
}

class _ReviewWritePageState extends State<ReviewWritePage> {
  bool isSwitched = false;

  List imagesList = [];
  List tempImagesList = [];

  TextEditingController reviewCommentController = TextEditingController();

  double productRating = 0;
  double sellerRating = 0;
  double deliveryBoyRating = 0;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        appBar: MyCustomAppBar(
          drawerShow: false,
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
                child: Text(
                  'GIVE REVIEW',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: customLightBlackColor),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///---product
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: customIconListTileColor,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                          child: Container(
                            height: 86,
                            decoration: BoxDecoration(
                                color:
                                    Theme.of(context).scaffoldBackgroundColor,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                ///---item-image
                                Container(
                                  height: double.infinity,
                                  width: 80,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/featuredImage.png'),
                                          fit: BoxFit.fill)),
                                ),

                                ///---item-detail
                                Expanded(
                                    child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Modern ladies casual bag',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline1
                                              .copyWith(
                                                  fontSize: 14,
                                                  color: customDarkGreyColor),
                                        ),
                                      ),
                                      Text(
                                        '\$150.00',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1
                                            .copyWith(
                                                fontSize: 14,
                                                color: customThemeColor),
                                      )
                                    ],
                                  ),
                                )),

                                ///---item-count
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 13, 16, 0),
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: Text(
                                      'x2',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4
                                          .copyWith(color: customDarkGreyColor),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),

                      ///---show-name-switch
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Don\'t Show My Name',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(
                                      fontSize: 12,
                                      color: customLightBlackColor),
                            ),
                            Switch(
                              value: isSwitched,
                              onChanged: (value) {
                                setState(() {
                                  isSwitched = value;
                                  print(isSwitched);
                                });
                              },
                              activeTrackColor: customSelectorColor,
                              activeColor: customThemeColor,
                            ),
                          ],
                        ),
                      ),

                      ///--rating-box
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
                        child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 2,
                                  blurRadius: 4,
                                )
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ///---product-rating
                                Text(
                                  'Product Rating',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(
                                          fontSize: 16,
                                          color: customDarkBlackColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 20),
                                  child: RatingBar(
                                    initialRating: productRating,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemSize: 24,
                                    ratingWidget: RatingWidget(
                                      full: SvgPicture.asset(
                                          'assets/Icons/star.svg'),
                                      half: SvgPicture.asset(
                                          'assets/Icons/star.svg'),
                                      empty: SvgPicture.asset(
                                          'assets/Icons/other_icons/star.svg'),
                                    ),
                                    itemPadding:
                                        EdgeInsets.fromLTRB(0, 10, 5, 0),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                      setState(() {
                                        productRating = rating;
                                      });
                                    },
                                  ),
                                ),

                                ///---seller-rating
                                Text(
                                  'Seller Rating',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(
                                          fontSize: 16,
                                          color: customDarkBlackColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 20),
                                  child: RatingBar(
                                    initialRating: sellerRating,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemSize: 24,
                                    ratingWidget: RatingWidget(
                                      full: SvgPicture.asset(
                                          'assets/Icons/star.svg'),
                                      half: SvgPicture.asset(
                                          'assets/Icons/star.svg'),
                                      empty: SvgPicture.asset(
                                          'assets/Icons/other_icons/star.svg'),
                                    ),
                                    itemPadding:
                                        EdgeInsets.fromLTRB(0, 10, 5, 0),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                      setState(() {
                                        sellerRating = rating;
                                      });
                                    },
                                  ),
                                ),

                                ///---delivery-boy-rating
                                Text(
                                  'Delivery Boy Rating',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(
                                          fontSize: 16,
                                          color: customDarkBlackColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 20),
                                  child: RatingBar(
                                    initialRating: deliveryBoyRating,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemSize: 24,
                                    ratingWidget: RatingWidget(
                                      full: SvgPicture.asset(
                                          'assets/Icons/star.svg'),
                                      half: SvgPicture.asset(
                                          'assets/Icons/star.svg'),
                                      empty: SvgPicture.asset(
                                          'assets/Icons/other_icons/star.svg'),
                                    ),
                                    itemPadding:
                                        EdgeInsets.fromLTRB(0, 10, 5, 0),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                      setState(() {
                                        deliveryBoyRating = rating;
                                      });
                                    },
                                  ),
                                ),

                                ///---comment
                                Text(
                                  'Write Comments',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(
                                          fontSize: 16,
                                          color: customDarkBlackColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12, bottom: 20),
                                  child: TextFormField(
                                    controller: reviewCommentController,
                                    maxLines: 4,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(color: customDarkGreyColor),
                                    decoration: InputDecoration(
                                        hintText:
                                            'tell us about your experience',
                                        hintStyle: Theme.of(context)
                                            .textTheme
                                            .subtitle2
                                            .copyWith(
                                                color: customDarkGreyColor),
                                        filled: true,
                                        fillColor: customIconListTileColor,
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                          borderSide: BorderSide(
                                              color: customThemeColor),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                        )),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Field Required';
                                      } else
                                        return null;
                                    },
                                  ),
                                ),

                                ///---image-upload
                                Text(
                                  'Upload Photos',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(
                                          fontSize: 16,
                                          color: customDarkBlackColor),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 12, 0, 20),
                                  child: Row(
                                    children: [
                                      imagesList.length != 0
                                          ? Row(
                                              children: List.generate(
                                                  imagesList.length, (index) {
                                                return InkWell(
                                                  onLongPress: () {
                                                    setState(() {
                                                      imagesList = [];
                                                      tempImagesList = [];
                                                    });
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(0, 0, 10, 0),
                                                    child: Container(
                                                      width: 70,
                                                      height: 70,
                                                      decoration: BoxDecoration(
                                                          color:
                                                              customIconListTileColor),
                                                      child: Image.file(
                                                        File(imagesList[index]
                                                            .path),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              }),
                                            )
                                          : SizedBox(),
                                      imagesList.length < 3
                                          ? InkWell(
                                              onTap: () {
                                                imagePickerDialog(context);
                                              },
                                              child: Container(
                                                  width: 70,
                                                  height: 70,
                                                  decoration: BoxDecoration(
                                                      color:
                                                          customIconListTileColor),
                                                  child: Center(
                                                    child: SvgPicture.asset(
                                                        'assets/Icons/other_icons/Group 661.svg'),
                                                  )),
                                            )
                                          : SizedBox(),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 60,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Theme.of(context).scaffoldBackgroundColor,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 5,
                blurRadius: 6,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: (){
                  setState(() {
                    isSwitched = false;
                    productRating = 0;
                    sellerRating = 0;
                    deliveryBoyRating = 0;
                    reviewCommentController.clear();
                    imagesList = [];
                    tempImagesList = [];
                  });
                },
                child: Container(
                  height: 40,
                  width: 119,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: customRatingStartColor),
                      color: Colors.white
                  ),
                  child: Center(
                    child: Text(
                      'Reset All',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                          fontSize: 15,
                          color: customRatingStartColor
                      ),
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: (){
                  Get.back();
                },
                child: Container(
                  height: 40,
                  width: 119,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: customThemeColor
                  ),
                  child: Center(
                    child: Text(
                      'Give Rating',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                          fontSize: 15,
                          color: Colors.white
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void imagePickerDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            actions: <Widget>[
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () async {
                    Navigator.pop(context);
                    setState(() {
                      tempImagesList = imagesList;
                    });
                    imagesList.add(
                        await ImagePickerGC.pickImage(
                          enableCloseButton: true,
                          context: context,
                          source: ImgSource.Camera,
                          barrierDismissible: true,)
                    );
                    if (imagesList != null) {
                      print(imagesList[0].path);
                      setState(() {});
                    }
                  },
                  child: Text(
                    "Camera",
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 18),
                  )),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () async {
                    Navigator.pop(context);
                    setState(() {
                      tempImagesList = imagesList;
                    });
                    imagesList.add(
                        await ImagePickerGC.pickImage(
                            enableCloseButton: true,
                            context: context,
                            source: ImgSource.Gallery,
                            barrierDismissible: true,)
                    );
                    if (imagesList != null) {
                      print(imagesList.map((e) => e.path).toList());
                      setState(() {});
                    }
                  },
                  child: Text(
                    "Gallery",
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 18),
                  )),
            ],
          );
        });
  }
}
