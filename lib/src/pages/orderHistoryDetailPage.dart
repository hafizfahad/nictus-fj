import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class OrderHistoryDetailPage extends StatefulWidget {
  @override
  _OrderHistoryDetailPageState createState() => _OrderHistoryDetailPageState();
}

class _OrderHistoryDetailPageState extends State<OrderHistoryDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(drawerShow: false,),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: Text(
                  'ORDER DETAIL',
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: customLightBlackColor
                  ),
                ),
              ),
              ///---page-viewer
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: Container(
                  height: 70,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Theme.of(context).scaffoldBackgroundColor,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 2,
                          blurRadius: 4,
                          offset: Offset(0,2)
                      )]
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ///---processing
                        Expanded(
                          flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                    'assets/Icons/other_icons/Group 583.svg'
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: Text(
                                    'Processing',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      fontSize: 12,
                                      color: customDarkGreyColor
                                    ),
                                  ),
                                )
                              ],
                            )
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                            child: VerticalDivider(
                              color: customSmoothGreyColor,
                              thickness: 1,
                            ),
                          ),
                        ),
                        ///---shipped
                        Expanded(
                            flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                  radius: 7,
                                  backgroundColor: customLightGreyColor,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: Text(
                                    'Shipped',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                        fontSize: 12,
                                        color: customDarkGreyColor
                                    ),
                                  ),
                                )
                              ],
                            )
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                            child: VerticalDivider(
                              color: customSmoothGreyColor,
                              thickness: 1,
                            ),
                          ),
                        ),
                        ///---delivered
                        Expanded(
                            flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                  radius: 7,
                                  backgroundColor: customLightGreyColor,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: Text(
                                    'Delivered',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                        fontSize: 12,
                                        color: customDarkGreyColor
                                    ),
                                  ),
                                )
                              ],
                            )
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              ///---order-detail
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: customIconListTileColor
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///---order-id-date
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text(
                                'Order ID: ',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              ),
                              Text(
                                'NIC-446622-25',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 12,
                                    color: customDarkGreyColor
                                ),
                              ),
                            ],
                          ),
                          Text(
                            '25/06/2021',
                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                fontSize: 12,
                                color: customDarkGreyColor
                            ),
                          )
                        ],
                      ),
                      ///---order-vendor-status
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Text(
                                  'By ',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customLightBlackColor
                                  ),
                                ),
                                Text(
                                  'Luxury house décor',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customRatingStartColor,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                                InkWell(
                                  onTap: (){},
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    child: SvgPicture.asset(
                                      'assets/Icons/Product_page_icons/fi-rr-comment-alt.svg'
                                    ),
                                  ),
                                )
                              ],
                            ),
                            InkWell(
                              onTap: (){
                                Get.toNamed(PageRoutes.reviewWrite);
                              },
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                                child: Text(
                                  'Write a Review',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customRatingStartColor,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      ///---order-items
                      SizedBox(height: 8,),
                      Wrap(
                          children: List.generate(2, (index){
                            return Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 0),
                                  child: Container(
                                    height: 86,
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).scaffoldBackgroundColor,
                                        borderRadius: BorderRadius.circular(10)
                                    ),
                                    child: Row(
                                      children: [
                                        ///---item-image
                                        Container(
                                          height: double.infinity,
                                          width: 80,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10),
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/featuredImage.png'
                                                  ),
                                                  fit: BoxFit.fill
                                              )
                                          ),
                                        ),
                                        ///---item-detail
                                        Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      'Modern ladies casual bag',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customDarkGreyColor
                                                      ),
                                                    ),
                                                  ),
                                                  Text(
                                                    '\$150.00',
                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                        fontSize: 14,
                                                        color: customThemeColor
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                        ),
                                        ///---item-count
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(20, 13, 16, 0),
                                          child: Align(
                                            alignment: Alignment.topCenter,
                                            child: Text(
                                              'x2',
                                              style: Theme.of(context).textTheme.headline4.copyWith(
                                                  color: customDarkGreyColor
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                index == 1
                                    ?SizedBox()
                                    :Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  child: Divider(
                                      color: customSmoothGreyColor
                                  ),
                                )
                              ],
                            );
                          })
                      ),

                    ],
                  ),
                ),
              ),
              ///---customer-detail
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 30, 15, 12),
                child: Text(
                  'Customer Info',
                  style: Theme.of(context).textTheme.headline1
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: customIconListTileColor
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///---customer-name
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Customer Name',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                fontSize: 12,
                                color: customLightBlackColor
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                'John Doe',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                  fontSize: 12,
                                  color: customThemeColor
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 18,),
                      ///---customer-number
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Phone Number',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                  fontSize: 12,
                                  color: customLightBlackColor
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                '+001-55554611-50',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 18,),
                      ///---customer-billing-address
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Billing Address',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                  fontSize: 12,
                                  color: customLightBlackColor
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                '658 Ashley Street Middletown, CT 06457',
                                textAlign: TextAlign.end,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 18,),
                      ///---customer-shipping-address
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Shipping Address',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                  fontSize: 12,
                                  color: customLightBlackColor
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                'Same As Billing Address',
                                textAlign: TextAlign.end,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              ///---billing-info
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 30, 15, 12),
                child: Text(
                    'Billing Info',
                    style: Theme.of(context).textTheme.headline1
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: customLightBlueColor
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                    child: Column(
                      children: [
                        ///---total
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  '\$250.00',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customThemeColor
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 18,),
                        ///---discount
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Discount',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  '\$25',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customThemeColor
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 18,),
                        ///---delivery-charges
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Delivery Charges',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  '\$25',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customThemeColor
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 18,),
                        ///---payment-method
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Payment Method',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'COD',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customThemeColor
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 18,),
                        ///---sub-total
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Subtotal',
                                style: Theme.of(context).textTheme.headline1.copyWith(
                                    fontSize: 12,
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  '\$300',
                                  style: Theme.of(context).textTheme.headline1.copyWith(
                                      fontSize: 12,
                                      color: customThemeColor
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
