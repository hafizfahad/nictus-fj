import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class ChattingPage extends StatefulWidget {
  @override
  _ChattingPageState createState() => _ChattingPageState();
}

class _ChattingPageState extends State<ChattingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        drawerShow: false,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ///---info
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 10, 15, 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Circle Clothing',
                        style: Theme.of(context).textTheme.headline1.copyWith(
                          color: customDarkBlackColor
                        ),
                      ),
                      SizedBox(height: 5,),
                      Text(
                        'Last Seen 1h Ago',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: customDarkGreyColor,
                          fontSize: 12
                        ),
                      ),
                    ],
                  ),
                  SvgPicture.asset(
                    'assets/Icons/other_icons/fi-rr-shop.svg'
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: List.generate(3, (index){
                  return index%2 == 0
                      ? Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: customIconListTileColor
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(14, 11, 14, 11),
                                  child: Text(
                                    'Nam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor.',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      fontSize: 12,
                                      color: customLightBlackColor
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 60,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '09.38',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  fontSize: 10,
                                  color: customDividerGreyColor
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                      :Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 16),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 60,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '09.38',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 10,
                                    color: customDividerGreyColor
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: customThemeColor
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(14, 11, 14, 11),
                                  child: Text(
                                    'Nam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor.',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                        fontSize: 12,
                                        color: Colors.white
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          height: 69,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Theme.of(context).scaffoldBackgroundColor,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 5,
                blurRadius: 6,
              ),
            ],
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 14, 15, 14),
              child: Container(
                color: Color(0xffF9F9F9),
                child: Padding(
                  padding: const EdgeInsets.only(left: 0),
                  child: Theme(
                    data: ThemeData(
                        primaryColor: Color(0xff363636)
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 5, 10),
                          child: SvgPicture.asset(
                            'assets/Icons/other_icons/emoji-smile.svg'
                          ),
                        ),
                          suffixIcon: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween, // added line
                              mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 5, 10),
                                child: SvgPicture.asset(
                                    'assets/Icons/other_icons/fi-rr-add.svg'
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                child: SvgPicture.asset(
                                  'assets/Icons/other_icons/fi-rr-paper-plane.svg'
                                ),
                              ),
                            ],
                          ),
                          border: InputBorder.none,
                          hintText: 'Type message here',
                          hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
