import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/repository/loginRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool obscureText = true;

  GlobalKey<FormState> _loginFromKey = GlobalKey();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        ///---keyboard-close
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: GetBuilder<AppController>(
        init: AppController(),
        builder:(appController)=> ModalProgressHUD(
          progressIndicator: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
          ),
          inAsyncCall: appController.loaderCheck,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: SafeArea(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: Form(
                            key: _loginFromKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .04,
                                ),
                                Text(
                                  'LOGIN',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .01,
                                ),
                                Text(
                                  'With Your Existing Account',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle2
                                      .copyWith(color: customLightBlackColor),
                                ),

                                ///---email-section
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .07,
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Email',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5
                                          .copyWith(color: customDarkBlackColor),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: TextFormField(
                                    controller: emailController,
                                    keyboardType: TextInputType.emailAddress,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(color: customDarkGreyColor),
                                    decoration: InputDecoration(
                                      hintText: 'Enter Email',
                                      hintStyle: Theme.of(context)
                                          .textTheme
                                          .headline4
                                          .copyWith(color: customLightGreyColor),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1,
                                            color: customGreenSaleFlagColor),
                                      ),
                                      errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1, color: customRedColor),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1, color: customBorderGreyColor),
                                      ),
                                    ),
                                    validator: (value) {
                                      Pattern pattern =
                                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                      RegExp regex = new RegExp(pattern);
                                      if (value.isEmpty) {
                                        return 'Field Required';
                                      } else if (!regex.hasMatch(value))
                                        return 'Please make sure your email address is valid';
                                      else
                                        return null;
                                    },
                                  ),
                                ),

                                ///---password-section
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .05,
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Password',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5
                                          .copyWith(color: customDarkBlackColor),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: TextFormField(
                                    controller: passwordController,
                                    keyboardType: TextInputType.text,
                                    obscureText: obscureText,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(color: customDarkGreyColor),
                                    decoration: InputDecoration(
                                        hintText: 'Enter Password',
                                        hintStyle: Theme.of(context)
                                            .textTheme
                                            .headline4
                                            .copyWith(color: customLightGreyColor),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: customGreenSaleFlagColor),
                                        ),
                                        errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1, color: customRedColor),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: customBorderGreyColor),
                                        ),
                                        suffixIcon: InkWell(
                                            onTap: () {
                                              setState(() {
                                                obscureText = !obscureText;
                                              });
                                            },
                                            child: Icon(
                                              obscureText
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              size: 20,
                                              color: customLightGreyColor,
                                            ))),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Field Required";
                                      }
                                      return null;
                                    },
                                  ),
                                ),

                                ///---forgot-password
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .02,
                                ),
                                InkWell(
                                  onTap: () {
                                    Get.toNamed(PageRoutes.resetSelection);
                                  },
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Text(
                                        'Forgot Password?',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4
                                            .copyWith(
                                                color: customThemeColor,
                                                decoration:
                                                    TextDecoration.underline),
                                      ),
                                    ),
                                  ),
                                ),

                                ///---login-button
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .08,
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: InkWell(
                                    onTap: (){
                                      ///---keyboard-close
                                      FocusScopeNode currentFocus = FocusScope.of(context);
                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }

                                      ///
                                      if(_loginFromKey.currentState.validate()){

                                        ///loader
                                        Get.find<AppController>().changeLoaderCheck(true);

                                        ///post-method
                                        postMethod(
                                          context,
                                          loginApi,
                                          {
                                            'email': emailController.text,
                                            'password': passwordController.text
                                          },
                                          false,
                                          loginDataRepo
                                        );
                                      }

                                    },
                                    child: Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width * .75,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                          color: customThemeColor),
                                      child: Center(
                                        child: Text(
                                          'Login',
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2
                                              .copyWith(
                                                  fontSize: 15, color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .05,
                                ),
                                Text(
                                  'Or Login With',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      .copyWith(
                                          fontSize: 12,
                                          color: customLightBlackColor),
                                ),

                                ///---social-buttons
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .02,
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    height: 40,
                                    width: MediaQuery.of(context).size.width * .75,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: customRedColor),
                                        color: Colors.white),
                                    child: Center(
                                      child: Text(
                                        'Google',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2
                                            .copyWith(
                                                fontSize: 15,
                                                color: customRedColor),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: MediaQuery.of(context).size.height * .02,
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    height: 40,
                                    width: MediaQuery.of(context).size.width * .75,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: customBlueColor),
                                        color: Colors.white),
                                    child: Center(
                                      child: Text(
                                        'Facebook',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2
                                            .copyWith(
                                                fontSize: 15,
                                                color: customBlueColor),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Don\'t have an account?',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .copyWith(color: customDarkBlackColor),
                            ),
                            InkWell(
                              onTap: () {
                                Get.toNamed(PageRoutes.signUp);
                              },
                              child: Text(
                                ' SignUp',
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
