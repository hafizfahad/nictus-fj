import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/customGeneralBottomBar.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:intl/intl.dart';

class EditUserProfilePage extends StatefulWidget {
  @override
  _EditUserProfilePageState createState() => _EditUserProfilePageState();
}

class _EditUserProfilePageState extends State<EditUserProfilePage> {

  final format = DateFormat("yyyy-MM-dd");
  DateTime selectedDate = DateTime.now();

  TextEditingController _dateController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateController.text = DateFormat.yMd().format(DateTime.now());
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        appBar: MyCustomAppBar(
          drawerShow: false,
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: Text(
                  'EDIT PROFILE',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: customLightBlackColor),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 6,
                                offset: Offset(0,3),
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 15, 0),
                            child: Column(
                              children: [
                                ///--edit-general-info
                                ListTile(
                                  onTap: (){
                                    bottomSheetForEditGeneralInfo(context);
                                  },
                                  contentPadding:EdgeInsets.all(0),
                                  leading: SvgPicture.asset(
                                      'assets/Icons/other_icons/General.svg'
                                  ),
                                  title: Text(
                                    'Edit General Info',
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                        color: customDarkGreyColor
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 1,
                                  width: double.infinity,
                                  color: customSmoothGreyColor,
                                ),
                                ///--edit-address
                                ListTile(
                                  contentPadding:EdgeInsets.all(0),
                                  leading: SvgPicture.asset(
                                      'assets/Icons/other_icons/Home.svg'
                                  ),
                                  title: Text(
                                    'Edit Address',
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                        color: customDarkGreyColor
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 1,
                                  width: double.infinity,
                                  color: customSmoothGreyColor,
                                ),
                                ///--change-email
                                ListTile(
                                  contentPadding:EdgeInsets.all(0),
                                  leading: SvgPicture.asset(
                                      'assets/Icons/other_icons/Email.svg'
                                  ),
                                  title: Text(
                                    'Change Email',
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                        color: customDarkGreyColor
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 1,
                                  width: double.infinity,
                                  color: customSmoothGreyColor,
                                ),
                                ///--change-password
                                ListTile(
                                  contentPadding:EdgeInsets.all(0),
                                  leading: SvgPicture.asset(
                                      'assets/Icons/other_icons/Password.svg'
                                  ),
                                  title: Text(
                                    'Change Password',
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                        color: customDarkGreyColor
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String selectedGender = '';
  List<String> genderList = [
    'Male','Female','Other'
  ];
  Future<Widget> bottomSheetForEditGeneralInfo(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context){
          return StatefulBuilder(
            builder:(context,state)=> GetBuilder<HomeController>(
              init: HomeController(),
              builder:(homeController)=> Container(
                  height: MediaQuery.of(context).size.height * 0.7,
                  decoration: BoxDecoration(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 25, 15, 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ///---name-field
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Full Name',
                                    style: Theme.of(context).textTheme.headline5.copyWith(
                                      color: customDarkBlackColor,
                                      fontSize: 16
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                child: TextFormField(
                                  keyboardType: TextInputType.emailAddress,
                                  style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                                  decoration: InputDecoration(
                                    hintText: 'Jhon Doe',
                                    hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(width: 1, color: customThemeColor),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(width: 1, color: customRedColor),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Field Required';
                                    } else
                                      return null;
                                  },
                                ),
                              ),
                              ///---gender-field
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15, 28, 0, 0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Gender',
                                    style: Theme.of(context).textTheme.headline5.copyWith(
                                        color: customDarkBlackColor,
                                        fontSize: 16
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButtonFormField<String>(
                                    decoration:  InputDecoration(
                                      hintText: 'Male',
                                      hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 0.0, horizontal: 0.0),
                                      fillColor: Colors.white,
                                      filled: true,
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(width: 1, color: customThemeColor),
                                      ),
                                      errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(width: 1, color: customRedColor),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                      ),
                                    ),
                                    isDense: true,
                                    isExpanded: true,
                                    focusColor: customThemeColor,
                                    icon: Icon(Icons.keyboard_arrow_down_outlined),
                                    iconSize: 20,
                                    style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                                    iconEnabledColor: Colors.black,
                                    items: genderList
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style:
                                              Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                                            ),
                                          );
                                        }).toList(),
                                    onChanged: (String value) {
                                      setState(() {
                                        selectedGender = value;
                                      });
                                    },
                                    validator: (String value) {
                                      if (value == null) {
                                        return 'Field Required';
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              ///---date-of-birth-field
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15, 28, 0, 0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Date Of Birth',
                                    style: Theme.of(context).textTheme.headline5.copyWith(
                                        color: customDarkBlackColor,
                                        fontSize: 16
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () async{
                                  final DateTime picked = await showDatePicker(
                                    context: context,
                                    initialDate: selectedDate, // Refer step 1
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2025),
                                  );
                                  if (picked != null && picked != selectedDate)
                                    setState(() {
                                      selectedDate = picked;
                                      _dateController.text = DateFormat.yMd().format(selectedDate);
                                    });
                                  },
                                child: Container(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                    child: TextFormField(
                                      enabled: false,
                                      controller: _dateController,
                                      style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                                      decoration: InputDecoration(
                                        suffix: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                                          child: SizedBox(
                                            height: 13,
                                            width: 13,
                                            child: SvgPicture.asset(
                                              'assets/Icons/other_icons/fi-rr-calendar2.svg',
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        hintText: 'Jhon Doe',
                                        hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(width: 1, color: customThemeColor),
                                        ),
                                        disabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                        ),
                                        errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(width: 1, color: customRedColor),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                        ),
                                      ),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'Field Required';
                                        } else
                                          return null;
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child:
                          InkWell(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: CustomGeneralBottomBar(
                                text: 'Update',
                              )
                          ),
                        ),
                      )
                    ],
                  )),
            ),
          );
        }
    );
  }

}
