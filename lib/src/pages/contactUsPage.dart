import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {

  Completer<GoogleMapController> _controller = Completer();

  LatLng center;

  final Set<Marker> _markers = {};


  MapType _currentMapType = MapType.normal;


  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(
          Marker(
            // This marker id can be anything that uniquely identifies each marker.
            markerId: MarkerId(
                center.toString()
            ),
            position: center,
            infoWindow: InfoWindow(
              title: 'INVICTUS',
              // snippet: '...',
            ),
            icon: BitmapDescriptor.defaultMarker,
          )
      );
    });
  }

  void _onCameraMove(CameraPosition position) {
    center = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    center = LatLng(31.408478, 73.116077);
    _onAddMarkerButtonPressed();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Stack(
          children: [
            ///---background
            Container(
              height: MediaQuery.of(context).size.height*.4,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          'assets/images/userProfileBackground-2.png'
                      ),
                      fit: BoxFit.fill
                  )
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              color: Colors.transparent,
              child: Column(
                children: [
                  MyCustomAppBar(drawerShow: false,transparent: true,),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child: Column(
                          children: [
                            ///---google-map
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                              child: Container(
                                height: 200,
                                width: double.infinity,
                                child: GoogleMap(
                                  onMapCreated: _onMapCreated,
                                  initialCameraPosition: CameraPosition(
                                    target: center,
                                    zoom: 15.0,
                                  ),
                                  mapType: _currentMapType,
                                  markers: _markers,
                                  onCameraMove: _onCameraMove,

                                ),
                              ),
                            ),
                            ///---contact-choice
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                              child: Row(
                                children: [
                                  ///---contact-email
                                  Expanded(
                                    child: Container(
                                      height: 106,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: customThemeColor,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.2),
                                            spreadRadius: 2,
                                            blurRadius: 6,
                                            offset: Offset(0,3),
                                          ),
                                        ],
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 102,
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(10),
                                                color: Colors.white
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  SvgPicture.asset(
                                                    'assets/Icons/other_icons/Group 386.svg'
                                                  ),
                                                  Text(
                                                    'support@nictus.com',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                      fontSize: 12,
                                                      color: customDarkGreyColor
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  ///---contact-number
                                  Expanded(
                                    child: Container(
                                      height: 106,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: customThemeColor,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.2),
                                            spreadRadius: 2,
                                            blurRadius: 6,
                                            offset: Offset(0,3),
                                          ),
                                        ],
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 102,
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(10),
                                                color: Colors.white
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  SvgPicture.asset(
                                                      'assets/Icons/other_icons/mic.svg'
                                                  ),
                                                  Text(
                                                    '+001-25467-205',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customDarkGreyColor
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                              child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 2,
                                      blurRadius: 6,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 18, 20, 0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      ///---name-field
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            'Full Name',
                                            style: Theme.of(context).textTheme.headline5.copyWith(
                                                color: customDarkBlackColor,
                                                fontSize: 16
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                        child: TextFormField(
                                          keyboardType: TextInputType.emailAddress,
                                          style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                                          decoration: InputDecoration(
                                            hintText: 'Enter Full Name',
                                            hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(width: 1, color: customThemeColor),
                                            ),
                                            errorBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(width: 1, color: customRedColor),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                            ),
                                          ),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Field Required';
                                            } else
                                              return null;
                                          },
                                        ),
                                      ),
                                      ///---email-field
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 28, 0, 0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            'Email',
                                            style: Theme.of(context).textTheme.headline5.copyWith(
                                                color: customDarkBlackColor,
                                                fontSize: 16
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 28),
                                        child: TextFormField(
                                          keyboardType: TextInputType.emailAddress,
                                          style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                                          decoration: InputDecoration(
                                            hintText: 'Enter Email',
                                            hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(width: 1, color: customThemeColor),
                                            ),
                                            errorBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(width: 1, color: customRedColor),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                            ),
                                          ),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Field Required';
                                            } else
                                              return null;
                                          },
                                        ),
                                      ),
                                      ///---query-message
                                      Text(
                                        'Write Message',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .copyWith(
                                            fontSize: 16,
                                            color: customDarkBlackColor),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 12, bottom: 20),
                                        child: TextFormField(
                                          maxLines: 4,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4
                                              .copyWith(color: customDarkGreyColor),
                                          decoration: InputDecoration(
                                              hintText:
                                              'tell us about your query',
                                              hintStyle: Theme.of(context)
                                                  .textTheme
                                                  .subtitle2
                                                  .copyWith(
                                                  color: customDarkGreyColor),
                                              filled: true,
                                              fillColor: customIconListTileColor,
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: BorderSide(
                                                    color: customThemeColor),
                                              ),
                                              errorBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide:
                                                BorderSide(color: Colors.white),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide:
                                                BorderSide(color: Colors.white),
                                              )),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Field Required';
                                            } else
                                              return null;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 6,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: (){
                Get.back();
              },
              child: Container(
                height: 40,
                width: 119,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: customThemeColor
                ),
                child: Center(
                  child: Text(
                    'Send Query',
                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                        fontSize: 15,
                        color: Colors.white
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
