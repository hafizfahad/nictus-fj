import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class ChatsPage extends StatefulWidget {
  @override
  _ChatsPageState createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(drawerShow: false,),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ///---title
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
              child: Text(
                'CHATS',
                style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: customLightBlackColor
                ),
              ),
            ),
            ///---search-bar
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 20, 15, 10),
              child: Container(
                color: Color(0xffF9F9F9),
                child: Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Theme(
                    data: ThemeData(
                        primaryColor: Color(0xff363636)
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                          suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                          border: InputBorder.none,
                          hintText: 'Search Order',
                          hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                      ),
                    ),
                  ),
                ),
              ),
            ),
            ///---chats
            Expanded(
              child: ListView(
                children: List.generate(8, (index){
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: InkWell(
                      onTap: (){
                        Get.toNamed(PageRoutes.chatting);
                      },
                      child: Container(
                        height: 74,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 6,
                                offset: Offset(0,3),
                              ),
                            ],
                          ),
                        child: Center(
                          child: ListTile(
                            leading: SvgPicture.asset(
                              'assets/Icons/other_icons/General.svg'
                            ),
                            title: Text(
                              'Circle Clothing',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                  color: customDarkGreyColor
                              ),
                            ),
                            subtitle: Text(
                              'Today',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                fontSize: 12,
                                  color: customDarkGreyColor
                              ),
                            ),
                            trailing: index%3 == 0
                                ?CircleAvatar(
                              radius: 10,
                              backgroundColor: customGreenSaleFlagColor,
                              child: Text(
                                '$index',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  fontSize: 10,
                                  color: Colors.white
                                ),
                              ),
                            )
                            :SizedBox(),
                          )
                        )
                      ),
                    ),
                  );
                }),
              )
            )
          ],
        ),
      ),
    );
  }
}
