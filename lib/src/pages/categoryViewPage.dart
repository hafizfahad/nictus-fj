import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

import 'homeModules/home1/widgets/homeDrawerComponent.dart';

class CategoryViewPage extends StatefulWidget {
  final index;
  CategoryViewPage({Key key, this.index}) : super(key: key);

  @override
  _CategoryViewPageState createState() => _CategoryViewPageState();
}

class _CategoryViewPageState extends State<CategoryViewPage> {
  /// categories page1
  Widget categoryView1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ///------------------------search-bar
        _searchBar(context),

        ///------------------------title
        _titleWidget(context),

        ///------------------------title
        _categoryRouteWidget(context),

        ///------------------------title
        _listDataWidget(
          context,
        ),
      ],
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    subcategoryIndex = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      drawer: MyCustomDrawer(),
      appBar: MyCustomAppBar(),
      body: SafeArea(
          child: SizedBox(
              height: double.infinity,
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                child: categoryView1(),
              ))),
    );
  }

  ///--------------search-bar-widget
  Widget _searchBar(BuildContext context) {
    return Container(
      // height: 40,
      color: Color(0xffF9F9F9),
      child: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Theme(
          data: ThemeData(primaryColor: Color(0xff363636)),
          child: Material(
            child: TextFormField(
              decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  border: InputBorder.none,
                  hintText: 'search here',
                  hintStyle: Theme.of(context).textTheme.subtitle2),
            ),
          ),
        ),
      ),
    );
  }

  ///--------------title-widget
  Widget _titleWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 25, 0, 20),
      child: Text(
        'Categories',
        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 23),
      ),
    );
  }

  ///--------------Category Route-widget
  Widget _categoryRouteWidget(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 3,
              blurRadius: 2,
            )
          ]),
      height: 70,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 12, 5, 12),
        child: Row(
          children: [
            // -----All Categories Route
            Flexible(
              child: InkWell(
                onTap: () {
                  Get.back();
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: SvgPicture.asset(
                    'assets/Icons/fi-rr-apps.svg',
                    color: customLightGreyColor,
                  ),
                ),
              ),
            ),

            VerticalDivider(
              thickness: 1,
            ),

            // -----Selected Category
            SizedBox(
              width: 80,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: InkWell(
                  onTap: () {
                    if (subcategoryIndex != null) subcategoryIndex = null;
                    setState(() {});
                  },
                  child: Text(
                    "${getAllCategoriesModel.data[widget.index].name}",
                    style: Theme.of(context).textTheme.button.copyWith(
                          color: subcategoryIndex == null
                              ? customThemeColor
                              : customDarkGreyColor,
                        ),
                    maxLines: 2,
                  ),
                ),
              ),
            ),

            VerticalDivider(
              thickness: 1,
            ),

            // -----Selected Sub Category
            if (subcategoryIndex != null)
              SizedBox(
                width: 80,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    "${getAllCategoriesModel.data[widget.index].childrens[subcategoryIndex].name}",
                    style: Theme.of(context)
                        .textTheme
                        .button
                        .copyWith(color: customThemeColor),
                    maxLines: 2,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  ///--------------List Data-widget
  Widget _listDataWidget(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 25),
        child: ListView(
            children: subcategoryIndex == null
                ? List.generate(
                    getAllCategoriesModel.data[widget.index].childrens.length,
                    (index) => Padding(
                          padding: const EdgeInsets.only(bottom: 15),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    left: BorderSide(
                                        color: customThemeColor, width: 8))),
                            child: ListTile(
                              onTap: () {
                                subcategoryIndex = index;
                                setState(() {});
                              },
                              trailing: Icon(
                                Icons.arrow_forward_ios_rounded,
                                size: 18,
                                color: customThemeColor,
                              ),
                              tileColor: customIconListTileColor,
                              title: Text(
                                getAllCategoriesModel
                                    .data[widget.index].childrens[index].name,
                                style: Theme.of(context).textTheme.subtitle2,
                              ),
                            ),
                          ),
                        ))
                : List.generate(
                    getAllCategoriesModel.data[widget.index]
                        .childrens[subcategoryIndex].childrens.length,
                    (index) => Padding(
                          padding: const EdgeInsets.only(bottom: 15),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    left: BorderSide(
                                        color: customThemeColor, width: 8))),
                            child: ListTile(
                              onTap: () {
                                subcategoryIndex = index;
                                setState(() {});
                              },
                              trailing: Icon(
                                Icons.arrow_forward_ios_rounded,
                                size: 18,
                                color: customThemeColor,
                              ),
                              tileColor: customIconListTileColor,
                              title: Text(
                                getAllCategoriesModel
                                    .data[widget.index]
                                    .childrens[subcategoryIndex]
                                    .childrens[index]
                                    .name,
                                style: Theme.of(context).textTheme.subtitle2,
                              ),
                            ),
                          ),
                        ))),
      ),
    );
  }

  int subcategoryIndex;
}
