import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class OrderConfirmationScreen extends StatefulWidget {
  @override
  _OrderConfirmationScreenState createState() => _OrderConfirmationScreenState();
}

class _OrderConfirmationScreenState extends State<OrderConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(drawerShow: false,),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: Text(
                  'PAYMENT',
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: customLightBlackColor
                  ),
                ),
              ),
              ///---page-viewer
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: Container(
                  height: 70,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Theme.of(context).scaffoldBackgroundColor,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 2,
                          blurRadius: 4,
                          offset: Offset(0,2)
                      )]
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ///-cart-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/Icons/Product_page_icons/shopping-cart.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 28,
                                child: SvgPicture.asset(
                                    'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-checkout-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/Icons/cart_checkout_icons/fi-rr-document.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 25,
                                child: SvgPicture.asset(
                                    'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-payment-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/Icons/cart_checkout_icons/fi-rr-credit-card.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 30,
                                child: SvgPicture.asset(
                                    'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-confirmation-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                        'assets/Icons/cart_checkout_icons/fi-rr-file-check.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 25,
                                child: SvgPicture.asset(
                                    'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                    ],
                  ),
                ),
              ),
              ///---page-view
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height*.05,
                    ),
                    Center(
                      child: Text(
                        'ORDER CONFIRMED',
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: customGreenSaleFlagColor
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        'Thank you for shopping with us',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: customLightBlackColor
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*.03,
                    ),
                    SvgPicture.asset(
                      'assets/Icons/cart_checkout_icons/checked.svg'
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    SvgPicture.asset(
                      'assets/Icons/cart_checkout_icons/Group 593.svg'
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*.1,
                    ),
                    InkWell(
                      onTap: (){
                        Get.offAllNamed(PageRoutes.bottomNavBar);
                      },
                      child: Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width*.75,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: customRatingStartColor),
                            color: Colors.white
                        ),
                        child: Center(
                          child: Text(
                            'Continue Shopping',
                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                fontSize: 15,
                                color: customRatingStartColor
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*.1,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Your order number is: ',
                          style: Theme.of(context).textTheme.headline5.copyWith(
                            color: customDarkBlackColor
                          ),
                        ),
                        Text(
                          'MV-45421-2',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
