import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/customGeneralBottomBar.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/pages/orderConfirmationScreen.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {


  bool selectedType1 = false;
  bool selectedType2 = false;
  bool selectedType3 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(drawerShow: false,),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: Text(
                  'PAYMENT',
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: customLightBlackColor
                  ),
                ),
              ),
              ///---page-viewer
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: Container(
                  height: 70,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Theme.of(context).scaffoldBackgroundColor,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 2,
                          blurRadius: 4,
                          offset: Offset(0,2)
                      )]
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ///-cart-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/Icons/Product_page_icons/shopping-cart.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 28,
                                child: SvgPicture.asset(
                                    'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-checkout-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/Icons/cart_checkout_icons/fi-rr-document.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 25,
                                child: SvgPicture.asset(
                                    'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-payment-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-credit-card.svg',
                            color: customThemeColor,
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-confirmation-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-file-check.svg'
                          )
                      ),
                    ],
                  ),
                ),
              ),
              ///---page-view
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: (){
                        setState(() {
                          selectedType1 = true;
                          selectedType2 = false;
                          selectedType3 = false;
                        });
                      },
                      child: Container(
                        height: 74,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: selectedType1
                              ?customThemeColor
                              :Colors.white,
                          borderRadius: BorderRadius.circular(5),
                            boxShadow: [BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 2,
                                blurRadius: 4,
                                offset: Offset(0,2)
                            )]
                        ),
                        child: Center(
                          child: ListTile(
                            leading: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/Group 387.svg',
                              color: selectedType1
                                  ?Colors.white
                                  :customThemeColor,
                            ),
                            title: Text(
                              'Cash On Delivery',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                color: selectedType1
                                    ?Colors.white
                                    :customDarkGreyColor
                              ),
                            ),
                            trailing: selectedType1
                                ?SvgPicture.asset(
                                'assets/Icons/cart_checkout_icons/Group 589.svg'
                            )
                                :SizedBox(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 18,),
                    InkWell(
                      onTap: (){
                        setState(() {
                          selectedType1 = false;
                          selectedType2 = true;
                          selectedType3 = false;
                        });
                      },
                      child: Container(
                        height: 74,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: selectedType2
                                ?customThemeColor
                                :Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 2,
                                blurRadius: 4,
                                offset: Offset(0,2)
                            )]
                        ),
                        child: Center(
                          child: ListTile(
                            leading: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/Group 385.svg',
                              color: selectedType2
                                  ?Colors.white
                                  :customThemeColor,
                            ),
                            title: Text(
                              'Credit Card',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                  color: selectedType2
                                      ?Colors.white
                                      :customDarkGreyColor
                              ),
                            ),
                            trailing: selectedType2
                                ?SvgPicture.asset(
                                'assets/Icons/cart_checkout_icons/Group 589.svg'
                            )
                                :SizedBox(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 18,),
                    InkWell(
                      onTap: (){
                        setState(() {
                          selectedType1 = false;
                          selectedType2 = false;
                          selectedType3 = true;
                        });
                      },
                      child: Container(
                        height: 74,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: selectedType3
                                ?customThemeColor
                                :Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 2,
                                blurRadius: 4,
                                offset: Offset(0,2)
                            )]
                        ),
                        child: Center(
                          child: ListTile(
                            leading: selectedType3
                                ?SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/paypal-white.svg',
                            )
                                :SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/Group 386.svg',
                            ),
                            title: Text(
                              'PayPal',
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                  color: selectedType3
                                      ?Colors.white
                                      :customDarkGreyColor
                              ),
                            ),
                            trailing: selectedType3
                                ?SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/Group 589.svg'
                            )
                                :SizedBox(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                  ],
                ),
              ),
              ///---voucher-text-field
              SizedBox(
                height: 85,
                width: double.infinity,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        child: TextFormField(
                          style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                              color: customLightBlackColor
                          ),
                          controller: voucherController,
                          decoration: InputDecoration(
                              hintText: 'Enter Voucher Code (If Any)',
                              hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                                  fontSize: 12,
                                  color: customLightGreyColor
                              ),
                              isDense: true,
                              contentPadding:const EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                borderSide: BorderSide(color: customThemeColor),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                borderSide: BorderSide(color: customSmoothGreyColor),),
                              suffixIcon: InkWell(
                                onTap: (){},
                                child: Container(
                                  height: 45,
                                  width: 93,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: customRatingStartColor
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Apply',
                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                          fontSize: 15,
                                          color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ///---bill-detail
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                        top: Radius.circular(15)
                    ),
                    color: customIconListTileColor
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Billing',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 18),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$250.00',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Discount',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$25',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Delivery Charges',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$25',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14,bottom: 50),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.headline1.copyWith(
                                    color: customThemeColor,
                                    fontSize: 14
                                ),
                              ),
                            ),
                            Text(
                              '\$300',
                              style: Theme.of(context).textTheme.headline1.copyWith(
                                  color: customThemeColor,
                                  fontSize: 14
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: InkWell(
          onTap: (){
            Get.toNamed(PageRoutes.orderConfirmation);
          },
          child: CustomGeneralBottomBar(
            text: 'Confirm Order',
          )
      ),
    );
  }
}