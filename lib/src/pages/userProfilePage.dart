import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/homeDrawerComponent.dart';
import 'package:multi_vendor_customer/src/repository/logoutRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class UserProfilePage extends StatefulWidget {
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(
      init: AppController(),
      builder:(appController)=> ModalProgressHUD(
        progressIndicator: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
        ),
        inAsyncCall: appController.loaderCheck,
        child: Scaffold(
          drawer: MyCustomDrawer(),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Theme.of(context).scaffoldBackgroundColor,
            child: Stack(
              children: [
                ///---background
                Container(
                  height: MediaQuery.of(context).size.height*.4,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/images/userProfileBackground-2.png'
                      ),
                      fit: BoxFit.fill
                    )
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.transparent,
                  child: Column(
                    children: [
                      MyCustomAppBar(drawerShow: true,transparent: true,),
                      currentUserModel == null
                          ?Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 70, 0, 0),
                            child: Center(
                              child: Container(
                                height: 300,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(300),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/not-logged in.jpg'
                                        ),
                                        fit: BoxFit.fill
                                    )
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                            child: InkWell(
                              onTap: (){
                                Get.toNamed(PageRoutes.login);
                              },
                              child: Container(
                                height: 40,
                                width: MediaQuery.of(context).size.width * .55,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: customThemeColor),
                                child: Center(
                                  child: Text(
                                    'Lets Login',
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle2
                                        .copyWith(
                                        fontSize: 15, color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                          :Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                            child: Column(
                              children: [
                                ListTile(
                                  contentPadding:EdgeInsets.all(0),
                                  leading: currentUserModel.profileImagePath != null
                                      ? Container(
                                    height: 60,
                                    width: 60,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(30),
                                      image: DecorationImage(
                                        image: NetworkImage(
                                          '${baseUrl+currentUserModel.profileImagePath}'
                                        ),
                                        fit: BoxFit.fill
                                      )
                                    ),
                                  )
                                      :CircleAvatar(
                                    radius: 30,
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.person,
                                      size: 40,
                                      color: customThemeColor,
                                    ),
                                  ),
                                  title: Text(
                                    '${currentUserModel.name}',
                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                      fontSize: 14,
                                      color: Colors.white
                                    ),
                                  ),
                                  subtitle: Text(
                                    '${currentUserModel.email}',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      fontSize: 12,
                                      color: Colors.white
                                    ),
                                  ),
                                  trailing: InkWell(
                                    onTap: (){
                                      Get.toNamed(PageRoutes.editUserProfile);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                      child: SvgPicture.asset(
                                        'assets/Icons/userProfileIcons/fi-rr-edit.svg'
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15,),
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 2,
                                        blurRadius: 6,
                                        offset: Offset(0,3),
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(20, 0, 15, 0),
                                    child: Column(
                                      children: [
                                        ///--my-orders
                                        ListTile(
                                          onTap: (){
                                            Get.toNamed(PageRoutes.orderHistory);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                            'assets/Icons/userProfileIcons/Group 385.svg'
                                          ),
                                          title: Text(
                                            'My Orders',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                              color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--my-wallet
                                        ListTile(
                                          onTap: (){
                                            Get.toNamed(PageRoutes.wallet);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/Wallet.svg'
                                          ),
                                          title: Text(
                                            'My Wallet',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--payment-method
                                        ListTile(
                                          onTap: (){
                                            Get.toNamed(PageRoutes.paymentMethods);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/payment.svg'
                                          ),
                                          title: Text(
                                            'Payment Methods',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--reviews
                                        ListTile(
                                          onTap: (){
                                            Get.toNamed(PageRoutes.reviews);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/Reviews.svg'
                                          ),
                                          title: Text(
                                            'Reviews',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--chats
                                        ListTile(
                                          onTap: (){
                                            Get.toNamed(PageRoutes.chats);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/Chats.svg'
                                          ),
                                          title: Text(
                                            'Chats',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15,),
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 2,
                                        blurRadius: 6,
                                        offset: Offset(0,3),
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(20, 0, 15, 0),
                                    child: Column(
                                      children: [
                                        ///--contact-us
                                        ListTile(
                                          onTap: (){
                                            Get.toNamed(PageRoutes.contactUs);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/other_icons/Group 386.svg'
                                          ),
                                          title: Text(
                                            'Contact Us',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--terms-conditions
                                        ListTile(
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/Terms.svg'
                                          ),
                                          title: Text(
                                            'Terms & Conditions',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--privacy
                                        ListTile(
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/Privacy.svg'
                                          ),
                                          title: Text(
                                            'Privacy Policy',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 1,
                                          width: double.infinity,
                                          color: customSmoothGreyColor,
                                        ),
                                        ///--logout
                                        ListTile(
                                          onTap: (){
                                            Get.find<AppController>().changeLoaderCheck(true);
                                            getMethod(
                                                context,
                                                '$logOutApi',
                                                null,
                                                true,
                                                logoutDataRepo);
                                          },
                                          contentPadding:EdgeInsets.all(0),
                                          leading: SvgPicture.asset(
                                              'assets/Icons/userProfileIcons/Logout.svg'
                                          ),
                                          title: Text(
                                            'Logout',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                color: customDarkGreyColor
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 25,),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
