import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/linearProgressBar.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:progress_indicator/progress_indicator.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class VendorStorePage extends StatefulWidget {
  @override
  _VendorStorePageState createState() => _VendorStorePageState();
}

class _VendorStorePageState extends State<VendorStorePage>
    with SingleTickerProviderStateMixin {
  TabController productDetailPageTabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    productDetailPageTabController = new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (homeController) => Scaffold(
        // Persistent AppBar that never scrolls
        appBar: MyCustomAppBar(
          drawerShow: false,
        ),
        body: NestedScrollView(
          // allows you to build a list of elements that would be scrolled away till the body reached the top
          headerSliverBuilder: (context, _) {
            return [
              SliverList(
                delegate: SliverChildListDelegate([
                  ///---search-bar
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 12),
                    child: Container(
                      color: Color(0xffF9F9F9),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Theme(
                          data: ThemeData(primaryColor: Color(0xff363636)),
                          child: TextFormField(
                            decoration: InputDecoration(
                                suffixIcon: Icon(
                                  Icons.search,
                                  color: customLightGreyColor,
                                ),
                                border: InputBorder.none,
                                hintText: 'Search within store',
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(color: customLightGreyColor)),
                          ),
                        ),
                      ),
                    ),
                  ),

                  ///---store-widget
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: Center(
                          child: !homeController.getVendorStoreDetailDataCheck
                              ? SkeletonLoader(
                                  period: Duration(seconds: 2),
                                  highlightColor: Colors.grey,
                                  direction: SkeletonDirection.ltr,
                                  builder: Container(
                                    height: 241,
                                    width: double.infinity,
                                    color: Colors.white,
                                  ))
                              : Container(
                                  height: 241,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: customVendorCardColor,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 1,
                                        blurRadius: 1,
                                        offset: Offset(0, 1),
                                      ),
                                    ],
                                  ),
                                  child: Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          getVendorStoreDetailModel.data.vendor
                                                      .store.coverImage ==
                                                  null
                                              ? Image.asset(
                                                  'assets/images/product_detail_vendor_cover.png',
                                                  fit: BoxFit.cover,
                                                  width: double.infinity,
                                                  height: 80,
                                                )
                                              : Image.network(
                                                  '${baseUrl + getVendorStoreDetailModel.data.vendor.store.coverImage.originalMediaPath}',
                                                  fit: BoxFit.cover,
                                                  width: double.infinity,
                                                  height: 80,
                                                ),
                                          SizedBox(
                                            height: 28,
                                          ),

                                          ///------vendor-name
                                          Center(
                                            child: Text(
                                              '${getVendorStoreDetailModel.data.vendor.store.name}',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline1
                                                  .copyWith(
                                                      color:
                                                          customLightBlackColor,
                                                      fontSize: 14),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 2,
                                          ),

                                          ///------vendor-follow
                                          Center(
                                            child: Text(
                                              'Follow',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline4
                                                  .copyWith(
                                                      decoration: TextDecoration
                                                          .underline,
                                                      color: customThemeColor),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                18, 15, 18, 0),
                                            child: SizedBox(
                                              height: 54,
                                              width: double.infinity,
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                      child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        '91%',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline1
                                                            .copyWith(
                                                                fontSize: 16,
                                                                color:
                                                                    customThemeColor),
                                                      ),
                                                      Text(
                                                        'Positive Seller\nRatings',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .subtitle2
                                                            .copyWith(
                                                                fontSize: 10,
                                                                color:
                                                                    customLightGreyColor),
                                                      ),
                                                    ],
                                                  )),
                                                  Expanded(
                                                      child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        '84%',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline1
                                                            .copyWith(
                                                                fontSize: 16,
                                                                color:
                                                                    customThemeColor),
                                                      ),
                                                      Text(
                                                        'Ship On Time',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .subtitle2
                                                            .copyWith(
                                                                fontSize: 10,
                                                                color:
                                                                    customLightGreyColor),
                                                      ),
                                                    ],
                                                  )),
                                                  Expanded(
                                                      child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        '55%',
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .headline1
                                                            .copyWith(
                                                                fontSize: 16,
                                                                color:
                                                                    customThemeColor),
                                                      ),
                                                      Text(
                                                        'Chat Response\nRate',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .subtitle2
                                                            .copyWith(
                                                                fontSize: 10,
                                                                color:
                                                                    customLightGreyColor),
                                                      ),
                                                    ],
                                                  )),
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                        ],
                                      ),
                                      Positioned(
                                        top: 55,
                                        left: 0,
                                        right: 0,
                                        child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: Align(
                                              alignment: Alignment.center,
                                              child: getVendorStoreDetailModel
                                                          .data
                                                          .vendor
                                                          .store
                                                          .storeLogo ==
                                                      null
                                                  ? CircleAvatar(
                                                      backgroundColor:
                                                          Colors.brown,
                                                      radius: 25,
                                                      child: Image.asset(
                                                          'assets/images/product_detail_circle_image.png'))
                                                  : Container(
                                                      height: 50,
                                                      width: 50,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(25),
                                                          image: DecorationImage(
                                                              image: NetworkImage(
                                                                  '${baseUrl + getVendorStoreDetailModel.data.vendor.store.storeLogo.originalMediaPath}'),
                                                              fit:
                                                                  BoxFit.fill)),
                                                    )),
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                    ),
                  ),
                ]
                    // _randomHeightWidgets(context),
                    ),
              ),
            ];
          },
          // You tab view goes here
          body: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              children: <Widget>[
                ///---tabs
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Container(
                    color: Colors.white,
                    height: 40,
                    width: double.infinity,
                    child: TabBar(
                        onTap: (index) {
                          setState(() {});
                        },
                        controller: productDetailPageTabController,
                        unselectedLabelColor: Colors.grey,
                        indicatorColor: Theme.of(context).buttonColor,
                        indicator: UnderlineTabIndicator(
                            borderSide: BorderSide(
                              width: 2.0,
                              color: Theme.of(context).buttonColor,
                            ),
                            insets: EdgeInsets.symmetric(horizontal: 0.0)),
                        labelStyle: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontSize: 12),
                        labelColor: Theme.of(context).buttonColor,
                        labelPadding: EdgeInsets.all(1),
                        tabs: [
                          Text(
                            'Home',
                          ),
                          Text(
                            'All Products',
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            'Rating',
                          ),
                        ]),
                  ),
                ),

                ///---tab-view
                Expanded(
                  child: TabBarView(
                    controller: productDetailPageTabController,
                    children: [
                      ///---first-view
                      ListView(
                        children: [
                          ///---banner
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                            child: Container(
                              height: 130,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child: Image.asset(
                                  'assets/images/mouse.png',
                                  height: double.infinity,
                                  width: double.infinity,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),

                          ///---new_arrival_products
                          (homeController.getVendorStoreDetailDataCheck &&
                                  getVendorStoreDetailModel
                                          .data.newArrivalProducts.length ==
                                      0)
                              ? SizedBox()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15, 30, 15, 0),
                                      child: Text(
                                        'New Arrival',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1,
                                      ),
                                    ),
                                    !homeController
                                            .getVendorStoreDetailDataCheck
                                        ? SkeletonLoader(
                                            builder: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 12, 0, 0),
                                              child: SizedBox(
                                                height: 142,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                child: ListView.builder(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    itemCount: 3,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Center(
                                                        child: Padding(
                                                          padding: index == 0
                                                              ? const EdgeInsets
                                                                      .fromLTRB(
                                                                  15, 0, 12, 0)
                                                              : const EdgeInsets
                                                                      .fromLTRB(
                                                                  0, 0, 12, 0),
                                                          child: Stack(
                                                            children: [
                                                              Container(
                                                                height: 135,
                                                                width: 280,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10),
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ),
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 12, 0, 0),
                                            child: SizedBox(
                                              height: 142,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: ListView.builder(
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemCount:
                                                      getVendorStoreDetailModel
                                                          .data
                                                          .newArrivalProducts
                                                          .length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Center(
                                                      child: Padding(
                                                        padding: index == 0
                                                            ? const EdgeInsets
                                                                    .fromLTRB(
                                                                15, 0, 12, 0)
                                                            : const EdgeInsets
                                                                    .fromLTRB(
                                                                0, 0, 12, 0),
                                                        child: Stack(
                                                          children: [
                                                            InkWell(
                                                              onTap: () {
                                                                ///------get-product-detail-data-api-call
                                                                getMethod(
                                                                    context,
                                                                    '$getProductsDetailApi/'
                                                                        '${getVendorStoreDetailModel
                                                                        .data
                                                                        .newArrivalProducts[index].slug}',
                                                                    null,
                                                                    false,
                                                                    getProductDetailData);
                                                                Get.find<
                                                                    HomeController>()
                                                                    .changeGetProductDetailDataCheck(
                                                                    false);
                                                                Get.toNamed(PageRoutes
                                                                    .productDetail);
                                                              },
                                                              child: Container(
                                                                height: 135,
                                                                width: 280,
                                                                decoration: BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                10),
                                                                    color: Colors
                                                                        .white,
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                          color: Colors
                                                                              .grey
                                                                              .withOpacity(
                                                                                  0.1),
                                                                          spreadRadius:
                                                                              2,
                                                                          blurRadius:
                                                                              4,
                                                                          offset: Offset(
                                                                              0,
                                                                              2))
                                                                    ]),
                                                                child: Row(
                                                                  children: [
                                                                    ///---image
                                                                    Container(
                                                                      height: double
                                                                          .infinity,
                                                                      width: 125,
                                                                      decoration: BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(
                                                                                  10),
                                                                          image: DecorationImage(
                                                                              image: getVendorStoreDetailModel.data.newArrivalProducts[index].media != null
                                                                                  ? NetworkImage('${baseUrl + getVendorStoreDetailModel.data.newArrivalProducts[index].media[0].originalMediaPath}')
                                                                                  : AssetImage('assets/images/imagePlaceHolder.jpg'),
                                                                              fit: BoxFit.fill)),
                                                                    ),

                                                                    ///---detail
                                                                    Expanded(
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.fromLTRB(
                                                                                12,
                                                                                10,
                                                                                0,
                                                                                0),
                                                                        child:
                                                                            Column(
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: [
                                                                            Expanded(
                                                                              child:
                                                                                  Align(
                                                                                alignment: Alignment.centerLeft,
                                                                                child: Text(
                                                                                  '${getVendorStoreDetailModel.data.newArrivalProducts[index].name}',
                                                                                  maxLines: 2,
                                                                                  overflow: TextOverflow.ellipsis,
                                                                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customDarkGreyColor),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Row(
                                                                              children: [
                                                                                RatingBar(
                                                                                  ignoreGestures: true,
                                                                                  initialRating: getVendorStoreDetailModel.data.newArrivalProducts[index].reviewsAverageRating,
                                                                                  direction: Axis.horizontal,
                                                                                  allowHalfRating: true,
                                                                                  itemCount: 5,
                                                                                  itemSize: 15,
                                                                                  ratingWidget: RatingWidget(
                                                                                    full: Icon(
                                                                                      Icons.star,
                                                                                      color: customRatingStartColor,
                                                                                    ),
                                                                                    half: Icon(Icons.star_half, color: customRatingStartColor),
                                                                                    empty: Icon(Icons.star_border_purple500_sharp, color: customRatingStartColor),
                                                                                  ),
                                                                                  itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                                                  onRatingUpdate: (rating) {
                                                                                    print(rating);
                                                                                  },
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.only(left: 3),
                                                                                  child: Text(
                                                                                    '(${getVendorStoreDetailModel.data.newArrivalProducts[index].totalReviews})',
                                                                                    style: Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 10),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            (getVendorStoreDetailModel.data.newArrivalProducts[index].flashSale != null || getVendorStoreDetailModel.data.newArrivalProducts[index].specialSale != null)
                                                                                ? Padding(
                                                                                    padding: const EdgeInsets.only(top: 8),
                                                                                    child: Text(
                                                                                      '${getVendorStoreDetailModel.data.newArrivalProducts[index].displayPrice}',
                                                                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                                            decoration: TextDecoration.lineThrough,
                                                                                          ),
                                                                                    ),
                                                                                  )
                                                                                : SizedBox(
                                                                                    height: 5,
                                                                                  ),
                                                                            Padding(
                                                                              padding:
                                                                                  const EdgeInsets.only(top: 4, right: 12),
                                                                              child:
                                                                                  Row(
                                                                                children: [
                                                                                  Expanded(
                                                                                    child: (getVendorStoreDetailModel.data.newArrivalProducts[index].flashSale != null && getVendorStoreDetailModel.data.newArrivalProducts[index].specialSale == null)
                                                                                        ? Text(
                                                                                            '${getVendorStoreDetailModel.data.newArrivalProducts[index].flashSale.displayPrice}',
                                                                                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                          )
                                                                                        : (getVendorStoreDetailModel.data.newArrivalProducts[index].flashSale == null && getVendorStoreDetailModel.data.newArrivalProducts[index].specialSale != null)
                                                                                            ? Text(
                                                                                                '${getVendorStoreDetailModel.data.newArrivalProducts[index].specialSale.displayPrice}',
                                                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                              )
                                                                                            : Text(
                                                                                                '${getVendorStoreDetailModel.data.newArrivalProducts[index].displayPrice}',
                                                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                              ),
                                                                                  ),
                                                                                  Icon(
                                                                                    Icons.add_box_rounded,
                                                                                    size: 20,
                                                                                    color: customThemeColor,
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Positioned(
                                                              left: 98.0,
                                                              top: 14,
                                                              child: Container(
                                                                height: 14,
                                                                width: 16,
                                                                child: Center(
                                                                    child: SvgPicture
                                                                        .asset(
                                                                  'assets/Icons/Product_page_icons/Group 366.svg',
                                                                  color:
                                                                      customThemeColor,
                                                                )),
                                                              ),
                                                            ),
                                                            Positioned(
                                                                top: 10.0,
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: List.generate(
                                                                      getVendorStoreDetailModel
                                                                          .data
                                                                          .newArrivalProducts[
                                                                              index]
                                                                          .tags
                                                                          .length,
                                                                      (tagIndex) {
                                                                    return Column(
                                                                      children: [
                                                                        Container(
                                                                          height:
                                                                              20,
                                                                          decoration: BoxDecoration(
                                                                              color: customThemeColor,
                                                                              borderRadius: BorderRadius.only(
                                                                                topRight: Radius.circular(6.0),
                                                                                bottomRight: Radius.circular(6.0),
                                                                              )),
                                                                          child: Center(
                                                                              child: Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                8,
                                                                                0,
                                                                                8,
                                                                                0),
                                                                            child:
                                                                                Text(
                                                                              '${getVendorStoreDetailModel.data.newArrivalProducts[index].tags[tagIndex]}',
                                                                              style: Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 12, color: Colors.white),
                                                                            ),
                                                                          )),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              6,
                                                                        ),
                                                                      ],
                                                                    );
                                                                  }),
                                                                )),
                                                          ],
                                                        ),
                                                      ),
                                                    );
                                                  }),
                                            ),
                                          ),
                                  ],
                                ),

                          ///---upcoming_products
                          (homeController.getVendorStoreDetailDataCheck &&
                                  getVendorStoreDetailModel
                                          .data.upcomingProducts.length ==
                                      0)
                              ? SizedBox()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15, 20, 15, 0),
                                      child: Text(
                                        'Upcoming',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1,
                                      ),
                                    ),
                                    !homeController
                                            .getVendorStoreDetailDataCheck
                                        ? SkeletonLoader(
                                            builder: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 8, 0, 0),
                                              child: SizedBox(
                                                height: 180,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                child: ListView.builder(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    itemCount: 5,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Center(
                                                        child: Padding(
                                                          padding: index == 0
                                                              ? const EdgeInsets
                                                                      .fromLTRB(
                                                                  15, 0, 12, 0)
                                                              : const EdgeInsets
                                                                      .fromLTRB(
                                                                  0, 0, 12, 0),
                                                          child: Stack(
                                                            children: [
                                                              Container(
                                                                height: 172,
                                                                width: 134,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10),
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ),
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                0, 8, 0, 0),
                                            child: SizedBox(
                                              height: 180,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: ListView.builder(
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemCount:
                                                      getVendorStoreDetailModel
                                                          .data
                                                          .upcomingProducts
                                                          .length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Center(
                                                      child: Padding(
                                                        padding: index == 0
                                                            ? const EdgeInsets
                                                                    .fromLTRB(
                                                                15, 0, 12, 0)
                                                            : const EdgeInsets
                                                                    .fromLTRB(
                                                                0, 0, 12, 0),
                                                        child: Stack(
                                                          children: [
                                                            InkWell(
                                                              onTap: () {
                                                                ///------get-product-detail-data-api-call
                                                                getMethod(
                                                                    context,
                                                                    '$getProductsDetailApi/'
                                                                        '${getVendorStoreDetailModel
                                                                        .data
                                                                        .upcomingProducts[index].slug}',
                                                                    null,
                                                                    false,
                                                                    getProductDetailData);
                                                                Get.find<
                                                                    HomeController>()
                                                                    .changeGetProductDetailDataCheck(
                                                                    false);
                                                                Get.toNamed(PageRoutes
                                                                    .productDetail);
                                                              },
                                                              child: Container(
                                                                height: 172,
                                                                width: 134,
                                                                decoration: BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                10),
                                                                    color: Colors
                                                                        .white,
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                          color: Colors
                                                                              .grey
                                                                              .withOpacity(
                                                                                  0.1),
                                                                          spreadRadius:
                                                                              2,
                                                                          blurRadius:
                                                                              4,
                                                                          offset: Offset(
                                                                              0,
                                                                              2))
                                                                    ]),
                                                                child: Column(
                                                                  children: [
                                                                    ///---image
                                                                    Container(
                                                                      height: 112,
                                                                      width: double
                                                                          .infinity,
                                                                      decoration: BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(
                                                                                  10),
                                                                          image: DecorationImage(
                                                                              image: getVendorStoreDetailModel.data.upcomingProducts[index].media == null
                                                                                  ? AssetImage('assets/images/imagePlaceHolder.jpg')
                                                                                  : NetworkImage('${baseUrl + getVendorStoreDetailModel.data.upcomingProducts[index].media[0].originalMediaPath}'),
                                                                              fit: BoxFit.fill)),
                                                                    ),

                                                                    ///---detail
                                                                    Expanded(
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.fromLTRB(
                                                                                8,
                                                                                8,
                                                                                0,
                                                                                0),
                                                                        child:
                                                                            Column(
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: [
                                                                            Text(
                                                                              '${getVendorStoreDetailModel.data.upcomingProducts[index].name}',
                                                                              softWrap:
                                                                                  true,
                                                                              overflow:
                                                                                  TextOverflow.ellipsis,
                                                                              maxLines:
                                                                                  1,
                                                                              style:
                                                                                  Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customDarkGreyColor),
                                                                            ),
                                                                            Padding(
                                                                              padding:
                                                                                  const EdgeInsets.only(top: 8, right: 8),
                                                                              child:
                                                                                  Row(
                                                                                children: [
                                                                                  Expanded(
                                                                                    child: (getVendorStoreDetailModel.data.upcomingProducts[index].flashSale != null && getVendorStoreDetailModel.data.upcomingProducts[index].specialSale == null)
                                                                                        ? Text(
                                                                                            '${getVendorStoreDetailModel.data.upcomingProducts[index].flashSale.displayPrice}',
                                                                                            overflow: TextOverflow.ellipsis,
                                                                                            style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                          )
                                                                                        : (getVendorStoreDetailModel.data.upcomingProducts[index].flashSale == null && getVendorStoreDetailModel.data.upcomingProducts[index].specialSale != null)
                                                                                            ? Text(
                                                                                                '${getVendorStoreDetailModel.data.upcomingProducts[index].specialSale.displayPrice}',
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                              )
                                                                                            : Text(
                                                                                                '${getVendorStoreDetailModel.data.upcomingProducts[index].displayPrice}',
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                              ),
                                                                                  ),
                                                                                  Icon(
                                                                                    Icons.add_box_rounded,
                                                                                    size: 20,
                                                                                    color: customThemeColor,
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Positioned(
                                                              right: 0.0,
                                                              top: 0,
                                                              child: InkWell(
                                                                onTap: (){
                                                                  print('------------->>press');
                                                                  if(currentUserModel != null){
                                                                    ///post-method
                                                                    postMethod(
                                                                        context,
                                                                        addToWishListApi,
                                                                        {
                                                                          'product_id': getVendorStoreDetailModel
                                                                              .data
                                                                              .upcomingProducts[index].id
                                                                        },
                                                                        true,
                                                                        addToWishListDataRepo
                                                                    );
                                                                  }else{
                                                                    notLoggedAlert(
                                                                        context,
                                                                        'Error',
                                                                        'You Have to Login First'
                                                                    );
                                                                    print('------------->>NotLoggedIn');
                                                                  }
                                                                },
                                                                child: Padding(
                                                                  padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                                                  child: Container(
                                                                    height: 14,
                                                                    width: 16,
                                                                    child: Center(
                                                                        child: SvgPicture.asset(
                                                                          'assets/Icons/Product_page_icons/Group 366.svg',
                                                                          color: customThemeColor,
                                                                        )
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Positioned(
                                                                top: 10.0,
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: List.generate(
                                                                      getVendorStoreDetailModel
                                                                          .data
                                                                          .upcomingProducts[
                                                                              index]
                                                                          .tags
                                                                          .length,
                                                                      (tagIndex) {
                                                                    return Column(
                                                                      children: [
                                                                        Container(
                                                                          height:
                                                                              20,
                                                                          decoration: BoxDecoration(
                                                                              color: customThemeColor,
                                                                              borderRadius: BorderRadius.only(
                                                                                topRight: Radius.circular(6.0),
                                                                                bottomRight: Radius.circular(6.0),
                                                                              )),
                                                                          child: Center(
                                                                              child: Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                8,
                                                                                0,
                                                                                8,
                                                                                0),
                                                                            child:
                                                                                Text(
                                                                              '${getVendorStoreDetailModel.data.upcomingProducts[index].tags[tagIndex]}',
                                                                              style: Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 12, color: Colors.white),
                                                                            ),
                                                                          )),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              6,
                                                                        ),
                                                                      ],
                                                                    );
                                                                  }),
                                                                )),
                                                          ],
                                                        ),
                                                      ),
                                                    );
                                                  }),
                                            ),
                                          ),
                                  ],
                                ),

                          ///---banners
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                            child: Column(
                              children: [
                                Center(
                                  child: Stack(
                                    children: [
                                      Container(
                                        height: 126,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(
                                                'assets/images/baby.png'),
                                            fit: BoxFit.cover,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      Positioned(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                .45,
                                        top: 36.0,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'our latest collection of',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .button
                                                  .copyWith(
                                                      color:
                                                          customLightBrownColor),
                                            ),
                                            Text(
                                              'Casual shirts',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline1
                                                  .copyWith(
                                                      color:
                                                          customLightBrownColor),
                                            ),
                                            Text(
                                              'Explore Now',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .button
                                                  .copyWith(
                                                      color: Colors.white,
                                                      decoration: TextDecoration
                                                          .underline),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 12, 12, 0),
                                          child: Stack(
                                            children: [
                                              Container(
                                                height: 85,
                                                width: double.infinity,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    image: AssetImage(
                                                        'assets/images/computerCategory.png'),
                                                    fit: BoxFit.cover,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                              Positioned(
                                                left: 10.0,
                                                bottom: 18.0,
                                                child: Text(
                                                  'Computer',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline1
                                                      .copyWith(
                                                          fontSize: 13,
                                                          color: Colors.white),
                                                ),
                                              ),
                                              Positioned(
                                                left: 10.0,
                                                bottom: 6.0,
                                                child: Text(
                                                  'Accessories',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .button
                                                      .copyWith(
                                                          color: Colors.white,
                                                          fontSize: 9),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 12, 0, 0),
                                          child: Stack(
                                            children: [
                                              Container(
                                                height: 85,
                                                width: double.infinity,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    image: AssetImage(
                                                        'assets/images/ladiesCategory.png'),
                                                    fit: BoxFit.cover,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                              Positioned(
                                                left: 85.0,
                                                bottom: 20.0,
                                                child: Text(
                                                  'Ladies',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline1
                                                      .copyWith(
                                                          fontSize: 13,
                                                          color:
                                                              customLightBlackColor),
                                                ),
                                              ),
                                              Positioned(
                                                left: 85.0,
                                                bottom: 8.0,
                                                child: Text(
                                                  'Summer Wear',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .button
                                                      .copyWith(
                                                          color:
                                                              customLightBlackColor,
                                                          fontSize: 9),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),

                          ///---top-selling
                          homeController.getVendorStoreDetailDataCheck &&
                                  getVendorStoreDetailModel
                                          .data.trendingProducts.length ==
                                      0
                              ? SizedBox()
                              : Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(15, 25, 15, 0),
                                  child: Column(
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text('Top Selling',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline1),
                                            ),
                                          ],
                                        ),
                                      ),
                                      !homeController
                                              .getVendorStoreDetailDataCheck
                                          ? SkeletonLoader(
                                              period: Duration(seconds: 2),
                                              highlightColor: Colors.grey,
                                              direction: SkeletonDirection.ltr,
                                              builder: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 7, 0, 0),
                                                child: Wrap(
                                                  children:
                                                      List.generate(4, (index) {
                                                    return Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              .45,
                                                      child: Padding(
                                                        padding: index % 2 == 0
                                                            ? const EdgeInsets
                                                                    .fromLTRB(
                                                                0, 5, 5, 5)
                                                            : const EdgeInsets
                                                                    .fromLTRB(
                                                                5, 5, 0, 5),
                                                        child: Container(
                                                          height: 230,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              .45,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .vertical(
                                                              top: Radius
                                                                  .circular(10),
                                                              bottom: Radius
                                                                  .circular(10),
                                                            ),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors
                                                                    .grey
                                                                    .withOpacity(
                                                                        0.2),
                                                                spreadRadius: 2,
                                                                blurRadius: 6,
                                                                offset: Offset(
                                                                    0, 3),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  }),
                                                ),
                                              ))
                                          : Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 7, 0, 0),
                                              child: Wrap(
                                                children: List.generate(
                                                    getVendorStoreDetailModel
                                                        .data
                                                        .trendingProducts
                                                        .length, (index) {
                                                  return Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            .45,
                                                    child: Padding(
                                                      padding: index % 2 == 0
                                                          ? const EdgeInsets
                                                                  .fromLTRB(
                                                              0, 5, 5, 5)
                                                          : const EdgeInsets
                                                                  .fromLTRB(
                                                              5, 5, 0, 5),
                                                      child: Stack(children: [
                                                        InkWell(
                                                          onTap: () {
                                                            ///------get-product-detail-data-api-call
                                                            getMethod(
                                                                context,
                                                                '$getProductsDetailApi/'
                                                                '${getVendorStoreDetailModel.data.trendingProducts[index].slug}',
                                                                null,
                                                                false,
                                                                getProductDetailData);
                                                            Get.find<
                                                                    HomeController>()
                                                                .changeGetProductDetailDataCheck(
                                                                    false);
                                                            Get.toNamed(PageRoutes
                                                                .productDetail);
                                                          },
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .vertical(
                                                                top: Radius
                                                                    .circular(
                                                                        10),
                                                                bottom: Radius
                                                                    .circular(
                                                                        10),
                                                              ),
                                                              boxShadow: [
                                                                BoxShadow(
                                                                  color: Colors
                                                                      .grey
                                                                      .withOpacity(
                                                                          0.2),
                                                                  spreadRadius:
                                                                      2,
                                                                  blurRadius: 6,
                                                                  offset:
                                                                      Offset(
                                                                          0, 3),
                                                                ),
                                                              ],
                                                            ),
                                                            child: Column(
                                                              children: [
                                                                ///--------------product-image
                                                                Container(
                                                                  height: 145,
                                                                  width: double
                                                                      .infinity,
                                                                  child:
                                                                      ClipRRect(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .vertical(
                                                                      top: Radius
                                                                          .circular(
                                                                              10),
                                                                      bottom: Radius
                                                                          .circular(
                                                                              10),
                                                                    ),
                                                                    child: getVendorStoreDetailModel.data.trendingProducts[index].media.length ==
                                                                            0
                                                                        ? Image
                                                                            .asset(
                                                                            'assets/images/imagePlaceHolder.jpg',
                                                                            fit:
                                                                                BoxFit.fill,
                                                                          )
                                                                        : Image
                                                                            .network(
                                                                            '${baseUrl + getVendorStoreDetailModel.data.trendingProducts[index].media[0].originalMediaPath}',
                                                                            fit:
                                                                                BoxFit.fill,
                                                                          ),
                                                                  ),
                                                                ),

                                                                ///--------------product-detail
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  child:
                                                                      Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            8.0,
                                                                        left:
                                                                            10.0),
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        ///--------------product-name
                                                                        Text(
                                                                          '${getVendorStoreDetailModel.data.trendingProducts[index].name}',
                                                                          softWrap:
                                                                              true,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                          maxLines:
                                                                              2,
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .headline1
                                                                              .copyWith(fontSize: 14, color: customDarkGreyColor),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              4.0,
                                                                        ),

                                                                        ///--------------product-rating-row
                                                                        Row(
                                                                          children: [
                                                                            RatingBar(
                                                                              ignoreGestures: true,
                                                                              initialRating: getVendorStoreDetailModel.data.trendingProducts[index].reviewsAverageRating,
                                                                              direction: Axis.horizontal,
                                                                              allowHalfRating: true,
                                                                              itemCount: 5,
                                                                              itemSize: 15,
                                                                              ratingWidget: RatingWidget(
                                                                                full: Icon(
                                                                                  Icons.star,
                                                                                  color: customRatingStartColor,
                                                                                ),
                                                                                half: Icon(Icons.star_half, color: customRatingStartColor),
                                                                                empty: Icon(Icons.star_border_purple500_sharp, color: customRatingStartColor),
                                                                              ),
                                                                              itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                                              onRatingUpdate: (rating) {
                                                                                print(rating);
                                                                              },
                                                                            ),
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(left: 3),
                                                                              child: Text(
                                                                                '(${getVendorStoreDetailModel.data.trendingProducts[index].totalReviews})',
                                                                                style: Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 10),
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              10.0,
                                                                        ),
                                                                        (getVendorStoreDetailModel.data.trendingProducts[index].flashSale != null ||
                                                                                getVendorStoreDetailModel.data.trendingProducts[index].specialSale != null)
                                                                            ? Text(
                                                                                '${getVendorStoreDetailModel.data.trendingProducts[index].displayPrice}',
                                                                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                                      decoration: TextDecoration.lineThrough,
                                                                                    ),
                                                                              )
                                                                            : SizedBox(),
                                                                        Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.start,
                                                                          children: [
                                                                            ///--------------product-price
                                                                            Expanded(
                                                                              child: (getVendorStoreDetailModel.data.trendingProducts[index].flashSale != null && getVendorStoreDetailModel.data.trendingProducts[index].specialSale == null)
                                                                                  ? Text(
                                                                                      '${getVendorStoreDetailModel.data.trendingProducts[index].flashSale.displayPrice}',
                                                                                      overflow: TextOverflow.ellipsis,
                                                                                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                    )
                                                                                  : (getVendorStoreDetailModel.data.trendingProducts[index].flashSale == null && getTopSellingProductsModel.data[index].specialSale != null)
                                                                                      ? Text(
                                                                                          '${getVendorStoreDetailModel.data.trendingProducts[index].specialSale.displayPrice}',
                                                                                          overflow: TextOverflow.ellipsis,
                                                                                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                        )
                                                                                      : Text(
                                                                                          '${getVendorStoreDetailModel.data.trendingProducts[index].displayPrice}',
                                                                                          overflow: TextOverflow.ellipsis,
                                                                                          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                        ),
                                                                            ),
                                                                            InkWell(
                                                                              onTap: () {},
                                                                              child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(5, 5, 10, 5),
                                                                                child: Icon(
                                                                                  Icons.add_box_rounded,
                                                                                  size: 20,
                                                                                  color: customThemeColor,
                                                                                ),
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              8.0,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          right: 0.0,
                                                          top: 0,
                                                          child: InkWell(
                                                            onTap: (){
                                                              print('------------->>press');
                                                              if(currentUserModel != null){
                                                                ///post-method
                                                                postMethod(
                                                                    context,
                                                                    addToWishListApi,
                                                                    {
                                                                      'product_id': getVendorStoreDetailModel
                                                                          .data
                                                                          .trendingProducts[index].id
                                                                    },
                                                                    true,
                                                                    addToWishListDataRepo
                                                                );
                                                              }else{
                                                                notLoggedAlert(
                                                                    context,
                                                                    'Error',
                                                                    'You Have to Login First'
                                                                );
                                                                print('------------->>NotLoggedIn');
                                                              }
                                                            },
                                                            child: Padding(
                                                              padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                                              child: Container(
                                                                height: 14,
                                                                width: 16,
                                                                child: Center(
                                                                    child: SvgPicture.asset(
                                                                      'assets/Icons/Product_page_icons/Group 366.svg',
                                                                      color: customThemeColor,
                                                                    )
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                            top: 10.0,
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: List.generate(
                                                                  getVendorStoreDetailModel
                                                                      .data
                                                                      .trendingProducts[
                                                                          index]
                                                                      .tags
                                                                      .length,
                                                                  (tagIndex) {
                                                                return Column(
                                                                  children: [
                                                                    Container(
                                                                      height:
                                                                          20,
                                                                      decoration: BoxDecoration(
                                                                          color: customThemeColor,
                                                                          borderRadius: BorderRadius.only(
                                                                            topRight:
                                                                                Radius.circular(6.0),
                                                                            bottomRight:
                                                                                Radius.circular(6.0),
                                                                          )),
                                                                      child: Center(
                                                                          child: Padding(
                                                                        padding: const EdgeInsets.fromLTRB(
                                                                            8,
                                                                            0,
                                                                            8,
                                                                            0),
                                                                        child:
                                                                            Text(
                                                                          '${getVendorStoreDetailModel.data.trendingProducts[index].tags[tagIndex]}',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .subtitle2
                                                                              .copyWith(fontSize: 12, color: Colors.white),
                                                                        ),
                                                                      )),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 6,
                                                                    ),
                                                                  ],
                                                                );
                                                              }),
                                                            )),
                                                      ]),
                                                    ),
                                                  );
                                                }),
                                              ),
                                            )
                                    ],
                                  ),
                                ),

                          ///---categories
                          homeController.getVendorStoreDetailDataCheck &&
                                  getVendorStoreDetailModel
                                          .data.categories.length ==
                                      0
                              ? SizedBox()
                              : Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15, 20, 15, 0),
                                      child: Container(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text('Main Categories',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline1),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 12),
                                      child: Container(
                                        height: 94,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.2),
                                              spreadRadius: 5,
                                              blurRadius: 6,
                                            ),
                                          ],
                                        ),
                                        child: !homeController
                                                .getVendorStoreDetailDataCheck
                                            ? SkeletonLoader(
                                                period: Duration(seconds: 2),
                                                highlightColor: Colors.grey,
                                                direction:
                                                    SkeletonDirection.ltr,
                                                builder: Container(
                                                  height: 94,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: ListView(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    children: List.generate(10,
                                                        (index) {
                                                      return Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                17, 0, 0, 0),
                                                        child: Container(
                                                          width: 70,
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Container(
                                                                height: 50,
                                                                width: 50,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              SizedBox(
                                                                height: 4,
                                                              ),
                                                              Container(
                                                                height: 4,
                                                                width: 30,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    }),
                                                  ),
                                                ),
                                              )
                                            : ListView(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                children: List.generate(
                                                    getVendorStoreDetailModel
                                                        .data
                                                        .categories
                                                        .length, (index) {
                                                  return Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(17, 0, 0, 0),
                                                    child: Container(
                                                      width: 50,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Container(
                                                            height: 50,
                                                            width: 50,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              image: DecorationImage(
                                                                  image: getVendorStoreDetailModel
                                                                              .data
                                                                              .categories[
                                                                                  index]
                                                                              .icon ==
                                                                          null
                                                                      ? AssetImage(
                                                                          'assets/images/vendorProduct1.png')
                                                                      : NetworkImage(
                                                                          '${getVendorStoreDetailModel.data.categories[index].icon.originalMediaPath}'),
                                                                  fit: BoxFit
                                                                      .fill),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 4,
                                                          ),
                                                          Text(
                                                            '${getVendorStoreDetailModel.data.categories[index].name}',
                                                            textAlign: TextAlign
                                                                .center,
                                                            softWrap: true,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style: Theme.of(
                                                                    context)
                                                                .textTheme
                                                                .subtitle2
                                                                .copyWith(
                                                                    fontSize:
                                                                        10,
                                                                    color:
                                                                        customThemeColor),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                }),
                                              ),
                                      ),
                                    )
                                  ],
                                ),
                          SizedBox(
                            height: 20,
                          )
                        ],
                      ),

                      ///---second-view
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: SingleChildScrollView(
                          child: !homeController.getProductsOfVendorDataCheck
                              ? SkeletonLoader(
                                  period: Duration(seconds: 2),
                                  highlightColor: Colors.grey,
                                  direction: SkeletonDirection.ltr,
                                  builder: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 7, 0, 0),
                                    child: Center(
                                      child: Wrap(
                                        children: List.generate(8, (index) {
                                          return Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .45,
                                            child: Padding(
                                              padding: index % 2 == 0
                                                  ? const EdgeInsets.fromLTRB(
                                                      0, 5, 5, 5)
                                                  : const EdgeInsets.fromLTRB(
                                                      5, 5, 0, 5),
                                              child: Container(
                                                height: 230,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .45,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.vertical(
                                                    top: Radius.circular(10),
                                                    bottom: Radius.circular(10),
                                                  ),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(0.2),
                                                      spreadRadius: 2,
                                                      blurRadius: 6,
                                                      offset: Offset(0, 3),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                      ),
                                    ),
                                  ))
                              : Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 7, 0, 0),
                                  child: Center(
                                    child: Wrap(
                                      children: List.generate(
                                          getProductsOfVendorModel
                                              .data.data.length, (index) {
                                        return Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .45,
                                          child: Padding(
                                            padding: index % 2 == 0
                                                ? const EdgeInsets.fromLTRB(
                                                    0, 5, 5, 5)
                                                : const EdgeInsets.fromLTRB(
                                                    5, 5, 0, 5),
                                            child: Stack(children: [
                                              InkWell(
                                                onTap: () {
                                                  ///------get-product-detail-data-api-call
                                                  getMethod(
                                                      context,
                                                      '$getProductsDetailApi/${getProductsOfVendorModel.data.data[index].slug}',
                                                      null,
                                                      false,
                                                      getProductDetailData);
                                                  Get.find<HomeController>()
                                                      .changeGetProductDetailDataCheck(
                                                          false);
                                                  Get.toNamed(
                                                      PageRoutes.productDetail);
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.vertical(
                                                      top: Radius.circular(10),
                                                      bottom:
                                                          Radius.circular(10),
                                                    ),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.2),
                                                        spreadRadius: 2,
                                                        blurRadius: 6,
                                                        offset: Offset(0, 3),
                                                      ),
                                                    ],
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      ///--------------product-image
                                                      Container(
                                                        height: 145,
                                                        width: double.infinity,
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .vertical(
                                                            top:
                                                                Radius.circular(
                                                                    10),
                                                            bottom:
                                                                Radius.circular(
                                                                    10),
                                                          ),
                                                          child: getProductsOfVendorModel
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .media
                                                                      .length ==
                                                                  0
                                                              ? Image.asset(
                                                                  'assets/images/onBoard1.png',
                                                                  fit: BoxFit
                                                                      .fill,
                                                                )
                                                              : Image.network(
                                                                  '${baseUrl + getProductsOfVendorModel.data.data[index].media[0].originalMediaPath}',
                                                                  fit: BoxFit
                                                                      .fill,
                                                                ),
                                                        ),
                                                      ),

                                                      ///--------------product-detail
                                                      Container(
                                                        width: double.infinity,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 8.0,
                                                                  left: 10.0),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              ///--------------product-name
                                                              Text(
                                                                '${getProductsOfVendorModel.data.data[index].name}',
                                                                softWrap: true,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                maxLines: 2,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .headline1
                                                                    .copyWith(
                                                                        fontSize:
                                                                            14,
                                                                        color:
                                                                            customDarkGreyColor),
                                                              ),
                                                              SizedBox(
                                                                height: 4.0,
                                                              ),

                                                              ///--------------product-rating-row
                                                              Row(
                                                                children: [
                                                                  RatingBar(
                                                                    ignoreGestures:
                                                                        true,
                                                                    initialRating: getProductsOfVendorModel
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .reviewsAverageRating,
                                                                    direction: Axis
                                                                        .horizontal,
                                                                    allowHalfRating:
                                                                        true,
                                                                    itemCount:
                                                                        5,
                                                                    itemSize:
                                                                        15,
                                                                    ratingWidget:
                                                                        RatingWidget(
                                                                      full:
                                                                          Icon(
                                                                        Icons
                                                                            .star,
                                                                        color:
                                                                            customRatingStartColor,
                                                                      ),
                                                                      half: Icon(
                                                                          Icons
                                                                              .star_half,
                                                                          color:
                                                                              customRatingStartColor),
                                                                      empty: Icon(
                                                                          Icons
                                                                              .star_border_purple500_sharp,
                                                                          color:
                                                                              customRatingStartColor),
                                                                    ),
                                                                    itemPadding:
                                                                        EdgeInsets.symmetric(
                                                                            horizontal:
                                                                                0.0),
                                                                    onRatingUpdate:
                                                                        (rating) {
                                                                      print(
                                                                          rating);
                                                                    },
                                                                  ),
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            3),
                                                                    child: Text(
                                                                      '(${getProductsOfVendorModel.data.data[index].totalReviews})',
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .subtitle2
                                                                          .copyWith(
                                                                              fontSize: 10),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                              SizedBox(
                                                                height: 10.0,
                                                              ),
                                                              (getProductsOfVendorModel
                                                                              .data
                                                                              .data[
                                                                                  index]
                                                                              .flashSale !=
                                                                          null ||
                                                                      getProductsOfVendorModel
                                                                              .data
                                                                              .data[index]
                                                                              .specialSale !=
                                                                          null)
                                                                  ? Text(
                                                                      '${getProductsOfVendorModel.data.data[index].displayPrice}',
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .subtitle2
                                                                          .copyWith(
                                                                            decoration:
                                                                                TextDecoration.lineThrough,
                                                                          ),
                                                                    )
                                                                  : SizedBox(),
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  ///--------------product-price
                                                                  Expanded(
                                                                    child: (getProductsOfVendorModel.data.data[index].flashSale !=
                                                                                null &&
                                                                            getProductsOfVendorModel.data.data[index].specialSale ==
                                                                                null)
                                                                        ? Text(
                                                                            '${getProductsOfVendorModel.data.data[index].flashSale.displayPrice}',
                                                                            style:
                                                                                Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                          )
                                                                        : (getProductsOfVendorModel.data.data[index].flashSale == null &&
                                                                                getProductsOfVendorModel.data.data[index].specialSale != null)
                                                                            ? Text(
                                                                                '${getProductsOfVendorModel.data.data[index].specialSale.displayPrice}',
                                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                              )
                                                                            : Text(
                                                                                '${getProductsOfVendorModel.data.data[index].displayPrice}',
                                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                              ),
                                                                  ),
                                                                  InkWell(
                                                                    onTap:
                                                                        () {},
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.fromLTRB(
                                                                              5,
                                                                              5,
                                                                              10,
                                                                              5),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .add_box_rounded,
                                                                        size:
                                                                            20,
                                                                        color:
                                                                            customThemeColor,
                                                                      ),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                              SizedBox(
                                                                height: 8.0,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                right: 0.0,
                                                top: 0,
                                                child: InkWell(
                                                  onTap: (){
                                                    print('------------->>press');
                                                    if(currentUserModel != null){
                                                      ///post-method
                                                      postMethod(
                                                          context,
                                                          addToWishListApi,
                                                          {
                                                            'product_id': getProductsOfVendorModel
                                                                .data.data[index].id
                                                          },
                                                          true,
                                                          addToWishListDataRepo
                                                      );
                                                    }else{
                                                      notLoggedAlert(
                                                          context,
                                                          'Error',
                                                          'You Have to Login First'
                                                      );
                                                      print('------------->>NotLoggedIn');
                                                    }
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                                    child: Container(
                                                      height: 14,
                                                      width: 16,
                                                      child: Center(
                                                          child: SvgPicture.asset(
                                                            'assets/Icons/Product_page_icons/Group 366.svg',
                                                            color: customThemeColor,
                                                          )
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                  top: 10.0,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: List.generate(
                                                        getProductsOfVendorModel
                                                            .data
                                                            .data[index]
                                                            .tags
                                                            .length,
                                                        (tagIndex) {
                                                      return Column(
                                                        children: [
                                                          Container(
                                                            height: 20,
                                                            decoration:
                                                                BoxDecoration(
                                                                    color:
                                                                        customThemeColor,
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                                      topRight:
                                                                          Radius.circular(
                                                                              6.0),
                                                                      bottomRight:
                                                                          Radius.circular(
                                                                              6.0),
                                                                    )),
                                                            child: Center(
                                                                child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .fromLTRB(
                                                                      8,
                                                                      0,
                                                                      8,
                                                                      0),
                                                              child: Text(
                                                                '${getProductsOfVendorModel.data.data[index].tags[tagIndex]}',
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .subtitle2
                                                                    .copyWith(
                                                                        fontSize:
                                                                            12,
                                                                        color: Colors
                                                                            .white),
                                                              ),
                                                            )),
                                                          ),
                                                          SizedBox(
                                                            height: 6,
                                                          ),
                                                        ],
                                                      );
                                                    }),
                                                  )),
                                            ]),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                        ),
                      ),

                      ///---third-view
                      ListView(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                            child: Container(
                              height: 200,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(10),
                                  bottom: Radius.circular(10),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 2,
                                    blurRadius: 6,
                                    offset: Offset(0, 3),
                                  ),
                                ],
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 18, 0, 14),
                                    child: Text(
                                      'Seller Rating',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5
                                          .copyWith(
                                              fontSize: 16,
                                              color: customDarkBlackColor),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            18, 0, 0, 0),
                                        child: Container(
                                          width: 100,
                                          height: 100,
                                          child: CircularProgress(
                                            //---must set the padding inside the package of 5.0
                                            percentage: 81.0,
                                            color: customGreenSaleFlagColor,
                                            backColor: customSmoothGreyColor,
                                            showPercentage: true,
                                            textStyle: Theme.of(context)
                                                .textTheme
                                                .headline1
                                                .copyWith(
                                                    color:
                                                        customLightBlackColor,
                                                    fontSize: 16),
                                            stroke: 0,
                                            round: true,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 15),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '5',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                  SizedBox(
                                                    width: 2,
                                                  ),
                                                  SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/star.svg'),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  SizedBox(
                                                      width: 120,
                                                      child: ProgressBar(
                                                        max: 100,
                                                        current: 80,
                                                        color:
                                                            customRatingStartColor,
                                                      )),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '(150)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 12,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '4',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                  SizedBox(
                                                    width: 2,
                                                  ),
                                                  SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/star.svg'),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  SizedBox(
                                                      width: 120,
                                                      child: ProgressBar(
                                                        max: 100,
                                                        current: 60,
                                                        color:
                                                            customRatingStartColor,
                                                      )),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '(98)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 12,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '3',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                  SizedBox(
                                                    width: 2,
                                                  ),
                                                  SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/star.svg'),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  SizedBox(
                                                      width: 120,
                                                      child: ProgressBar(
                                                        max: 100,
                                                        current: 45,
                                                        color:
                                                            customRatingStartColor,
                                                      )),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '(20)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 12,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '2',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                  SizedBox(
                                                    width: 2,
                                                  ),
                                                  SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/star.svg'),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  SizedBox(
                                                      width: 120,
                                                      child: ProgressBar(
                                                        max: 100,
                                                        current: 30,
                                                        color:
                                                            customRatingStartColor,
                                                      )),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '(8)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 12,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '1',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                  SizedBox(
                                                    width: 2,
                                                  ),
                                                  SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/star.svg'),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  SizedBox(
                                                      width: 120,
                                                      child: ProgressBar(
                                                        max: 100,
                                                        current: 10,
                                                        color:
                                                            customRatingStartColor,
                                                      )),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '(1)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle2
                                                        .copyWith(
                                                            fontSize: 10,
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 16, 15, 0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: 195,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(10),
                                        bottom: Radius.circular(10),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.2),
                                          spreadRadius: 2,
                                          blurRadius: 6,
                                          offset: Offset(0, 3),
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              20, 18, 0, 0),
                                          child: Text(
                                            'Shipped On Time',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5
                                                .copyWith(
                                                    fontSize: 16,
                                                    color:
                                                        customDarkBlackColor),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                18, 0, 0, 0),
                                            child: Container(
                                              width: 100,
                                              height: 100,
                                              child: CircularProgress(
                                                //---must set the padding inside the package of 5.0
                                                percentage: 81.0,
                                                color: customGreenSaleFlagColor,
                                                backColor:
                                                    customSmoothGreyColor,
                                                showPercentage: true,
                                                textStyle: Theme.of(context)
                                                    .textTheme
                                                    .headline1
                                                    .copyWith(
                                                        color:
                                                            customLightBlackColor,
                                                        fontSize: 16),
                                                stroke: 0,
                                                round: true,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 195,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(10),
                                        bottom: Radius.circular(10),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.2),
                                          spreadRadius: 2,
                                          blurRadius: 6,
                                          offset: Offset(0, 3),
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              20, 18, 0, 0),
                                          child: Text(
                                            'Cancellation Rate',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5
                                                .copyWith(
                                                    fontSize: 16,
                                                    color:
                                                        customDarkBlackColor),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                18, 0, 0, 0),
                                            child: Container(
                                              width: 100,
                                              height: 100,
                                              child: CircularProgress(
                                                //---must set the padding inside the package of 5.0
                                                percentage: 10.0,
                                                color: Colors.red,
                                                backColor:
                                                    customSmoothGreyColor,
                                                showPercentage: true,
                                                textStyle: Theme.of(context)
                                                    .textTheme
                                                    .headline1
                                                    .copyWith(
                                                        color:
                                                            customLightBlackColor,
                                                        fontSize: 16),
                                                stroke: 0,
                                                round: true,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(15, 25, 15, 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Ratings & Reviews',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .copyWith(
                                                color: customDarkBlackColor),
                                      ),
                                    ),
                                    Text(
                                      'View All',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5
                                          .copyWith(
                                              fontSize: 12,
                                              decoration:
                                                  TextDecoration.underline),
                                    )
                                  ],
                                ),
                                Text(
                                  'Total Ratings: 120',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle2
                                      .copyWith(
                                          fontSize: 10,
                                          color: customLightGreyColor),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Wrap(
                                    children: List.generate(3, (index) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      index == 0
                                          ? SizedBox()
                                          : SizedBox(
                                              height: 8,
                                            ),
                                      Row(
                                        children: [
                                          SvgPicture.asset(
                                              'assets/Icons/Product_page_icons/profile.svg'),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            'John Doe',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle2
                                                .copyWith(
                                                    fontSize: 10,
                                                    color:
                                                        customLightGreyColor),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            '(21 May 2021)',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle2
                                                .copyWith(
                                                    fontSize: 10,
                                                    color:
                                                        customLightGreyColor),
                                          ),
                                          Expanded(
                                            child: Align(
                                              alignment: Alignment.centerRight,
                                              child: RatingBar(
                                                initialRating: 5,
                                                direction: Axis.horizontal,
                                                allowHalfRating: true,
                                                itemCount: 5,
                                                itemSize: 15,
                                                ratingWidget: RatingWidget(
                                                  full: Icon(
                                                    Icons.star,
                                                    color:
                                                        customRatingStartColor,
                                                  ),
                                                  half: Icon(Icons.star_half,
                                                      color:
                                                          customRatingStartColor),
                                                  empty: Icon(
                                                      Icons
                                                          .star_border_purple500_sharp,
                                                      color:
                                                          customRatingStartColor),
                                                ),
                                                itemPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 0.0),
                                                onRatingUpdate: (rating) {
                                                  print(rating);
                                                },
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Text(
                                          'Nam tempus turpis at metus scelerisque placerat '
                                          'nulla deumantos solicitud felis. Pellentesque diam dolor.',
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2
                                              .copyWith(
                                                  fontSize: 12,
                                                  color: customLightBlackColor),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: SizedBox(
                                          height: 50,
                                          width: double.infinity,
                                          child: ListView(
                                            scrollDirection: Axis.horizontal,
                                            children: List.generate(3, (index) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 10, 0),
                                                child: Container(
                                                  height: 50,
                                                  width: 60,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                  ),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    child: Image.asset(
                                                      'assets/images/productImage.png',
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            30, 15, 20, 0),
                                        child: Divider(
                                          color: customDividerGreyColor,
                                        ),
                                      )
                                    ],
                                  );
                                })),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
