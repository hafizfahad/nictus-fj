import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/productFilterModel.dart';
import 'package:multi_vendor_customer/src/pages/shopPage.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:multi_vendor_customer/src/repository/getShopPageDataRepository.dart';
import 'package:multi_vendor_customer/src/repository/getShopPageProductsRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';

class ShopFiltersPage extends StatefulWidget {
  @override
  _ShopFiltersPageState createState() => _ShopFiltersPageState();
}

class _ShopFiltersPageState extends State<ShopFiltersPage> {
  RangeValues _currentRangeValues = RangeValues(
      productFilterModel.maxPrice == 1 ? 0 : productFilterModel.minPrice,
      productFilterModel.maxPrice != 1 ? productFilterModel.maxPrice : 1);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(
        drawerShow: false,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---filter-row
              // Padding(
              //   padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.start,
              //     children: [
              //       // InkWell(
              //       //   onTap: (){
              //       //     Get.offNamed(PageRoutes.shopCategories);
              //       //   },
              //       //   child: Padding(
              //       //     padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
              //       //     child: Text(
              //       //       'Categories',
              //       //       style: Theme.of(context).textTheme.headline4.copyWith(
              //       //           color: customLightGreyColor
              //       //       ),
              //       //     ),
              //       //   ),
              //       // ),
              //       // SizedBox(width: 22,),
              //       Padding(
              //         padding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
              //         child: Text(
              //           'Filters',
              //           style: Theme.of(context).textTheme.headline4.copyWith(
              //               color: customThemeColor
              //           ),
              //         ),
              //       ),
              //       SizedBox(
              //         width: 20,
              //       ),
              //     ],
              //   ),
              // ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Text(
                  'FILTERS',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: customLightBlackColor),
                ),
              ),
              Divider(
                thickness: 1,
                color: customSmoothGreyColor,
              ),
              Expanded(
                child: ListView(
                  children: [
                    // Padding(
                    //   padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    //   child: Row(
                    //     children: [
                    //       Expanded(
                    //         child: Text(
                    //           'Specific Vendors',
                    //           style: Theme.of(context).textTheme.headline5.copyWith(
                    //               color: customDarkBlackColor
                    //           ),
                    //         ),
                    //       ),
                    //       InkWell(
                    //         onTap: (){},
                    //         child: Text(
                    //           'Reset',
                    //           style: Theme.of(context).textTheme.headline5.copyWith(
                    //               fontSize: 12
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // ///---search-bar
                    // Padding(
                    //   padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                    //   child: Container(
                    //     color: Color(0xffF9F9F9),
                    //     child: Padding(
                    //       padding: const EdgeInsets.only(left: 15),
                    //       child: Theme(
                    //         data: ThemeData(
                    //             primaryColor: Color(0xff363636)
                    //         ),
                    //         child: TextFormField(
                    //           decoration: InputDecoration(
                    //               suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                    //               border: InputBorder.none,
                    //               hintText: 'search here',
                    //               hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    // Divider(
                    //   thickness: 1,
                    //   color: customSmoothGreyColor,
                    // ),
                    // SizedBox(height: 12,),
                    // ///---free-shipping-choice
                    // Text(
                    //   'Free Shipping',
                    //   style: Theme.of(context).textTheme.headline5.copyWith(
                    //       color: customDarkBlackColor
                    //   ),
                    // ),
                    // SizedBox(height: 12,),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.start,
                    //   children: [
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           freeShippingCheck = 1;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 53,
                    //         decoration: BoxDecoration(
                    //           color: freeShippingCheck == 1
                    //               ?customSelectorColor
                    //               :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Text(
                    //             'Yes',
                    //             style: Theme.of(context).textTheme.headline4.copyWith(
                    //               fontSize: 12,
                    //               color: freeShippingCheck == 1
                    //                   ?customThemeColor
                    //                   :customLightBlackColor
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           freeShippingCheck = 2;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 53,
                    //         decoration: BoxDecoration(
                    //             color: freeShippingCheck == 2
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Text(
                    //             'No',
                    //             style: Theme.of(context).textTheme.headline4.copyWith(
                    //                 fontSize: 12,
                    //                 color: freeShippingCheck == 2
                    //                     ?customThemeColor
                    //                     :customLightBlackColor
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Divider(
                    //   thickness: 1,
                    //   color: customSmoothGreyColor,
                    // ),
                    // SizedBox(height: 12,),
                    // ///---rating-choice
                    // Text(
                    //   'Rating',
                    //   style: Theme.of(context).textTheme.headline5.copyWith(
                    //       color: customDarkBlackColor
                    //   ),
                    // ),
                    // SizedBox(height: 12,),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.start,
                    //   children: [
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           ratingCheck = 1;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 54,
                    //         decoration: BoxDecoration(
                    //             color: ratingCheck == 1
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.center,
                    //           children: [
                    //             SvgPicture.asset(
                    //               'assets/Icons/Product_page_icons/star.svg'
                    //             ),
                    //             SizedBox(width: 5,),
                    //             Text(
                    //               '1',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: ratingCheck == 1
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           ratingCheck = 2;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 54,
                    //         decoration: BoxDecoration(
                    //             color: ratingCheck == 2
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.center,
                    //           children: [
                    //             SvgPicture.asset(
                    //                 'assets/Icons/Product_page_icons/star.svg'
                    //             ),
                    //             SizedBox(width: 5,),
                    //             Text(
                    //               '2',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: ratingCheck == 2
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           ratingCheck = 3;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 54,
                    //         decoration: BoxDecoration(
                    //             color: ratingCheck == 3
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.center,
                    //           children: [
                    //             SvgPicture.asset(
                    //                 'assets/Icons/Product_page_icons/star.svg'
                    //             ),
                    //             SizedBox(width: 5,),
                    //             Text(
                    //               '3',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: ratingCheck == 3
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           ratingCheck = 4;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 54,
                    //         decoration: BoxDecoration(
                    //             color: ratingCheck == 4
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.center,
                    //           children: [
                    //             SvgPicture.asset(
                    //                 'assets/Icons/Product_page_icons/star.svg'
                    //             ),
                    //             SizedBox(width: 5,),
                    //             Text(
                    //               '4',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: ratingCheck == 4
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           ratingCheck = 5;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         width: 54,
                    //         decoration: BoxDecoration(
                    //             color: ratingCheck == 5
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Row(
                    //           mainAxisAlignment: MainAxisAlignment.center,
                    //           children: [
                    //             SvgPicture.asset(
                    //                 'assets/Icons/Product_page_icons/star.svg'
                    //             ),
                    //             SizedBox(width: 5,),
                    //             Text(
                    //               '5',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: ratingCheck == 5
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Divider(
                    //   thickness: 1,
                    //   color: customSmoothGreyColor,
                    // ),
                    // SizedBox(height: 12,),
                    // ///---services-choice
                    // Text(
                    //   'Service',
                    //   style: Theme.of(context).textTheme.headline5.copyWith(
                    //       color: customDarkBlackColor
                    //   ),
                    // ),
                    // SizedBox(height: 12,),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.start,
                    //   children: [
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           servicesCheck = 1;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         decoration: BoxDecoration(
                    //             color: servicesCheck == 1
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Padding(
                    //             padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //             child: Text(
                    //               'Installments',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: servicesCheck == 1
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           servicesCheck = 2;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         decoration: BoxDecoration(
                    //             color: servicesCheck == 2
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Padding(
                    //             padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //             child: Text(
                    //               'COD',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: servicesCheck == 2
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           servicesCheck = 3;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         decoration: BoxDecoration(
                    //             color: servicesCheck == 3
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Padding(
                    //             padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //             child: Text(
                    //               'Free Shipping',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: servicesCheck == 3
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Divider(
                    //   thickness: 1,
                    //   color: customSmoothGreyColor,
                    // ),
                    // SizedBox(height: 12,),
                    // ///---location-choice
                    // Text(
                    //   'Location',
                    //   style: Theme.of(context).textTheme.headline5.copyWith(
                    //       color: customDarkBlackColor
                    //   ),
                    // ),
                    // SizedBox(height: 12,),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.start,
                    //   children: [
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           locationCheck = 1;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         decoration: BoxDecoration(
                    //             color: locationCheck == 1
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Padding(
                    //             padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //             child: Text(
                    //               'United States',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: locationCheck == 1
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(width: 4,),
                    //     InkWell(
                    //       onTap: (){
                    //         setState(() {
                    //           locationCheck = 2;
                    //         });
                    //       },
                    //       child: Container(
                    //         height: 33,
                    //         decoration: BoxDecoration(
                    //             color: locationCheck == 2
                    //                 ?customSelectorColor
                    //                 :customIconListTileColor
                    //         ),
                    //         child: Center(
                    //           child: Padding(
                    //             padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    //             child: Text(
                    //               'China',
                    //               style: Theme.of(context).textTheme.headline4.copyWith(
                    //                   fontSize: 12,
                    //                   color: locationCheck == 2
                    //                       ?customThemeColor
                    //                       :customLightBlackColor
                    //               ),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Divider(
                    //   thickness: 1,
                    //   color: customSmoothGreyColor,
                    // ),
                    ///---categories
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Categories',
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customDarkBlackColor),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Wrap(
                      children: List.generate(
                          getShopPageDataModel.data.categories.length, (index) {
                        return Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                          child: InkWell(
                            onTap: () {
                              categoryFilterColorSelectorList
                                  .forEach((element) {
                                setState(() {
                                  element.isSelected = false;
                                });
                              });
                              setState(() {
                                categoryFilterColorSelectorList[index]
                                    .isSelected = true;

                                productFilterModel.selectedCategorySlug =
                                    categoryFilterColorSelectorList[index].slug;
                              });
                            },
                            child: Container(
                              height: 33,
                              decoration: BoxDecoration(
                                  color: categoryFilterColorSelectorList[index]
                                          .isSelected
                                      ? customSelectorColor
                                      : customIconListTileColor),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: Text(
                                      '${getShopPageDataModel.data.categories[index].name}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4
                                          .copyWith(
                                              fontSize: 12,
                                              color:
                                                  categoryFilterColorSelectorList[
                                                              index]
                                                          .isSelected
                                                      ? customThemeColor
                                                      : customLightBlackColor),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                    Divider(
                      thickness: 1,
                      color: customSmoothGreyColor,
                    ),

                    ///---brands
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Brands',
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customDarkBlackColor),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Wrap(
                      children: List.generate(
                          getShopPageDataModel.data.brands.length, (index) {
                        return Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                brandFilterColorSelectorList[index].isSelected =
                                    !brandFilterColorSelectorList[index]
                                        .isSelected;
                                productFilterModel.selectedBrandList.add(
                                    brandFilterColorSelectorList[index].id);
                              });
                            },
                            child: Container(
                              height: 33,
                              decoration: BoxDecoration(
                                  color: brandFilterColorSelectorList[index]
                                          .isSelected
                                      ? customSelectorColor
                                      : customIconListTileColor),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: Text(
                                      '${getShopPageDataModel.data.brands[index].name}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4
                                          .copyWith(
                                              fontSize: 12,
                                              color:
                                                  brandFilterColorSelectorList[
                                                              index]
                                                          .isSelected
                                                      ? customThemeColor
                                                      : customLightBlackColor),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                    Divider(
                      thickness: 1,
                      color: customSmoothGreyColor,
                    ),

                    ///---attributes
                    Wrap(
                      children: List.generate(
                          getShopPageDataModel.data.attributes.length,
                          (attributesIndex) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 12,
                            ),
                            Row(
                              children: [
                                Text(
                                  '${getShopPageDataModel.data.attributes[attributesIndex].name}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(color: customDarkBlackColor),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            Wrap(
                              children: List.generate(
                                  getShopPageDataModel
                                      .data
                                      .attributes[attributesIndex]
                                      .values
                                      .length, (valuesIndex) {
                                return Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 10, 10, 0),
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        getShopPageDataModel
                                                .data
                                                .attributes[attributesIndex]
                                                .values[valuesIndex]
                                                .isSelected =
                                            !getShopPageDataModel
                                                .data
                                                .attributes[attributesIndex]
                                                .values[valuesIndex]
                                                .isSelected;
                                      });

                                      getShopPageDataModel.data.attributes.forEach((element1) {
                                        element1.values.forEach((element2) {
                                          if(element2.isSelected){
                                            productFilterModel.selectedAttributesList.add(
                                                {
                                                  'attribute_id': element1.id,
                                                  'slug': element2.slug
                                                }
                                            );
                                            print('selectedAttribute--->>${element1.id}');
                                            print('selectedValueSlug--->>${element2.slug}');
                                          }

                                        });
                                      });

                                    },
                                    child: Container(
                                      height: 33,
                                      decoration: BoxDecoration(
                                          color: getShopPageDataModel
                                                  .data
                                                  .attributes[attributesIndex]
                                                  .values[valuesIndex]
                                                  .isSelected
                                              ? customSelectorColor
                                              : customIconListTileColor),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: Text(
                                              '${getShopPageDataModel.data.attributes[attributesIndex].values[valuesIndex].name}',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline4
                                                  .copyWith(
                                                      fontSize: 12,
                                                      color: getShopPageDataModel
                                                              .data
                                                              .attributes[
                                                                  attributesIndex]
                                                              .values[
                                                                  valuesIndex]
                                                              .isSelected
                                                          ? customThemeColor
                                                          : customLightBlackColor),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }),
                            ),
                            Divider(
                              thickness: 1,
                              color: customSmoothGreyColor,
                            ),
                          ],
                        );
                      }),
                    ),

                    ///---price-choice
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Price Range',
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customDarkBlackColor),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    RangeSlider(
                      values: _currentRangeValues,
                      min: 0,
                      max: 10000,
                      divisions: 1000,
                      labels: RangeLabels(
                        '\$${_currentRangeValues.start.round().toString()}',
                        '\$${_currentRangeValues.end.round().toString()}',
                      ),
                      onChanged: (RangeValues values) {
                        setState(() {
                          print('RangeSlider---->> ${values.end}');
                          productFilterModel.minPrice = values.start;
                          productFilterModel.maxPrice = values.end;
                          _currentRangeValues = values;
                        });
                      },
                    ),
                    Divider(
                      thickness: 1,
                      color: customSmoothGreyColor,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 6,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {
                // Navigator.pop(context);
                Get.find<HomeController>()
                    .changeGetShopPageProductsDataCheck(false);

                setState(() {
                  productFilterModel = new ProductFilterModel();
                  productFilterModel.selectedSorting =
                      ProductFilterModelForSort();
                  productFilterModel.selectedBrandList = [];
                  productFilterModel.selectedAttributesList = [];
                  productFilterModel.minPrice = 0;
                  productFilterModel.maxPrice = 1;
                  productFilterModel.selectedPage = null;
                  _currentRangeValues = RangeValues(
                      productFilterModel.minPrice, productFilterModel.maxPrice);
                });
                categoryFilterColorSelectorList.forEach((element) {
                  setState(() {
                    element.isSelected = false;
                  });
                });
                brandFilterColorSelectorList.forEach((element) {
                  setState(() {
                    element.isSelected = false;
                  });
                });
                getShopPageDataModel.data.attributes.forEach((element) {
                  element.values.forEach((element) {
                    setState(() {
                      element.isSelected = false;
                    });
                  });
                });

                ///------get-shop-page-filtered-products-api-call\
                // getMethodForFilterProduct(context, productFilterModel);
              },
              child: Container(
                height: 40,
                width: 119,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: customRatingStartColor),
                    color: Colors.white),
                child: Center(
                  child: Text(
                    'Reset All',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(fontSize: 15, color: customRatingStartColor),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                Get.find<HomeController>()
                    .changeGetShopPageProductsDataCheck(false);

                getMethodForFilterProduct(context, productFilterModel);
              },
              child: Container(
                height: 40,
                width: 119,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: customThemeColor),
                child: Center(
                  child: Text(
                    'Apply',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ColorChangeSelectorClass {
  ColorChangeSelectorClass({this.id, this.name, this.slug, this.isSelected});

  int id;
  String name;
  String slug;
  bool isSelected;
}
