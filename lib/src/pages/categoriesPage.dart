import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/allCategoriesRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';
import 'package:multi_vendor_customer/src/repository/categoriesViewRepository.dart';

import 'homeModules/home1/widgets/homeDrawerComponent.dart';
class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  /// categories page1
  Widget categoriesView1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ///------------------------search-bar
        _searchBar(context),

        ///------------------------title
        _titleWidget(context),

        ///-------------------------gridViewWidget
        _gridViewWidget(context),
      ],
    );
  }

  @override
  void initState() {
    super.initState();

    ///------get-all-categories-api-call
    getMethod(context, getAllCategoriesApi, null,
        false, getAllCategories);
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      drawer: MyCustomDrawer(),
      appBar: MyCustomAppBar(drawerShow: true,),
      body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
            child: categoriesView1(),
          )),
    )
    ;
  }

  ///--------------search-bar-widget
  Widget _searchBar(BuildContext context) {
    return Container(
      color: Color(0xffF9F9F9),
      child: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Theme(
          data: ThemeData(
              primaryColor: Color(0xff363636)
          ),
          child: TextFormField(
            decoration: InputDecoration(
                suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                border: InputBorder.none,
                hintText: 'search here',
                hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
            ),
          ),
        ),
      ),
    );
  }

  ///--------------title-widget
  Widget _titleWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 25, 0, 20),
      child: Text(
        'Categories',
        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 23),
      ),
    );
  }

  ///--------------gridView-widget
  Widget _gridViewWidget(BuildContext context) {
    return GetBuilder<HomeController>(
        init: HomeController(),
        builder: (_) {
          return !_.getAllCategoriesCheck
              ?
              //----------------- skeleton loader
              SkeletonLoader(
                  period: Duration(seconds: 2),
                  highlightColor: Colors.grey,
                  direction: SkeletonDirection.ltr,
                  builder: Expanded(
                    child: GridView.count(
                        padding: EdgeInsets.symmetric(horizontal: 2),
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        crossAxisCount: 3,
                        childAspectRatio: 1 / 1.4,
                        children: List.generate(
                            10,
                            (index) => Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ))),
                  ))
              :
              // --------------- display data
              Expanded(
                  child: GridView.count(
                      padding: EdgeInsets.symmetric(horizontal: 2),
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      crossAxisCount: 3,
                      childAspectRatio: 1 / 1.4,
                      children: List.generate(
                          getAllCategoriesModel.data.length,
                          (index) => InkWell(
                            onTap: (){
                              categoriesViewRepository(index);
                            },
                            child: Container(

                                  decoration: BoxDecoration(
                                      color: Theme.of(context)
                                          .scaffoldBackgroundColor,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.2),
                                            spreadRadius: 0.9,
                                            blurRadius: 1,
                                            offset: Offset(0.9, 1))
                                      ]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      getAllCategoriesModel.data[index].image ==
                                              null
                                          ?
                                          // category with no image
                                          Expanded(
                                              flex: 2,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        10, 10, 0, 15),
                                                child: Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      // border: Border.all(color: Colors.grey),
                                                      color: Colors.grey
                                                          .withOpacity(0.4)),
                                                  child: Center(
                                                    child: Icon(
                                                      Icons.category,
                                                      color: Theme.of(context)
                                                          .buttonColor,
                                                      size: 25,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          :
                                          // category with image
                                          Expanded(
                                              flex: 2,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        10, 10, 0, 15),
                                                child: Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          spreadRadius: 1.5,
                                                        )
                                                      ],
                                                      // border: Border.all(color: Colors.grey),
                                                      image: DecorationImage(
                                                          image: NetworkImage(
                                                            '${baseUrl + getAllCategoriesModel.data[index].icon.originalMediaPath}',
                                                          ),
                                                          fit: BoxFit.fill),
                                                      shape: BoxShape.circle),
                                                ),
                                              ),
                                            ),

                                      // category name
                                      Flexible(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 0, 0, 10),
                                          child: Text(
                                            '${getAllCategoriesModel.data[index].name}',
                                            // textAlign: TextAlign.center,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle2
                                                .copyWith(color: customDarkGreyColor),
                                            softWrap: true,
                                            maxLines: 3,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          ))),
                );
        });
  }
}
