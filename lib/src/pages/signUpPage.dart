import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  bool obscureText = true;
  bool obscureTextConfirm = true;

  GlobalKey<FormState> _signUpFromKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: Form(
                      key: _signUpFromKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: MediaQuery.of(context).size.height*.04,),
                          Text(
                            'Sign Up',
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*.01,),
                          Text(
                            'Create Your New Account',
                            style: Theme.of(context).textTheme.subtitle2.copyWith(color: customLightBlackColor),
                          ),
                          ///---email-section
                          SizedBox(height: MediaQuery.of(context).size.height*.07,),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Email',
                                style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                              decoration: InputDecoration(
                                hintText: 'Enter Email',
                                hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(width: 1, color: customGreenSaleFlagColor),
                                ),
                                errorBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(width: 1, color: customRedColor),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                ),
                              ),
                              validator: (value) {
                                Pattern pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                RegExp regex = new RegExp(pattern);
                                if (value.isEmpty) {
                                  return 'Field Required';
                                } else if (!regex.hasMatch(value))
                                  return 'Please make sure your email address is valid';
                                else
                                  return null;
                              },
                            ),
                          ),
                          ///---password-section
                          SizedBox(height: MediaQuery.of(context).size.height*.05,),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Password',
                                style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              obscureText: obscureText,
                              style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                              decoration: InputDecoration(
                                  hintText: 'Enter Password',
                                  hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: customGreenSaleFlagColor
                                    ),
                                  ),
                                  errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(width: 1, color: customRedColor),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                  ),
                                  suffixIcon: InkWell(
                                      onTap: (){
                                        setState(() {
                                          obscureText = !obscureText;
                                        });
                                      },
                                      child: Icon(
                                        obscureText
                                            ?Icons.visibility
                                            :Icons.visibility_off,
                                        size: 20,
                                        color: customLightGreyColor,))
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Field Required";
                                }
                                return null;
                              },
                            ),
                          ),
                          ///---confirm-password-section
                          SizedBox(height: MediaQuery.of(context).size.height*.05,),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Confirm Password',
                                style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              obscureText: obscureText,
                              style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                              decoration: InputDecoration(
                                  hintText: 'Enter Password',
                                  hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: customGreenSaleFlagColor
                                    ),
                                  ),
                                  errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(width: 1, color: customRedColor),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(width: 1, color: customBorderGreyColor),
                                  ),
                                  suffixIcon: InkWell(
                                      onTap: (){
                                        setState(() {
                                          obscureTextConfirm = !obscureTextConfirm;
                                        });
                                      },
                                      child: Icon(
                                        obscureTextConfirm
                                            ?Icons.visibility
                                            :Icons.visibility_off,
                                        size: 20,
                                        color: customLightGreyColor,))
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Field Required";
                                }
                                return null;
                              },
                            ),
                          ),
                          ///---sign-up-button
                          SizedBox(height: MediaQuery.of(context).size.height*.08,),
                          Padding(
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Container(
                              height: 40,
                              width: MediaQuery.of(context).size.width*.75,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: customThemeColor
                              ),
                              child: Center(
                                child: Text(
                                  'Sign Up',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      fontSize: 15,
                                      color: Colors.white
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*.05,),
                          Text(
                            'Or Sign Up With',
                            style: Theme.of(context).textTheme.headline4.copyWith(fontSize: 12,color: customLightBlackColor),
                          ),
                          ///---social-buttons
                          SizedBox(height: MediaQuery.of(context).size.height*.02,),
                          Padding(
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Container(
                              height: 40,
                              width: MediaQuery.of(context).size.width*.75,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: customRedColor),
                                  color: Colors.white
                              ),
                              child: Center(
                                child: Text(
                                  'Google',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      fontSize: 15,
                                      color: customRedColor
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*.02,),
                          Padding(
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Container(
                              height: 40,
                              width: MediaQuery.of(context).size.width*.75,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: customBlueColor),
                                  color: Colors.white
                              ),
                              child: Center(
                                child: Text(
                                  'Facebook',
                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      fontSize: 15,
                                      color: customBlueColor
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*.05,),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Already have an account?',
                                    style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                                  ),
                                  InkWell(
                                    onTap: (){
                                      Get.offNamed(PageRoutes.login);
                                    },
                                    child: Text(
                                      ' Login',
                                      style: Theme.of(context).textTheme.headline5,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
