import 'package:flutter/material.dart';

class EnvironmentMaintenancePage extends StatefulWidget {
  @override
  _EnvironmentMaintenancePageState createState() => _EnvironmentMaintenancePageState();
}

class _EnvironmentMaintenancePageState extends State<EnvironmentMaintenancePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 3,
              child: ClipRRect(
                child: Image.asset('assets/images/maintenanceImage.png')
              )
            ),
            Spacer(),
            Expanded(
              flex: 1,
              child: Text(
                'UNDER\nMAINTENANCE',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 40),
              ),
            )
          ],
        ),
      ),
    );
  }
}

