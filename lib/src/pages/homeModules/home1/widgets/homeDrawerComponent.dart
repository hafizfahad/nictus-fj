import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';

class MyCustomDrawer extends StatefulWidget {
  @override
  _MyCustomDrawerState createState() => _MyCustomDrawerState();
}

class _MyCustomDrawerState extends State<MyCustomDrawer> {
  @override
  void initState() {
    super.initState();
    // Add code after super
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ClipRRect(
        child: Drawer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                color: Theme.of(context).buttonColor,
                height: 100,
                width: double.infinity,
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 25,
                        child: Icon(Icons.person_outline_outlined,size: 30,),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Login & Register',
                            textAlign: TextAlign.left,
                            style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white),
                          ),
                          Text(
                            'Please login or create an account for free',
                            textAlign: TextAlign.left,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.subtitle2.copyWith(color: Colors.white),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                  flex: 4,
                  child: DrawerList()
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DrawerList extends StatefulWidget {
  final viewChange;
  DrawerList({
    this.viewChange,
  });
  @override
  _DrawerListState createState() => _DrawerListState();
}

class _DrawerListState extends State<DrawerList> {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height*.5,
              child: ListView(
                children: [
                  ListTile(
                    onTap: (){

                      Get.find<HomeController>().changeGetAllCategoriesCheck(false);
                      Get.find<HomeController>().changeGetAllGenericAppDataCheck(false);
                      Get.find<HomeController>().changeGetCategoryWiseProductsDataCheck(false);
                      Get.find<HomeController>().changeGetAFlashSaleDataCheck(false);
                      Get.find<HomeController>().changeGetTopSellingProductsDataCheck(false);

                      Get.offAllNamed('/bottomNavBar');
                    },
                    leading: Icon(Icons.home_outlined),
                    title: Text(
                      'Home',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.category_outlined),
                    title: Text(
                      'Categories',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.shop),
                    title: Text(
                      'Shop',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),ListTile(
                    onTap: (){},
                    leading: Icon(Icons.favorite),
                    title: Text(
                      'My Wish List',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.phone),
                    title: Text(
                      'Contact Us',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.info),
                    title: Text(
                      'About Us',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.insert_drive_file_rounded),
                    title: Text(
                      'News',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.perm_device_info_rounded),
                    title: Text(
                      'Intro',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){},
                    leading: Icon(Icons.star_rate_outlined),
                    title: Text(
                      'Rate Us',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  ListTile(
                    onTap: (){
                      Get.toNamed('/settings');
                    },
                    leading: Icon(Icons.settings),
                    title: Text(
                      'Settings',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  )
                ]
              ),
            )
        ),
      ],
    );
  }
}
