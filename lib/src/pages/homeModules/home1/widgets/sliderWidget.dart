
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeSliderWidget extends StatefulWidget {
  @override
  _HomeSliderWidgetState createState() => _HomeSliderWidgetState();
}
class _HomeSliderWidgetState extends State<HomeSliderWidget> {

  int _currentSliderIndexForImageSlider;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => !_.getAllGenericAppDataCheck
          ?SkeletonLoader(
        period: Duration(seconds: 2),
        highlightColor: Colors.grey,
        direction: SkeletonDirection.ltr,
        builder: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 130,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)
                    ),
                  ),
                ),
                SizedBox(height: 5,),
                Container(
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)
                  ),
                ),
              ],
            )
        ),
      )
          :genericAppDataModel.data.sliderImages.length == 0
          ?SizedBox()
          :Column(
        children: [
          Container(
            height: 130,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
              child: CarouselSlider(
                items: List.generate(genericAppDataModel.data.sliderImages.length, (index){
                  return Container(
                    // margin: EdgeInsets.symmetric(horizontal: 5.0),
                    // height: 400,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15)
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: Image.network(
                        '${baseUrl+genericAppDataModel.data.sliderImages[index].image.originalMediaPath}',
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.fill,
                      ),
                    ),
                  );
                }),

                ///Slider Container properties
                options: CarouselOptions(

                  onPageChanged: (index, value){
                    setState(() {
                      _currentSliderIndexForImageSlider = index;
                    });
                  },
                  // height: 600,
                  // aspectRatio: 16/9,
                  viewportFraction: 1,
                  // initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 3),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  // enlargeCenterPage: true,
                  // onPageChanged: callbackFunction,
                  scrollDirection: Axis.horizontal,
                ),

              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: genericAppDataModel.data.sliderImages.map((image) {       //these two lines
              int index = genericAppDataModel.data.sliderImages.indexOf(image); //are changed
              return Container(
                width: _currentSliderIndexForImageSlider == index
                    ?10
                    :5.0,
                height: 5.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: _currentSliderIndexForImageSlider == index
                        ? customThemeColor
                        : Color.fromRGBO(0, 0, 0, 0.4)),
              );
            },
            ).toList(),
          ),
        ],
      ),
    );
  }
}