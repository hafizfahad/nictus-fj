import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/getProductsOfVendorRepository.dart';
import 'package:multi_vendor_customer/src/repository/getVendorStoreDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeFeaturedVendorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => _.getFeaturedVendorsProductsDataCheck && getFeaturedVendorsModel.data.length == 0
          ?SizedBox()
          :Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 25, 15, 9 ),
              child: Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          'Featured Vendors',
                          style: Theme.of(context).textTheme.headline1
                      ),
                    ),
                  ],
                ),
              ),
            ),
            !_.getFeaturedVendorsProductsDataCheck
                ?SkeletonLoader(
                period: Duration(seconds: 2),
                highlightColor: Colors.grey,
                direction: SkeletonDirection.ltr,
                builder: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
                    child: Center(
                        child: Container(
                          height: 272,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: customVendorCardColor,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset: Offset(0,1),
                              ),
                            ],
                          ),
                        )
                    ),
                  ),
                )
            )
                :Wrap(
              children: List.generate(getFeaturedVendorsModel.data.length, (index){
                return Container(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
                    child: Center(
                        child: Container(
                          height: 272,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: customVendorCardColor,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset: Offset(0,1),
                              ),
                            ],
                          ),
                          child: Stack(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  getFeaturedVendorsModel.data[index].store.coverImage == null
                                      ?Image.asset(
                                    'assets/images/vendorCover.png',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 80,
                                  )
                                      :Image.network(
                                    '${baseUrl+getFeaturedVendorsModel.data[index].store.coverImage.originalMediaPath}',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 80,
                                  ),
                                  SizedBox(height: 28,),
                                  ///------vendor-name
                                  Center(
                                    child: Text(
                                      '${getFeaturedVendorsModel.data[index].store.name}',
                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                          color: customLightBlackColor,
                                          fontSize: 14
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 2,),
                                  ///------vendor-desc
                                  InkWell(
                                    onTap: (){

                                      ///------get-vendor-products-api-call
                                      getMethod(
                                          context,
                                          '$getVendorDetailDataApi/${getFeaturedVendorsModel.data[index].slug}/products',
                                          null,
                                          false,
                                          getProductsOfVendorData
                                      );

                                      ///------get-vendor-detail-api-call
                                      getMethod(
                                          context,
                                          '$getVendorDetailDataApi/${getFeaturedVendorsModel.data[index].slug}',
                                          null,
                                          false,
                                          getVendorStoreDetailData
                                      );

                                      Get.toNamed(PageRoutes.vendorStore);
                                    },
                                    child: Center(
                                      child: Text(
                                        'visit store',
                                        style: Theme.of(context).textTheme.headline4.copyWith(
                                            decoration: TextDecoration.underline,
                                            color: customThemeColor
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 12,),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12),
                                    child: Text(
                                      'Top Products',
                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                          fontSize: 12,
                                          color: customLightBlackColor
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(12, 8, 12, 12),
                                    child: SizedBox(
                                      height: 70,
                                      width: double.infinity,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: List.generate(getFeaturedVendorsModel.data[index]
                                            .products.length, (productIndex){
                                          return Padding(
                                            padding: const EdgeInsets.fromLTRB(0, 0, 12, 0),
                                            child: Container(
                                              height: 70,
                                              width: 70,
                                              child: getFeaturedVendorsModel.data[index]
                                                  .products[productIndex].media[0].originalMediaPath == null
                                                  ?Image.asset(
                                                'assets/images/vendorProduct1.png',
                                                fit: BoxFit.cover,
                                              )
                                                  :Image.network(
                                                '${baseUrl+getFeaturedVendorsModel.data[index]
                                                    .products[productIndex].media[0].originalMediaPath}',
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          );
                                        }),
                                      ),
                                    )
                                  )
                                ],
                              ),
                              Positioned(
                                top: 55,
                                left: 0,
                                right: 0,
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: getFeaturedVendorsModel.data[index].store.storeLogo == null
                                        ? CircleAvatar(
                                        backgroundColor: Colors.brown,
                                        radius: 25,
                                        child: Image.asset('assets/images/product_detail_circle_image.png')
                                    )
                                        :Container(
                                      height: 50,
                                      width: 50,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(25),
                                        image: DecorationImage(
                                          image: NetworkImage(
                                              '${baseUrl+
                                                  getFeaturedVendorsModel.data[index].store.storeLogo.originalMediaPath}'
                                          ),
                                          fit: BoxFit.fill
                                        )
                                      ),
                                    )
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                    ),
                  ),
                );
              }),
            )
          ],
        ),
      ),
    );
  }
}
