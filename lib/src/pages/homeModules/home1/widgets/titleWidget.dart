import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class HomeTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 5, 15, 0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'WELCOME,',
              style: Theme.of(context).textTheme.subtitle1.copyWith(color: customLightBlackColor),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 5, 0),
              child: SvgPicture.asset(
                'assets/Icons/user.svg',
                color: Theme.of(context).buttonColor,
                width: 14,
              ),
            ),
            currentUserModel == null
                ?InkWell(
              onTap: (){
                Get.find<AppController>().changeLoaderCheck(false);
                Get.toNamed(PageRoutes.login);
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                child: Text(
                  'Login',
                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                      color: customThemeColor
                  ),
                ),
              ),
            )
                :Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
              child: Text(
                '${currentUserModel.name}',
                style: Theme.of(context).textTheme.subtitle2.copyWith(
                    color: customThemeColor
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
