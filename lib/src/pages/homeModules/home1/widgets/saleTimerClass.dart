
import 'package:flutter/material.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class FlashSaleTimer extends StatelessWidget {
  final AnimationController controller;
  final bool largeWidgetCheck;
  FlashSaleTimer(this.controller,this.largeWidgetCheck);

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inDays}:${duration.inHours % 24}:${duration.inMinutes % 60}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';

  }

  Duration get duration {
    Duration duration = controller.duration;
    return duration;
  }

  Duration get durationTest {
    Duration duration = controller.duration * controller.value;
    return duration;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context, Widget child) {
          return largeWidgetCheck
              ?Row(
            children: [
              Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                    color: customThemeColor,
                    borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${durationTest.inDays}',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                          fontSize: 10
                        ),
                      ),
                      Text(
                        'Days',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5,right: 5),
                child: Text(
                  ':',
                  style: TextStyle(
                      color: customThemeColor,
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                  color: customThemeColor,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${durationTest.inHours % 24}',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                      Text(
                        'Hour',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5,right: 5),
                child: Text(
                  ':',
                  style: TextStyle(
                      color: customThemeColor,
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                  color: customThemeColor,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${durationTest.inMinutes % 60}',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                      Text(
                        'Min',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5,right: 5),
                child: Text(
                  ':',
                  style: TextStyle(
                      color: customThemeColor,
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                  color: customThemeColor,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${durationTest.inSeconds % 60}',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                      Text(
                        'Sec',
                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Colors.white,
                            fontSize: 10
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
              : Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Container(
                      height: 16,
                      width: 16,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(color: customThemeColor)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${
                                  (durationTest.inHours%24).toString().length >= 2
                                      ?(durationTest.inHours%24).toString()[0]
                                      :'0'
                              }',
                              style: Theme.of(context).textTheme.headline3.copyWith(
                                  fontSize: 10
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Container(
                      height: 16,
                      width: 16,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(color: customThemeColor)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${
                                  (durationTest.inHours%24).toString().length < 2
                                      ?(durationTest.inHours%24).toString()
                                      :(durationTest.inHours%24).toString()[1]
                              }',
                              style: Theme.of(context).textTheme.headline3.copyWith(
                                  fontSize: 10
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Text(
                      ':',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                        color: customThemeColor
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Container(
                      height: 16,
                      width: 16,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(color: customThemeColor)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${
                                  (durationTest.inMinutes%60).toString().length >= 2
                                  ?(durationTest.inMinutes%60).toString()[0]
                                  :'0'
                              }',
                              style: Theme.of(context).textTheme.headline3.copyWith(
                                  fontSize: 10
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Container(
                      height: 16,
                      width: 16,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(color: customThemeColor)
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${
                                  (durationTest.inMinutes%60).toString().length < 2
                                      ?(durationTest.inMinutes%60).toString()
                                      :(durationTest.inMinutes%60).toString()[1]
                              }',
                              style: Theme.of(context).textTheme.headline3.copyWith(
                                  fontSize: 10
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // Container(
                  //   height: 100,
                  //   width: 30,
                  //   decoration: BoxDecoration(
                  //       color: Colors.white,
                  //       borderRadius: BorderRadius.circular(5),
                  //       border: Border.all(color: Color(0xffD4BE8B))
                  //   ),
                  //   child: Center(
                  //     child: Column(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: [
                  //         Text(
                  //           '${durationTest.inHours % 24}',
                  //           style: Theme.of(context).textTheme.headline3.copyWith(
                  //               fontSize: 12
                  //           ),
                  //         ),
                  //         Text(
                  //           'Hour',
                  //           style: Theme.of(context).textTheme.headline3.copyWith(
                  //               fontSize: 9
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 5,right: 5),
                  //   child: Text(
                  //     ':',
                  //     style: TextStyle(
                  //         color: Colors.black,
                  //         fontSize: 14,
                  //         fontWeight: FontWeight.bold
                  //     ),
                  //   ),
                  // ),
                  // Container(
                  //   height: 100,
                  //   width: 30,
                  //   decoration: BoxDecoration(
                  //       color: Colors.white,
                  //       borderRadius: BorderRadius.circular(5),
                  //       border: Border.all(color: Color(0xffD4BE8B))
                  //   ),
                  //   child: Center(
                  //     child: Column(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: [
                  //         Text(
                  //           '${durationTest.inMinutes % 60}',
                  //           style: Theme.of(context).textTheme.headline3.copyWith(
                  //               fontSize: 12
                  //           ),
                  //         ),
                  //         Text(
                  //           'Min',
                  //           style: Theme.of(context).textTheme.headline3.copyWith(
                  //               fontSize: 9
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 5,right: 5),
                  //   child: Text(
                  //     ':',
                  //     style: TextStyle(
                  //         color: Colors.black,
                  //         fontSize: 14,
                  //         fontWeight: FontWeight.bold
                  //     ),
                  //   ),
                  // ),
                  // Container(
                  //   height: 100,
                  //   width: 30,
                  //   decoration: BoxDecoration(
                  //       color: Colors.white,
                  //       borderRadius: BorderRadius.circular(5),
                  //       border: Border.all(color: Color(0xffD4BE8B))
                  //   ),
                  //   child: Center(
                  //     child: Column(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: [
                  //         Text(
                  //           '${durationTest.inSeconds % 60}',
                  //           style: Theme.of(context).textTheme.headline3.copyWith(
                  //               fontSize: 12
                  //           ),
                  //         ),
                  //         Text(
                  //           'Sec',
                  //           style: Theme.of(context).textTheme.headline3.copyWith(
                  //               fontSize: 9
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                ],
              );
        });
  }
}