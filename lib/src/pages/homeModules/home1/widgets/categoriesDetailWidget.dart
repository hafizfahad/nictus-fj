
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

class HomeCategoriesDetailWidget extends StatefulWidget {
  @override
  _HomeCategoriesDetailWidgetState createState() => _HomeCategoriesDetailWidgetState();
}
class _HomeCategoriesDetailWidgetState extends State<HomeCategoriesDetailWidget>
    with TickerProviderStateMixin{

  TabController allCategoriesTabController;
  ScrollController _scrollViewController;
  bool _showAppbar = true;
  bool isScrollingDown = false;
  @override
  void initState() {
    super.initState();
    allCategoriesTabController = new TabController(length: ((getCategoryWiseProductsModel.data.otherCats.length)+1), vsync: this);
    _scrollViewController = new ScrollController();
    _scrollViewController.addListener(() {
      print('listner');
      if (_scrollViewController.position.userScrollDirection == ScrollDirection.reverse) {
        print('reverse');
        if (!isScrollingDown) {
          isScrollingDown = true;
          _showAppbar = false;
          setState(() {});
        }
      }

      if (_scrollViewController.position.userScrollDirection == ScrollDirection.forward) {
        print('forward');
        if (isScrollingDown) {
          isScrollingDown = false;
          _showAppbar = true;
          setState(() {});
        }
      }
    });
  }
  @override
  void dispose() {
    allCategoriesTabController.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }
  int _selectedTabBar = 0;
  ValueNotifier<int> _reBuilder = ValueNotifier<int>(0);
  
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_)=> !_.getCategoryWiseProductsDataCheck
          ?SizedBox()
          :getCategoryWiseProductsModel.data.allCategories.length == 0 ||
          getCategoryWiseProductsModel.data.otherCats.length == 0
          ?SizedBox()
        :ValueListenableBuilder(
        builder: (BuildContext context, int value, Widget child) {
          // This builder will only get called when the _counter
          // is updated.
          return Padding(
            padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: StickyHeader(
                header: Container(
                  color:Colors.white,
                  height: 30,
                  width: double.infinity,
                  child: TabBar(
                      onTap: (index){
                        setState(() {
                          _selectedTabBar = index;
                          _reBuilder = ValueNotifier<int>(_selectedTabBar);
                        });
                      },
                      controller: allCategoriesTabController,
                      labelColor: Theme.of(context).buttonColor,
                      unselectedLabelColor: Colors.grey,
                      indicatorColor: Theme.of(context).buttonColor,
                      indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(
                            width: 2.0,
                            color: Theme.of(context).buttonColor,
                          ),
                          insets: EdgeInsets.symmetric(horizontal:16.0)
                      ),
                      labelStyle: Theme.of(context).textTheme.headline5,
                      isScrollable: true,
                      tabs: categoryWiseProductsTabList
                    /*[
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'All Categories',
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Accessories',
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Laptops',
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Cell Phones',
                          ),
                        ],
                      ),
                    ),
                  ],*/
                  ),
                ),
                content: Padding(
                  padding: const EdgeInsets.only(top: 7),
                  child: SingleChildScrollView(
                    // controller: _scrollViewController,
                    child: Builder(builder: (_) {
                      if (_selectedTabBar == 0) {
                        return GetBuilder<HomeController>(
                          init: HomeController(),
                          builder:(_)=> Center(
                            child: Wrap(
                              children: List.generate(getCategoryWiseProductsModel.data.allCategories.length, (index){
                                return Container(
                                  width: MediaQuery.of(context).size.width*.45,
                                  child: Padding(
                                    padding: index%2 == 0
                                        ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                                        :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                                    child: Stack(
                                        children: [
                                          InkWell(
                                            onTap: (){
                                              ///------get-product-detail-data-api-call
                                              getMethod(
                                                  context,
                                                  '$getProductsDetailApi/${getCategoryWiseProductsModel.data.allCategories[index].slug}',
                                                  null,
                                                  false,
                                                  getProductDetailData
                                              );
                                              Get.find<HomeController>().changeGetProductDetailDataCheck(false);
                                              Get.toNamed(PageRoutes.productDetail);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.vertical(
                                                  top: Radius.circular(10),
                                                  bottom: Radius.circular(10),
                                                ),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey.withOpacity(0.2),
                                                    spreadRadius: 2,
                                                    blurRadius: 6,
                                                    offset: Offset(0,3),
                                                  ),
                                                ],
                                              ),
                                              child: Column(
                                                children: [
                                                  ///--------------product-image
                                                  Container(
                                                    height: 145,
                                                    width: double.infinity,
                                                    child: ClipRRect(
                                                      borderRadius: BorderRadius.vertical(
                                                        top: Radius.circular(10),
                                                        bottom: Radius.circular(10),
                                                      ),
                                                      child:
                                                      getCategoryWiseProductsModel.data.allCategories[index].media.length == 0
                                                          ?Image.asset(
                                                        'assets/images/imagePlaceHolder.jpg',
                                                        fit: BoxFit.fill,
                                                      )
                                                          :Image.network('${baseUrl+getCategoryWiseProductsModel.data.allCategories[index].media[0].originalMediaPath}',
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                  ///--------------product-detail
                                                  Container(
                                                    width:
                                                    double.infinity,
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .only(
                                                          top: 8.0,
                                                          left: 10.0),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          ///--------------product-name
                                                          Text(
                                                            '${getCategoryWiseProductsModel.data.allCategories[index].name}',
                                                            softWrap:
                                                            true,
                                                            overflow:
                                                            TextOverflow
                                                                .ellipsis,
                                                            maxLines: 2,
                                                            style: Theme.of(
                                                                context)
                                                                .textTheme
                                                                .headline1
                                                                .copyWith(
                                                                fontSize:
                                                                14,
                                                                color:
                                                                customDarkGreyColor),
                                                          ),
                                                          SizedBox(
                                                            height: 4.0,
                                                          ),

                                                          ///--------------product-rating-row
                                                          Row(
                                                            children: [
                                                              RatingBar(
                                                                ignoreGestures: true,
                                                                initialRating:
                                                                getCategoryWiseProductsModel.data.allCategories[index]
                                                                    .reviewsAverageRating,
                                                                direction:
                                                                Axis.horizontal,
                                                                allowHalfRating:
                                                                true,
                                                                itemCount:
                                                                5,
                                                                itemSize:
                                                                15,
                                                                ratingWidget:
                                                                RatingWidget(
                                                                  full:
                                                                  Icon(
                                                                    Icons
                                                                        .star,
                                                                    color:
                                                                    customRatingStartColor,
                                                                  ),
                                                                  half: Icon(
                                                                      Icons
                                                                          .star_half,
                                                                      color:
                                                                      customRatingStartColor),
                                                                  empty: Icon(
                                                                      Icons
                                                                          .star_border_purple500_sharp,
                                                                      color:
                                                                      customRatingStartColor),
                                                                ),
                                                                itemPadding:
                                                                EdgeInsets.symmetric(
                                                                    horizontal: 0.0),
                                                                onRatingUpdate:
                                                                    (rating) {
                                                                  print(
                                                                      rating);
                                                                },
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets
                                                                    .only(
                                                                    left:
                                                                    3),
                                                                child:
                                                                Text(
                                                                  '(${getCategoryWiseProductsModel.data.allCategories[index].totalReviews})',
                                                                  style: Theme.of(context)
                                                                      .textTheme
                                                                      .subtitle2
                                                                      .copyWith(fontSize: 10),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          SizedBox(
                                                            height: 10.0,
                                                          ),
                                                          (getCategoryWiseProductsModel.data.allCategories[index].flashSale !=
                                                              null ||
                                                              getCategoryWiseProductsModel.data.allCategories[index].specialSale !=
                                                                  null)
                                                              ? Text(
                                                            '${getCategoryWiseProductsModel.data.allCategories[index].displayPrice}',
                                                            style: Theme.of(context)
                                                                .textTheme
                                                                .subtitle2
                                                                .copyWith(
                                                              decoration: TextDecoration.lineThrough,
                                                            ),
                                                          )
                                                              : SizedBox(),
                                                          Row(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                            children: [
                                                              ///--------------product-price
                                                              Expanded(
                                                                child: (getCategoryWiseProductsModel.data.allCategories[index].flashSale != null &&
                                                                    getCategoryWiseProductsModel.data.allCategories[index].specialSale == null)
                                                                    ? Text(
                                                                  '${getCategoryWiseProductsModel.data.allCategories[index].flashSale.displayPrice}',
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                )
                                                                    : (getCategoryWiseProductsModel.data.allCategories[index].flashSale == null &&
                                                                    getCategoryWiseProductsModel.data.allCategories[index].specialSale != null)
                                                                    ? Text(
                                                                  '${getCategoryWiseProductsModel.data.allCategories[index].specialSale.displayPrice}',
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                )
                                                                    : Text(
                                                                  '${getCategoryWiseProductsModel.data.allCategories[index].displayPrice}',
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                ),
                                                              ),
                                                              InkWell(
                                                                onTap:
                                                                    () {},
                                                                child:
                                                                Padding(
                                                                  padding: const EdgeInsets.fromLTRB(
                                                                      5,
                                                                      5,
                                                                      10,
                                                                      5),
                                                                  child:
                                                                  Icon(
                                                                    Icons
                                                                        .add_box_rounded,
                                                                    size:
                                                                    20,
                                                                    color:
                                                                    customThemeColor,
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          SizedBox(
                                                            height: 8.0,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            right: 0.0,
                                            top: 0,
                                            child: InkWell(
                                              onTap: (){
                                                print('------------->>press');
                                                if(currentUserModel != null){
                                                  ///post-method
                                                  postMethod(
                                                      context,
                                                      addToWishListApi,
                                                      {
                                                        'product_id': getCategoryWiseProductsModel.data
                                                            .allCategories[index].id
                                                      },
                                                      true,
                                                      addToWishListDataRepo
                                                  );
                                                }else{
                                                  notLoggedAlert(
                                                      context,
                                                      'Error',
                                                      'You Have to Login First'
                                                  );
                                                  print('------------->>NotLoggedIn');
                                                }
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                                child: Container(
                                                  height: 14,
                                                  width: 16,
                                                  child: Center(
                                                      child: SvgPicture.asset(
                                                        'assets/Icons/Product_page_icons/Group 366.svg',
                                                        color: customThemeColor,
                                                      )
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                              top: 10.0,
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: List.generate(
                                                    getCategoryWiseProductsModel.data
                                                        .allCategories[index].tags.length, (tagIndex){
                                                  return Column(
                                                    children: [
                                                      Container(
                                                        height: 20,
                                                        decoration: BoxDecoration(
                                                            color: customThemeColor,
                                                            borderRadius: BorderRadius.only(
                                                              topRight: Radius.circular(6.0),
                                                              bottomRight: Radius.circular(6.0),
                                                            )
                                                        ),
                                                        child: Center(
                                                            child: Padding(
                                                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                              child: Text('${getCategoryWiseProductsModel.data
                                                                  .allCategories[index].tags[tagIndex]}',
                                                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                    fontSize: 12,
                                                                    color: Colors.white
                                                                ),
                                                              ),
                                                            )),
                                                      ),
                                                      SizedBox(height: 6,),
                                                    ],
                                                  );
                                                }),
                                              )
                                          ),
                                        ]
                                    ),
                                  ),
                                );
                              }),
                            ),
                          ),
                        );
                      }
                      else return GetBuilder<HomeController>(
                        init: HomeController(),
                        builder:(_)=> Center(
                          child: Wrap(
                            children: List.generate(getCategoryWiseProductsModel.data
                                .otherCats[_selectedTabBar-1].products.length, (index){
                              return Container(
                                width: MediaQuery.of(context).size.width*.45,
                                child: Padding(
                                  padding: index%2 == 0
                                      ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                                      :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                                  child: Stack(
                                      children: [
                                        InkWell(
                                          onTap: (){
                                            ///------get-product-detail-data-api-call
                                            getMethod(
                                                context,
                                                '$getProductsDetailApi/${getCategoryWiseProductsModel.data
                                                    .otherCats[_selectedTabBar-1].products[index].slug}',
                                                null,
                                                false,
                                                getProductDetailData
                                            );
                                            Get.find<HomeController>().changeGetProductDetailDataCheck(false);
                                            Get.toNamed(PageRoutes.productDetail);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.vertical(
                                                top: Radius.circular(10),
                                                bottom: Radius.circular(10),
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.2),
                                                  spreadRadius: 2,
                                                  blurRadius: 6,
                                                  offset: Offset(0,3),
                                                ),
                                              ],
                                            ),
                                            child: Column(
                                              children: [
                                                ///--------------product-image
                                                Container(
                                                  height: 145,
                                                  width: double.infinity,
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.vertical(
                                                      top: Radius.circular(10),
                                                      bottom: Radius.circular(10),
                                                    ),
                                                    child:
                                                    getCategoryWiseProductsModel.data
                                                        .otherCats[_selectedTabBar-1].products[index].media.length == 0
                                                        ?Image.asset(
                                                      'assets/images/imagePlaceHolder.jpg',
                                                      fit: BoxFit.fill,
                                                    )
                                                        :Image.network
                                                      ('${baseUrl+
                                                        getCategoryWiseProductsModel.data
                                                            .otherCats[_selectedTabBar-1].products[index].media[0].originalMediaPath}',
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ),
                                                ///--------------product-detail
                                                Container(
                                                  width:
                                                  double.infinity,
                                                  child: Padding(
                                                    padding:
                                                    const EdgeInsets
                                                        .only(
                                                        top: 8.0,
                                                        left: 10.0),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        ///--------------product-name
                                                        Text(
                                                          '${getCategoryWiseProductsModel.data
                                                              .otherCats[_selectedTabBar-1].products[index].name}',
                                                          softWrap:
                                                          true,
                                                          overflow:
                                                          TextOverflow
                                                              .ellipsis,
                                                          maxLines: 2,
                                                          style: Theme.of(
                                                              context)
                                                              .textTheme
                                                              .headline1
                                                              .copyWith(
                                                              fontSize:
                                                              14,
                                                              color:
                                                              customDarkGreyColor),
                                                        ),
                                                        SizedBox(
                                                          height: 4.0,
                                                        ),

                                                        ///--------------product-rating-row
                                                        Row(
                                                          children: [
                                                            RatingBar(
                                                              ignoreGestures: true,
                                                              initialRating:
                                                              getCategoryWiseProductsModel.data
                                                                  .otherCats[_selectedTabBar-1].products[index]
                                                                  .reviewsAverageRating,
                                                              direction:
                                                              Axis.horizontal,
                                                              allowHalfRating:
                                                              true,
                                                              itemCount:
                                                              5,
                                                              itemSize:
                                                              15,
                                                              ratingWidget:
                                                              RatingWidget(
                                                                full:
                                                                Icon(
                                                                  Icons
                                                                      .star,
                                                                  color:
                                                                  customRatingStartColor,
                                                                ),
                                                                half: Icon(
                                                                    Icons
                                                                        .star_half,
                                                                    color:
                                                                    customRatingStartColor),
                                                                empty: Icon(
                                                                    Icons
                                                                        .star_border_purple500_sharp,
                                                                    color:
                                                                    customRatingStartColor),
                                                              ),
                                                              itemPadding:
                                                              EdgeInsets.symmetric(
                                                                  horizontal: 0.0),
                                                              onRatingUpdate:
                                                                  (rating) {
                                                                print(
                                                                    rating);
                                                              },
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets
                                                                  .only(
                                                                  left:
                                                                  3),
                                                              child:
                                                              Text(
                                                                '(${getCategoryWiseProductsModel.data
                                                                    .otherCats[_selectedTabBar-1].products[index].totalReviews})',
                                                                style: Theme.of(context)
                                                                    .textTheme
                                                                    .subtitle2
                                                                    .copyWith(fontSize: 10),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10.0,
                                                        ),
                                                        (getCategoryWiseProductsModel.data
                                                            .otherCats[_selectedTabBar-1].products[index].flashSale !=
                                                            null ||
                                                            getCategoryWiseProductsModel.data
                                                                .otherCats[_selectedTabBar-1].products[index].specialSale !=
                                                                null)
                                                            ? Text(
                                                          '${getCategoryWiseProductsModel.data
                                                              .otherCats[_selectedTabBar-1].products[index].displayPrice}',
                                                          style: Theme.of(context)
                                                              .textTheme
                                                              .subtitle2
                                                              .copyWith(
                                                            decoration: TextDecoration.lineThrough,
                                                          ),
                                                        )
                                                            : SizedBox(),
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                          children: [
                                                            ///--------------product-price
                                                            Expanded(
                                                              child: (getCategoryWiseProductsModel.data
                                                                  .otherCats[_selectedTabBar-1].products[index].flashSale != null &&
                                                                  getCategoryWiseProductsModel.data
                                                                      .otherCats[_selectedTabBar-1].products[index].specialSale == null)
                                                                  ? Text(
                                                                '${getCategoryWiseProductsModel.data
                                                                    .otherCats[_selectedTabBar-1].products[index].flashSale.displayPrice}',
                                                                overflow: TextOverflow.ellipsis,
                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                              )
                                                                  : (getCategoryWiseProductsModel.data
                                                                  .otherCats[_selectedTabBar-1].products[index].flashSale == null &&
                                                                  getCategoryWiseProductsModel.data
                                                                      .otherCats[_selectedTabBar-1].products[index].specialSale != null)
                                                                  ? Text(
                                                                '${getCategoryWiseProductsModel.data
                                                                    .otherCats[_selectedTabBar-1].products[index].specialSale.displayPrice}',
                                                                overflow: TextOverflow.ellipsis,
                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                              )
                                                                  : Text(
                                                                '${getCategoryWiseProductsModel.data
                                                                    .otherCats[_selectedTabBar-1].products[index].displayPrice}',
                                                                overflow: TextOverflow.ellipsis,
                                                                style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              onTap:
                                                                  () {},
                                                              child:
                                                              Padding(
                                                                padding: const EdgeInsets.fromLTRB(
                                                                    5,
                                                                    5,
                                                                    10,
                                                                    5),
                                                                child:
                                                                Icon(
                                                                  Icons
                                                                      .add_box_rounded,
                                                                  size:
                                                                  20,
                                                                  color:
                                                                  customThemeColor,
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 8.0,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          right: 0.0,
                                          top: 0,
                                          child: InkWell(
                                            onTap: (){
                                              print('------------->>press');
                                              if(currentUserModel != null){
                                                ///post-method
                                                postMethod(
                                                    context,
                                                    addToWishListApi,
                                                    {
                                                      'product_id': getCategoryWiseProductsModel.data
                                                          .otherCats[_selectedTabBar-1].products[index].id
                                                    },
                                                    true,
                                                    addToWishListDataRepo
                                                );
                                              }else{
                                                notLoggedAlert(
                                                    context,
                                                    'Error',
                                                    'You Have to Login First'
                                                );
                                                print('------------->>NotLoggedIn');
                                              }
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                              child: Container(
                                                height: 14,
                                                width: 16,
                                                child: Center(
                                                    child: SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/Group 366.svg',
                                                      color: customThemeColor,
                                                    )
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                            top: 10.0,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: List.generate(
                                                  getCategoryWiseProductsModel.data
                                                      .otherCats[_selectedTabBar-1].products[index].tags.length, (tagIndex){
                                                return Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      height: 20,
                                                      decoration: BoxDecoration(
                                                          color: customThemeColor,
                                                          borderRadius: BorderRadius.only(
                                                            topRight: Radius.circular(6.0),
                                                            bottomRight: Radius.circular(6.0),
                                                          )
                                                      ),
                                                      child: Center(
                                                          child: Padding(
                                                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                            child: Text('${getCategoryWiseProductsModel.data
                                                                .otherCats[_selectedTabBar-1].products[index].tags[tagIndex]}',
                                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                  fontSize: 12,
                                                                  color: Colors.white
                                                              ),
                                                            ),
                                                          )),
                                                    ),
                                                    SizedBox(height: 6,),
                                                  ],
                                                );
                                              }),
                                            )
                                        ),
                                      ]
                                  ),
                                ),
                              );
                            }),
                          ),
                        ),
                      );
                    }),
                  ),
                ),
                // content: Padding(
                //   padding: const EdgeInsets.only(top: 7),
                //   child: SingleChildScrollView(
                //     controller: _scrollViewController,
                //     child: Builder(builder: (_) {
                //       if (_selectedTabbar == 0) {
                //         return Center(
                //           child: Wrap(
                //             children: List.generate(getCategoryWiseProductsModel.data.allCategories.length, (index){
                //               return Container(
                //                 width: MediaQuery.of(context).size.width*.45,
                //                 child: Padding(
                //                   padding: index%2 == 0
                //                       ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                       :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                   child: Stack(
                //                       children: [
                //                         Container(
                //                           decoration: BoxDecoration(
                //                             color: Colors.white,
                //                             borderRadius: BorderRadius.vertical(
                //                               top: Radius.circular(10),
                //                               bottom: Radius.circular(10),
                //                             ),
                //                             boxShadow: [
                //                               BoxShadow(
                //                                 color: Colors.grey.withOpacity(0.2),
                //                                 spreadRadius: 2,
                //                                 blurRadius: 6,
                //                                 offset: Offset(0,3),
                //                               ),
                //                             ],
                //                           ),
                //                           child: Column(
                //                             children: [
                //                               ///--------------product-image
                //                               Container(
                //                                 height: 145,
                //                                 width: double.infinity,
                //                                 child: ClipRRect(
                //                                   borderRadius: BorderRadius.vertical(
                //                                     top: Radius.circular(10),
                //                                     bottom: Radius.circular(10),
                //                                   ),
                //                                   child:
                //                                   getCategoryWiseProductsModel.data.allCategories[index].media.length == 0
                //                                       ?Image.asset(
                //                                     'assets/images/onBoard1.png',
                //                                     fit: BoxFit.fill,
                //                                   )
                //                                       :Image.network('${baseUrl+getCategoryWiseProductsModel.data.allCategories[index].media[0].originalMediaPath}',
                //                                     fit: BoxFit.fill,
                //                                   ),
                //                                 ),
                //                               ),
                //                               ///--------------product-detail
                //                               Container(
                //                                 width: double.infinity,
                //                                 child: Padding(
                //                                   padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                   child: Column(
                //                                     crossAxisAlignment: CrossAxisAlignment.start,
                //                                     children: [
                //                                       ///--------------product-name
                //                                       Text(
                //                                         '${getCategoryWiseProductsModel.data.allCategories[index].name}',
                //                                         softWrap: true,
                //                                         overflow: TextOverflow.ellipsis,
                //                                         maxLines: 2,
                //                                         style: Theme.of(context).textTheme.headline1.copyWith(
                //                                             fontSize: 14,
                //                                             color: customDarkGreyColor
                //                                         ),
                //                                       ),
                //                                       SizedBox(height: 4.0,),
                //                                       ///--------------product-rating-row
                //                                       Row(
                //                                         children: [
                //                                           RatingBar(
                //                                             initialRating: getCategoryWiseProductsModel.data.allCategories[index].reviewsAverageRating,
                //                                             direction: Axis.horizontal,
                //                                             allowHalfRating: true,
                //                                             itemCount: 5,
                //                                             itemSize: 15,
                //                                             ratingWidget: RatingWidget(
                //                                               full: Icon(Icons.star,color: customRatingStartColor,),
                //                                               half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                               empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                             ),
                //                                             itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                             onRatingUpdate: (rating) {
                //                                               print(rating);
                //                                             },
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(left: 3),
                //                                             child: Text(
                //                                               '(${getCategoryWiseProductsModel.data.allCategories[index].totalReviews})',
                //                                               style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                   fontSize: 10
                //                                               ),
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //                                       SizedBox(height: 10.0,),
                //                                       getCategoryWiseProductsModel.data.allCategories[index].flashSale != null
                //                                           ?Text(
                //                                         '${getCategoryWiseProductsModel.data.allCategories[index].flashSale.displayPrice}',
                //                                         style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                           decoration: TextDecoration.lineThrough,
                //                                         ),
                //                                       )
                //                                           :SizedBox(),
                //                                       Row(
                //                                         mainAxisAlignment: MainAxisAlignment.start,
                //                                         children: [
                //                                           ///--------------product-price
                //                                           Expanded(
                //                                             child: Text(
                //                                               '${getCategoryWiseProductsModel.data.allCategories[index].displayPrice}',
                //                                               style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                   fontSize: 14,
                //                                                   color: customThemeColor
                //                                               ),
                //                                             ),
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(right: 10),
                //                                             child: Icon(
                //                                               Icons.add_box_rounded,
                //                                               size: 20,
                //                                               color: customThemeColor,
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //                                       SizedBox(height: 8.0,),
                //                                     ],
                //                                   ),
                //                                 ),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //                         Positioned(
                //                           right: 12.0,
                //                           top: 12,
                //                           child: Container(
                //                             height: 14,
                //                             width: 16,
                //                             child: Center(
                //                                 child: SvgPicture.asset(
                //                                   'assets/Icons/Product_page_icons/Group 366.svg',
                //                                   color: customThemeColor,
                //                                 )
                //                             ),
                //                           ),
                //                         ),
                //                         Positioned(
                //                           top: 13.0,
                //                           child: Column(
                //                             children: [
                //                               // Container(
                //                               //   height: 20,
                //                               //   width: 56,
                //                               //   decoration: BoxDecoration(
                //                               //       color: customThemeColor,
                //                               //       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                               //   ),
                //                               //   child: Center(child: Text('New',
                //                               //     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                               //         fontSize: 12,
                //                               //         color: Colors.white
                //                               //     ),
                //                               //   )),
                //                               // ),
                //                               // SizedBox(height: 6,),
                //                               getCategoryWiseProductsModel.data.allCategories[index].flashSale == null
                //                                   ?SizedBox()
                //                                   :Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customGreenSaleFlagColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('On sale',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //                       ]
                //                   ),
                //                 ),
                //               );
                //             }),
                //           ),
                //         );
                //       } else if (_selectedTabbar == 1) {
                //         return Center(
                //           child: Wrap(
                //             children: List.generate(10, (index){
                //               return Container(
                //                 width: MediaQuery.of(context).size.width*.45,
                //                 child: Padding(
                //                   padding: index%2 == 0
                //                       ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                       :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                   child: Stack(
                //                       children: [
                //                         Container(
                //                           decoration: BoxDecoration(
                //                             color: Colors.white,
                //                             borderRadius: BorderRadius.vertical(
                //                               top: Radius.circular(10),
                //                               bottom: Radius.circular(10),
                //                             ),
                //                             boxShadow: [
                //                               BoxShadow(
                //                                 color: Colors.grey.withOpacity(0.2),
                //                                 spreadRadius: 2,
                //                                 blurRadius: 6,
                //                                 offset: Offset(0,3),
                //                               ),
                //                             ],
                //                           ),
                //                           child: Column(
                //                             children: [
                //                               ///--------------product-image
                //                               Container(
                //                                 height: 145,
                //                                 width: /*160*/double.infinity,
                //                                 child: ClipRRect(
                //                                   borderRadius: BorderRadius.vertical(
                //                                     top: Radius.circular(10),
                //                                     bottom: Radius.circular(10),
                //                                   ),
                //                                   child:
                //                                   Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                   // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                   //   // height: 300,
                //                                   //   fit: BoxFit.fill,
                //                                   // ),
                //                                 ),
                //                               ),
                //                               ///--------------product-detail
                //                               Container(
                //                                 width: /*160*/double.infinity,
                //                                 child: Padding(
                //                                   padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                   child: Column(
                //                                     crossAxisAlignment: CrossAxisAlignment.start,
                //                                     children: [
                //                                       ///--------------product-name
                //                                       Text(
                //                                         // '${getFlashSaleProductsModel.data[index].name}',
                //                                         'NAME $index',
                //                                         softWrap: true,
                //                                         overflow: TextOverflow.ellipsis,
                //                                         maxLines: 2,
                //                                         style: Theme.of(context).textTheme.headline1.copyWith(
                //                                             fontSize: 14,
                //                                             color: customDarkGreyColor
                //                                         ),
                //                                       ),
                //                                       SizedBox(height: 4.0,),
                //                                       ///--------------product-rating-row
                //                                       Row(
                //                                         children: [
                //                                           RatingBar(
                //                                             initialRating: 3.5,
                //                                             direction: Axis.horizontal,
                //                                             allowHalfRating: true,
                //                                             itemCount: 5,
                //                                             itemSize: 15,
                //                                             ratingWidget: RatingWidget(
                //                                               full: Icon(Icons.star,color: customRatingStartColor,),
                //                                               half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                               empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                             ),
                //                                             itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                             onRatingUpdate: (rating) {
                //                                               print(rating);
                //                                             },
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(left: 3),
                //                                             child: Text(
                //                                               '(150)',
                //                                               style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                   fontSize: 10
                //                                               ),
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //
                //                                       SizedBox(height: 10.0,),
                //                                       Text(
                //                                         // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                         '\$123.0',
                //                                         style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                           decoration: TextDecoration.lineThrough,
                //                                         ),
                //                                       ),
                //                                       Row(
                //                                         mainAxisAlignment: MainAxisAlignment.start,
                //                                         children: [
                //                                           ///--------------product-price
                //
                //                                           Expanded(
                //                                             child: Text(
                //                                               // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                               '\$123.0',
                //                                               style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                   fontSize: 14,
                //                                                   color: customThemeColor
                //                                               ),
                //                                             ),
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(right: 10),
                //                                             child: Icon(
                //                                               Icons.add_box_rounded,
                //                                               size: 20,
                //                                               color: customThemeColor,
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //                                       SizedBox(height: 8.0,),
                //                                     ],
                //                                   ),
                //                                 ),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //                         Positioned(
                //                           right: 12.0,
                //                           top: 12,
                //                           child: Container(
                //                             height: 14,
                //                             width: 16,
                //                             child: Center(
                //                                 child: SvgPicture.asset(
                //                                   'assets/Icons/Product_page_icons/Group 366.svg',
                //                                   color: customThemeColor,
                //                                 )
                //                             ),
                //                           ),
                //                         ),
                //                         Positioned(
                //                           top: 13.0,
                //                           child: Column(
                //                             children: [
                //                               Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customThemeColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('New',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                               SizedBox(height: 6,),
                //                               Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customGreenSaleFlagColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('On sale',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //
                //                       ]
                //                   ),
                //                 ),
                //               );
                //             }),
                //           ),
                //         );
                //       } else if (_selectedTabbar == 2) {
                //         return Center(
                //           child: Wrap(
                //             children: List.generate(10, (index){
                //               return Container(
                //                 width: MediaQuery.of(context).size.width*.45,
                //                 child: Padding(
                //                   padding: index%2 == 0
                //                       ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                       :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                   child: Stack(
                //                       children: [
                //                         Container(
                //                           decoration: BoxDecoration(
                //                             color: Colors.white,
                //                             borderRadius: BorderRadius.vertical(
                //                               top: Radius.circular(10),
                //                               bottom: Radius.circular(10),
                //                             ),
                //                             boxShadow: [
                //                               BoxShadow(
                //                                 color: Colors.grey.withOpacity(0.2),
                //                                 spreadRadius: 2,
                //                                 blurRadius: 6,
                //                                 offset: Offset(0,3),
                //                               ),
                //                             ],
                //                           ),
                //                           child: Column(
                //                             children: [
                //                               ///--------------product-image
                //                               Container(
                //                                 height: 145,
                //                                 width: /*160*/double.infinity,
                //                                 child: ClipRRect(
                //                                   borderRadius: BorderRadius.vertical(
                //                                     top: Radius.circular(10),
                //                                     bottom: Radius.circular(10),
                //                                   ),
                //                                   child:
                //                                   Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                   // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                   //   // height: 300,
                //                                   //   fit: BoxFit.fill,
                //                                   // ),
                //                                 ),
                //                               ),
                //                               ///--------------product-detail
                //                               Container(
                //                                 width: /*160*/double.infinity,
                //                                 child: Padding(
                //                                   padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                   child: Column(
                //                                     crossAxisAlignment: CrossAxisAlignment.start,
                //                                     children: [
                //                                       ///--------------product-name
                //                                       Text(
                //                                         // '${getFlashSaleProductsModel.data[index].name}',
                //                                         'NAME $index',
                //                                         softWrap: true,
                //                                         overflow: TextOverflow.ellipsis,
                //                                         maxLines: 2,
                //                                         style: Theme.of(context).textTheme.headline1.copyWith(
                //                                             fontSize: 14,
                //                                             color: customDarkGreyColor
                //                                         ),
                //                                       ),
                //                                       SizedBox(height: 4.0,),
                //                                       ///--------------product-rating-row
                //                                       Row(
                //                                         children: [
                //                                           RatingBar(
                //                                             initialRating: 3.5,
                //                                             direction: Axis.horizontal,
                //                                             allowHalfRating: true,
                //                                             itemCount: 5,
                //                                             itemSize: 15,
                //                                             ratingWidget: RatingWidget(
                //                                               full: Icon(Icons.star,color: customRatingStartColor,),
                //                                               half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                               empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                             ),
                //                                             itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                             onRatingUpdate: (rating) {
                //                                               print(rating);
                //                                             },
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(left: 3),
                //                                             child: Text(
                //                                               '(150)',
                //                                               style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                   fontSize: 10
                //                                               ),
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //
                //                                       SizedBox(height: 10.0,),
                //                                       Text(
                //                                         // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                         '\$123.0',
                //                                         style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                           decoration: TextDecoration.lineThrough,
                //                                         ),
                //                                       ),
                //                                       Row(
                //                                         mainAxisAlignment: MainAxisAlignment.start,
                //                                         children: [
                //                                           ///--------------product-price
                //
                //                                           Expanded(
                //                                             child: Text(
                //                                               // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                               '\$123.0',
                //                                               style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                   fontSize: 14,
                //                                                   color: customThemeColor
                //                                               ),
                //                                             ),
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(right: 10),
                //                                             child: Icon(
                //                                               Icons.add_box_rounded,
                //                                               size: 20,
                //                                               color: customThemeColor,
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //                                       SizedBox(height: 8.0,),
                //                                     ],
                //                                   ),
                //                                 ),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //                         Positioned(
                //                           right: 12.0,
                //                           top: 12,
                //                           child: Container(
                //                             height: 14,
                //                             width: 16,
                //                             child: Center(
                //                                 child: SvgPicture.asset(
                //                                   'assets/Icons/Product_page_icons/Group 366.svg',
                //                                   color: customThemeColor,
                //                                 )
                //                             ),
                //                           ),
                //                         ),
                //                         Positioned(
                //                           top: 13.0,
                //                           child: Column(
                //                             children: [
                //                               Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customThemeColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('New',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                               SizedBox(height: 6,),
                //                               Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customGreenSaleFlagColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('On sale',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //
                //                       ]
                //                   ),
                //                 ),
                //               );
                //             }),
                //           ),
                //         );
                //       } else if (_selectedTabbar == 3) {
                //         return Center(
                //           child: Wrap(
                //             children: List.generate(10, (index){
                //               return Container(
                //                 width: MediaQuery.of(context).size.width*.45,
                //                 child: Padding(
                //                   padding: index%2 == 0
                //                       ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                       :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                   child: Stack(
                //                       children: [
                //                         Container(
                //                           decoration: BoxDecoration(
                //                             color: Colors.white,
                //                             borderRadius: BorderRadius.vertical(
                //                               top: Radius.circular(10),
                //                               bottom: Radius.circular(10),
                //                             ),
                //                             boxShadow: [
                //                               BoxShadow(
                //                                 color: Colors.grey.withOpacity(0.2),
                //                                 spreadRadius: 2,
                //                                 blurRadius: 6,
                //                                 offset: Offset(0,3),
                //                               ),
                //                             ],
                //                           ),
                //                           child: Column(
                //                             children: [
                //                               ///--------------product-image
                //                               Container(
                //                                 height: 145,
                //                                 width: /*160*/double.infinity,
                //                                 child: ClipRRect(
                //                                   borderRadius: BorderRadius.vertical(
                //                                     top: Radius.circular(10),
                //                                     bottom: Radius.circular(10),
                //                                   ),
                //                                   child:
                //                                   Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                   // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                   //   // height: 300,
                //                                   //   fit: BoxFit.fill,
                //                                   // ),
                //                                 ),
                //                               ),
                //                               ///--------------product-detail
                //                               Container(
                //                                 width: /*160*/double.infinity,
                //                                 child: Padding(
                //                                   padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                   child: Column(
                //                                     crossAxisAlignment: CrossAxisAlignment.start,
                //                                     children: [
                //                                       ///--------------product-name
                //                                       Text(
                //                                         // '${getFlashSaleProductsModel.data[index].name}',
                //                                         'NAME $index',
                //                                         softWrap: true,
                //                                         overflow: TextOverflow.ellipsis,
                //                                         maxLines: 2,
                //                                         style: Theme.of(context).textTheme.headline1.copyWith(
                //                                             fontSize: 14,
                //                                             color: customDarkGreyColor
                //                                         ),
                //                                       ),
                //                                       SizedBox(height: 4.0,),
                //                                       ///--------------product-rating-row
                //                                       Row(
                //                                         children: [
                //                                           RatingBar(
                //                                             initialRating: 3.5,
                //                                             direction: Axis.horizontal,
                //                                             allowHalfRating: true,
                //                                             itemCount: 5,
                //                                             itemSize: 15,
                //                                             ratingWidget: RatingWidget(
                //                                               full: Icon(Icons.star,color: customRatingStartColor,),
                //                                               half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                               empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                             ),
                //                                             itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                             onRatingUpdate: (rating) {
                //                                               print(rating);
                //                                             },
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(left: 3),
                //                                             child: Text(
                //                                               '(150)',
                //                                               style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                   fontSize: 10
                //                                               ),
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //
                //                                       SizedBox(height: 10.0,),
                //                                       Text(
                //                                         // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                         '\$123.0',
                //                                         style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                           decoration: TextDecoration.lineThrough,
                //                                         ),
                //                                       ),
                //                                       Row(
                //                                         mainAxisAlignment: MainAxisAlignment.start,
                //                                         children: [
                //                                           ///--------------product-price
                //
                //                                           Expanded(
                //                                             child: Text(
                //                                               // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                               '\$123.0',
                //                                               style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                   fontSize: 14,
                //                                                   color: customThemeColor
                //                                               ),
                //                                             ),
                //                                           ),
                //                                           Padding(
                //                                             padding: const EdgeInsets.only(right: 10),
                //                                             child: Icon(
                //                                               Icons.add_box_rounded,
                //                                               size: 20,
                //                                               color: customThemeColor,
                //                                             ),
                //                                           )
                //                                         ],
                //                                       ),
                //                                       SizedBox(height: 8.0,),
                //                                     ],
                //                                   ),
                //                                 ),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //                         Positioned(
                //                           right: 12.0,
                //                           top: 12,
                //                           child: Container(
                //                             height: 14,
                //                             width: 16,
                //                             child: Center(
                //                                 child: SvgPicture.asset(
                //                                   'assets/Icons/Product_page_icons/Group 366.svg',
                //                                   color: customThemeColor,
                //                                 )
                //                             ),
                //                           ),
                //                         ),
                //                         Positioned(
                //                           top: 13.0,
                //                           child: Column(
                //                             children: [
                //                               Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customThemeColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('New',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                               SizedBox(height: 6,),
                //                               Container(
                //                                 height: 20,
                //                                 width: 56,
                //                                 decoration: BoxDecoration(
                //                                     color: customGreenSaleFlagColor,
                //                                     borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                 ),
                //                                 child: Center(child: Text('On sale',
                //                                   style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                       fontSize: 12,
                //                                       color: Colors.white
                //                                   ),
                //                                 )),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //
                //                       ]
                //                   ),
                //                 ),
                //               );
                //             }),
                //           ),
                //         );
                //       } else {
                //         return StaggeredGridView.countBuilder(
                //           crossAxisCount: 4, // I only need two card horizontally
                //           padding: const EdgeInsets.all(2.0),
                //           itemCount: 20,
                //           itemBuilder: (BuildContext context, int index){
                //             return Container(
                //               width: MediaQuery.of(context).size.width,
                //               child: Padding(
                //                 padding: index%2 == 0
                //                     ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                     :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                 child: Stack(
                //                     children: [
                //                       Container(
                //                         decoration: BoxDecoration(
                //                           color: Colors.white,
                //                           borderRadius: BorderRadius.vertical(
                //                             top: Radius.circular(10),
                //                             bottom: Radius.circular(10),
                //                           ),
                //                           boxShadow: [
                //                             BoxShadow(
                //                               color: Colors.grey.withOpacity(0.2),
                //                               spreadRadius: 2,
                //                               blurRadius: 6,
                //                               offset: Offset(0,3),
                //                             ),
                //                           ],
                //                         ),
                //                         child: Column(
                //                           children: [
                //                             ///--------------product-image
                //                             Container(
                //                               height: 145,
                //                               width: /*160*/double.infinity,
                //                               child: ClipRRect(
                //                                 borderRadius: BorderRadius.vertical(
                //                                   top: Radius.circular(10),
                //                                   bottom: Radius.circular(10),
                //                                 ),
                //                                 child:
                //                                 Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                 // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                 //   // height: 300,
                //                                 //   fit: BoxFit.fill,
                //                                 // ),
                //                               ),
                //                             ),
                //                             ///--------------product-detail
                //                             Container(
                //                               width: /*160*/double.infinity,
                //                               child: Padding(
                //                                 padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                 child: Column(
                //                                   crossAxisAlignment: CrossAxisAlignment.start,
                //                                   children: [
                //                                     ///--------------product-name
                //                                     Text(
                //                                       // '${getFlashSaleProductsModel.data[index].name}',
                //                                       'NAME $index',
                //                                       softWrap: true,
                //                                       overflow: TextOverflow.ellipsis,
                //                                       maxLines: 2,
                //                                       style: Theme.of(context).textTheme.headline1.copyWith(
                //                                           fontSize: 14,
                //                                           color: customDarkGreyColor
                //                                       ),
                //                                     ),
                //                                     SizedBox(height: 4.0,),
                //                                     ///--------------product-rating-row
                //                                     Row(
                //                                       children: [
                //                                         RatingBar(
                //                                           initialRating: 3.5,
                //                                           direction: Axis.horizontal,
                //                                           allowHalfRating: true,
                //                                           itemCount: 5,
                //                                           itemSize: 15,
                //                                           ratingWidget: RatingWidget(
                //                                             full: Icon(Icons.star,color: customRatingStartColor,),
                //                                             half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                             empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                           ),
                //                                           itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                           onRatingUpdate: (rating) {
                //                                             print(rating);
                //                                           },
                //                                         ),
                //                                         Padding(
                //                                           padding: const EdgeInsets.only(left: 3),
                //                                           child: Text(
                //                                             '(150)',
                //                                             style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                 fontSize: 10
                //                                             ),
                //                                           ),
                //                                         )
                //                                       ],
                //                                     ),
                //                                     SizedBox(height: 10.0,),
                //                                     index % 3 == 0?Text(
                //                                       // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                       '\$123.0',
                //                                       style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         decoration: TextDecoration.lineThrough,
                //                                       ),
                //                                     ):SizedBox(),
                //                                     Row(
                //                                       mainAxisAlignment: MainAxisAlignment.start,
                //                                       children: [
                //                                         ///--------------product-price
                //                                         Expanded(
                //                                           child: Text(
                //                                             // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                             '\$123.0',
                //                                             style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                 fontSize: 14,
                //                                                 color: customThemeColor
                //                                             ),
                //                                           ),
                //                                         ),
                //                                         Padding(
                //                                           padding: const EdgeInsets.only(right: 10),
                //                                           child: Icon(
                //                                             Icons.add_box_rounded,
                //                                             size: 20,
                //                                             color: customThemeColor,
                //                                           ),
                //                                         )
                //                                       ],
                //                                     ),
                //                                     SizedBox(height: 8.0,),
                //                                   ],
                //                                 ),
                //                               ),
                //                             ),
                //                           ],
                //                         ),
                //                       ),
                //                       Positioned(
                //                         right: 12.0,
                //                         top: 12,
                //                         child: Container(
                //                           height: 14,
                //                           width: 16,
                //                           child: Center(
                //                               child: SvgPicture.asset(
                //                                 'assets/Icons/Product_page_icons/Group 366.svg',
                //                                 color: customThemeColor,
                //                               )
                //                           ),
                //                         ),
                //                       ),
                //                       Positioned(
                //                         top: 13.0,
                //                         child: Column(
                //                           children: [
                //                             Container(
                //                               height: 20,
                //                               width: 56,
                //                               decoration: BoxDecoration(
                //                                   color: customThemeColor,
                //                                   borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                               ),
                //                               child: Center(child: Text('New',
                //                                 style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                     fontSize: 12,
                //                                     color: Colors.white
                //                                 ),
                //                               )),
                //                             ),
                //                             SizedBox(height: 6,),
                //                             Container(
                //                               height: 20,
                //                               width: 56,
                //                               decoration: BoxDecoration(
                //                                   color: customGreenSaleFlagColor,
                //                                   borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                               ),
                //                               child: Center(child: Text('On sale',
                //                                 style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                     fontSize: 12,
                //                                     color: Colors.white
                //                                 ),
                //                               )),
                //                             ),
                //                           ],
                //                         ),
                //                       ),
                //                     ]
                //                 ),
                //               ),
                //             );
                //           },
                //           //Here is the place that we are getting flexible/ dynamic card for various images
                //           staggeredTileBuilder: (int index) =>
                //               StaggeredTile.fit(2),
                //           mainAxisSpacing: 3.0,
                //           crossAxisSpacing: 4.0, // add some space
                //         );
                //       }
                //     }),
                //   ),
                // ),
                // content: Padding(
                //   padding: const EdgeInsets.fromLTRB(15, 12, 15, 0),
                //   child: Container(
                //     height: MediaQuery.of(context).size.height*.7,
                //     width: MediaQuery.of(context).size.width,
                //     // color: Colors.grey,
                //     child: TabBarView(
                //       children: [
                //         SingleChildScrollView(
                //           child: Center(
                //             child: Wrap(
                //               children: List.generate(10, (index){
                //                 return Container(
                //                   width: MediaQuery.of(context).size.width*.45,
                //                   child: Padding(
                //                     padding: index%2 == 0
                //                         ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                         :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                     child: Stack(
                //                         children: [
                //                           Container(
                //                             decoration: BoxDecoration(
                //                               color: Colors.white,
                //                               borderRadius: BorderRadius.vertical(
                //                                 top: Radius.circular(10),
                //                                 bottom: Radius.circular(10),
                //                               ),
                //                               boxShadow: [
                //                                 BoxShadow(
                //                                   color: Colors.grey.withOpacity(0.2),
                //                                   spreadRadius: 2,
                //                                   blurRadius: 6,
                //                                   offset: Offset(0,3),
                //                                 ),
                //                               ],
                //                             ),
                //                             child: Column(
                //                               children: [
                //                                 ///--------------product-image
                //                                 Container(
                //                                   height: 145,
                //                                   width: /*160*/double.infinity,
                //                                   child: ClipRRect(
                //                                     borderRadius: BorderRadius.vertical(
                //                                       top: Radius.circular(10),
                //                                       bottom: Radius.circular(10),
                //                                     ),
                //                                     child:
                //                                     Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                     // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                     //   // height: 300,
                //                                     //   fit: BoxFit.fill,
                //                                     // ),
                //                                   ),
                //                                 ),
                //                                 ///--------------product-detail
                //                                 Container(
                //                                   width: /*160*/double.infinity,
                //                                   child: Padding(
                //                                     padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                     child: Column(
                //                                       crossAxisAlignment: CrossAxisAlignment.start,
                //                                       children: [
                //                                         ///--------------product-name
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].name}',
                //                                           'NAME $index',
                //                                           softWrap: true,
                //                                           overflow: TextOverflow.ellipsis,
                //                                           maxLines: 2,
                //                                           style: Theme.of(context).textTheme.headline1.copyWith(
                //                                               fontSize: 14,
                //                                               color: customDarkGreyColor
                //                                           ),
                //                                         ),
                //                                         SizedBox(height: 4.0,),
                //                                         ///--------------product-rating-row
                //                                         Row(
                //                                           children: [
                //                                             RatingBar(
                //                                               initialRating: 3.5,
                //                                               direction: Axis.horizontal,
                //                                               allowHalfRating: true,
                //                                               itemCount: 5,
                //                                               itemSize: 15,
                //                                               ratingWidget: RatingWidget(
                //                                                 full: Icon(Icons.star,color: customRatingStartColor,),
                //                                                 half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                                 empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                               ),
                //                                               itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                               onRatingUpdate: (rating) {
                //                                                 print(rating);
                //                                               },
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(left: 3),
                //                                               child: Text(
                //                                                 '(150)',
                //                                                 style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                     fontSize: 10
                //                                                 ),
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //
                //                                         SizedBox(height: 10.0,),
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                           '\$123.0',
                //                                           style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                             decoration: TextDecoration.lineThrough,
                //                                           ),
                //                                         ),
                //                                         Row(
                //                                           mainAxisAlignment: MainAxisAlignment.start,
                //                                           children: [
                //                                             ///--------------product-price
                //
                //                                             Expanded(
                //                                               child: Text(
                //                                                 // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                                 '\$123.0',
                //                                                 style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                     fontSize: 14,
                //                                                     color: customThemeColor
                //                                                 ),
                //                                               ),
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(right: 10),
                //                                               child: Icon(
                //                                                 Icons.add_box_rounded,
                //                                                 size: 20,
                //                                                 color: customThemeColor,
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //                                         SizedBox(height: 8.0,),
                //                                       ],
                //                                     ),
                //                                   ),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //                           Positioned(
                //                             right: 12.0,
                //                             top: 12,
                //                             child: Container(
                //                               height: 14,
                //                               width: 16,
                //                               child: Center(
                //                                   child: SvgPicture.asset(
                //                                     'assets/Icons/Product_page_icons/Group 366.svg',
                //                                     color: customThemeColor,
                //                                   )
                //                               ),
                //                             ),
                //                           ),
                //                           Positioned(
                //                             top: 13.0,
                //                             child: Column(
                //                               children: [
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customThemeColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('New',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                                 SizedBox(height: 6,),
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customGreenSaleFlagColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('On sale',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //
                //                         ]
                //                     ),
                //                   ),
                //                 );
                //               }),
                //             ),
                //           ),
                //         ),
                //         SingleChildScrollView(
                //           child: Center(
                //             child: Wrap(
                //               children: List.generate(10, (index){
                //                 return Container(
                //                   width: MediaQuery.of(context).size.width*.45,
                //                   child: Padding(
                //                     padding: index%2 == 0
                //                         ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                         :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                     child: Stack(
                //                         children: [
                //                           Container(
                //                             decoration: BoxDecoration(
                //                               color: Colors.white,
                //                               borderRadius: BorderRadius.vertical(
                //                                 top: Radius.circular(10),
                //                                 bottom: Radius.circular(10),
                //                               ),
                //                               boxShadow: [
                //                                 BoxShadow(
                //                                   color: Colors.grey.withOpacity(0.2),
                //                                   spreadRadius: 2,
                //                                   blurRadius: 6,
                //                                   offset: Offset(0,3),
                //                                 ),
                //                               ],
                //                             ),
                //                             child: Column(
                //                               children: [
                //                                 ///--------------product-image
                //                                 Container(
                //                                   height: 145,
                //                                   width: /*160*/double.infinity,
                //                                   child: ClipRRect(
                //                                     borderRadius: BorderRadius.vertical(
                //                                       top: Radius.circular(10),
                //                                       bottom: Radius.circular(10),
                //                                     ),
                //                                     child:
                //                                     Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                     // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                     //   // height: 300,
                //                                     //   fit: BoxFit.fill,
                //                                     // ),
                //                                   ),
                //                                 ),
                //                                 ///--------------product-detail
                //                                 Container(
                //                                   width: /*160*/double.infinity,
                //                                   child: Padding(
                //                                     padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                     child: Column(
                //                                       crossAxisAlignment: CrossAxisAlignment.start,
                //                                       children: [
                //                                         ///--------------product-name
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].name}',
                //                                           'NAME $index',
                //                                           softWrap: true,
                //                                           overflow: TextOverflow.ellipsis,
                //                                           maxLines: 2,
                //                                           style: Theme.of(context).textTheme.headline1.copyWith(
                //                                               fontSize: 14,
                //                                               color: customDarkGreyColor
                //                                           ),
                //                                         ),
                //                                         SizedBox(height: 4.0,),
                //                                         ///--------------product-rating-row
                //                                         Row(
                //                                           children: [
                //                                             RatingBar(
                //                                               initialRating: 3.5,
                //                                               direction: Axis.horizontal,
                //                                               allowHalfRating: true,
                //                                               itemCount: 5,
                //                                               itemSize: 15,
                //                                               ratingWidget: RatingWidget(
                //                                                 full: Icon(Icons.star,color: customRatingStartColor,),
                //                                                 half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                                 empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                               ),
                //                                               itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                               onRatingUpdate: (rating) {
                //                                                 print(rating);
                //                                               },
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(left: 3),
                //                                               child: Text(
                //                                                 '(150)',
                //                                                 style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                     fontSize: 10
                //                                                 ),
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //
                //                                         SizedBox(height: 10.0,),
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                           '\$123.0',
                //                                           style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                             decoration: TextDecoration.lineThrough,
                //                                           ),
                //                                         ),
                //                                         Row(
                //                                           mainAxisAlignment: MainAxisAlignment.start,
                //                                           children: [
                //                                             ///--------------product-price
                //
                //                                             Expanded(
                //                                               child: Text(
                //                                                 // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                                 '\$123.0',
                //                                                 style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                     fontSize: 14,
                //                                                     color: customThemeColor
                //                                                 ),
                //                                               ),
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(right: 10),
                //                                               child: Icon(
                //                                                 Icons.add_box_rounded,
                //                                                 size: 20,
                //                                                 color: customThemeColor,
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //                                         SizedBox(height: 8.0,),
                //                                       ],
                //                                     ),
                //                                   ),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //                           Positioned(
                //                             right: 12.0,
                //                             top: 12,
                //                             child: Container(
                //                               height: 14,
                //                               width: 16,
                //                               child: Center(
                //                                   child: SvgPicture.asset(
                //                                     'assets/Icons/Product_page_icons/Group 366.svg',
                //                                     color: customThemeColor,
                //                                   )
                //                               ),
                //                             ),
                //                           ),
                //                           Positioned(
                //                             top: 13.0,
                //                             child: Column(
                //                               children: [
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customThemeColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('New',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                                 SizedBox(height: 6,),
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customGreenSaleFlagColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('On sale',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //
                //                         ]
                //                     ),
                //                   ),
                //                 );
                //               }),
                //             ),
                //           ),
                //         ),
                //         SingleChildScrollView(
                //           child: Center(
                //             child: Wrap(
                //               children: List.generate(10, (index){
                //                 return Container(
                //                   width: MediaQuery.of(context).size.width*.45,
                //                   child: Padding(
                //                     padding: index%2 == 0
                //                         ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                         :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                     child: Stack(
                //                         children: [
                //                           Container(
                //                             decoration: BoxDecoration(
                //                               color: Colors.white,
                //                               borderRadius: BorderRadius.vertical(
                //                                 top: Radius.circular(10),
                //                                 bottom: Radius.circular(10),
                //                               ),
                //                               boxShadow: [
                //                                 BoxShadow(
                //                                   color: Colors.grey.withOpacity(0.2),
                //                                   spreadRadius: 2,
                //                                   blurRadius: 6,
                //                                   offset: Offset(0,3),
                //                                 ),
                //                               ],
                //                             ),
                //                             child: Column(
                //                               children: [
                //                                 ///--------------product-image
                //                                 Container(
                //                                   height: 145,
                //                                   width: /*160*/double.infinity,
                //                                   child: ClipRRect(
                //                                     borderRadius: BorderRadius.vertical(
                //                                       top: Radius.circular(10),
                //                                       bottom: Radius.circular(10),
                //                                     ),
                //                                     child:
                //                                     Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                     // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                     //   // height: 300,
                //                                     //   fit: BoxFit.fill,
                //                                     // ),
                //                                   ),
                //                                 ),
                //                                 ///--------------product-detail
                //                                 Container(
                //                                   width: /*160*/double.infinity,
                //                                   child: Padding(
                //                                     padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                     child: Column(
                //                                       crossAxisAlignment: CrossAxisAlignment.start,
                //                                       children: [
                //                                         ///--------------product-name
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].name}',
                //                                           'NAME $index',
                //                                           softWrap: true,
                //                                           overflow: TextOverflow.ellipsis,
                //                                           maxLines: 2,
                //                                           style: Theme.of(context).textTheme.headline1.copyWith(
                //                                               fontSize: 14,
                //                                               color: customDarkGreyColor
                //                                           ),
                //                                         ),
                //                                         SizedBox(height: 4.0,),
                //                                         ///--------------product-rating-row
                //                                         Row(
                //                                           children: [
                //                                             RatingBar(
                //                                               initialRating: 3.5,
                //                                               direction: Axis.horizontal,
                //                                               allowHalfRating: true,
                //                                               itemCount: 5,
                //                                               itemSize: 15,
                //                                               ratingWidget: RatingWidget(
                //                                                 full: Icon(Icons.star,color: customRatingStartColor,),
                //                                                 half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                                 empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                               ),
                //                                               itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                               onRatingUpdate: (rating) {
                //                                                 print(rating);
                //                                               },
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(left: 3),
                //                                               child: Text(
                //                                                 '(150)',
                //                                                 style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                     fontSize: 10
                //                                                 ),
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //
                //                                         SizedBox(height: 10.0,),
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                           '\$123.0',
                //                                           style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                             decoration: TextDecoration.lineThrough,
                //                                           ),
                //                                         ),
                //                                         Row(
                //                                           mainAxisAlignment: MainAxisAlignment.start,
                //                                           children: [
                //                                             ///--------------product-price
                //
                //                                             Expanded(
                //                                               child: Text(
                //                                                 // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                                 '\$123.0',
                //                                                 style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                     fontSize: 14,
                //                                                     color: customThemeColor
                //                                                 ),
                //                                               ),
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(right: 10),
                //                                               child: Icon(
                //                                                 Icons.add_box_rounded,
                //                                                 size: 20,
                //                                                 color: customThemeColor,
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //                                         SizedBox(height: 8.0,),
                //                                       ],
                //                                     ),
                //                                   ),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //                           Positioned(
                //                             right: 12.0,
                //                             top: 12,
                //                             child: Container(
                //                               height: 14,
                //                               width: 16,
                //                               child: Center(
                //                                   child: SvgPicture.asset(
                //                                     'assets/Icons/Product_page_icons/Group 366.svg',
                //                                     color: customThemeColor,
                //                                   )
                //                               ),
                //                             ),
                //                           ),
                //                           Positioned(
                //                             top: 13.0,
                //                             child: Column(
                //                               children: [
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customThemeColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('New',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                                 SizedBox(height: 6,),
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customGreenSaleFlagColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('On sale',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //
                //                         ]
                //                     ),
                //                   ),
                //                 );
                //               }),
                //             ),
                //           ),
                //         ),
                //         SingleChildScrollView(
                //           child: Center(
                //             child: Wrap(
                //               children: List.generate(10, (index){
                //                 return Container(
                //                   width: MediaQuery.of(context).size.width*.45,
                //                   child: Padding(
                //                     padding: index%2 == 0
                //                         ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                //                         :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                //                     child: Stack(
                //                         children: [
                //                           Container(
                //                             decoration: BoxDecoration(
                //                               color: Colors.white,
                //                               borderRadius: BorderRadius.vertical(
                //                                 top: Radius.circular(10),
                //                                 bottom: Radius.circular(10),
                //                               ),
                //                               boxShadow: [
                //                                 BoxShadow(
                //                                   color: Colors.grey.withOpacity(0.2),
                //                                   spreadRadius: 2,
                //                                   blurRadius: 6,
                //                                   offset: Offset(0,3),
                //                                 ),
                //                               ],
                //                             ),
                //                             child: Column(
                //                               children: [
                //                                 ///--------------product-image
                //                                 Container(
                //                                   height: 145,
                //                                   width: /*160*/double.infinity,
                //                                   child: ClipRRect(
                //                                     borderRadius: BorderRadius.vertical(
                //                                       top: Radius.circular(10),
                //                                       bottom: Radius.circular(10),
                //                                     ),
                //                                     child:
                //                                     Image.asset('assets/images/blog2.png',fit: BoxFit.fill,),
                //                                     // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                //                                     //   // height: 300,
                //                                     //   fit: BoxFit.fill,
                //                                     // ),
                //                                   ),
                //                                 ),
                //                                 ///--------------product-detail
                //                                 Container(
                //                                   width: /*160*/double.infinity,
                //                                   child: Padding(
                //                                     padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                //                                     child: Column(
                //                                       crossAxisAlignment: CrossAxisAlignment.start,
                //                                       children: [
                //                                         ///--------------product-name
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].name}',
                //                                           'NAME $index',
                //                                           softWrap: true,
                //                                           overflow: TextOverflow.ellipsis,
                //                                           maxLines: 2,
                //                                           style: Theme.of(context).textTheme.headline1.copyWith(
                //                                               fontSize: 14,
                //                                               color: customDarkGreyColor
                //                                           ),
                //                                         ),
                //                                         SizedBox(height: 4.0,),
                //                                         ///--------------product-rating-row
                //                                         Row(
                //                                           children: [
                //                                             RatingBar(
                //                                               initialRating: 3.5,
                //                                               direction: Axis.horizontal,
                //                                               allowHalfRating: true,
                //                                               itemCount: 5,
                //                                               itemSize: 15,
                //                                               ratingWidget: RatingWidget(
                //                                                 full: Icon(Icons.star,color: customRatingStartColor,),
                //                                                 half: Icon(Icons.star_half,color: customRatingStartColor),
                //                                                 empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                //                                               ),
                //                                               itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                //                                               onRatingUpdate: (rating) {
                //                                                 print(rating);
                //                                               },
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(left: 3),
                //                                               child: Text(
                //                                                 '(150)',
                //                                                 style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                                     fontSize: 10
                //                                                 ),
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //
                //                                         SizedBox(height: 10.0,),
                //                                         Text(
                //                                           // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                           '\$123.0',
                //                                           style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                             decoration: TextDecoration.lineThrough,
                //                                           ),
                //                                         ),
                //                                         Row(
                //                                           mainAxisAlignment: MainAxisAlignment.start,
                //                                           children: [
                //                                             ///--------------product-price
                //
                //                                             Expanded(
                //                                               child: Text(
                //                                                 // '${getFlashSaleProductsModel.data[index].displayPrice}',
                //                                                 '\$123.0',
                //                                                 style: Theme.of(context).textTheme.headline1.copyWith(
                //                                                     fontSize: 14,
                //                                                     color: customThemeColor
                //                                                 ),
                //                                               ),
                //                                             ),
                //                                             Padding(
                //                                               padding: const EdgeInsets.only(right: 10),
                //                                               child: Icon(
                //                                                 Icons.add_box_rounded,
                //                                                 size: 20,
                //                                                 color: customThemeColor,
                //                                               ),
                //                                             )
                //                                           ],
                //                                         ),
                //                                         SizedBox(height: 8.0,),
                //                                       ],
                //                                     ),
                //                                   ),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //                           Positioned(
                //                             right: 12.0,
                //                             top: 12,
                //                             child: Container(
                //                               height: 14,
                //                               width: 16,
                //                               child: Center(
                //                                   child: SvgPicture.asset(
                //                                     'assets/Icons/Product_page_icons/Group 366.svg',
                //                                     color: customThemeColor,
                //                                   )
                //                               ),
                //                             ),
                //                           ),
                //                           Positioned(
                //                             top: 13.0,
                //                             child: Column(
                //                               children: [
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customThemeColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('New',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                                 SizedBox(height: 6,),
                //                                 Container(
                //                                   height: 20,
                //                                   width: 56,
                //                                   decoration: BoxDecoration(
                //                                       color: customGreenSaleFlagColor,
                //                                       borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                //                                   ),
                //                                   child: Center(child: Text('On sale',
                //                                     style: Theme.of(context).textTheme.subtitle2.copyWith(
                //                                         fontSize: 12,
                //                                         color: Colors.white
                //                                     ),
                //                                   )),
                //                                 ),
                //                               ],
                //                             ),
                //                           ),
                //
                //                         ]
                //                     ),
                //                   ),
                //                 );
                //               }),
                //             ),
                //           ),
                //         ),
                //       ],
                //       controller: allCategoriesTabController,
                //     ),
                //   ),
                // ),
              ),
            ),
          );
        },
        valueListenable: _reBuilder,
        child: SizedBox(),
      ),
    );
  }
}