import 'package:flutter/material.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class HomeTopCategoriesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
      child: Column(
        children: [
          Container(
            child: Row(
              children: [
                Expanded(
                  child: Text('Top Categories',
                      style: Theme.of(context).textTheme.headline1
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
              child: Stack(
                children: [
                  Container(
                    height: 90,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/baby.png'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  Positioned(
                    left: 200.0,
                    top: 25.0,
                    child: Text('KIDS WEAR',
                      style: Theme.of(context).textTheme.headline1.copyWith(color: customLightBlackColor),
                    ),
                  ),
                  Positioned(
                    left: 200.0,
                    top: 50.0,
                    child: Text('winter night suits',
                      style: Theme.of(context).textTheme.button.copyWith(
                          color: customLightBlackColor
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 12, 12, 0),
                    child: Stack(
                      children: [
                        Container(
                          height: 90,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/images/computerCategory.png'),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        Positioned(
                          left: 10.0,
                          bottom: 18.0,
                          child: Text('Computer',
                            style: Theme.of(context).textTheme.headline1.copyWith(
                                fontSize: 13,
                                color: customLightBlackColor
                            ),
                          ),
                        ),
                        Positioned(
                          left: 10.0,
                          bottom:6.0,
                          child: Text('Accessories',
                            style: Theme.of(context).textTheme.button.copyWith(
                                color: customLightBlackColor,
                                fontSize: 9
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                    child: Stack(
                      children: [
                        Container(
                          height: 90,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/images/ladiesCategory.png'),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        Positioned(
                          left: 85.0,
                          bottom: 20.0,
                          child: Text('Ladies',
                            style: Theme.of(context).textTheme.headline1.copyWith(
                                fontSize: 13,
                                color: customLightBlackColor
                            ),
                          ),
                        ),
                        Positioned(
                          left: 85.0,
                          bottom: 8.0,
                          child: Text('Summer Wear',
                            style: Theme.of(context).textTheme.button.copyWith(
                                color: customLightBlackColor,
                                fontSize: 9
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
