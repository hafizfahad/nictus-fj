
import 'package:flutter/material.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class HomeSearchWidget extends StatefulWidget {
  @override
  _HomeSearchWidgetState createState() => _HomeSearchWidgetState();
}
class _HomeSearchWidgetState extends State<HomeSearchWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
      child: Container(
        color: Color(0xffF9F9F9),
        child: Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Theme(
            data: ThemeData(
                primaryColor: Color(0xff363636)
            ),
            child: TextFormField(
              decoration: InputDecoration(
                  suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                  border: InputBorder.none,
                  hintText: 'search here',
                  hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
              ),
            ),
          ),
        ),
      ),
    );
  }
}