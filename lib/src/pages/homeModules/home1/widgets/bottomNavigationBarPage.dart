import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getLoginDataModel.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/customBottomDotBar.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/homeDrawerComponent.dart';
import 'package:multi_vendor_customer/src/repository/bottomNavigationRoutesRepository.dart';

class BottomNavigationBarPage extends StatefulWidget {
  @override
  _BottomNavigationBarPageState createState() =>
      _BottomNavigationBarPageState();
}

class _BottomNavigationBarPageState extends State<BottomNavigationBarPage>
    with TickerProviderStateMixin {
  int _currentIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(box.hasData('userData')){
      currentUserModel = UserModel.fromJson(jsonDecode(box.read('userData')));
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: homeBottomNavigationRepository(_currentIndex, context,),
      bottomNavigationBar: BottomNavigationDotBar(

          color: Colors.white.withOpacity(0.5),
          items: <BottomNavigationDotBarItem>[
            BottomNavigationDotBarItem(
                icon: 'assets/Icons/home.svg',
                onTap: () {
                  setState(() {
                    _currentIndex = 0;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: 'assets/Icons/shop.svg',
                onTap: () {
                  setState(() {
                    _currentIndex = 1;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: 'assets/Icons/fi-rr-apps.svg',
                onTap: () {
                  setState(() {
                    _currentIndex = 2;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: 'assets/Icons/Group 446.svg',
                onTap: () {
                  setState(() {
                    _currentIndex = 3;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: 'assets/Icons/user.svg',
                onTap: () {
                  setState(() {
                    _currentIndex = 4;
                  });
                }),
          ])
    );
  }
}
