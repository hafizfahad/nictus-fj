import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeCategoriesListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 25, 15, 12),
            child: Text(
              'Categories',
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          !_.getAllCategoriesCheck
              ?SkeletonLoader(
            period: Duration(seconds: 2),
            highlightColor: Colors.grey,
            direction: SkeletonDirection.ltr,
            builder: Container(
                height: 90,
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: List.generate(10, (index){
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(20, 3, 0, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            radius: 25,
                          ),
                          SizedBox(height: 5,),
                          Container(
                            width: 30,
                            height: 5,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    );
                  }),
                )
            ),
          )
              :Container(
            height: 115,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(getAllCategoriesModel.data.length, (index){
                return Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child: Container(
                    width: 69,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        getAllCategoriesModel.data[index].icon == null
                            ? Container(
                          height:69,
                          width:69,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              // border: Border.all(color: Colors.grey),
                              color: Colors.grey.withOpacity(0.4)
                          ),
                          child: Center(
                            child: Icon(
                              Icons.category,
                              color: Theme.of(context).buttonColor,
                              size: 25,
                            ),
                          ),
                        )
                            :Container(
                          height:69,
                          width:69,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage('${baseUrl+getAllCategoriesModel.data[index].icon.originalMediaPath}',),
                                  fit: BoxFit.fill
                              ),
                              shape: BoxShape.circle
                          ),
                        ),
                        SizedBox(height: 4,),
                        Text(
                          '${getAllCategoriesModel.data[index].name}',
                          textAlign: TextAlign.center,
                          softWrap: true,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.headline4.copyWith(color: customDarkGreyColor),
                        ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
