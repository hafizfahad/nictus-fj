import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/saleTimerClass.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/pages/productDetailPage.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeFlashSaleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => _.getAFlashSaleDataCheck && getFlashSaleProductsModel.data.length == 0
          ?SizedBox()
          :Container(
        // height: 390,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 20, 15, 12 ),
              child: Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          'Flash Sale',
                          style: Theme.of(context).textTheme.headline1
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        print('-------->> View Flash Sale');
                      },
                      child: Text(
                          'View All',
                          style: Theme.of(context).textTheme.headline5.copyWith(
                            decoration: TextDecoration.underline,
                          )
                      ),
                    ),
                  ],
                ),
              ),
            ),
            !_.getAFlashSaleDataCheck
                ?SkeletonLoader(
                period: Duration(seconds: 2),
                highlightColor: Colors.grey,
                direction: SkeletonDirection.ltr,
                builder: Container(
                  height: 290,
                  width: double.infinity,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: List.generate(3, (index) {
                      return Row(
                        children: [
                          index == 0
                              ?SizedBox(width: 15,)
                              :SizedBox(),
                          Container(
                            height: 290,
                            width: 266,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  spreadRadius: 1,
                                  blurRadius: 2,
                                  offset: Offset(0,1),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 15.0,),
                        ],
                      );
                    }),
                  ),
                )
            )
                :Container(
              height: 300,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: List.generate(getFlashSaleProductsModel.data.length, (index) {
                  return Row(
                    children: [
                      index == 0
                          ?SizedBox(width: 15,)
                          :SizedBox(),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Stack(
                            children: [
                              InkWell(
                                onTap: (){
                                  ///------get-product-detail-data-api-call
                                  getMethod(
                                      context,
                                      '$getProductsDetailApi/${getFlashSaleProductsModel.data[index].slug}',
                                      null,
                                      false,
                                      getProductDetailData
                                  );
                                  Get.find<HomeController>().changeGetProductDetailDataCheck(false);
                                  Get.toNamed(PageRoutes.productDetail);
                                },
                                child: Container(
                                  height: 290,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 2,
                                        blurRadius: 6,
                                        offset: Offset(0,3),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      ///--------------product-image
                                      Container(
                                        height: 170,
                                        width: 266,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(10),
                                            bottom: Radius.circular(10),
                                          ),
                                          child:
                                          getFlashSaleProductsModel.data[index].media.length == 0
                                              ?Image.asset(
                                            'assets/images/imagePlaceHolder.jpg',
                                            fit: BoxFit.fill,
                                          )
                                              :Image.network(
                                            '${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                      ///--------------product-detail
                                      Container(
                                        height: 120,
                                        width: 266,
                                        child: Padding(
                                          padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              ///--------------product-name
                                              Text(
                                                '${getFlashSaleProductsModel.data[index].name}',
                                                style: Theme.of(context).textTheme.headline1.copyWith(
                                                    fontSize: 14,
                                                    color: customDarkGreyColor
                                                ),
                                              ),
                                              SizedBox(height: 8.0,),

                                              ///-----------product-timer
                                              Container(
                                                height: 40,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    FlashSaleTimer(flashSaleTimerControllerList[index],true),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height: 12.0,),

                                              ///--------------product-price
                                              Row(
                                                children: [
                                                  Text(
                                                    '${getFlashSaleProductsModel.data[index].displayPrice}',
                                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      decoration: TextDecoration.lineThrough,
                                                    ),
                                                  ),
                                                  VerticalDivider(),
                                                  Text(
                                                    '${getFlashSaleProductsModel.data[index].flashSale.displayPrice}',
                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                        fontSize: 14,
                                                        color: customThemeColor
                                                    ),
                                                  ),
                                                ],
                                              ),

                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                  top: 15.0,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: List.generate(
                                        getFlashSaleProductsModel.data[index].tags.length, (tagIndex){
                                      return Column(
                                        children: [
                                          Container(
                                            height: 20,
                                            decoration: BoxDecoration(
                                                color: customThemeColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(6.0),
                                                  bottomRight: Radius.circular(6.0),
                                                )
                                            ),
                                            child: Center(
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                  child: Text('${getFlashSaleProductsModel.data[index].tags[tagIndex]}',
                                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                        fontSize: 12,
                                                        color: Colors.white
                                                    ),
                                                  ),
                                                )),
                                          ),
                                          SizedBox(height: 6,),
                                        ],
                                      );
                                    }),
                                  )
                              ),
                              Positioned(
                                right: 0.0,
                                top: 0,
                                child: InkWell(
                                  onTap: (){
                                    print('------------->>press');
                                    if(currentUserModel != null){
                                      ///post-method
                                      postMethod(
                                          context,
                                          addToWishListApi,
                                          {
                                            'product_id': getFlashSaleProductsModel.data[index].id
                                          },
                                          true,
                                          addToWishListDataRepo
                                      );
                                    }else{
                                      notLoggedAlert(
                                        context,
                                        'Error',
                                        'You Have to Login First'
                                      );
                                      print('------------->>NotLoggedIn');
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                    child: Container(
                                      height: 14,
                                      width: 16,
                                      child: Center(
                                          child: SvgPicture.asset(
                                            'assets/Icons/Product_page_icons/Group 366.svg',
                                            color: customThemeColor,
                                          )
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ]
                        ),
                      ),
                      SizedBox(width: 10.0,),
                    ],
                  );
                }),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
