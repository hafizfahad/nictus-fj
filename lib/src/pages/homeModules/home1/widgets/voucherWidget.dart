import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class HomeVoucherWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 20, 15, 12 ),
              child: Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          'Collect Vouchers',
                          style: Theme.of(context).textTheme.headline1
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 90,
              child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: List.generate(5, (index){
                    return Row(
                      children: [
                        index == 0
                            ?SizedBox(width: 15,)
                            :SizedBox(),
                        Row(
                          children: [
                            Container(
                              height: 90,
                              width: 220,
                              decoration: BoxDecoration(
                                  color: index % 2 == 0
                                      ?customVoucherColor1
                                      :customVoucherColor2,
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: DottedBorder(
                                    color: Colors.white,
                                    dashPattern: [6, 3],
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                      child: Stack(
                                        children: [
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Spend:',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        color: Colors.white
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.only(left: 5),
                                                    child: Text(
                                                      '\$150',
                                                      style: Theme.of(context).textTheme.headline5.copyWith(
                                                          color: Colors.white,
                                                          fontSize: 18
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              Text(
                                                'and get',
                                                style: Theme.of(context).textTheme.headline4.copyWith(
                                                    color: Colors.white
                                                ),
                                              ),
                                              Text(
                                                '\$20 Off',
                                                style: Theme.of(context).textTheme.headline5.copyWith(
                                                    color: Colors.white,
                                                    fontSize: 18
                                                ),
                                              ),
                                            ],
                                          ),
                                          Positioned(
                                              bottom: 7,
                                              right: 0,
                                              child: InkWell(
                                                onTap: (){},
                                                child: Container(
                                                  height: 25,
                                                  width: 70,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius: BorderRadius.circular(5)
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'Collect',
                                                      style: Theme.of(context).textTheme.headline4.copyWith(
                                                          color: customThemeColor
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                ),
                              ),
                            ),
                            SizedBox(width: 12,)
                          ],
                        ),
                      ],
                    );
                  })
              ),
            )
          ],
        ),
      ),
    );
  }
}
