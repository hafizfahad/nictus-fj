
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeTopReviewedWidget extends StatefulWidget {
  @override
  _HomeTopReviewedWidgetState createState() => _HomeTopReviewedWidgetState();
}
class _HomeTopReviewedWidgetState extends State<HomeTopReviewedWidget> {

  int _currentSliderIndexForTopReviewed;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => _.getTopReviewedProductsDataCheck && getTopReviewedProductsModel.data.length == 0
          ?SizedBox()
          :Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 30, 15, 12),
            child: Text(
              'Top Reviewed',
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          !_.getTopReviewedProductsDataCheck
              ?SkeletonLoader(
              period: Duration(seconds: 2),
              highlightColor: Colors.grey,
              direction: SkeletonDirection.ltr,
              builder: Container(
                height: 230,
                width: MediaQuery.of(context).size.width,
                child: CarouselSlider(
                  items: List.generate(4, (index){
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 10),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10),
                            bottom: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 6,
                              offset: Offset(0,3),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),

                  ///Slider Container properties
                  options: CarouselOptions(

                    onPageChanged: (index, value){
                      setState(() {
                        _currentSliderIndexForTopReviewed = index;
                      });
                    },
                    height: 238,
                    // aspectRatio: 16/9,
                    viewportFraction: .6,
                    // initialPage: 0,
                    enableInfiniteScroll: true,
                    reverse: false,
                    autoPlay: false,
                    autoPlayInterval: Duration(seconds: 3),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    // enlargeCenterPage: true,
                    // onPageChanged: callbackFunction,
                    scrollDirection: Axis.horizontal,
                  ),

                ),

              )
          )
              :Container(
            // height: 240,
            width: MediaQuery.of(context).size.width,
            child: CarouselSlider(
              items: List.generate(getTopReviewedProductsModel.data.length, (index){
                return Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 10),
                  child: Stack(
                      children: [
                        InkWell(
                          onTap: (){
                            ///------get-product-detail-data-api-call
                            getMethod(
                                context,
                                '$getProductsDetailApi/${getTopReviewedProductsModel.data[index].slug}',
                                null,
                                false,
                                getProductDetailData
                            );
                            Get.find<HomeController>().changeGetProductDetailDataCheck(false);
                            Get.toNamed(PageRoutes.productDetail);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.vertical(
                                top: Radius.circular(10),
                                bottom: Radius.circular(10),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 2,
                                  blurRadius: 6,
                                  offset: Offset(0,3),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                ///--------------product-image
                                Container(
                                  height: 143,
                                  width: 220,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
                                    child: getTopReviewedProductsModel.data[index].media.length == 0
                                        ?Image.asset(
                                      'assets/images/imagePlaceHolder.jpg',
                                      fit: BoxFit.fill,
                                    )
                                        :Image.network(
                                      '${baseUrl+getTopReviewedProductsModel.data[index].media[0].originalMediaPath}',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                ///--------------product-detail
                                Container(
                                  height: 105,
                                  width: 220,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        ///--------------product-name
                                        Text(
                                          '${getTopReviewedProductsModel.data[index].name}',
                                          softWrap: true,
                                          overflow: TextOverflow.ellipsis,
                                          style: Theme.of(context).textTheme.headline1.copyWith(
                                              fontSize: 14,
                                              color: customDarkGreyColor
                                          ),
                                        ),
                                        SizedBox(height: 4.0,),
                                        ///--------------product-rating-row
                                        Row(
                                          children: [
                                            RatingBar(
                                              initialRating: getTopReviewedProductsModel.data[index].reviewsAverageRating,
                                              direction: Axis.horizontal,
                                              ignoreGestures: true,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 15,
                                              ratingWidget: RatingWidget(
                                                full: Icon(Icons.star,color: customRatingStartColor,),
                                                half: Icon(Icons.star_half,color: customRatingStartColor),
                                                empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                                              ),
                                              itemPadding: EdgeInsets.symmetric(horizontal: 0.0),

                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 3),
                                              child: Text(
                                                '(${getTopReviewedProductsModel.data[index].totalReviews})',
                                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                    fontSize: 10
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        SizedBox(height: 5.0,),
                                        (getTopReviewedProductsModel.data[index].flashSale !=
                                            null ||
                                            getTopReviewedProductsModel.data[index].specialSale !=
                                                null)
                                            ? Text(
                                          '${getTopReviewedProductsModel.data[index].displayPrice}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2
                                              .copyWith(
                                            decoration: TextDecoration.lineThrough,
                                          ),
                                        )
                                            : SizedBox(),
                                        Expanded(
                                          child: Align(
                                            alignment: Alignment.bottomCenter,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                ///--------------product-price
                                                Expanded(
                                                  child: (getTopReviewedProductsModel.data[index].flashSale != null &&
                                                      getTopReviewedProductsModel.data[index].specialSale == null)
                                                      ? Text(
                                                    '${getTopReviewedProductsModel.data[index].flashSale.displayPrice}',
                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                        fontSize: 14,
                                                        color: customThemeColor
                                                    ),
                                                  )
                                                      : (getTopReviewedProductsModel.data[index].flashSale == null &&
                                                      getTopReviewedProductsModel.data[index].specialSale != null)
                                                      ? Text(
                                                    '${getTopReviewedProductsModel.data[index].specialSale.displayPrice}',
                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                        fontSize: 14,
                                                        color: customThemeColor
                                                    ),
                                                  )
                                                      : Text(
                                                    '${getTopReviewedProductsModel.data[index].displayPrice}',
                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                        fontSize: 14,
                                                        color: customThemeColor
                                                    ),
                                                  ),
                                                ),

                                                InkWell(
                                                  onTap: (){},
                                                  child: Padding(
                                                    padding: const EdgeInsets.fromLTRB(5, 5, 10, 5),
                                                    child: Icon(
                                                      Icons.add_box_rounded,
                                                      size: 20,
                                                      color: customThemeColor,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0.0,
                          top: 0,
                          child: InkWell(
                            onTap: (){
                              print('------------->>press');
                              if(currentUserModel != null){
                                ///post-method
                                postMethod(
                                    context,
                                    addToWishListApi,
                                    {
                                      'product_id': getTopReviewedProductsModel.data[index].id
                                    },
                                    true,
                                    addToWishListDataRepo
                                );
                              }else{
                                notLoggedAlert(
                                    context,
                                    'Error',
                                    'You Have to Login First'
                                );
                                print('------------->>NotLoggedIn');
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                              child: Container(
                                height: 14,
                                width: 16,
                                child: Center(
                                    child: SvgPicture.asset(
                                      'assets/Icons/Product_page_icons/Group 366.svg',
                                      color: customThemeColor,
                                    )
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                            top: 10.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: List.generate(
                                  getTopReviewedProductsModel.data[index].tags.length, (tagIndex){
                                return Column(
                                  children: [
                                    Container(
                                      height: 20,
                                      decoration: BoxDecoration(
                                          color: customThemeColor,
                                          borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(6.0),
                                            bottomRight: Radius.circular(6.0),
                                          )
                                      ),
                                      child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                            child: Text('${getTopReviewedProductsModel.data[index].tags[tagIndex]}',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: Colors.white
                                              ),
                                            ),
                                          )),
                                    ),
                                    SizedBox(height: 6,),
                                  ],
                                );
                              }),
                            )
                        ),
                      ]
                  ),
                );
              }),

              ///Slider Container properties
              options: CarouselOptions(

                onPageChanged: (index, value){
                  setState(() {
                    _currentSliderIndexForTopReviewed = index;
                  });
                },
                height: 258,
                // aspectRatio: 16/9,
                viewportFraction: .6,
                // initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: false,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                // enlargeCenterPage: true,
                // onPageChanged: callbackFunction,
                scrollDirection: Axis.horizontal,
              ),

            ),

          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(getTopReviewedProductsModel.data.length, (i) {       //these two lines
              int index = i; //are changed
              return Container(
                width: 5.0,
                height: 5.0,
                margin: EdgeInsets.fromLTRB(2, 0, 2, 0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentSliderIndexForTopReviewed == index
                        ? Theme.of(context).buttonColor
                        : Color.fromRGBO(0, 0, 0, 0.4)),
              );
            },
            ).toList(),
          ),
        ],
      ),
    );
  }
}