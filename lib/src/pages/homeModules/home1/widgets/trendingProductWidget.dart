import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeTrendingProductWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => _.getTrendingProductsDataCheck && getTrendingProductsModel.data.length == 0
          ?SizedBox()
          :Padding(
        padding: const EdgeInsets.fromLTRB(15, 30, 15, 0 ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          'Trending Products',
                          style: Theme.of(context).textTheme.headline1
                      ),
                    ),
                  ],
                ),
              ),
              !_.getTrendingProductsDataCheck
                  ?SkeletonLoader(
                  period: Duration(seconds: 2),
                  highlightColor: Colors.grey,
                  direction: SkeletonDirection.ltr,
                  builder: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 7, 0, 0),
                    child: Wrap(
                      children: List.generate(4, (index){
                        return Container(
                          width: MediaQuery.of(context).size.width*.45,
                          child: Padding(
                            padding: index%2 == 0
                                ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                                :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                            child: Container(
                              height: 230,
                              width: MediaQuery.of(context).size.width*.45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(10),
                                  bottom: Radius.circular(10),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 2,
                                    blurRadius: 6,
                                    offset: Offset(0,3),
                                  ),
                                ],
                              ),

                            ),
                          ),
                        );
                      }),
                    ),
                  )
              )
                  :Padding(
                padding: const EdgeInsets.fromLTRB(0, 7, 0, 0),
                child: Wrap(
                  children: List.generate(getTrendingProductsModel.data.length, (index){
                    return Container(
                      width: MediaQuery.of(context).size.width*.45,
                      child: Padding(
                        padding: index%2 == 0
                            ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                            :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                        child: Stack(
                            children: [
                              InkWell(
                                onTap: (){
                                  ///------get-product-detail-data-api-call
                                  getMethod(
                                      context,
                                      '$getProductsDetailApi/${getTrendingProductsModel.data[index].slug}',
                                      null,
                                      false,
                                      getProductDetailData
                                  );
                                  Get.find<HomeController>().changeGetProductDetailDataCheck(false);
                                  Get.toNamed(PageRoutes.productDetail);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(10),
                                      bottom: Radius.circular(10),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 2,
                                        blurRadius: 6,
                                        offset: Offset(0,3),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      ///--------------product-image
                                      Container(
                                        height: 145,
                                        width: /*160*/double.infinity,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(10),
                                            bottom: Radius.circular(10),
                                          ),
                                          child: getTrendingProductsModel.data[index].media.length == 0
                                              ?Image.asset(
                                            'assets/images/imagePlaceHolder.jpg',
                                            fit: BoxFit.fill,
                                          )
                                              :Image.network(
                                            '${baseUrl+getTrendingProductsModel.data[index].media[0].originalMediaPath}',
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                      ///--------------product-detail
                                      Container(
                                        width:
                                        double.infinity,
                                        child: Padding(
                                          padding:
                                          const EdgeInsets
                                              .only(
                                              top: 8.0,
                                              left: 10.0),
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment
                                                .start,
                                            children: [
                                              ///--------------product-name
                                              Text(
                                                '${getTrendingProductsModel.data[index].name}',
                                                softWrap:
                                                true,
                                                overflow:
                                                TextOverflow
                                                    .ellipsis,
                                                maxLines: 2,
                                                style: Theme.of(
                                                    context)
                                                    .textTheme
                                                    .headline1
                                                    .copyWith(
                                                    fontSize:
                                                    14,
                                                    color:
                                                    customDarkGreyColor),
                                              ),
                                              SizedBox(
                                                height: 4.0,
                                              ),

                                              ///--------------product-rating-row
                                              Row(
                                                children: [
                                                  RatingBar(
                                                    ignoreGestures: true,
                                                    initialRating:
                                                    getTrendingProductsModel.data[index]
                                                        .reviewsAverageRating,
                                                    direction:
                                                    Axis.horizontal,
                                                    allowHalfRating:
                                                    true,
                                                    itemCount:
                                                    5,
                                                    itemSize:
                                                    15,
                                                    ratingWidget:
                                                    RatingWidget(
                                                      full:
                                                      Icon(
                                                        Icons
                                                            .star,
                                                        color:
                                                        customRatingStartColor,
                                                      ),
                                                      half: Icon(
                                                          Icons
                                                              .star_half,
                                                          color:
                                                          customRatingStartColor),
                                                      empty: Icon(
                                                          Icons
                                                              .star_border_purple500_sharp,
                                                          color:
                                                          customRatingStartColor),
                                                    ),
                                                    itemPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 0.0),
                                                    onRatingUpdate:
                                                        (rating) {
                                                      print(
                                                          rating);
                                                    },
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                        .only(
                                                        left:
                                                        3),
                                                    child:
                                                    Text(
                                                      '(${getTrendingProductsModel.data[index].totalReviews})',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .subtitle2
                                                          .copyWith(fontSize: 10),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10.0,
                                              ),
                                              (getTrendingProductsModel.data[index].flashSale !=
                                                  null ||
                                                  getTrendingProductsModel.data[index].specialSale !=
                                                      null)
                                                  ? Text(
                                                '${getTrendingProductsModel.data[index].displayPrice}',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle2
                                                    .copyWith(
                                                  decoration: TextDecoration.lineThrough,
                                                ),
                                              )
                                                  : SizedBox(),
                                              Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .start,
                                                children: [
                                                  ///--------------product-price
                                                  Expanded(
                                                    child: (getTrendingProductsModel.data[index].flashSale != null &&
                                                        getTrendingProductsModel.data[index].specialSale == null)
                                                        ? Text(
                                                      '${getTrendingProductsModel.data[index].flashSale.displayPrice}',
                                                      overflow: TextOverflow.ellipsis,
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14, color: customThemeColor),
                                                    )
                                                        : (getTrendingProductsModel.data[index].flashSale == null &&
                                                        getTrendingProductsModel.data[index].specialSale != null)
                                                        ? Text(
                                                      '${getTrendingProductsModel.data[index].specialSale.displayPrice}',
                                                      overflow: TextOverflow.ellipsis,
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14, color: customThemeColor),
                                                    )
                                                        : Text(
                                                      '${getTrendingProductsModel.data[index].displayPrice}',
                                                      overflow: TextOverflow.ellipsis,
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14, color: customThemeColor),
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap:
                                                        () {},
                                                    child:
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(
                                                          5,
                                                          5,
                                                          10,
                                                          5),
                                                      child:
                                                      Icon(
                                                        Icons
                                                            .add_box_rounded,
                                                        size:
                                                        20,
                                                        color:
                                                        customThemeColor,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8.0,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 0.0,
                                top: 0,
                                child: InkWell(
                                  onTap: (){
                                    print('------------->>press');
                                    if(currentUserModel != null){
                                      ///post-method
                                      postMethod(
                                          context,
                                          addToWishListApi,
                                          {
                                            'product_id': getTrendingProductsModel.data[index].id
                                          },
                                          true,
                                          addToWishListDataRepo
                                      );
                                    }else{
                                      notLoggedAlert(
                                          context,
                                          'Error',
                                          'You Have to Login First'
                                      );
                                      print('------------->>NotLoggedIn');
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                    child: Container(
                                      height: 14,
                                      width: 16,
                                      child: Center(
                                          child: SvgPicture.asset(
                                            'assets/Icons/Product_page_icons/Group 366.svg',
                                            color: customThemeColor,
                                          )
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                  top: 10.0,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: List.generate(
                                        getTrendingProductsModel.data[index].tags.length, (tagIndex){
                                      return Column(
                                        children: [
                                          Container(
                                            height: 20,
                                            decoration: BoxDecoration(
                                                color: customThemeColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(6.0),
                                                  bottomRight: Radius.circular(6.0),
                                                )
                                            ),
                                            child: Center(
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                  child: Text('${getTrendingProductsModel.data[index].tags[tagIndex]}',
                                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                        fontSize: 12,
                                                        color: Colors.white
                                                    ),
                                                  ),
                                                )),
                                          ),
                                          SizedBox(height: 6,),
                                        ],
                                      );
                                    }),
                                  )
                              ),
                            ]
                        ),
                      ),
                    );
                  }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
