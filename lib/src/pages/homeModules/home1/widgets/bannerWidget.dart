import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeBannerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: GetBuilder<HomeController>(
        init: HomeController(),
        builder: (_) => !_.getAllGenericAppDataCheck
            ?SkeletonLoader(
          period: Duration(seconds: 2),
          highlightColor: Colors.grey,
          direction: SkeletonDirection.ltr,
          builder: Container(
            height: 150,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
          ),
        )
            :genericAppDataModel.data.staticBanners.length == 0
            ?Container()
            :Container(
          height: 150,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      '${baseUrl + genericAppDataModel.data.staticBanners[0].image.originalMediaPath}'
                  ),
                  fit: BoxFit.fill
              )
          ),
        ),
      ),
    );
  }
}
