import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/models/getLoginDataModel.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/bannerWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/categoriesDetailWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/categoriesListWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/featuredVendorsWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/flashSaleWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/homeDrawerComponent.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/saleTimerClass.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getFlashSaleProductsModel.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/searchBarWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/sliderWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/titleWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/topCategoriesWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/topReviewedProductsWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/topSellingWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/trendingProductWidget.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/voucherWidget.dart';
import 'package:multi_vendor_customer/src/repository/allCategoriesRepository.dart';
import 'package:multi_vendor_customer/src/repository/allCurrencyRepository.dart';
import 'package:multi_vendor_customer/src/repository/allLanguagesRepository.dart';
import 'package:multi_vendor_customer/src/repository/getAllGenericAppDataRepository.dart';
import 'package:multi_vendor_customer/src/repository/getCategoryWiseProductsRepository.dart';
import 'package:multi_vendor_customer/src/repository/getFeaturedVendorsRepository.dart';
import 'package:multi_vendor_customer/src/repository/getTopReviewedProductsRepository.dart';
import 'package:multi_vendor_customer/src/repository/getTopSellingProductsRepository.dart';
import 'package:multi_vendor_customer/src/repository/getTrendingProductsRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with TickerProviderStateMixin{

  //----------initialize-Get-x-Home-controller-----------------
  final HomeController _homeController = Get.put(HomeController());
  final AppController _appController = Get.put(AppController());

  ///-------- get-flash-sale-data-API-call
  getFlashSaleData(bool responseCheck, Map<String, dynamic> response){

    if(responseCheck){

      flashSaleTimerControllerList = [];

      Get.find<AppController>().changeServerErrorCheck(false);

      Get.find<HomeController>().changeGetAFlashSaleDataCheck(true);

      getFlashSaleProductsModel = GetFlashSaleProductsModel.fromJson(response);
      print('getFlashSaleData ------>> ${getFlashSaleProductsModel.data.length}');

      AnimationController _timerAnimationControllerLocal;

      getFlashSaleProductsModel.data.forEach((element) {

        int minuteTemp;
        final expiryDate = DateTime.parse(element.flashSale.expireDate);
        final toDayDate = DateTime.now();
        var different = expiryDate.difference(toDayDate).inMinutes;

        print('------------>> difference-1  : $different');

        setState(() {
          minuteTemp = different;
        });

        _timerAnimationControllerLocal = AnimationController(
          vsync: this,
          duration: Duration(minutes: minuteTemp.abs()),
        );
        _timerAnimationControllerLocal.reverse(
            from: _timerAnimationControllerLocal.value == 0.0 ? 1.0 : _timerAnimationControllerLocal.value);

        flashSaleTimerControllerList.add(_timerAnimationControllerLocal);

      });
    }
    else if(!responseCheck && response == null){

      print('Exception........................');
      Get.find<AppController>().changeServerErrorCheck(true);

    }
  }


  @override
  void initState() {
    super.initState();

    ///------get-all-currency-api-call
    getMethod(
        context,
        getAllCurrencyApi,
        null,
        false,
        getAllCurrencies
    );
    ///------get-all-language-api-call
    getMethod(
        context,
        getAllLanguageApi,
        null,
        false,
        getAllLanguages
    );
    ///------get-all-categories-api-call
    getMethod(
        context,
        getAllCategoriesApi,
        null,
        false,
        getAllCategories
    );
    ///------get-all-generic-app-data-api-call
    getMethod(
        context,
        getAllGenericAppDataApi,
        null,
        false,
        getAllGenericAppData
    );
    ///------get-flash-sale-data-api-call
    getMethod(
        context,
        getFlashSaleDataApi,
        null,
        false,
        getFlashSaleData
    );
    ///------get-top-selling-products-data-api-call
    getMethod(
        context,
        getTopSellingProductsDataApi,
        null,
        false,
        getTopSellingProductsData
    );
    ///------get-trending-products-data-api-call
    getMethod(
        context,
        getTrendingProductsDataApi,
        null,
        false,
        getTrendingProductsData
    );
    ///------get-top-reviewed-products-data-api-call
    getMethod(
        context,
        getTopReviewedProductsDataApi,
        null,
        false,
        getTopReviewedProductsData
    );
    ///------get-featured-vendors-products-data-api-call
    getMethod(
        context,
        getFeaturedVendorsDataApi,
        null,
        false,
        getFeaturedVendorsProductsData
    );
    ///------get-category-wise-products-data-api-call
    getMethod(
        context,
        getCategoryWiseProductsDataApi,
        null,
        false,
        getCategoryWiseProductsData
    );
    // ///------get-new-arrival-data-api-call
    // getMethod(
    //     context,
    //     getNewArrivalDataApi,
    //     null,
    //     getNewArrivalData
    // );
    // ///------get-featured-products-data-api-call
    // getMethod(
    //     context,
    //     getFeaturedProductsDataApi,
    //     null,
    //     getFeaturedProductsData
    // );

  }
  @override
  void dispose() {
    flashSaleTimerControllerList.forEach((element) {
      element.dispose();
    });
    super.dispose();
  }
  List<Widget> homeWidgetsList = [
    ///------------------------title
    HomeTitleWidget(),
    ///------------------------search-bar
    HomeSearchWidget(),
    ///------------------------slider
    HomeSliderWidget(),
    ///------------------------flash-sale-products
    HomeFlashSaleWidget(),
    ///------------------------vouchers
    HomeVoucherWidget(),
    ///------------------------top-selling-products
    HomeTopSellingWidget(),
    ///------------------------top-categories-products
    HomeTopCategoriesWidget(),
    ///------------------------trending-products
    HomeTrendingProductWidget(),
    ///------------------------categories-list
    HomeCategoriesListWidget(),
    ///------------------------top-reviewed
    HomeTopReviewedWidget(),
    ///------------------------featured-vendors
    defaultSettingsModel.data.generalSettings.isMultiVendor == 1
        ?HomeFeaturedVendorWidget()
        :SizedBox(),
    ///------------------------banner
    HomeBannerWidget(),
    ///-------------all-categories's-product-list-widget
    HomeCategoriesDetailWidget(),
  ];

  ///-----------Home-view-widget------------------------
  Widget homeView1(BuildContext context){
    return Scaffold(
      drawer: MyCustomDrawer(),
      appBar: MyCustomAppBar(drawerShow: true,),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SafeArea(
          child: ListView.builder(
              itemCount: homeWidgetsList.length,
              itemBuilder: (BuildContext context, int index){
                return homeWidgetsList[index];
              }),
          // child: SingleChildScrollView(
          //   child: Column(
          //     mainAxisAlignment: MainAxisAlignment.start,
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       ///------------------------title
          //       HomeTitleWidget(),
          //       ///------------------------search-bar
          //       HomeSearchWidget(),
          //       ///------------------------slider
          //       HomeSliderWidget(),
          //       ///------------------------flash-sale-products
          //       HomeFlashSaleWidget(),
          //       ///------------------------vouchers
          //       HomeVoucherWidget(),
          //       ///------------------------top-selling-products
          //       HomeTopSellingWidget(),
          //       ///------------------------top-categories-products
          //       HomeTopCategoriesWidget(),
          //       ///------------------------trending-products
          //       HomeTrendingProductWidget(),
          //       ///------------------------categories-list
          //       HomeCategoriesListWidget(),
          //       ///------------------------top-reviewed
          //       HomeTopReviewedWidget(),
          //       ///------------------------featured-vendors
          //       HomeFeaturedVendorWidget(),
          //       ///------------------------banner
          //       HomeBannerWidget(),
          //       ///-------------all-categories's-product-list-widget
          //       HomeCategoriesDetailWidget(),
          //
          //       SizedBox(height: 30,),
          //     ],
          //   ),
          // ),
        ),
      ),
    );
  }

  Widget _flashSaleLargeWidget(BuildContext context){
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_) => Container(
        height: 300,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0 ),
              child: Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          'Flash Sale',
                          style: Theme.of(context).textTheme.headline1
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        print('-------->> View Flash Sale');
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Text(
                            'View All',
                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                decoration: TextDecoration.underline,
                                color: customThemeColor
                            )
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            !_.getAFlashSaleDataCheck
                ?SkeletonLoader(
                period: Duration(seconds: 2),
                highlightColor: Colors.grey,
                direction: SkeletonDirection.ltr,
                builder: Container(
                  height: 230,
                  width: double.infinity,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: List.generate(3, (index) {
                      return Row(
                        children: [
                          index == 0
                              ?SizedBox(width: 20,)
                              :SizedBox(),
                          Container(
                            height: 220,
                            width: 320,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  spreadRadius: 1,
                                  blurRadius: 2,
                                  offset: Offset(0,1),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 15.0,),
                        ],
                      );
                    }),
                  ),
                )
            )
                :Container(
              height: 260,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: List.generate(getFlashSaleProductsModel.data.length, (index) {
                  return Row(
                    children: [
                      index == 0
                          ?SizedBox(width: 15,)
                          :SizedBox(),
                      Stack(
                          children: [
                            Container(
                              height: 220,
                              width: 320,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.3),
                                    spreadRadius: 1,
                                    blurRadius: 2,
                                    offset: Offset(0,1),
                                  ),
                                ],
                              ),
                              child: Row(
                                children: [
                                  ///-----------product-image
                                  Container(
                                    height: double.infinity,
                                    width: 130,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.horizontal(
                                          left: Radius.circular(15)
                                      ),
                                      child: getFlashSaleProductsModel.data[index].media == null
                                          ?Image.asset(
                                        'assets/images/onBoard1.png',
                                        fit: BoxFit.fill,
                                      )
                                          :Image.network(
                                        '${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  ///-----------product-detail
                                  Expanded(
                                      child: Container(
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                            color: Color(0xffF6EEDB),
                                            borderRadius: BorderRadius.horizontal(
                                                right: Radius.circular(15)
                                            )
                                        ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            ///-----------product-name
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(15, 20, 1, 2),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      height: 20,
                                                      child: Text(
                                                        '${getFlashSaleProductsModel.data[index].name}',
                                                        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 16),
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(4, 4, 12, 4),
                                                    child: Icon(
                                                      Icons.favorite,
                                                      color: Colors.red,
                                                      size: 17,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            ///-----------product-description
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(15, 1, 1, 5),
                                              child: Container(
                                                height: 60,
                                                width: 140,
                                                child: Text(
                                                  '${getFlashSaleProductsModel.data[index].description}',
                                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 11),
                                                ),
                                              ),
                                            ),
                                            ///-----------product-timer
                                            Container(
                                              height: 35,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  FlashSaleTimer(flashSaleTimerControllerList[index],true),
                                                ],
                                              ),
                                            ),
                                            ///-----------product-actual-price
                                            Padding(
                                                padding: const EdgeInsets.fromLTRB(15, 20, 1, 2),
                                                child: Text(
                                                  '${getFlashSaleProductsModel.data[index].displayPrice}',
                                                  style: Theme.of(context).textTheme.headline2.copyWith(
                                                      fontSize: 12,
                                                      color: Colors.red,
                                                      decoration: TextDecoration.lineThrough
                                                  ),
                                                )
                                            ),
                                            ///-----------product-sale-price
                                            Padding(
                                                padding: const EdgeInsets.fromLTRB(15, 0, 12, 5),
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        '${getFlashSaleProductsModel.data[index].flashSale.displayPrice}',
                                                        style: Theme.of(context).textTheme.headline2.copyWith(
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                    Icon(
                                                      Icons.add_box_outlined,
                                                      size: 20,
                                                      color: Colors.black,
                                                    )
                                                  ],
                                                )
                                            )
                                          ],
                                        ),
                                      )
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              top: 15.0,
                              child: Container(
                                height: 20,
                                width: 40,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                                ),
                                child: Center(child: Text('On Sale',
                                  style: Theme.of(context).textTheme.headline2.copyWith(
                                      fontSize: 8,
                                      color: Color(0xff005B15)),
                                )),
                              ),
                            ),
                          ]
                      ),
                      SizedBox(width: 15.0,),
                    ],
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
  ///-------------new-arrival-widget
  Widget _newArrivalWidget(BuildContext context){
    return Container(
      height: 310,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 15, 10 ),
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                        'Newest',
                        style: Theme.of(context).textTheme.headline1
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      print('-------->> View Flash Sale');
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                          'View All >>',
                          style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 10)
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 266,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(4, (index) {
                return Row(
                  children: [
                    index == 0
                        ?SizedBox(width: 20,)
                        :SizedBox(),
                    Stack(
                        children: [
                          Column(
                            children: [
                              ///--------------product-image
                              Container(
                                height: 170,
                                width: 155,
                                decoration: BoxDecoration(
                                  color: Color(0xffDAD9DA),
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(15)
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
                                  child:
                                  Image.asset('assets/images/onBoard1.png',fit: BoxFit.fill,),
                                  // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                                  //   // height: 300,
                                  //   fit: BoxFit.fill,
                                  // ),
                                ),
                              ),
                              ///--------------product-detail
                              Container(
                                height: 90,
                                width: 155,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.vertical(
                                      bottom: Radius.circular(15)
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, left: 20.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      ///--------------product-name
                                      Text(
                                        // '${getFlashSaleProductsModel.data[index].name}',
                                        'NAME',
                                        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 16),
                                      ),
                                      SizedBox(height: 10.0,),
                                      ///--------------product-price
                                      Text(
                                        // '${getFlashSaleProductsModel.data[index].displayPrice}',
                                        '\$ 123.0',
                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                          fontSize: 14,
                                        ),
                                      ),
                                      ///--------------product-rating-row
                                      SizedBox(height: 5.0,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: RatingBar(
                                              initialRating: 3.5,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 15,
                                              ratingWidget: RatingWidget(
                                                full: Icon(Icons.star,color: Color(0xffE2B500),),
                                                half: Icon(Icons.star_half,color: Color(0xffE2B500)),
                                                empty: Icon(Icons.star_border_purple500_sharp,color: Color(0xffE2B500)),
                                              ),
                                              itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.add_box_outlined,
                                              size: 20,
                                              color: Colors.black,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Positioned(
                            top: 15.0,
                            child: Container(
                              height: 20,
                              width: 40,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(topRight: Radius.circular(6.0), bottomRight: Radius.circular(6.0) )
                              ),
                              child: Center(child: Text('New',
                                style: Theme.of(context).textTheme.headline2.copyWith(
                                    fontSize: 10,
                                    color: Color(0xff005B15)),
                              )),
                            ),
                          ),
                          Positioned(
                            right: 5.0,
                            top: 12,
                            child: Container(
                              height: 20,
                              width: 40,
                              child: Center(
                                child: Icon(
                                  Icons.favorite_border,
                                  size: 18,
                                  color: Colors.red,
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    SizedBox(width: 10.0,),
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }


  ///-------------recommendation-widget
  List category = [
    {'image': 'assets/images/mouse.png'},
    {'image': 'assets/images/computer.png'},
    {'image': 'assets/images/Crockery.png'},
    {'image': 'assets/images/Grocery.png'},
    {'image': 'assets/images/style.png'}
  ];
  Widget _recommendationWidget(BuildContext context){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 10,left:20.0, right: 10.0, bottom: 10.0),
          child: Container(
            child: Text('Recommend For You',
                style: Theme.of(context).textTheme.headline1),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: Container(
            height: 135,
            width: double.infinity,
            //  color: Colors.grey,
            child: ListView(
                scrollDirection: Axis.horizontal,
                children: List.generate(category.length, (index) {
                  return  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Container(
                      height: 135,
                      width: 325,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        image: DecorationImage(
                          image: AssetImage(category[index]['image'],
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  );
                })
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15.0, left: 8.0, right: 8.0),
          child: Container(
            height: 200,
            width: double.infinity,
            //  color: Colors.grey,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Column(
                  children: [
                    Container(
                      height: 120,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Color(0xffDAD9DA),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
                      ),
                      child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(top:12.0, left: 0.0),
                            child: Image.asset('assets/images/laptop2.png',
                              width: 120,
                              fit: BoxFit.fill,
                            ),
                          )),
                    ),
                    Container(
                      height: 50,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(0,3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Center(
                              child: Text('Laptops',
                                style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 14),
                              ),
                            ),
                            SizedBox(height: 7.0,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 10.0,),
                Column(
                  children: [
                    Container(
                      height: 120,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Color(0xffDAD9DA),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
                      ),
                      child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 6.0),
                            child: Image.asset('assets/images/iPhone3.png',
                              fit: BoxFit.fill,
                            ),
                          )),
                    ),
                    Container(
                      height: 50,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(0,3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Mobiles',
                              style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 14),
                            ),
                            SizedBox(height: 7.0,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 10.0),
                Column(
                  children: [
                    Container(
                      height: 120,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Color(0xffDAD9DA),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
                      ),
                      child: Center(
                          child: Image.asset('assets/images/appliances.png',
                            fit: BoxFit.fill,
                          )),
                    ),
                    Container(
                      height: 50,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(0,3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Appliances',
                              style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 14),
                            ),
                            SizedBox(height: 7.0,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 10.0),
                Column(
                  children: [
                    Container(
                      height: 120,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Color(0xffDAD9DA),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
                      ),
                      child: Center(
                          child: Image.asset('assets/images/headphone.png',
                            fit: BoxFit.fill,
                          )),
                    ),
                    Container(
                      height: 50,
                      width: 135,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(0,3),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Accessories',
                              style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 14),
                            ),
                            SizedBox(height: 7.0,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }


  ///-------------vendors-list-widget
  List vendors = [
    {'img': 'assets/images/GAP.png'},
    {'img': 'assets/images/Dior.png'},
    {'img': 'assets/images/gucci.png'},
    {'img': 'assets/images/Levis.png'},
    {'img': 'assets/images/zara.png'},
    {'img': 'assets/images/Dior.png'}
  ];
  Widget _vendorListWidget(BuildContext context){
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 15.0),
      child: Container(
        height: 130,
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 20,
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text('Vendors',
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
            ),
            SizedBox(height: 10.0),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 10.0),
              child: Container(
                height: 90,
                // color: Colors.greenAccent,
                child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: List.generate(vendors.length, (index) {
                      return   Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Container(
                          height: 90,
                          width: 90,
                          decoration: BoxDecoration(
                            //    color: Colors.black,
                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                            image: DecorationImage(
                                image: AssetImage(vendors[index]['img']),
                                fit: BoxFit.fill
                            ),
                          ),
                        ),
                      );
                    })
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///--------------on-sale-widget
  Widget _onSaleProductsWidget(BuildContext context){
    return Container(
      height: 310,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 15, 10 ),
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                        'On Sale',
                        style: Theme.of(context).textTheme.headline1
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      print('-------->> View On Sale');
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                          'View All >>',
                          style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 10)
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 266,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(4, (index) {
                return Row(
                  children: [
                    index == 0
                        ?SizedBox(width: 20,)
                        :SizedBox(),
                    Stack(
                        children: [
                          Column(
                            children: [
                              ///--------------product-image
                              Container(
                                height: 170,
                                width: 155,
                                decoration: BoxDecoration(
                                  color: Color(0xffDAD9DA),
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(15)
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
                                  child:
                                  Image.asset('assets/images/onSaleImage.png',fit: BoxFit.fill,),
                                  // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                                  //   // height: 300,
                                  //   fit: BoxFit.fill,
                                  // ),
                                ),
                              ),
                              ///--------------product-detail
                              Container(
                                height: 90,
                                width: 155,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.vertical(
                                      bottom: Radius.circular(15)
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, left: 20.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      ///--------------product-name
                                      Text(
                                        // '${getFlashSaleProductsModel.data[index].name}',
                                        'NAME',
                                        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 16),
                                      ),
                                      SizedBox(height: 10.0,),
                                      ///--------------product-price
                                      Text(
                                        // '${getFlashSaleProductsModel.data[index].displayPrice}',
                                        '\$ 123.0',
                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                          fontSize: 14,
                                        ),
                                      ),
                                      ///--------------product-rating-row
                                      SizedBox(height: 5.0,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: RatingBar(
                                              initialRating: 3.5,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 15,
                                              ratingWidget: RatingWidget(
                                                full: Icon(Icons.star,color: Color(0xffE2B500),),
                                                half: Icon(Icons.star_half,color: Color(0xffE2B500)),
                                                empty: Icon(Icons.star_border_purple500_sharp,color: Color(0xffE2B500)),
                                              ),
                                              itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.add_box_outlined,
                                              size: 20,
                                              color: Colors.black,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Positioned(
                            right: 5.0,
                            top: 12,
                            child: Container(
                              height: 20,
                              width: 40,
                              child: Center(
                                child: Icon(
                                  Icons.favorite_border,
                                  size: 18,
                                  color: Colors.red,
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    SizedBox(width: 10.0,),
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  ///-------------best-seller-widget
  Widget _bestSellerWidget(BuildContext context){
    return Container(
      height: 235,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 15, 15, 15 ),
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                        'Best Seller',
                        style: Theme.of(context).textTheme.headline1
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 180,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(10, (index) {
                return Row(
                  children: [
                    index == 0
                        ?SizedBox(width: 20,)
                        :SizedBox(),
                    Column(
                      children: [
                        Container(
                          height: 120,
                          width: 135,
                          decoration: BoxDecoration(
                            color: Color(0xffDAD9DA),
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
                          ),
                          child: Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top:12.0, left: 0.0),
                                child: Image.asset('assets/images/bestSellerImage.png',
                                  width: 120,
                                  fit: BoxFit.fill,
                                ),
                              )),
                        ),
                        Container(
                          height: 50,
                          width: 135,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(0,3),
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Center(
                                  child: Text('Name',
                                    style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 14),
                                  ),
                                ),
                                SizedBox(height: 7.0,),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 10.0,),
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  ///-------------second-banner-widget
  Widget _secondBannerWidget(BuildContext context){
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
      child: GetBuilder<HomeController>(
        init: HomeController(),
        builder: (_) => !_.getAllGenericAppDataCheck
            ?SkeletonLoader(
          period: Duration(seconds: 2),
          highlightColor: Colors.grey,
          direction: SkeletonDirection.ltr,
          builder: Container(
            height: 140,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
          ),
        )
            :genericAppDataModel.data.staticBanners.length <= 1
            ?Container()
            :Container(
          height: 140,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      '${baseUrl + genericAppDataModel.data.staticBanners[1].image.originalMediaPath}'
                  ),
                  fit: BoxFit.fill
              )
          ),
        ),
      ),
    );
  }

  ///--------------featured-products-widget
  Widget _featuredProductsWidget(BuildContext context){
    return Container(
      height: 310,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 15, 10 ),
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                        'Featured Products',
                        style: Theme.of(context).textTheme.headline1
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      print('-------->> View On Sale');
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                          'View All >>',
                          style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 10)
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 266,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(4, (index) {
                return Row(
                  children: [
                    index == 0
                        ?SizedBox(width: 20,)
                        :SizedBox(),
                    Stack(
                        children: [
                          Column(
                            children: [
                              ///--------------product-image
                              Container(
                                height: 170,
                                width: 155,
                                decoration: BoxDecoration(
                                  color: Color(0xffDAD9DA),
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(15)
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
                                  child:
                                  Image.asset('assets/images/featuredImage.png',fit: BoxFit.fill,),
                                  // Image.network('${baseUrl+getFlashSaleProductsModel.data[index].media[0].originalMediaPath}',
                                  //   // height: 300,
                                  //   fit: BoxFit.fill,
                                  // ),
                                ),
                              ),
                              ///--------------product-detail
                              Container(
                                height: 90,
                                width: 155,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.vertical(
                                      bottom: Radius.circular(15)
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(0,3),
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0, left: 20.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      ///--------------product-name
                                      Text(
                                        // '${getFlashSaleProductsModel.data[index].name}',
                                        'NAME',
                                        style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 16),
                                      ),
                                      SizedBox(height: 10.0,),
                                      ///--------------product-price
                                      Text(
                                        // '${getFlashSaleProductsModel.data[index].displayPrice}',
                                        '\$ 123.0',
                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                          fontSize: 14,
                                        ),
                                      ),
                                      ///--------------product-rating-row
                                      SizedBox(height: 5.0,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: RatingBar(
                                              initialRating: 3.5,
                                              direction: Axis.horizontal,
                                              allowHalfRating: true,
                                              itemCount: 5,
                                              itemSize: 15,
                                              ratingWidget: RatingWidget(
                                                full: Icon(Icons.star,color: Color(0xffE2B500),),
                                                half: Icon(Icons.star_half,color: Color(0xffE2B500)),
                                                empty: Icon(Icons.star_border_purple500_sharp,color: Color(0xffE2B500)),
                                              ),
                                              itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                              onRatingUpdate: (rating) {
                                                print(rating);
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(right: 10),
                                            child: Icon(
                                              Icons.add_box_outlined,
                                              size: 20,
                                              color: Colors.black,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Positioned(
                            right: 5.0,
                            top: 12,
                            child: Container(
                              height: 20,
                              width: 40,
                              child: Center(
                                child: Icon(
                                  Icons.favorite_border,
                                  size: 18,
                                  color: Colors.red,
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    SizedBox(width: 10.0,),
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  ///-------------third-banner-widget
  Widget _thirdBannerWidget(BuildContext context){
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
      child: GetBuilder<HomeController>(
        init: HomeController(),
        builder: (_) => !_.getAllGenericAppDataCheck
            ?SkeletonLoader(
          period: Duration(seconds: 2),
          highlightColor: Colors.grey,
          direction: SkeletonDirection.ltr,
          builder: Container(
            height: 140,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
          ),
        )
            :genericAppDataModel.data.staticBanners.length <= 2
            ?Container()
            :Container(
          height: 140,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      '${baseUrl + genericAppDataModel.data.staticBanners[2].image.originalMediaPath}'
                  ),
                  fit: BoxFit.fill
              )
          ),
        ),
      ),
    );
  }

  ///-------------our-blog-widget
  List blog = [
    {'img': 'assets/images/blog1.png'},
    {'img': 'assets/images/blog2.png'},
    {'img': 'assets/images/blog3.png'},
    {'img': 'assets/images/blog4.png'},
  ];
  Widget _ourBlogWidget(BuildContext context){
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 8.0, right: 8.0),
      child: Container(
        child: Column(
          children: [
            Container(
              height: 40,
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Text('Our Blog',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                  ),
                  Text('View All >>',
                  ),
                ],
              ),
            ),
            Container(
              height: 250,
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                itemCount: 4,
                itemBuilder: (BuildContext context, int index) =>
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          image: DecorationImage(
                              image: AssetImage(blog[index]['img']),
                              fit: BoxFit.fill
                          )
                      ),
                    ),
                staggeredTileBuilder: (int index) =>
                    StaggeredTile.count(2, index.isEven ? 2 : 1.5),
                mainAxisSpacing: 5.0,
                crossAxisSpacing: 5.0,
              ),
            ),
            SizedBox(height: 20.0),
          ],
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(
        init: AppController(),
        builder:(_)=> _.internetChecker
            ?homeView1(context)
            :noInternetWidget(context)
    );
  }

  ///-----------no-internet-view-widget------------------------
  Widget noInternetWidget(BuildContext context){
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Color(0xffF6F6F6),
        child: SafeArea(
          child: Column(
            children: [
              Expanded(
                  flex: 3,
                  child: Image.asset('assets/images/noInternetImage.png')
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    InkWell(
                      onTap: (){
                        ///------get-all-currency-api-call
                        getMethod(
                            context,
                            getAllCurrencyApi,
                            null,
                            false,
                            getAllCurrencies
                        );
                        ///------get-all-language-api-call
                        getMethod(
                            context,
                            getAllLanguageApi,
                            null,
                            false,
                            getAllLanguages
                        );
                        ///------get-all-categories-api-call
                        getMethod(
                            context,
                            getAllCategoriesApi,
                            null,
                            false,
                            getAllCategories
                        );
                        ///------get-all-generic-app-data-api-call
                        getMethod(
                            context,
                            getAllGenericAppDataApi,
                            null,
                            false,
                            getAllGenericAppData
                        );
                        ///------get-flash-sale-data-api-call
                        getMethod(
                            context,
                            getFlashSaleDataApi,
                            null,
                            false,
                            getFlashSaleData
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width*.75,
                        height: 50,
                        decoration: BoxDecoration(
                            color: Theme.of(context).buttonColor,
                            borderRadius: BorderRadius.circular(20)
                        ),
                        child: Center(
                          child: Text(
                            'Reload',
                            style: Theme.of(context).textTheme.button.copyWith(color: Colors.white,fontSize: 25),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}
