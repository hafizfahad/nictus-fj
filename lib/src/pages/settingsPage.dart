import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  Language _getLanguage = Language.fromJson(jsonDecode(box.read('customLanguageSettings')));
  Currency _getCurrency = Currency.fromJson(jsonDecode(box.read('customCurrencySettings')));

  int _selectedLanguageIndex;
  int _selectedCurrencyIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('selectedLanguage--->>${_getLanguage.name}');
    print('selectedCurrency--->>${_getCurrency.name}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'App Settings',
          style: Theme.of(context).textTheme.headline1.copyWith(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  ///---------------currency-dropdown
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.white,
                      // height: 50,
                      width: MediaQuery.of(context).size.width * 0.85,
                      child: Center(
                        child: Theme(
                          data: ThemeData(
                            primaryColor: Colors.black,
                          ),
                          child: DropdownSearch(
                            validator: (value){
                              if(value == null){
                                return 'Field Required';
                              }
                              else
                                return null;
                            },
                            mode: Mode.MENU,
                            showSearchBox: true,
                            showSelectedItem: true,
                            label: 'Currency',
                            selectedItem: _getCurrency.name,
                            dropdownSearchDecoration: InputDecoration(
                            ),
                            searchBoxDecoration: InputDecoration(),
                            items: List.generate(getAllCurrencyModel.data.length, (index) {
                              return getAllCurrencyModel.data[index].name.toString();
                            }),
                            onChanged: (value){
                              print('$value');
                              getAllCurrencyModel.data.forEach((element) {
                                if(element.name == value){
                                  setState(() {
                                    _selectedCurrencyIndex = getAllCurrencyModel.data.indexOf(element);
                                    print('Index---->> $_selectedCurrencyIndex');
                                  });
                                }
                              });

                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  ///---------------language-dropdown
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.white,
                      // height: 50,
                      width: MediaQuery.of(context).size.width * 0.85,
                      child: Center(
                        child: Theme(
                          data: ThemeData(
                            primaryColor: Colors.black,
                          ),
                          child: DropdownSearch(
                            validator: (value){
                              if(value == null){
                                return 'Field Required';
                              }
                              else
                                return null;
                            },
                            mode: Mode.MENU,
                            showSearchBox: true,
                            showSelectedItem: true,
                            label: 'Language',
                            selectedItem: _getLanguage.name,
                            dropdownSearchDecoration: InputDecoration(
                            ),
                            searchBoxDecoration: InputDecoration(),
                            items: List.generate(getAllLanguageModel.data.length, (index) {
                              return getAllLanguageModel.data[index].name.toString();
                            }),
                            onChanged: (value){
                              print('$value');
                              getAllLanguageModel.data.forEach((element) {
                                if(element.name == value){
                                  setState(() {
                                    _selectedLanguageIndex = getAllLanguageModel.data.indexOf(element);
                                    print('Index---->> $_selectedLanguageIndex');
                                  });
                                }
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: InkWell(
                onTap: (){
                  if(_selectedCurrencyIndex != null){
                    print('selectedCurrencyModel---->> ${getAllCurrencyModel.data[_selectedCurrencyIndex].name}');
                    box.write('customCurrencySettings', jsonEncode(getAllCurrencyModel.data[_selectedCurrencyIndex]));
                  }
                  if(_selectedLanguageIndex != null){
                    print('selectedLanguageModel---->> ${getAllLanguageModel.data[_selectedLanguageIndex].name}');
                    box.write('customLanguageSettings', jsonEncode(getAllLanguageModel.data[_selectedLanguageIndex]));
                  }

                  Get.find<HomeController>().changeGetAllCategoriesCheck(false);
                  Get.find<HomeController>().changeGetAllGenericAppDataCheck(false);
                  Get.find<HomeController>().changeGetCategoryWiseProductsDataCheck(false);
                  Get.find<HomeController>().changeGetAFlashSaleDataCheck(false);
                  Get.find<HomeController>().changeGetTopSellingProductsDataCheck(false);

                  Get.offAllNamed('/bottomNavBar');
                },
                child: Container(
                  width: MediaQuery.of(context).size.width*.75,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Theme.of(context).buttonColor,
                      borderRadius: BorderRadius.circular(20)
                  ),
                  child: Center(
                    child: Text(
                      'Save',
                      style: Theme.of(context).textTheme.button.copyWith(color: Colors.white,fontSize: 25),
                    ),
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
