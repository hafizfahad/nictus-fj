import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/customGeneralBottomBar.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(drawerShow: false,),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: Row(
                  children: [
                    Text(
                      'CART',
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: customLightBlackColor
                        ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      '(3 Items)',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                          color: customThemeColor
                      ),
                    ),
                  ],
                ),
              ),
              ///---page-viewer
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: Container(
                  height: 70,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 2,
                      blurRadius: 4,
                      offset: Offset(0,2)
                    )]
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ///-cart-icon
                      Expanded(
                        child: SvgPicture.asset(
                            'assets/Icons/Product_page_icons/shopping-cart.svg'
                        )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-checkout-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-document.svg'
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-payment-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-credit-card.svg'
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-confirmation-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-file-check.svg'
                          )
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20,),
              ///---page-view
              Wrap(
                children: [
                  Container(
                    height: 6,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  Wrap(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                        child: Column(
                          children: [
                            ///---vendor-name
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'By ',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: customLightBlackColor
                                  ),
                                ),
                                Text(
                                  'Luxury house décor',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: customRatingStartColor,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                              ],
                            ),

                            ///---cart-items
                            Wrap(
                              children: List.generate(2, (index){
                                return Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                                      child: Container(
                                        height: 143,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: Theme.of(context).scaffoldBackgroundColor
                                        ),
                                        child: Row(
                                          children: [
                                            ///product-image
                                            Container(
                                              height: double.infinity,
                                              width: 125,
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/productImage.png'
                                                      ),
                                                      fit: BoxFit.cover
                                                  )
                                              ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            'Modern ladies casual bag',
                                                            style: Theme.of(context).textTheme.headline1.copyWith(
                                                                color: customDarkGreyColor
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                          child: SvgPicture.asset('assets/Icons/cart_checkout_icons/fi-rr-cross-small.svg'),
                                                        )
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                                                      child: Row(
                                                        children: [
                                                          RatingBar(
                                                            initialRating: 3.5,
                                                            direction: Axis.horizontal,
                                                            allowHalfRating: true,
                                                            itemCount: 5,
                                                            itemSize: 15,
                                                            ratingWidget: RatingWidget(
                                                              full: Icon(Icons.star,color: customRatingStartColor,),
                                                              half: Icon(Icons.star_half,color: customRatingStartColor),
                                                              empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                                                            ),
                                                            itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                            onRatingUpdate: (rating) {
                                                              print(rating);
                                                            },
                                                          ),
                                                          Padding(
                                                            padding: const EdgeInsets.only(left: 3),
                                                            child: Text(
                                                              '(150)',
                                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                  fontSize: 10
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                                      child: Text(
                                                        '\$150.00',
                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                            fontSize: 14,
                                                            color: customThemeColor
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                                      child: Container(
                                                        height: 24,
                                                        width: 84,
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(5),
                                                            color: customIconListTileColor
                                                        ),
                                                        child: Row(
                                                          children: [
                                                            InkWell(
                                                              onTap: (){},
                                                              child: Container(
                                                                height: double.infinity,
                                                                width: 24,
                                                                decoration: BoxDecoration(
                                                                    color: customThemeColor,
                                                                    borderRadius: BorderRadius.circular(5)
                                                                ),
                                                                child: Icon(
                                                                  Icons.remove,
                                                                  color: Colors.white,
                                                                  size: 25,
                                                                ),
                                                              ),
                                                            ),
                                                            Expanded(
                                                                child: Center(
                                                                  child: Text(
                                                                    '1',
                                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                                        fontSize: 14,
                                                                        color: customThemeColor
                                                                    ),
                                                                  ),
                                                                )
                                                            ),
                                                            InkWell(
                                                              onTap: (){},
                                                              child: Container(
                                                                height: double.infinity,
                                                                width: 24,
                                                                decoration: BoxDecoration(
                                                                    color: customThemeColor,
                                                                    borderRadius: BorderRadius.circular(5)
                                                                ),
                                                                child: Icon(
                                                                  Icons.add,
                                                                  color: Colors.white,
                                                                  size: 25,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    index != 1
                                        ?Padding(
                                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                      child: Divider(
                                        color: customSmoothGreyColor,
                                      ),
                                    ):SizedBox()
                                  ],
                                );
                              }),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    height: 6,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  SizedBox(height: 20,),
                  Container(
                    height: 6,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  Wrap(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                        child: Column(
                          children: [
                            ///---vendor-name
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'By ',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: customLightBlackColor
                                  ),
                                ),
                                Text(
                                  'The fashionista',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: customRatingStartColor,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                              ],
                            ),

                            ///---cart-items
                            Wrap(
                              children: List.generate(1, (index){
                                return Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                                      child: Container(
                                        height: 143,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: Theme.of(context).scaffoldBackgroundColor
                                        ),
                                        child: Row(
                                          children: [
                                            ///product-image
                                            Container(
                                              height: double.infinity,
                                              width: 125,
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/productImage.png'
                                                      ),
                                                      fit: BoxFit.cover
                                                  )
                                              ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            'Modern ladies casual bag',
                                                            style: Theme.of(context).textTheme.headline1.copyWith(
                                                                color: customDarkGreyColor
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                          child: SvgPicture.asset('assets/Icons/cart_checkout_icons/fi-rr-cross-small.svg'),
                                                        )
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                                                      child: Row(
                                                        children: [
                                                          RatingBar(
                                                            initialRating: 3.5,
                                                            direction: Axis.horizontal,
                                                            allowHalfRating: true,
                                                            itemCount: 5,
                                                            itemSize: 15,
                                                            ratingWidget: RatingWidget(
                                                              full: Icon(Icons.star,color: customRatingStartColor,),
                                                              half: Icon(Icons.star_half,color: customRatingStartColor),
                                                              empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                                                            ),
                                                            itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                            onRatingUpdate: (rating) {
                                                              print(rating);
                                                            },
                                                          ),
                                                          Padding(
                                                            padding: const EdgeInsets.only(left: 3),
                                                            child: Text(
                                                              '(150)',
                                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                  fontSize: 10
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                                      child: Text(
                                                        '\$150.00',
                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                            fontSize: 14,
                                                            color: customThemeColor
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                                      child: Container(
                                                        height: 24,
                                                        width: 84,
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(5),
                                                            color: customIconListTileColor
                                                        ),
                                                        child: Row(
                                                          children: [
                                                            InkWell(
                                                              onTap: (){},
                                                              child: Container(
                                                                height: double.infinity,
                                                                width: 24,
                                                                decoration: BoxDecoration(
                                                                    color: customThemeColor,
                                                                    borderRadius: BorderRadius.circular(5)
                                                                ),
                                                                child: Icon(
                                                                  Icons.remove,
                                                                  color: Colors.white,
                                                                  size: 25,
                                                                ),
                                                              ),
                                                            ),
                                                            Expanded(
                                                                child: Center(
                                                                  child: Text(
                                                                    '1',
                                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                                        fontSize: 14,
                                                                        color: customThemeColor
                                                                    ),
                                                                  ),
                                                                )
                                                            ),
                                                            InkWell(
                                                              onTap: (){},
                                                              child: Container(
                                                                height: double.infinity,
                                                                width: 24,
                                                                decoration: BoxDecoration(
                                                                    color: customThemeColor,
                                                                    borderRadius: BorderRadius.circular(5)
                                                                ),
                                                                child: Icon(
                                                                  Icons.add,
                                                                  color: Colors.white,
                                                                  size: 25,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              }),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    height: 6,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  SizedBox(height: 20,),
                ],
              ),
              ///---voucher-text-field
              SizedBox(
                height: 85,
                width: double.infinity,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        child: TextFormField(
                          style: Theme.of(context).textTheme.headline4.copyWith(
                            fontSize: 12,
                            color: customLightBlackColor
                          ),
                          controller: voucherController,
                          decoration: InputDecoration(
                            hintText: 'Enter Voucher Code (If Any)',
                              hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                                fontSize: 12,
                                color: customLightGreyColor
                              ),
                              isDense: true,
                              contentPadding:const EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10.0),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                borderSide: BorderSide(color: customThemeColor),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(color: customSmoothGreyColor),),
                              suffixIcon: InkWell(
                                onTap: (){},
                                child: Container(
                                  height: 45,
                                  width: 93,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: customRatingStartColor
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Apply',
                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                        fontSize: 15,
                                        color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ///---bill-detail
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(15)
                  ),
                  color: customIconListTileColor
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Billing',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 18),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$250.00',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Discount',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$25',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Delivery Charges',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$25',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14,bottom: 50),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.headline1.copyWith(
                                    color: customThemeColor,
                                  fontSize: 14
                                ),
                              ),
                            ),
                            Text(
                              '\$300',
                              style: Theme.of(context).textTheme.headline1.copyWith(
                                  color: customThemeColor,
                                  fontSize: 14
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: (){
          Get.toNamed(PageRoutes.checkout);
        },
        child: CustomGeneralBottomBar(
          text: 'Continue To Checkout',
        )
      ),
    );
  }
}
