import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/repository/defaultSettingRepository.dart';
import 'package:multi_vendor_customer/src/repository/environmentRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  ///-----navigation-function
  void navigator() {
    Get.offAllNamed('/onBoard');
  }

  ///-------- Set-timer-SplashPage
  _timer() async {
    return Timer(Duration(milliseconds: 5000), navigator);
  }


  //---------internet-checker-functions----------------------


  @override
  void dispose() {
    connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return updateConnectionStatus(result);
  }
  Future<void> updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState((){
          connectionStatus = result.toString();
          if(result != ConnectivityResult.none){
            internetCheck = true;
            _timer();
            ///------get-environment-api-call
            getMethod(
                context,
                getEnvironmentApi,
                null,
                false,
                getEnvironment
            );

            ///------get-default-settings-api-call
            getMethod(
                context,
                getDefaultSettingsApi,
                null,
                false,
                getDefaultSettings
            );
          }else{
            internetCheck = false;
            print('InternetOFF');
          }
        });
        break;
      default:
        setState(() => connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }
  //-------------------------------------------------------------------

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.put(AppController());
    initConnectivity();
    connectivitySubscription = connectivity.onConnectivityChanged.listen(updateConnectionStatus);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: internetCheck
          ?Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('${configSplashData.splashBackground}'),
            fit: BoxFit.fill
          )
        ),
        child: SafeArea(
          child: Column(
            children: [
              ///----------logo part
              Expanded(
                flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset(
                      '${configSplashData.splashLogo}',
                    ),
                  )
              ),
              ///----------powered-by part
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      'Powered By',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(fontSize: 15),
                    ),
                    Flexible(
                      child: Image(
                        fit: BoxFit.contain,
                        width: MediaQuery.of(context).size.width*.3,
                        image: AssetImage(
                          '${configSplashData.poweredByLogo}',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ),
      )
          :Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Color(0xffF6F6F6),
        child: SafeArea(
            child: Image.asset('assets/images/noInternetImage.png'),
        ),
      ),
    );
  }
}