import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/productFilterModel.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/widgets/homeDrawerComponent.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/repository/getShopPageDataRepository.dart';
import 'package:multi_vendor_customer/src/repository/getShopPageProductsRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class ShopPage extends StatefulWidget {
  @override
  _ShopPageState createState() => _ShopPageState();
}

ProductFilterModel productFilterModel = ProductFilterModel();

class _ShopPageState extends State<ShopPage> {

  TextEditingController shopPageSearchController = TextEditingController();

  Future _paginationDataLoad() async {
    // perform fetching data delay
    // await new Future.delayed(new Duration(seconds: 2));

    print("load more");
    // update data and loading status
    if(getShopPageProductsModel.data.meta.lastPage > getShopPageProductsModel.data.meta.currentPage){
      Get.find<AppController>()
          .changeGetPaginationProgressCheck(true);

      setState(() {
        productFilterModel.selectedPage = getShopPageProductsModel.data.meta.currentPage;
      });

      ///------get-shop-page-filtered-products-api-call
      getMethodForFilterProductForPagination(
          context,
          productFilterModel);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    productFilterModel.selectedSorting = ProductFilterModelForSort();
    productFilterModel.selectedBrandList = [];
    productFilterModel.selectedAttributesList = [];
    productFilterModel.minPrice = 0;
    productFilterModel.maxPrice = 1;
    productFilterModel.selectedPage = null;

    Get.find<AppController>().changeGetPaginationProgressCheck(false);

    ///------get-shop-page-filtered-products-api-call
    getMethodForFilterProduct(context, productFilterModel);

    ///------get-shop-page-data-api-call
    getMethod(context, getShopPageDataApi, null,
        false, getShopPageData);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (homeController) => GestureDetector(
        onTap: (){
          ///---keyboard-close
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          drawer: MyCustomDrawer(),
          appBar: MyCustomAppBar(
            drawerShow: true,
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GetBuilder<AppController>(
              init: AppController(),
              builder: (appController) => Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Column(
                  children: [
                    ///---search-bar
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Container(
                        color: Color(0xffF9F9F9),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Theme(
                            data: ThemeData(primaryColor: Color(0xff363636)),
                            child: TextFormField(
                              controller: shopPageSearchController,
                              onFieldSubmitted: (v){
                                Get.find<HomeController>()
                                    .changeGetShopPageProductsDataCheck(
                                    false);

                                setState(() {
                                  productFilterModel.searchName = shopPageSearchController.text;
                                });
                                ///------get-shop-page-filtered-products-api-call
                                getMethodForFilterProduct(
                                    context,
                                    productFilterModel);

                                ///---keyboard-close
                                FocusScopeNode currentFocus = FocusScope.of(context);
                                if (!currentFocus.hasPrimaryFocus) {
                                  currentFocus.unfocus();
                                }
                              },
                              decoration: InputDecoration(
                                  suffixIcon: InkWell(
                                    onTap: (){

                                      Get.find<HomeController>()
                                          .changeGetShopPageProductsDataCheck(
                                          false);

                                      setState(() {
                                        productFilterModel.searchName = shopPageSearchController.text;
                                      });
                                      ///------get-shop-page-filtered-products-api-call
                                      getMethodForFilterProduct(
                                          context,
                                          productFilterModel);

                                      ///---keyboard-close
                                      FocusScopeNode currentFocus = FocusScope.of(context);
                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }
                                    },
                                    child: Icon(
                                      Icons.search,
                                      color: customLightGreyColor,
                                    ),
                                  ),
                                  border: InputBorder.none,
                                  hintText: 'search here',
                                  hintStyle: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      .copyWith(color: customLightGreyColor)),
                            ),
                          ),
                        ),
                      ),
                    ),

                    ///---filter-row
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                      child: !homeController.getShopPageDataCheck
                          ? SkeletonLoader(
                              period: Duration(seconds: 2),
                              highlightColor: Colors.grey,
                              direction: SkeletonDirection.ltr,
                              builder: Container(
                                height: 30,
                                width: double.infinity,
                                color: Colors.white,
                              ),
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // InkWell(
                                //   onTap: (){
                                //     Get.toNamed(PageRoutes.shopCategories);
                                //   },
                                //   child: Padding(
                                //     padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                                //     child: Text(
                                //       'Categories',
                                //       style: Theme.of(context).textTheme.headline4.copyWith(
                                //           color: customLightGreyColor
                                //       ),
                                //     ),
                                //   ),
                                // ),
                                Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        Get.toNamed(PageRoutes.shopFilters);
                                      },
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.fromLTRB(0, 8, 0, 8),
                                        child: Text(
                                          'Filters',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4
                                              .copyWith(
                                                  color: customLightGreyColor),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    PopupMenuButton(
                                        child: Center(
                                            child: Text(
                                          'Sort',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4
                                              .copyWith(
                                                  color: customLightGreyColor),
                                        )),
                                        itemBuilder: (context) =>
                                            <PopupMenuEntry>[
                                              PopupMenuItem(
                                                enabled: false,
                                                child: Text(
                                                  'Sort By',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5
                                                      .copyWith(
                                                          color:
                                                              customDarkBlackColor),
                                                ),
                                              ),
                                              PopupMenuItem(
                                                enabled: false,
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    Get.find<HomeController>()
                                                        .changeGetShopPageProductsDataCheck(
                                                            false);

                                                    setState(() {
                                                      productFilterModel.selectedSorting =
                                                      new ProductFilterModelForSort();
                                                    });

                                                    ///------get-shop-page-filtered-products-api-call
                                                    getMethodForFilterProduct(
                                                        context,
                                                        productFilterModel);
                                                  },
                                                  child: Text(
                                                    'Default',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline4
                                                        .copyWith(
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ),
                                              ),
                                              PopupMenuItem(
                                                enabled: false,
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    Get.find<HomeController>()
                                                        .changeGetShopPageProductsDataCheck(
                                                            false);

                                                    setState(() {
                                                      productFilterModel
                                                          .selectedSorting
                                                          .sortField = 'price';
                                                      productFilterModel
                                                          .selectedSorting
                                                          .sortType = 'asc';
                                                    });

                                                    ///------get-shop-page-filtered-products-api-call
                                                    getMethodForFilterProduct(
                                                        context,
                                                        productFilterModel);
                                                  },
                                                  child: Text(
                                                    'Price (Low to High)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline4
                                                        .copyWith(
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ),
                                              ),
                                              PopupMenuItem(
                                                enabled: false,
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                    Get.find<HomeController>()
                                                        .changeGetShopPageProductsDataCheck(
                                                            false);

                                                    setState(() {
                                                      productFilterModel
                                                          .selectedSorting
                                                          .sortField = 'price';
                                                      productFilterModel
                                                          .selectedSorting
                                                          .sortType = 'desc';
                                                    });

                                                    ///------get-shop-page-filtered-products-api-call
                                                    getMethodForFilterProduct(
                                                        context,
                                                        productFilterModel);
                                                  },
                                                  child: Text(
                                                    'Price (High to Low)',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline4
                                                        .copyWith(
                                                            color:
                                                                customLightBlackColor),
                                                  ),
                                                ),
                                              ),
                                            ]),
                                  ],
                                ),
                                Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        print(
                                            '---->> gridViewForShopPageCheck ${appController.gridViewForShopPageCheck}');
                                        if (!appController
                                            .gridViewForShopPageCheck) {
                                          appController
                                              .changeGridViewForShopPageCheck(
                                                  true);
                                        }
                                      },
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.fromLTRB(8, 8, 0, 8),
                                        child: SizedBox(
                                          height: 14,
                                          width: 14,
                                          child: SvgPicture.asset(
                                            'assets/Icons/grid_2.svg',
                                            color: appController
                                                    .gridViewForShopPageCheck
                                                ? customThemeColor
                                                : Colors.black,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 25,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        print(
                                            '---->> gridViewForShopPageCheck ${appController.gridViewForShopPageCheck}');
                                        if (appController
                                            .gridViewForShopPageCheck) {
                                          appController
                                              .changeGridViewForShopPageCheck(
                                                  false);
                                        }
                                      },
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.fromLTRB(4, 8, 0, 8),
                                        child: SizedBox(
                                          height: 16,
                                          width: 16,
                                          child: SvgPicture.asset(
                                            'assets/Icons/list_2.svg',
                                            color: appController
                                                    .gridViewForShopPageCheck
                                                ? Colors.black
                                                : customThemeColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                    ),
                    Divider(
                      thickness: 1,
                      color: customSmoothGreyColor,
                    ),

                    ///---all-products-view
                    appController.gridViewForShopPageCheck
                        ? Expanded(
                            child: NotificationListener<ScrollNotification>(
                              onNotification: (ScrollNotification scrollInfo) {
                                if (!appController.getPaginationProgressCheck &&
                                    scrollInfo.metrics.pixels ==
                                        scrollInfo.metrics.maxScrollExtent) {
                                  _paginationDataLoad();
                                  print('ScrollNotification----->>');
                                }
                                return null;
                              },
                              child: SingleChildScrollView(
                                child: !homeController
                                        .getShopPageProductsDataCheck
                                    ? SkeletonLoader(
                                        period: Duration(seconds: 2),
                                        highlightColor: Colors.grey,
                                        direction: SkeletonDirection.ltr,
                                        builder: Center(
                                          child: Wrap(
                                            children: List.generate(6, (index) {
                                              return Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .45,
                                                child: Padding(
                                                  padding: index % 2 == 0
                                                      ? const EdgeInsets.fromLTRB(
                                                          0, 5, 5, 5)
                                                      : const EdgeInsets.fromLTRB(
                                                          5, 5, 0, 5),
                                                  child: Container(
                                                    height: 270,
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.vertical(
                                                        top: Radius.circular(10),
                                                        bottom:
                                                            Radius.circular(10),
                                                      ),
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.grey
                                                              .withOpacity(0.2),
                                                          spreadRadius: 2,
                                                          blurRadius: 6,
                                                          offset: Offset(0, 3),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Wrap(
                                          children: List.generate(
                                              shopPageProductListForPagination
                                                  .length, (index) {
                                            return Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .45,
                                              child: Padding(
                                                padding: index % 2 == 0
                                                    ? const EdgeInsets.fromLTRB(
                                                        0, 5, 5, 5)
                                                    : const EdgeInsets.fromLTRB(
                                                        5, 5, 0, 5),
                                                child: Stack(children: [
                                                  InkWell(
                                                    onTap: () {
                                                      ///------get-product-detail-data-api-call
                                                      getMethod(
                                                          context,
                                                          '$getProductsDetailApi/${shopPageProductListForPagination[index].slug}',
                                                          null,
                                                          false,
                                                          getProductDetailData);
                                                      Get.find<HomeController>()
                                                          .changeGetProductDetailDataCheck(
                                                              false);
                                                      Get.toNamed(PageRoutes
                                                          .productDetail);
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius.vertical(
                                                          top:
                                                              Radius.circular(10),
                                                          bottom:
                                                              Radius.circular(10),
                                                        ),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.grey
                                                                .withOpacity(0.2),
                                                            spreadRadius: 2,
                                                            blurRadius: 6,
                                                            offset: Offset(0, 3),
                                                          ),
                                                        ],
                                                      ),
                                                      child: Column(
                                                        children: [
                                                          ///--------------product-image
                                                          Container(
                                                            height: 145,
                                                            width:
                                                                double.infinity,
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .vertical(
                                                                top: Radius
                                                                    .circular(10),
                                                                bottom: Radius
                                                                    .circular(10),
                                                              ),
                                                              child: shopPageProductListForPagination[
                                                                              index]
                                                                          .media
                                                                          .length ==
                                                                      0
                                                                  ? Image.asset(
                                                                      'assets/images/imagePlaceHolder.jpg',
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    )
                                                                  : Image.network(
                                                                      '${baseUrl + shopPageProductListForPagination[index].media[0].originalMediaPath}',
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    ),
                                                            ),
                                                          ),

                                                          ///--------------product-detail
                                                          Container(
                                                            width:
                                                                double.infinity,
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 8.0,
                                                                      left: 10.0),
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  ///--------------product-name
                                                                  Text(
                                                                    '${shopPageProductListForPagination[index].name}',
                                                                    softWrap:
                                                                        true,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    maxLines: 2,
                                                                    style: Theme.of(
                                                                            context)
                                                                        .textTheme
                                                                        .headline1
                                                                        .copyWith(
                                                                            fontSize:
                                                                                14,
                                                                            color:
                                                                                customDarkGreyColor),
                                                                  ),
                                                                  SizedBox(
                                                                    height: 4.0,
                                                                  ),

                                                                  ///--------------product-rating-row
                                                                  Row(
                                                                    children: [
                                                                      RatingBar(
                                                                        ignoreGestures: true,
                                                                        initialRating:
                                                                            shopPageProductListForPagination[index]
                                                                                .reviewsAverageRating,
                                                                        direction:
                                                                            Axis.horizontal,
                                                                        allowHalfRating:
                                                                            true,
                                                                        itemCount:
                                                                            5,
                                                                        itemSize:
                                                                            15,
                                                                        ratingWidget:
                                                                            RatingWidget(
                                                                          full:
                                                                              Icon(
                                                                            Icons
                                                                                .star,
                                                                            color:
                                                                                customRatingStartColor,
                                                                          ),
                                                                          half: Icon(
                                                                              Icons
                                                                                  .star_half,
                                                                              color:
                                                                                  customRatingStartColor),
                                                                          empty: Icon(
                                                                              Icons
                                                                                  .star_border_purple500_sharp,
                                                                              color:
                                                                                  customRatingStartColor),
                                                                        ),
                                                                        itemPadding:
                                                                            EdgeInsets.symmetric(
                                                                                horizontal: 0.0),
                                                                        onRatingUpdate:
                                                                            (rating) {
                                                                          print(
                                                                              rating);
                                                                        },
                                                                      ),
                                                                      Padding(
                                                                        padding: const EdgeInsets
                                                                                .only(
                                                                            left:
                                                                                3),
                                                                        child:
                                                                            Text(
                                                                          '(${shopPageProductListForPagination[index].totalReviews})',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .subtitle2
                                                                              .copyWith(fontSize: 10),
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 10.0,
                                                                  ),
                                                                  (shopPageProductListForPagination[index].flashSale !=
                                                                              null ||
                                                                          shopPageProductListForPagination[index].specialSale !=
                                                                              null)
                                                                      ? Text(
                                                                          '${shopPageProductListForPagination[index].displayPrice}',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .subtitle2
                                                                              .copyWith(
                                                                                decoration: TextDecoration.lineThrough,
                                                                              ),
                                                                        )
                                                                      : SizedBox(),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      ///--------------product-price
                                                                      Expanded(
                                                                        child: (shopPageProductListForPagination[index].flashSale != null &&
                                                                                shopPageProductListForPagination[index].specialSale == null)
                                                                            ? Text(
                                                                                '${shopPageProductListForPagination[index].flashSale.displayPrice}',
                                                                                style: Theme.of(context).textTheme.headline1.copyWith(
                                                                                    fontSize: 14,
                                                                                    color: customThemeColor),
                                                                              )
                                                                            : (shopPageProductListForPagination[index].flashSale == null &&
                                                                            shopPageProductListForPagination[index].specialSale != null)
                                                                                ? Text(
                                                                                    '${shopPageProductListForPagination[index].specialSale.displayPrice}',
                                                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                                                        fontSize: 14,
                                                                                        color: customThemeColor),
                                                                                  )
                                                                                : Text(
                                                                                    '${shopPageProductListForPagination[index].displayPrice}',
                                                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                                                        fontSize: 14,
                                                                                        color: customThemeColor),
                                                                                  ),
                                                                      ),
                                                                      InkWell(
                                                                        onTap:
                                                                            () {},
                                                                        child:
                                                                            Padding(
                                                                          padding: const EdgeInsets.fromLTRB(
                                                                              5,
                                                                              5,
                                                                              10,
                                                                              5),
                                                                          child:
                                                                              Icon(
                                                                            Icons
                                                                                .add_box_rounded,
                                                                            size:
                                                                                20,
                                                                            color:
                                                                                customThemeColor,
                                                                          ),
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 8.0,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    right: 0.0,
                                                    top: 0,
                                                    child: InkWell(
                                                      onTap: (){
                                                        print('------------->>press');
                                                        if(currentUserModel != null){
                                                          ///post-method
                                                          postMethod(
                                                              context,
                                                              addToWishListApi,
                                                              {
                                                                'product_id': shopPageProductListForPagination[index].id
                                                              },
                                                              true,
                                                              addToWishListDataRepo
                                                          );
                                                        }else{
                                                          notLoggedAlert(
                                                              context,
                                                              'Error',
                                                              'You Have to Login First'
                                                          );
                                                          print('------------->>NotLoggedIn');
                                                        }
                                                      },
                                                      child: Padding(
                                                        padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                                        child: Container(
                                                          height: 14,
                                                          width: 16,
                                                          child: Center(
                                                              child: SvgPicture.asset(
                                                                'assets/Icons/Product_page_icons/Group 366.svg',
                                                                color: customThemeColor,
                                                              )
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                      top: 10.0,
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: List.generate(
                                                            shopPageProductListForPagination[index]
                                                                .tags.length, (tagIndex){
                                                          return Column(
                                                            children: [
                                                              Container(
                                                                height: 20,
                                                                decoration: BoxDecoration(
                                                                    color: customThemeColor,
                                                                    borderRadius: BorderRadius.only(
                                                                      topRight: Radius.circular(6.0),
                                                                      bottomRight: Radius.circular(6.0),
                                                                    )
                                                                ),
                                                                child: Center(
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                                      child: Text('${shopPageProductListForPagination
                                                                      [index].tags[tagIndex]}',
                                                                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                            fontSize: 12,
                                                                            color: Colors.white
                                                                        ),
                                                                      ),
                                                                    )),
                                                              ),
                                                              SizedBox(height: 6,),
                                                            ],
                                                          );
                                                        }),
                                                      )
                                                  ),
                                                ]),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                              ),
                            ),
                          )
                        : Expanded(
                            child: NotificationListener<ScrollNotification>(
                              onNotification: (ScrollNotification scrollInfo) {
                                if (!appController.getPaginationProgressCheck &&
                                    scrollInfo.metrics.pixels ==
                                        scrollInfo.metrics.maxScrollExtent) {
                                  _paginationDataLoad();
                                  print('ScrollNotification----->>');
                                }
                                return null;
                              },
                              child: SingleChildScrollView(
                                child: !homeController
                                        .getShopPageProductsDataCheck
                                    ? SkeletonLoader(
                                        period: Duration(seconds: 2),
                                        highlightColor: Colors.grey,
                                        direction: SkeletonDirection.ltr,
                                        builder: Center(
                                          child: Wrap(
                                            children: List.generate(6, (index) {
                                              return Container(
                                                width: double.infinity,
                                                height: 110,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0, 12, 0, 0),
                                                  child: Container(
                                                    height: 270,
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.vertical(
                                                        top: Radius.circular(10),
                                                        bottom:
                                                            Radius.circular(10),
                                                      ),
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.grey
                                                              .withOpacity(0.2),
                                                          spreadRadius: 2,
                                                          blurRadius: 6,
                                                          offset: Offset(0, 3),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Wrap(
                                          children: List.generate(
                                              shopPageProductListForPagination
                                                  .length, (index) {
                                            return Container(
                                              width: double.infinity,
                                              height: (shopPageProductListForPagination[
                                                                  index]
                                                              .flashSale !=
                                                          null &&
                                                      shopPageProductListForPagination[
                                                                  index]
                                                              .specialSale !=
                                                          null)
                                                  ? 130
                                                  : 110,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 12, 0, 0),
                                                child: Stack(children: [
                                                  InkWell(
                                                    onTap: () {
                                                      ///------get-product-detail-data-api-call
                                                      getMethod(
                                                          context,
                                                          '$getProductsDetailApi/${shopPageProductListForPagination[index].slug}',
                                                          null,
                                                          false,
                                                          getProductDetailData);
                                                      Get.find<HomeController>()
                                                          .changeGetProductDetailDataCheck(
                                                              false);
                                                      Get.toNamed(PageRoutes
                                                          .productDetail);
                                                    },
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius.vertical(
                                                          top:
                                                              Radius.circular(10),
                                                          bottom:
                                                              Radius.circular(10),
                                                        ),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.grey
                                                                .withOpacity(0.2),
                                                            spreadRadius: 2,
                                                            blurRadius: 6,
                                                            offset: Offset(0, 3),
                                                          ),
                                                        ],
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          ///--------------product-image
                                                          Container(
                                                            height:
                                                                double.infinity,
                                                            width: 125,
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .vertical(
                                                                top: Radius
                                                                    .circular(10),
                                                                bottom: Radius
                                                                    .circular(10),
                                                              ),
                                                              child: shopPageProductListForPagination[
                                                                              index]
                                                                          .media
                                                                          .length ==
                                                                      0
                                                                  ? Image.asset(
                                                                      'assets/images/imagePlaceHolder.jpg',
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    )
                                                                  : Image.network(
                                                                      '${baseUrl + shopPageProductListForPagination[index].media[0].originalMediaPath}',
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    ),
                                                            ),
                                                          ),

                                                          ///--------------product-detail
                                                          Expanded(
                                                            child: Container(
                                                              height:
                                                                  double.infinity,
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top: 8.0,
                                                                        left:
                                                                            10.0),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    ///--------------product-name
                                                                    Expanded(
                                                                      child: Text(
                                                                        '${shopPageProductListForPagination[index].name}',
                                                                        softWrap:
                                                                            true,
                                                                        overflow:
                                                                            TextOverflow
                                                                                .ellipsis,
                                                                        maxLines:
                                                                            2,
                                                                        style: Theme.of(
                                                                                context)
                                                                            .textTheme
                                                                            .headline1
                                                                            .copyWith(
                                                                                fontSize: 14,
                                                                                color: customDarkGreyColor),
                                                                      ),
                                                                    ),
                                                                    // SizedBox(height: 4.0,),
                                                                    ///--------------product-rating-row
                                                                    Row(
                                                                      children: [
                                                                        RatingBar(
                                                                          ignoreGestures: true,
                                                                          initialRating:
                                                                              shopPageProductListForPagination[index].reviewsAverageRating,
                                                                          direction:
                                                                              Axis.horizontal,
                                                                          allowHalfRating:
                                                                              true,
                                                                          itemCount:
                                                                              5,
                                                                          itemSize:
                                                                              15,
                                                                          ratingWidget:
                                                                              RatingWidget(
                                                                            full:
                                                                                Icon(
                                                                              Icons.star,
                                                                              color:
                                                                                  customRatingStartColor,
                                                                            ),
                                                                            half: Icon(
                                                                                Icons.star_half,
                                                                                color: customRatingStartColor),
                                                                            empty: Icon(
                                                                                Icons.star_border_purple500_sharp,
                                                                                color: customRatingStartColor),
                                                                          ),
                                                                          itemPadding:
                                                                              EdgeInsets.symmetric(horizontal: 0.0),
                                                                          onRatingUpdate:
                                                                              (rating) {
                                                                            print(
                                                                                rating);
                                                                          },
                                                                        ),
                                                                        Padding(
                                                                          padding:
                                                                              const EdgeInsets.only(left: 3),
                                                                          child:
                                                                              Text(
                                                                            '(${shopPageProductListForPagination[index].totalReviews})',
                                                                            style: Theme.of(context)
                                                                                .textTheme
                                                                                .subtitle2
                                                                                .copyWith(fontSize: 10),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    SizedBox(
                                                                      height: 3.0,
                                                                    ),
                                                                    (shopPageProductListForPagination[index].flashSale !=
                                                                                null ||
                                                                            shopPageProductListForPagination[index].specialSale !=
                                                                                null)
                                                                        ? Text(
                                                                            '${shopPageProductListForPagination[index].displayPrice}',
                                                                            style: Theme.of(context)
                                                                                .textTheme
                                                                                .subtitle2
                                                                                .copyWith(
                                                                                  decoration: TextDecoration.lineThrough,
                                                                                ),
                                                                          )
                                                                        : SizedBox(),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      children: [
                                                                        ///--------------product-price
                                                                        Expanded(
                                                                          child: (shopPageProductListForPagination[index].flashSale != null &&
                                                                                  shopPageProductListForPagination[index].specialSale == null)
                                                                              ? Text(
                                                                                  '${shopPageProductListForPagination[index].flashSale.displayPrice}',
                                                                                  style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                )
                                                                              : (shopPageProductListForPagination[index].flashSale == null && shopPageProductListForPagination[index].specialSale != null)
                                                                                  ? Text(
                                                                                      '${shopPageProductListForPagination[index].specialSale.displayPrice}',
                                                                                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                    )
                                                                                  : Text(
                                                                                      '${shopPageProductListForPagination[index].displayPrice}',
                                                                                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14, color: customThemeColor),
                                                                                    ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap:
                                                                              () {},
                                                                          child:
                                                                              Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                5,
                                                                                5,
                                                                                5,
                                                                                5),
                                                                            child:
                                                                                Icon(
                                                                              Icons.add_box_rounded,
                                                                              size:
                                                                                  20,
                                                                              color:
                                                                                  customThemeColor,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    left: 75.0,
                                                    top: 0,
                                                    child: InkWell(
                                                      onTap: (){
                                                        print('------------->>press');
                                                        if(currentUserModel != null){
                                                          ///post-method
                                                          postMethod(
                                                              context,
                                                              addToWishListApi,
                                                              {
                                                                'product_id': shopPageProductListForPagination[index].id
                                                              },
                                                              true,
                                                              addToWishListDataRepo
                                                          );
                                                        }
                                                        else{
                                                          notLoggedAlert(
                                                              context,
                                                              'Error',
                                                              'You Have to Login First'
                                                          );
                                                          print('------------->>NotLoggedIn');
                                                        }
                                                      },
                                                      child: Padding(
                                                        padding: const EdgeInsets.fromLTRB(25, 12, 20, 30),
                                                        child: Container(
                                                          height: 14,
                                                          width: 16,
                                                          child: Center(
                                                              child: SvgPicture.asset(
                                                                'assets/Icons/Product_page_icons/Group 366.svg',
                                                                color: customThemeColor,
                                                              )
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                      top: 10.0,
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: List.generate(
                                                            shopPageProductListForPagination[index]
                                                                .tags.length, (tagIndex){
                                                          return Column(
                                                            children: [
                                                              Container(
                                                                height: 20,
                                                                decoration: BoxDecoration(
                                                                    color: customThemeColor,
                                                                    borderRadius: BorderRadius.only(
                                                                      topRight: Radius.circular(6.0),
                                                                      bottomRight: Radius.circular(6.0),
                                                                    )
                                                                ),
                                                                child: Center(
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                                      child: Text('${shopPageProductListForPagination
                                                                      [index].tags[tagIndex]}',
                                                                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                            fontSize: 12,
                                                                            color: Colors.white
                                                                        ),
                                                                      ),
                                                                    )),
                                                              ),
                                                              SizedBox(height: 6,),
                                                            ],
                                                          );
                                                        }),
                                                      )
                                                  ),
                                                ]),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                              ),
                            ),
                          ),
                    Container(
                      height: appController.getPaginationProgressCheck ? 50.0 : 0,
                      color: Colors.transparent,
                      child: Center(
                        child: new CircularProgressIndicator(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
