import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/customGeneralBottomBar.dart';
import 'package:multi_vendor_customer/src/components/notLoggedAlertDialog.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/pages/cartPage.dart';
import 'package:multi_vendor_customer/src/repository/addToWishListRepository.dart';
import 'package:multi_vendor_customer/src/repository/getProductDetailRepository.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';
import 'package:multi_vendor_customer/src/services/postService.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class ProductDetailPage extends StatefulWidget {
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage>
    with TickerProviderStateMixin{

  int _indexForImageSlider;

  List sliderList = [
    'assets/images/productImage.png',
    'assets/images/productImage.png',
    'assets/images/productImage.png',
  ];

  int _selectedTabBar = 0;


  String selectedVariant = '';
  int comparedVariantIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.find<HomeController>().quantity = 1;
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(homeController)=> Scaffold(
        appBar: MyCustomAppBar(drawerShow: false,),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
              child: !homeController.getProductDetailDataCheck
                  ?SkeletonLoader(
                period: Duration(seconds: 2),
                highlightColor: Colors.grey,
                direction: SkeletonDirection.ltr,
                builder: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    /// product-image-slider
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Container(
                        height: 270,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      )
                    ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Center(
                      child: Container(
                        width: 30,
                        height: 5,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15)
                        ),
                      ),
                    ),
                  ),

                    /// product-detail
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                      child: Container(
                        width: 200,
                        height: 10,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15)
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 9, 15, 0),
                      child: Container(
                        width: 100,
                        height: 10,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15)
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 5, 15, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 50,
                            height: 10,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)
                            ),
                          ),
                          Container(
                            width: 100,
                            height: 10,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
                      child: Container(
                        height: 74,
                        child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: List.generate(5, (index){
                              return Row(
                                children: [
                                  index == 0
                                      ?SizedBox(width: 15,)
                                      :SizedBox(),
                                  Row(
                                    children: [
                                      Container(
                                        height: 74,
                                        width: 142,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5)
                                        ),
                                      ),
                                      SizedBox(width: 12,)
                                    ],
                                  ),
                                ],
                              );
                            })
                        ),
                      ),
                    ),
                    Container(
                      height: 10,
                      width: double.infinity,
                    ),
                    /// vendor-name
                    SizedBox(
                      height: 45,
                      width: double.infinity,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                          child: Row(
                            children: [
                              Container(
                                width: 150,
                                height: 10,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15)
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 10,
                      width: double.infinity,
                    ),
                    /// delivery-detail
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 18,
                            ),
                            Container(
                              width: 200,
                              height: 10,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: 100,
                              height: 10,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                            SizedBox(
                              height: 18,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 100,
                                  height: 10,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                                Container(
                                  width: 50,
                                  height: 10,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 5,),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 10,
                      width: double.infinity,
                    ),
                ],
              ),
                  )
                  :Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// product-image-slider
                  getProductDetailModel.data.product.media.length == 0
                      ?SizedBox()
                      :Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child: CarouselSlider(
                          items: List.generate(getProductDetailModel.data.product.media.length, (index){
                            return Container(
                                height: 270,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                        '${baseUrl+getProductDetailModel.data.product.media[index].originalMediaPath}',
                                      ),
                                      fit: BoxFit.fill
                                  ),
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: List.generate(getProductDetailModel.data
                                                .product.tags.length, (tagIndex){
                                              return Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Container(
                                                        height: 20,
                                                        decoration: BoxDecoration(
                                                            color: customThemeColor,
                                                            borderRadius: BorderRadius.only(
                                                                topRight: Radius.circular(6.0),
                                                                bottomRight: Radius.circular(6.0) )
                                                        ),
                                                        child: Center(child: Padding(
                                                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                          child: Text('${getProductDetailModel.data
                                                              .product.tags[tagIndex]}',
                                                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                fontSize: 12,
                                                                color: Colors.white
                                                            ),
                                                          ),
                                                        )),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 6,
                                                  ),

                                                ],
                                              );
                                            }),
                                          ),
                                        )
                                    ),
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 15, 15, 0),
                                              child: Container(
                                                height: 14,
                                                width: 16,
                                                child: Center(
                                                    child: SvgPicture.asset(
                                                      'assets/Icons/Product_page_icons/Group 366.svg',
                                                      color: customThemeColor,
                                                    )
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                    )
                                  ],
                                )
                            );
                          }),

                          ///Slider Container properties
                          options: CarouselOptions(

                            onPageChanged: (index, value){
                              setState(() {
                                _indexForImageSlider = index;
                              });
                            },
                            height: 270,
                            // aspectRatio: 16/9,
                            viewportFraction: 1,
                            // initialPage: 0,
                            enableInfiniteScroll: true,
                            reverse: false,
                            autoPlay: true,
                            autoPlayInterval: Duration(seconds: 3),
                            autoPlayAnimationDuration: Duration(milliseconds: 800),
                            autoPlayCurve: Curves.fastOutSlowIn,
                            // enlargeCenterPage: true,
                            // onPageChanged: callbackFunction,
                            scrollDirection: Axis.horizontal,
                          ),

                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(getProductDetailModel.data.product.media.length, (i) {       //these two lines
                          int index = i; //are changed
                          return Container(
                            width: 5.0,
                            height: 5.0,
                            margin: EdgeInsets.fromLTRB(2, 10, 2, 0),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _indexForImageSlider == index
                                    ? Theme.of(context).buttonColor
                                    : Color.fromRGBO(0, 0, 0, 0.4)),
                          );
                        },
                        ).toList(),
                      ),
                    ],
                  ),

                  /// product-detail
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                    child: Text(
                      '${getProductDetailModel.data.product.name}',
                      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 16,color: customLightBlackColor),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 9, 15, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: (getProductDetailModel.data.product.flashSale == null &&
                              getProductDetailModel.data.product.specialSale == null)
                              ?Text(
                            '${getProductDetailModel.data.product.displayPrice}',
                            style: Theme.of(context).textTheme.headline1.copyWith(
                                fontSize: 16,
                                color: customThemeColor
                            ),
                          )
                              :(getProductDetailModel.data.product.flashSale == null &&
                              getProductDetailModel.data.product.specialSale != null)
                              ?Row(
                                children: [
                                  Text(
                                    '${getProductDetailModel.data.product.displayPrice}',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                        decoration: TextDecoration.lineThrough,
                                        fontSize: 16
                                    ),
                                  ),
                                  SizedBox(width: 8,),
                                  Text(
                                    '${getProductDetailModel.data.product.specialSale.displayPrice}',
                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                        fontSize: 16,
                                        color: customThemeColor
                                    ),
                                  ),
                                ],
                              )
                              :Row(
                                children: [
                                  Text(
                                    '${getProductDetailModel.data.product.displayPrice}',
                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                      decoration: TextDecoration.lineThrough,
                                      fontSize: 16
                                    ),
                                  ),
                                  SizedBox(width: 8,),
                                  Text(
                                    '${getProductDetailModel.data.product.flashSale.displayPrice}',
                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                        fontSize: 16,
                                        color: customThemeColor
                                    ),
                                  ),
                                ],
                              ),
                        ),
                        SvgPicture.asset('assets/Icons/Product_page_icons/fi-rr-share.svg')
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 5, 15, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            getProductDetailModel.data.product.productType == 2
                                ?''
                                :getProductDetailModel.data.product.stock == 0
                                ?'Out of stock'
                                :'Stock ${getProductDetailModel.data.product.stock}',
                            style: Theme.of(context).textTheme.headline5.copyWith(
                                color: getProductDetailModel.data.product.stock == 0
                                    ?customRedColor
                                    :customGreenSaleFlagColor
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            RatingBar(
                              initialRating: getProductDetailModel.data.product.reviewsAverageRating,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              ignoreGestures: true,
                              itemCount: 5,
                              itemSize: 15,
                              ratingWidget: RatingWidget(
                                full: Icon(Icons.star,color: customRatingStartColor,),
                                half: Icon(Icons.star_half,color: customRatingStartColor),
                                empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                              ),
                              itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3),
                              child: Text(
                                '(${getProductDetailModel.data.product.totalReviews})',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 10
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
                    child: Container(
                      height: 74,
                      child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: List.generate(5, (index){
                            return Row(
                              children: [
                                index == 0
                                    ?SizedBox(width: 15,)
                                    :SizedBox(),
                                Row(
                                  children: [
                                    Container(
                                      height: 74,
                                      width: 142,
                                      decoration: BoxDecoration(
                                          color: index % 2 == 0
                                              ?customVoucherColor1
                                              :customVoucherColor2,
                                          borderRadius: BorderRadius.circular(5)
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: DottedBorder(
                                            color: Colors.white,
                                            dashPattern: [6, 3],
                                            child: Padding(
                                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                              child: Stack(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: [
                                                          Text(
                                                            'Spend:',
                                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                                color: Colors.white,
                                                              fontSize: 12
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: const EdgeInsets.only(left: 5),
                                                            child: Text(
                                                              '\$150',
                                                              style: Theme.of(context).textTheme.headline5.copyWith(
                                                                  color: Colors.white,
                                                                  fontSize: 12
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      Row(
                                                        children: [
                                                          Text(
                                                            'and get:',
                                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                                color: Colors.white,
                                                                fontSize: 12
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: const EdgeInsets.only(left: 5),
                                                            child: Text(
                                                              '\$20 Off',
                                                              style: Theme.of(context).textTheme.headline5.copyWith(
                                                                  color: Colors.white,
                                                                  fontSize: 12
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                        'Collect',
                                                        style: Theme.of(context).textTheme.headline4.copyWith(
                                                            color: Colors.white,
                                                            fontSize: 12,
                                                          decoration: TextDecoration.underline
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 12,)
                                  ],
                                ),
                              ],
                            );
                          })
                      ),
                    ),
                  ),
                  Container(
                    height: 10,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  /// vendor-name
                  defaultSettingsModel.data.generalSettings.isMultiVendor == 1
                      ?getProductDetailModel.data.product.vendor == null
                      ?SizedBox()
                      :Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 45,
                        width: double.infinity,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                            child: Row(
                              children: [
                                Text(
                                  'By ',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,color: customLightBlackColor),
                                ),
                                Text(
                                  '${getProductDetailModel.data.product.vendor.store.name} ',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,color: customRatingStartColor,
                                      decoration: TextDecoration.underline
                                  ),
                                ),
                                Expanded(
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: SvgPicture.asset('assets/Icons/Product_page_icons/fi-rr-comment-alt.svg'),
                                    )
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 10,
                        width: double.infinity,
                        color: customIconListTileColor,
                      ),
                    ],
                  )
                  :SizedBox(),
                  /// delivery-detail
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 18,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  'Delivery Charges:',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customLightBlackColor
                                  ),
                                ),
                              ),
                              Text(
                                '\$34',
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Estimate delivery 5-7 days',
                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                fontSize: 10,
                                color: customLightGreyColor),
                          ),
                          SizedBox(
                            height: 18,
                          ),
                          Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'Location:',
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customLightBlackColor
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                    alignment: Alignment.centerRight,
                                    child: SvgPicture.asset(
                                        'assets/Icons/Product_page_icons/fi-rr-marker.svg')
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Text(
                                  '658 Ashley Street Middletown, CT 06457',
                                  textAlign: TextAlign.end,
                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                      fontSize: 12,
                                      color: customThemeColor
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 5,),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 10,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  /// tabs-views
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                      child: Column(
                        children: [
                          Container(
                            color:Colors.white,
                            height: 30,
                            width: double.infinity,
                            child: DefaultTabController(
                              length: 3/*homeController.getProductDetailTabsDataCount*/,
                              child: TabBar(
                                  onTap: (index){
                                    setState(() {
                                      _selectedTabBar = index;
                                    });
                                  },
                                  controller: productDetailPageTabController,
                                  unselectedLabelColor: Colors.grey,
                                  indicatorColor: Theme.of(context).buttonColor,
                                  indicator: UnderlineTabIndicator(
                                      borderSide: BorderSide(
                                        width: 2.0,
                                        color: Theme.of(context).buttonColor,
                                      ),
                                      insets: EdgeInsets.symmetric(horizontal:5.0)
                                  ),
                                  labelStyle: Theme.of(context).textTheme.headline5.copyWith(fontSize: 12),
                                  labelColor: Theme.of(context).buttonColor,
                                  labelPadding: EdgeInsets.all(1),
                                  tabs: [
                                    Text(
                                      'Description',
                                    ),
                                    Text(
                                      'Refund Policy',
                                      softWrap: true,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Text(
                                      'Reviews',
                                    ),
                                  ]
                              ),
                            ),
                          ),
                          SizedBox(height: 20,),
                          Padding(
                            padding: const EdgeInsets.only(top: 7),
                            child: SingleChildScrollView(
                              // controller: _scrollViewController,
                              child: Builder(builder: (_) {
                                if (_selectedTabBar == 0) {
                                  return Wrap(
                                    children: [
                                      getProductDetailModel.data.product.description == null ||
                                          getProductDetailModel.data.product.description.toString() == ''
                                          ?SizedBox()
                                          :Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                              Text(
                                        '${getProductDetailModel.data.product.description}',
                                        textAlign: TextAlign.left,
                                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                fontSize: 12,
                                                color: customLightBlackColor),
                                      ),
                                            ],
                                          ),
                                    ]
                                  );
                                }
                                else if (_selectedTabBar == 1) {
                                  return Wrap(
                                      children: [
                                        getProductDetailModel.data.product.refundPolicy == null ||
                                            getProductDetailModel.data.product.refundPolicy.toString() == ''
                                            ?SizedBox()
                                            :Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                Text(
                                          '${getProductDetailModel.data.product.refundPolicy}',
                                          textAlign: TextAlign.left,
                                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: customLightBlackColor),
                                        ),
                                              ],
                                            ),
                                      ]
                                  );
                                }
                                else if (_selectedTabBar == 2) {
                                  return getProductDetailModel.data.product.reviews.length == 0
                                      ?SizedBox()
                                      :Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              'Ratings & Reviews',
                                              style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                                            ),
                                          ),
                                          Text(
                                            'View All',
                                            style: Theme.of(context).textTheme.headline5.copyWith(
                                                fontSize: 12,
                                                decoration: TextDecoration.underline
                                            ),
                                          )
                                        ],
                                      ),
                                      Text(
                                        'Total Ratings: ${getProductDetailModel.data.product.totalReviews}',
                                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                                            fontSize: 10,
                                            color: customLightGreyColor
                                        ),
                                      ),
                                      SizedBox(height: 15,),
                                      Wrap(
                                          children: List.generate(getProductDetailModel.data.product.reviews.length, (index){
                                            return Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                index == 0
                                                    ?SizedBox()
                                                    :SizedBox(height: 8,),
                                                Row(
                                                  children: [
                                                    SvgPicture.asset('assets/Icons/other_icons/profile.svg'),
                                                    SizedBox(width: 10,),
                                                    Text(
                                                      '${getProductDetailModel.data.product.reviews[index].customer.name}',
                                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                          fontSize: 10,
                                                          color: customLightGreyColor
                                                      ),
                                                    ),
                                                    // SizedBox(width: 10,),
                                                    // Text(
                                                    //   '(21 May 2021)',
                                                    //   style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                    //       fontSize: 10,
                                                    //       color: customLightGreyColor
                                                    //   ),
                                                    // ),
                                                    Expanded(
                                                      child: Align(
                                                        alignment: Alignment.centerRight,
                                                        child:  RatingBar(
                                                          initialRating: getProductDetailModel.data.product.reviews[index].rating,
                                                          direction: Axis.horizontal,
                                                          allowHalfRating: true,
                                                          ignoreGestures: true,
                                                          itemCount: 5,
                                                          itemSize: 15,
                                                          ratingWidget: RatingWidget(
                                                            full: Icon(Icons.star,color: customRatingStartColor,),
                                                            half: Icon(Icons.star_half,color: customRatingStartColor),
                                                            empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                                                          ),
                                                          itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                          onRatingUpdate: (rating) {
                                                            print(rating);
                                                          },
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(top: 10),
                                                  child: Text(
                                                    '${getProductDetailModel.data.product.reviews[index].description}',
                                                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                        fontSize: 12,
                                                        color: customLightBlackColor),
                                                  ),
                                                ),
                                                // Padding(
                                                //   padding: const EdgeInsets.only(top: 10),
                                                //   child: SizedBox(
                                                //     height: 50,
                                                //     width: double.infinity,
                                                //     child: ListView(
                                                //       scrollDirection: Axis.horizontal,
                                                //       children: List.generate(3, (index){
                                                //         return Padding(
                                                //           padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                                //           child: Container(
                                                //             height: 50,
                                                //             width: 60,
                                                //             decoration: BoxDecoration(
                                                //               borderRadius: BorderRadius.circular(5),
                                                //             ),
                                                //             child: ClipRRect(
                                                //               borderRadius: BorderRadius.circular(5),
                                                //               child: Image.asset('assets/images/productImage.png',
                                                //                 fit: BoxFit.fill,),
                                                //             ),
                                                //           ),
                                                //         );
                                                //       }),
                                                //     ),
                                                //   ),
                                                // ),
                                                Padding(
                                                  padding: const EdgeInsets.fromLTRB(30, 15, 20, 0),
                                                  child: Divider(color: customDividerGreyColor,),
                                                )
                                              ],
                                            );
                                          })
                                      ),
                                    ],
                                  );
                                }
                                else return null;
                              }),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  /// vendor-details
                  defaultSettingsModel.data.generalSettings.isMultiVendor == 1
                      ?getProductDetailModel.data.product.vendor == null
                      ?SizedBox()
                      :Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
                          child: Center(
                              child: Container(
                                height: 350,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: customVendorCardColor,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 1,
                                      blurRadius: 1,
                                      offset: Offset(0,1),
                                    ),
                                  ],
                                ),
                                child: Stack(
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        getProductDetailModel.data.product.vendor.store.coverImage == null
                                            ? Image.asset(
                                          'assets/images/product_detail_vendor_cover.png',
                                          fit: BoxFit.cover,
                                          width: double.infinity,
                                          height: 80,
                                        )
                                            : Image.network(
                                          '${baseUrl+getProductDetailModel.data.product.vendor.store.coverImage.originalMediaPath}',
                                          fit: BoxFit.cover,
                                          width: double.infinity,
                                          height: 80,
                                        ),
                                        SizedBox(height: 28,),
                                        ///------vendor-name
                                        Center(
                                          child: Text(
                                            '${getProductDetailModel.data.product.vendor.store.name}',
                                            style: Theme.of(context).textTheme.headline1.copyWith(
                                                color: customLightBlackColor,
                                                fontSize: 14
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 2,),
                                        ///------vendor-follow
                                        Center(
                                          child: Text(
                                            'Follow',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                decoration: TextDecoration.underline,
                                                color: customThemeColor
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(18, 15, 18, 0),
                                          child: SizedBox(
                                            height: 54,
                                            width: double.infinity,
                                            child: Row(
                                              children: [
                                                Expanded(
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        Text(
                                                          '91%',
                                                          style: Theme.of(context).textTheme.headline1.copyWith(
                                                              fontSize: 16,
                                                              color: customThemeColor
                                                          ),
                                                        ),
                                                        Text(
                                                          'Positive Seller\nRatings',
                                                          textAlign: TextAlign.center,
                                                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                              fontSize: 10,
                                                              color: customLightGreyColor
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                                Expanded(
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        Text(
                                                          '84%',
                                                          style: Theme.of(context).textTheme.headline1.copyWith(
                                                              fontSize: 16,
                                                              color: customThemeColor
                                                          ),
                                                        ),
                                                        Text(
                                                          'Ship On Time',
                                                          textAlign: TextAlign.center,
                                                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                              fontSize: 10,
                                                              color: customLightGreyColor
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                                Expanded(
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        Text(
                                                          '55%',
                                                          style: Theme.of(context).textTheme.headline1.copyWith(
                                                              fontSize: 16,
                                                              color: customThemeColor
                                                          ),
                                                        ),
                                                        Text(
                                                          'Chat Response\nRate',
                                                          textAlign: TextAlign.center,
                                                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                              fontSize: 10,
                                                              color: customLightGreyColor
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 20,),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 12),
                                          child: Text(
                                            'Other Products By Seller',
                                            style: Theme.of(context).textTheme.headline1.copyWith(
                                                fontSize: 12,
                                                color: customLightBlackColor
                                            ),
                                          ),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.fromLTRB(12, 8, 12, 12),
                                            child: SizedBox(
                                              height: 70,
                                              width: double.infinity,
                                              child: ListView(
                                                scrollDirection: Axis.horizontal,
                                                children: List.generate(getProductDetailModel.data.product
                                                    .vendor.products.length, (productIndex){
                                                  return Padding(
                                                    padding: const EdgeInsets.fromLTRB(0, 0, 12, 0),
                                                    child: Container(
                                                      height: 70,
                                                      width: 70,
                                                      child: getProductDetailModel.data.product
                                                          .vendor.products[productIndex].media.length == 0
                                                          ?Image.asset(
                                                        'assets/images/vendorProduct1.png',
                                                        fit: BoxFit.cover,
                                                      )
                                                          :Image.network(
                                                        '${baseUrl+getProductDetailModel.data.product
                                                            .vendor.products[productIndex].media[0].originalMediaPath}',
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  );
                                                }),
                                              ),
                                            )
                                        )
                                      ],
                                    ),
                                    Positioned(
                                      top: 55,
                                      left: 0,
                                      right: 0,
                                      child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: getProductDetailModel.data.product.vendor.profileImage == null
                                              ? CircleAvatar(
                                              backgroundColor: Colors.brown,
                                              radius: 25,
                                              child: Image.asset('assets/images/product_detail_circle_image.png')
                                          )
                                              :Container(
                                            height: 50,
                                            width: 50,
                                            child: ClipRRect(
                                              borderRadius: BorderRadius.circular(30),
                                              child: Image.network('${baseUrl+getProductDetailModel.data.product.vendor
                                                  .store.storeLogo.originalMediaPath}',
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(
                        height: 10,
                        width: double.infinity,
                        color: customIconListTileColor,
                      ),
                    ],
                  )
                  :SizedBox(),
                  /// product-questions
                  getProductDetailModel.data.product.faq.length == 0
                      ?SizedBox()
                      :Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    'Questions About Product',
                                    style: Theme.of(context).textTheme.headline5.copyWith(color: customDarkBlackColor),
                                  ),
                                ),
                                Text(
                                  'View All',
                                  style: Theme.of(context).textTheme.headline5.copyWith(
                                      fontSize: 12,
                                      decoration: TextDecoration.underline
                                  ),
                                )
                              ],
                            ),
                            Text(
                              'Total Questions: ${getProductDetailModel.data.product.faq.length}',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  fontSize: 10,
                                  color: customLightGreyColor
                              ),
                            ),
                            SizedBox(height: 15,),
                            Wrap(
                                children: List.generate(getProductDetailModel.data.product.faq.length, (index){
                                  return Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      index == 0
                                          ?SizedBox()
                                          :SizedBox(height: 8,),
                                      Row(
                                        children: [
                                          SvgPicture.asset('assets/Icons/Product_page_icons/fi-rr-shield-interrogation.svg'),
                                          SizedBox(width: 10,),
                                          Text(
                                            '${getProductDetailModel.data.product.faq[index].question}',
                                            style: Theme.of(context).textTheme.headline4.copyWith(
                                                fontSize: 12,
                                                color: customLightBlackColor
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Row(
                                          children: [
                                            SvgPicture.asset('assets/Icons/Product_page_icons/fi-rr-shield-check.svg'),
                                            SizedBox(width: 10,),
                                            Expanded(
                                              child: Text(
                                                '${getProductDetailModel.data.product.faq[index].answer}',
                                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                    fontSize: 12,
                                                    color: customLightBlackColor
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      index == 1
                                          ?SizedBox(height: 15,)
                                          :Padding(
                                        padding: const EdgeInsets.fromLTRB(30, 15, 20, 0),
                                        child: Divider(color: customDividerGreyColor,),
                                      ),
                                    ],
                                  );
                                })
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 10,
                        width: double.infinity,
                        color: customIconListTileColor,
                      ),
                    ],
                  ),
                  /// recommended-products
                  getProductDetailModel.data.relatedProducts.length == 0
                      ?SizedBox()
                      :Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 10, 15, 7),
                        child: Text(
                          'Recommended Products',
                          style: Theme.of(context).textTheme.headline1,
                        ),
                      ),
                      Center(
                        child: Wrap(
                          children: List.generate(getProductDetailModel.data.relatedProducts.length, (index){
                            return Container(
                              width: MediaQuery.of(context).size.width*.45,
                              child: Padding(
                                padding: index%2 == 0
                                    ?const EdgeInsets.fromLTRB(0, 5, 5, 5)
                                    :const EdgeInsets.fromLTRB(5, 5, 0, 5),
                                child: Stack(
                                    children: [
                                      InkWell(
                                        onTap: (){
                                          ///------get-product-detail-data-api-call
                                          getMethod(
                                              context,
                                              '$getProductsDetailApi/${getProductDetailModel.data.relatedProducts[index].slug}',
                                              null,
                                              false,
                                              getProductDetailData
                                          );
                                          Get.find<HomeController>().changeGetProductDetailDataCheck(false);
                                          Get.offNamed(PageRoutes.productDetail);
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(10),
                                              bottom: Radius.circular(10),
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey.withOpacity(0.2),
                                                spreadRadius: 2,
                                                blurRadius: 6,
                                                offset: Offset(0,3),
                                              ),
                                            ],
                                          ),
                                          child: Column(
                                            children: [
                                              ///--------------product-image
                                              Container(
                                                height: 145,
                                                width: double.infinity,
                                                child: ClipRRect(
                                                  borderRadius: BorderRadius.vertical(
                                                    top: Radius.circular(10),
                                                    bottom: Radius.circular(10),
                                                  ),
                                                  child:
                                                  getProductDetailModel.data.relatedProducts[index].media.length == 0
                                                      ?Image.asset(
                                                    'assets/images/imagePlaceHolder.jpg',
                                                    fit: BoxFit.fill,
                                                  )
                                                      :Image.network(''
                                                      '${baseUrl+getProductDetailModel.data
                                                      .relatedProducts[index].media[0].originalMediaPath}',
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                              ///--------------product-detail
                                              Container(
                                                width: double.infinity,
                                                child: Padding(
                                                  padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      ///--------------product-name
                                                      Text(
                                                        '${getProductDetailModel.data.relatedProducts[index].name}',
                                                        softWrap: true,
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 2,
                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                            fontSize: 14,
                                                            color: customDarkGreyColor
                                                        ),
                                                      ),
                                                      SizedBox(height: 4.0,),
                                                      ///--------------product-rating-row
                                                      Row(
                                                        children: [
                                                          RatingBar(
                                                            initialRating: getProductDetailModel.data.relatedProducts[index].reviewsAverageRating,
                                                            direction: Axis.horizontal,
                                                            allowHalfRating: true,
                                                            ignoreGestures: true,
                                                            itemCount: 5,
                                                            itemSize: 15,
                                                            ratingWidget: RatingWidget(
                                                              full: Icon(Icons.star,color: customRatingStartColor,),
                                                              half: Icon(Icons.star_half,color: customRatingStartColor),
                                                              empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                                                            ),
                                                            itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                            onRatingUpdate: (rating) {
                                                              print(rating);
                                                            },
                                                          ),
                                                          Padding(
                                                            padding: const EdgeInsets.only(left: 3),
                                                            child: Text(
                                                              '(${getCategoryWiseProductsModel.data.allCategories[index].totalReviews})',
                                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                  fontSize: 10
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 10.0,),
                                                      getProductDetailModel.data.relatedProducts[index].flashSale != null
                                                          ?Text(
                                                        '${getProductDetailModel.data.relatedProducts[index].displayPrice}',
                                                        style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                          decoration: TextDecoration.lineThrough,
                                                        ),
                                                      )
                                                          :SizedBox(),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: [
                                                          ///--------------product-price
                                                          Expanded(
                                                            child: getProductDetailModel.data.relatedProducts[index].flashSale != null
                                                                ?Text(
                                                              '${getProductDetailModel.data.relatedProducts[index].flashSale.displayPrice}',
                                                              style: Theme.of(context).textTheme.headline1.copyWith(
                                                                  fontSize: 14,
                                                                  color: customThemeColor
                                                              ),
                                                            )
                                                                :Text(
                                                              '${getProductDetailModel.data.relatedProducts[index].displayPrice}',
                                                              style: Theme.of(context).textTheme.headline1.copyWith(
                                                                  fontSize: 14,
                                                                  color: customThemeColor
                                                              ),
                                                            ),
                                                          ),
                                                          InkWell(
                                                            onTap: (){},
                                                            child: Padding(
                                                              padding: const EdgeInsets.fromLTRB(5, 5, 10, 5),
                                                              child: Icon(
                                                                Icons.add_box_rounded,
                                                                size: 20,
                                                                color: customThemeColor,
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 8.0,),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        right: 0.0,
                                        top: 0,
                                        child: InkWell(
                                          onTap: (){
                                            print('------------->>press');
                                            if(currentUserModel != null){
                                              ///post-method
                                              postMethod(
                                                  context,
                                                  addToWishListApi,
                                                  {
                                                    'product_id': getProductDetailModel.data
                                                        .relatedProducts[index].id
                                                  },
                                                  true,
                                                  addToWishListDataRepo
                                              );
                                            }else{
                                              notLoggedAlert(
                                                  context,
                                                  'Error',
                                                  'You Have to Login First'
                                              );
                                              print('------------->>NotLoggedIn');
                                            }
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(30, 12, 12, 30),
                                            child: Container(
                                              height: 14,
                                              width: 16,
                                              child: Center(
                                                  child: SvgPicture.asset(
                                                    'assets/Icons/Product_page_icons/Group 366.svg',
                                                    color: customThemeColor,
                                                  )
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                          top: 10.0,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: List.generate(
                                                getProductDetailModel.data
                                                    .relatedProducts[index].tags.length, (tagIndex){
                                              return Column(
                                                children: [
                                                  Container(
                                                    height: 20,
                                                    decoration: BoxDecoration(
                                                        color: customThemeColor,
                                                        borderRadius: BorderRadius.only(
                                                          topRight: Radius.circular(6.0),
                                                          bottomRight: Radius.circular(6.0),
                                                        )
                                                    ),
                                                    child: Center(
                                                        child: Padding(
                                                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                                          child: Text('${getProductDetailModel.data
                                                              .relatedProducts[index].tags[tagIndex]}',
                                                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                                fontSize: 12,
                                                                color: Colors.white
                                                            ),
                                                          ),
                                                        )),
                                                  ),
                                                  SizedBox(height: 6,),
                                                ],
                                              );
                                            }),
                                          )
                                      ),
                                    ]
                                ),
                              ),
                            );
                          }),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: !homeController.getProductDetailDataCheck
            ?SizedBox()
            :(getProductDetailModel.data.product.stock == 0 &&
            getProductDetailModel.data.product.productType != 2)
            ?CustomGeneralBottomBar(
          text: 'Out Of Stock',
          // icon: 'assets/Icons/Product_page_icons/shopping-cart.svg',
        )
            :getProductDetailModel.data.product.productType != 2
            ?InkWell(
            onTap: (){

              bottomSheetForCart(context);
            },
          child: CustomGeneralBottomBar(
            text: 'Add To Cart',
            icon: 'assets/Icons/Product_page_icons/shopping-cart.svg',
          )
      )
            :InkWell(
            onTap: (){

              ///---making-variant
              selectedVariant = '';
              getProductDetailModel.data.product.attributes.forEach((element) {
                element.values.forEach((element2) {
                  if(element2.isSelected){
                    selectedVariant = selectedVariant+'${element2.slug}-';
                  }
                });
              });
              selectedVariant =
                  selectedVariant.substring(0,(selectedVariant.length-1));
              print('---->>SelectedVariant--->>$selectedVariant');

              ///---compare-vairant
              comparedVariantIndex = null;
              getProductDetailModel.data.product.variants.forEach((element) {
                if(element.variant == selectedVariant){
                  comparedVariantIndex = getProductDetailModel.data.product.variants.indexOf(element);
                  print('CompareVariant--->>${element.variant}');
                  print('CompareVariantIndex--->>$comparedVariantIndex');
                }
              });

              bottomSheetForCart(context);
            },
            child: CustomGeneralBottomBar(
              text: 'Add To Cart',
              icon: 'assets/Icons/Product_page_icons/shopping-cart.svg',
            )
        ),
      ),
    );
  }

  Future<Widget> bottomSheetForCart(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context){
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState)=> GetBuilder<HomeController>(
            init: HomeController(),
            builder:(homeController)=> Container(
                height: MediaQuery.of(context).size.height * 0.85,
                decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 25, 15, 10),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.7,
                        width: double.infinity,
                        child: ListView(
                          children: [
                            getProductDetailModel.data.product.productType == 2
                                ?Column(
                              children: [
                                Wrap(
                                  children: List.generate(
                                      getProductDetailModel.data.product.attributes.length, (attributeIndex){
                                    return Column(
                                      children: [
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                            'Select ${getProductDetailModel.data.product.attributes[attributeIndex].name}',
                                            style: Theme.of(context).textTheme.headline1,
                                          ),
                                        ),
                                        SizedBox(height: 12,),
                                        Row(
                                          children: [
                                            Wrap(
                                              children: List.generate(
                                                getProductDetailModel.data.product.attributes[attributeIndex].values.length,
                                                    (valueIndex){
                                                  return Padding(
                                                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                                    child: InkWell(
                                                      onTap: (){

                                                        ///---set-all-false
                                                        getProductDetailModel.data.product.attributes[attributeIndex]
                                                            .values.forEach((element) {
                                                          setState(() {
                                                            element.isSelected = false;
                                                          });
                                                        });

                                                        ///---set-selected-true
                                                        setState(() {
                                                          getProductDetailModel.data.product.attributes[attributeIndex]
                                                              .values[valueIndex].isSelected = true;
                                                        });

                                                        ///---making-variant
                                                        selectedVariant = '';
                                                        getProductDetailModel.data.product.attributes.forEach((element) {
                                                          element.values.forEach((element2) {
                                                            if(element2.isSelected){
                                                              selectedVariant = selectedVariant+'${element2.slug}-';
                                                            }
                                                          });
                                                        });
                                                        selectedVariant =
                                                            selectedVariant.substring(0,(selectedVariant.length-1));
                                                        print('---->>SelectedVariant--->>$selectedVariant');

                                                        ///---compare-vairant
                                                        comparedVariantIndex = null;
                                                        getProductDetailModel.data.product.variants.forEach((element) {
                                                          if(element.variant == selectedVariant){
                                                            comparedVariantIndex = getProductDetailModel.data.product.variants.indexOf(element);
                                                            print('CompareVariant--->>${element.variant}');
                                                            print('CompareVariantIndex--->>$comparedVariantIndex');
                                                          }
                                                        });

                                                      },
                                                      child: Container(
                                                        height: 33,
                                                        decoration: BoxDecoration(
                                                            color: getProductDetailModel.data.product.attributes[attributeIndex]
                                                                .values[valueIndex].isSelected
                                                                ?customThemeColor
                                                                :customIconListTileColor
                                                        ),
                                                        child: Center(
                                                          child: Padding(
                                                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                            child: Text(
                                                              '${getProductDetailModel.data.product.attributes[attributeIndex]
                                                                  .values[valueIndex].name}',
                                                              style: Theme.of(context).textTheme.headline4.copyWith(
                                                                  fontSize: 12,
                                                                  color: getProductDetailModel.data.product.attributes[attributeIndex]
                                                                      .values[valueIndex].isSelected
                                                                      ?Colors.white
                                                                      :customLightBlackColor
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                    }),
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: 10,),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(20, 0, 30, 0),
                                          child: Container(
                                            height: 1,
                                            width: double.infinity,
                                            color: customSmoothGreyColor,
                                          ),
                                        ),
                                        SizedBox(height: 10,),
                                      ],
                                    );
                                  }),
                                ),
                                // SizedBox(height: 20,),

                              ],
                            )
                                :SizedBox(),
                            Text(
                              'Select Quantity',
                              style: Theme.of(context).textTheme.headline1,
                            ),
                            SizedBox(height: 12,),
                            Align(
                              alignment: Alignment.centerLeft,

                              child: Container(
                                height: 32,
                                width: 143,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: customIconListTileColor
                                ),
                                child: Row(
                                  children: [
                                    InkWell(
                                      onTap: (){
                                        if(homeController.quantity > 1 ){
                                          setState(() {
                                            homeController.quantity = homeController.quantity - 1;
                                          });
                                          homeController.updateQuantity(homeController.quantity);
                                        }
                                      },
                                      child: Container(
                                        height: double.infinity,
                                        width: 32,
                                        decoration: BoxDecoration(
                                            color: customThemeColor,
                                            borderRadius: BorderRadius.circular(5)
                                        ),
                                        child: Icon(
                                          Icons.remove,
                                          color: Colors.white,
                                          size: 25,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                        child: Center(
                                          child: Text(
                                            '${homeController.quantity}',
                                            style: Theme.of(context).textTheme.headline1.copyWith(
                                                fontSize: 14,
                                                color: customThemeColor
                                            ),
                                          ),
                                        )
                                    ),
                                    InkWell(
                                      onTap: (){
                                        if(homeController.quantity > 0){
                                          setState(() {
                                            homeController.quantity = homeController.quantity + 1;
                                          });
                                          homeController.updateQuantity(homeController.quantity);
                                        }
                                      },
                                      child: Container(
                                        height: double.infinity,
                                        width: 32,
                                        decoration: BoxDecoration(
                                            color: customThemeColor,
                                            borderRadius: BorderRadius.circular(5)
                                        ),
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.white,
                                          size: 25,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(20, 10, 30, 0),
                              child: Container(
                                height: 1,
                                width: double.infinity,
                                color: customSmoothGreyColor,
                              ),
                            ),
                            SizedBox(height: 20,),
                            comparedVariantIndex == null
                                ?Text(
                              'This variant is not available',
                              style: Theme.of(context).textTheme.headline1.copyWith(
                                color: customRedColor
                              ),
                            )
                                :Row(
                              children: [
                                Text(
                                  'Total Price:',
                                  style: Theme.of(context).textTheme.headline1,
                                ),
                                SizedBox(width: 20,),
                                Text(
                                  '${getProductDetailModel.data.product.variants[comparedVariantIndex].displayPrice}',
                                  style: Theme.of(context).textTheme.headline1.copyWith(
                                    color: customThemeColor
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child:
                        InkWell(
                            onTap: (){
                              Navigator.pop(context);
                              Get.toNamed(PageRoutes.cart);
                            },
                            child: CustomGeneralBottomBar(
                              text: 'Save & Continue',
                              icon: 'assets/Icons/Product_page_icons/shopping-cart.svg',
                            )
                        ),
                      ),
                    )
                  ],
                )),
          ),
        );
      }
    );
  }

}
