import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/components/customGeneralBottomBar.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class CheckoutPage extends StatefulWidget {
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyCustomAppBar(drawerShow: false,),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: Text(
                  'Checkout',
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: customLightBlackColor
                  ),
                ),
              ),
              ///---page-viewer
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: Container(
                  height: 70,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Theme.of(context).scaffoldBackgroundColor,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 2,
                          blurRadius: 4,
                          offset: Offset(0,2)
                      )]
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ///-cart-icon
                      Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                        'assets/Icons/Product_page_icons/shopping-cart.svg',
                                      color: customGreenSaleFlagColor,
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 28,
                                child: SvgPicture.asset(
                                  'assets/Icons/cart_checkout_icons/Group 583.svg'
                                ),
                              )
                            ],
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-checkout-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-document.svg',
                            color: customThemeColor,
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-payment-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-credit-card.svg'
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: VerticalDivider(
                            color: customSmoothGreyColor,
                            thickness: 1,
                          ),
                        ),
                      ),
                      ///-confirmation-icon
                      Expanded(
                          child: SvgPicture.asset(
                              'assets/Icons/cart_checkout_icons/fi-rr-file-check.svg'
                          )
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20,),
              ///---page-view
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 6,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Text(
                            'Location:',
                            style: Theme.of(context).textTheme.headline5.copyWith(
                                fontSize: 16,
                                color: customDarkBlackColor
                            ),
                          ),
                        ),
                        Expanded(
                          child: Align(
                              alignment: Alignment.centerRight,
                              child: SvgPicture.asset(
                                  'assets/Icons/Product_page_icons/fi-rr-marker.svg')
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            '658 Ashley Street Middletown, CT 06457',
                            textAlign: TextAlign.end,
                            style: Theme.of(context).textTheme.headline4.copyWith(
                                fontSize: 12,
                                color: customThemeColor
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 6,
                    width: double.infinity,
                    color: customIconListTileColor,
                  ),
                  ///---name-field
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
                    child: Text(
                      'Full Name',
                      style: Theme.of(context).textTheme.headline5.copyWith(
                          fontSize: 16,
                          color: customDarkBlackColor
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Theme.of(context).scaffoldBackgroundColor,
                      child: TextFormField(
                        style: Theme.of(context).textTheme.headline4.copyWith(
                            fontSize: 12,
                            color: customLightBlackColor
                        ),
                        controller: checkoutNameController,
                        decoration: InputDecoration(
                            hintText: 'Enter Full Name',
                            hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                                fontSize: 12,
                                color: customLightGreyColor
                            ),
                            isDense: true,
                            contentPadding:const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 0.0),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: customThemeColor),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: customSmoothGreyColor),),),
                      ),
                    ),
                  ),
                  ///---email-field
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
                    child: Text(
                      'Email',
                      style: Theme.of(context).textTheme.headline5.copyWith(
                          fontSize: 16,
                          color: customDarkBlackColor
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Theme.of(context).scaffoldBackgroundColor,
                      child: TextFormField(
                        style: Theme.of(context).textTheme.headline4.copyWith(
                            fontSize: 12,
                            color: customLightBlackColor
                        ),
                        controller: checkoutEmailController,
                        decoration: InputDecoration(
                          hintText: 'Enter Email',
                          hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                              color: customLightGreyColor
                          ),
                          isDense: true,
                          contentPadding:const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 0.0),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: customThemeColor),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: customSmoothGreyColor),),),
                      ),
                    ),
                  ),
                  ///---phone-field
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
                    child: Text(
                      'Phone No.',
                      style: Theme.of(context).textTheme.headline5.copyWith(
                          fontSize: 16,
                          color: customDarkBlackColor
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
                    child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Theme.of(context).scaffoldBackgroundColor,
                      child: TextFormField(
                        style: Theme.of(context).textTheme.headline4.copyWith(
                            fontSize: 12,
                            color: customLightBlackColor
                        ),
                        controller: checkoutPhoneController,
                        decoration: InputDecoration(
                          hintText: 'Enter Phone No.',
                          hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                              color: customLightGreyColor
                          ),
                          isDense: true,
                          contentPadding:const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 0.0),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: customThemeColor),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: customSmoothGreyColor),),),
                      ),
                    ),
                  ),
                ],
              ),
              ///---voucher-text-field
              SizedBox(
                height: 85,
                width: double.infinity,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        child: TextFormField(
                          style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                              color: customLightBlackColor
                          ),
                          controller: voucherController,
                          decoration: InputDecoration(
                              hintText: 'Enter Voucher Code (If Any)',
                              hintStyle: Theme.of(context).textTheme.headline4.copyWith(
                                  fontSize: 12,
                                  color: customLightGreyColor
                              ),
                              isDense: true,
                              contentPadding:const EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10.0),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                borderSide: BorderSide(color: customThemeColor),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                borderSide: BorderSide(color: customSmoothGreyColor),),
                              suffixIcon: InkWell(
                                onTap: (){},
                                child: Container(
                                  height: 45,
                                  width: 93,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: customRatingStartColor
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Apply',
                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                          fontSize: 15,
                                          color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ///---bill-detail
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                        top: Radius.circular(15)
                    ),
                    color: customIconListTileColor
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Billing',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 18),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$250.00',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Discount',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$25',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Delivery Charges',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    color: customLightBlackColor
                                ),
                              ),
                            ),
                            Text(
                              '\$25',
                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                  color: customLightBlackColor
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 14,bottom: 50),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Total',
                                style: Theme.of(context).textTheme.headline1.copyWith(
                                    color: customThemeColor,
                                    fontSize: 14
                                ),
                              ),
                            ),
                            Text(
                              '\$300',
                              style: Theme.of(context).textTheme.headline1.copyWith(
                                  color: customThemeColor,
                                  fontSize: 14
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: InkWell(
          onTap: (){
            Get.toNamed(PageRoutes.payment);
          },
          child: CustomGeneralBottomBar(
            text: 'Continue To Payment',
          )
      ),
    );
  }
}

