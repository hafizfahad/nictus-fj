import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class OrderHistoryPage extends StatefulWidget {
  @override
  _OrderHistoryPageState createState() => _OrderHistoryPageState();
}

class _OrderHistoryPageState extends State<OrderHistoryPage>
    with SingleTickerProviderStateMixin{

  DateTime selectedDate = DateTime.now();
  TabController orderHistoryPageTabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    orderHistoryPageTabController =
    new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Persistent AppBar that never scrolls
      appBar: MyCustomAppBar(drawerShow: false,),
      body: NestedScrollView(
        // allows you to build a list of elements that would be scrolled away till the body reached the top
        headerSliverBuilder: (context, _) {
          return [
            SliverList(
              delegate: SliverChildListDelegate([
                ///---title
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'ORDERS',
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: customDarkBlackColor
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          _selectDate(context);
                        },
                        child: Container(
                          height: 30,
                          width: 105,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: customSmoothGreyColor)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              SvgPicture.asset(
                                'assets/Icons/other_icons/fi-rr-calendar.svg',
                                color: customThemeColor,
                              ),
                              Text(
                                '${selectedDate.toString().substring(0,11)}',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ]
                // _randomHeightWidgets(context),
              ),
            ),
          ];
        },
        // You tab view goes here
        body: Padding(
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom
          ),
          child: Column(
            children: <Widget>[
              ///---tabs
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Container(
                  color:Colors.white,
                  height: 40,
                  width: double.infinity,
                  child: TabBar(
                      onTap: (index){
                        setState(() {});
                      },
                      controller: orderHistoryPageTabController,
                      unselectedLabelColor: Colors.grey,
                      indicatorColor: Theme.of(context).buttonColor,
                      indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(
                            width: 2.0,
                            color: Theme.of(context).buttonColor,
                          ),
                          insets: EdgeInsets.symmetric(horizontal:0.0)
                      ),
                      labelStyle: Theme.of(context).textTheme.headline5.copyWith(fontSize: 12),
                      labelColor: Theme.of(context).buttonColor,
                      labelPadding: EdgeInsets.all(1),
                      tabs: [
                        Text(
                          'Pending',
                        ),
                        Text(
                          'Completed',
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          'Cancelled',
                        ),
                      ]
                  ),
                ),
              ),
              ///---tab-view
              Expanded(
                child: TabBarView(
                  controller: orderHistoryPageTabController,
                  children: [
                    ///---first-view
                    ListView(
                      children: [
                        ///---search-bar
                        Padding(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          child: Container(
                            color: Color(0xffF9F9F9),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Theme(
                                data: ThemeData(
                                    primaryColor: Color(0xff363636)
                                ),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                                      border: InputBorder.none,
                                      hintText: 'Search Order',
                                      hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Wrap(
                          children: List.generate(2, (index){
                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: customIconListTileColor
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 10),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        ///---order-id-date
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  'Order ID: ',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                    fontSize: 12,
                                                    color: customThemeColor
                                                  ),
                                                ),
                                                Text(
                                                  'NIC-446622-25',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customDarkGreyColor
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              '25/06/2021',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: customDarkGreyColor
                                              ),
                                            )
                                          ],
                                        ),
                                        ///---order-vendor-status
                                        Padding(
                                          padding: const EdgeInsets.only(top: 20),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    'By ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customLightBlackColor
                                                    ),
                                                  ),
                                                  Text(
                                                    'Luxury house décor',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customRatingStartColor,
                                                      decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                height: 21,
                                                decoration: BoxDecoration(
                                                  color: customRatingStartColor,
                                                  borderRadius: BorderRadius.circular(5)
                                                ),
                                                child: Center(
                                                  child: Padding(
                                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Text(
                                                      'Pending',
                                                      style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 10,
                                                        color: Colors.white
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        ///---order-items
                                        SizedBox(height: 18,),
                                        Wrap(
                                          children: List.generate(2, (index){
                                            return Column(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(top: 0),
                                                  child: Container(
                                                    height: 86,
                                                    decoration: BoxDecoration(
                                                        color: Theme.of(context).scaffoldBackgroundColor,
                                                        borderRadius: BorderRadius.circular(10)
                                                    ),
                                                    child: Row(
                                                      children: [
                                                        ///---item-image
                                                        Container(
                                                          height: double.infinity,
                                                          width: 80,
                                                          decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(10),
                                                            image: DecorationImage(
                                                              image: AssetImage(
                                                                'assets/images/featuredImage.png'
                                                              ),
                                                              fit: BoxFit.fill
                                                            )
                                                          ),
                                                        ),
                                                        ///---item-detail
                                                        Expanded(
                                                          child: Padding(
                                                            padding: const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                                            child: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Expanded(
                                                                  child: Text(
                                                                    'Modern ladies casual bag',
                                                                    style: Theme.of(context).textTheme.headline1.copyWith(
                                                                      fontSize: 14,
                                                                      color: customDarkGreyColor
                                                                    ),
                                                                  ),
                                                                ),
                                                                Text(
                                                                  '\$150.00',
                                                                  style: Theme.of(context).textTheme.headline1.copyWith(
                                                                    fontSize: 14,
                                                                    color: customThemeColor
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          )
                                                        ),
                                                        ///---item-count
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(20, 13, 16, 0),
                                                          child: Align(
                                                            alignment: Alignment.topCenter,
                                                            child: Text(
                                                              'x2',
                                                              style: Theme.of(context).textTheme.headline4.copyWith(
                                                                color: customDarkGreyColor
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                index == 1
                                                    ?SizedBox()
                                                    :Padding(
                                                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                                  child: Divider(
                                                    color: customSmoothGreyColor
                                                  ),
                                                )
                                              ],
                                            );
                                          })
                                        ),
                                        ///---order-subtotal
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              InkWell(
                                                onTap: (){
                                                  Get.toNamed(PageRoutes.orderHistoryDetail);
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(0, 10, 15, 8),
                                                  child: Text(
                                                    'View Order Details ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customThemeColor,
                                                      decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 8),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      'Sub Total:',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customLightBlackColor
                                                      ),
                                                    ),
                                                    SizedBox(width: 20,),
                                                    Text(
                                                      '\$300',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customThemeColor
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20,)
                              ],
                            );
                          }),
                        )
                      ],
                    ),
                    ///---second-view
                    ListView(
                      children: [
                        ///---search-bar
                        Padding(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          child: Container(
                            color: Color(0xffF9F9F9),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Theme(
                                data: ThemeData(
                                    primaryColor: Color(0xff363636)
                                ),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                                      border: InputBorder.none,
                                      hintText: 'Search Order',
                                      hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Wrap(
                          children: List.generate(2, (index){
                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: customIconListTileColor
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 10),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        ///---order-id-date
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  'Order ID: ',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customThemeColor
                                                  ),
                                                ),
                                                Text(
                                                  'NIC-446622-25',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customDarkGreyColor
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              '25/06/2021',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: customDarkGreyColor
                                              ),
                                            )
                                          ],
                                        ),
                                        ///---order-vendor-status
                                        Padding(
                                          padding: const EdgeInsets.only(top: 20),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    'By ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customLightBlackColor
                                                    ),
                                                  ),
                                                  Text(
                                                    'Luxury house décor',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customRatingStartColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                height: 21,
                                                decoration: BoxDecoration(
                                                    color: customRatingStartColor,
                                                    borderRadius: BorderRadius.circular(5)
                                                ),
                                                child: Center(
                                                  child: Padding(
                                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Text(
                                                      'Completed',
                                                      style: Theme.of(context).textTheme.headline4.copyWith(
                                                          fontSize: 10,
                                                          color: Colors.white
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        ///---order-items
                                        SizedBox(height: 18,),
                                        Wrap(
                                            children: List.generate(2, (index){
                                              return Column(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(top: 0),
                                                    child: Container(
                                                      height: 86,
                                                      decoration: BoxDecoration(
                                                          color: Theme.of(context).scaffoldBackgroundColor,
                                                          borderRadius: BorderRadius.circular(10)
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          ///---item-image
                                                          Container(
                                                            height: double.infinity,
                                                            width: 80,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(10),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        'assets/images/featuredImage.png'
                                                                    ),
                                                                    fit: BoxFit.fill
                                                                )
                                                            ),
                                                          ),
                                                          ///---item-detail
                                                          Expanded(
                                                              child: Padding(
                                                                padding: const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Expanded(
                                                                      child: Text(
                                                                        'Modern ladies casual bag',
                                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                                            fontSize: 14,
                                                                            color: customDarkGreyColor
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      '\$150.00',
                                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                                          fontSize: 14,
                                                                          color: customThemeColor
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                          ),
                                                          ///---item-count
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(20, 13, 16, 0),
                                                            child: Align(
                                                              alignment: Alignment.topCenter,
                                                              child: Text(
                                                                'x2',
                                                                style: Theme.of(context).textTheme.headline4.copyWith(
                                                                    color: customDarkGreyColor
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  index == 1
                                                      ?SizedBox()
                                                      :Padding(
                                                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                                    child: Divider(
                                                        color: customSmoothGreyColor
                                                    ),
                                                  )
                                                ],
                                              );
                                            })
                                        ),
                                        ///---order-subtotal
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              InkWell(
                                                onTap: (){
                                                  Get.toNamed(PageRoutes.orderHistoryDetail);
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(0, 10, 15, 8),
                                                  child: Text(
                                                    'View Order Details ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customThemeColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 8),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      'Sub Total:',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customLightBlackColor
                                                      ),
                                                    ),
                                                    SizedBox(width: 20,),
                                                    Text(
                                                      '\$300',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customThemeColor
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20,)
                              ],
                            );
                          }),
                        )
                      ],
                    ),
                    ///---third-view
                    ListView(
                      children: [
                        ///---search-bar
                        Padding(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          child: Container(
                            color: Color(0xffF9F9F9),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Theme(
                                data: ThemeData(
                                    primaryColor: Color(0xff363636)
                                ),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                                      border: InputBorder.none,
                                      hintText: 'Search Order',
                                      hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Wrap(
                          children: List.generate(2, (index){
                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: customIconListTileColor
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 10),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        ///---order-id-date
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  'Order ID: ',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customThemeColor
                                                  ),
                                                ),
                                                Text(
                                                  'NIC-446622-25',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customDarkGreyColor
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              '25/06/2021',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: customDarkGreyColor
                                              ),
                                            )
                                          ],
                                        ),
                                        ///---order-vendor-status
                                        Padding(
                                          padding: const EdgeInsets.only(top: 20),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    'By ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customLightBlackColor
                                                    ),
                                                  ),
                                                  Text(
                                                    'Luxury house décor',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customRatingStartColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                height: 21,
                                                decoration: BoxDecoration(
                                                    color: customRatingStartColor,
                                                    borderRadius: BorderRadius.circular(5)
                                                ),
                                                child: Center(
                                                  child: Padding(
                                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Text(
                                                      'Canceled',
                                                      style: Theme.of(context).textTheme.headline4.copyWith(
                                                          fontSize: 10,
                                                          color: Colors.white
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        ///---order-items
                                        SizedBox(height: 18,),
                                        Wrap(
                                            children: List.generate(2, (index){
                                              return Column(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(top: 0),
                                                    child: Container(
                                                      height: 86,
                                                      decoration: BoxDecoration(
                                                          color: Theme.of(context).scaffoldBackgroundColor,
                                                          borderRadius: BorderRadius.circular(10)
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          ///---item-image
                                                          Container(
                                                            height: double.infinity,
                                                            width: 80,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(10),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        'assets/images/featuredImage.png'
                                                                    ),
                                                                    fit: BoxFit.fill
                                                                )
                                                            ),
                                                          ),
                                                          ///---item-detail
                                                          Expanded(
                                                              child: Padding(
                                                                padding: const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Expanded(
                                                                      child: Text(
                                                                        'Modern ladies casual bag',
                                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                                            fontSize: 14,
                                                                            color: customDarkGreyColor
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      '\$150.00',
                                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                                          fontSize: 14,
                                                                          color: customThemeColor
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                          ),
                                                          ///---item-count
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(20, 13, 16, 0),
                                                            child: Align(
                                                              alignment: Alignment.topCenter,
                                                              child: Text(
                                                                'x2',
                                                                style: Theme.of(context).textTheme.headline4.copyWith(
                                                                    color: customDarkGreyColor
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  index == 1
                                                      ?SizedBox()
                                                      :Padding(
                                                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                                    child: Divider(
                                                        color: customSmoothGreyColor
                                                    ),
                                                  )
                                                ],
                                              );
                                            })
                                        ),
                                        ///---order-subtotal
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              InkWell(
                                                onTap: (){
                                                  Get.toNamed(PageRoutes.orderHistoryDetail);
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(0, 10, 15, 8),
                                                  child: Text(
                                                    'View Order Details ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customThemeColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 8),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      'Sub Total:',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customLightBlackColor
                                                      ),
                                                    ),
                                                    SizedBox(width: 20,),
                                                    Text(
                                                      '\$300',
                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                          fontSize: 14,
                                                          color: customThemeColor
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20,)
                              ],
                            );
                          }),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

}
