import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class ReviewsPage extends StatefulWidget {
  @override
  _ReviewsPageState createState() => _ReviewsPageState();
}

class _ReviewsPageState extends State<ReviewsPage>
    with SingleTickerProviderStateMixin{
  DateTime selectedDate = DateTime.now();

  TabController reviewsPageTabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    reviewsPageTabController =
    new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Persistent AppBar that never scrolls
      appBar: MyCustomAppBar(drawerShow: false,),
      body: NestedScrollView(
        // allows you to build a list of elements that would be scrolled away till the body reached the top
        headerSliverBuilder: (context, _) {
          return [
            SliverList(
              delegate: SliverChildListDelegate([
                ///---title
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'REVIEWS',
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                            color: customDarkBlackColor
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          _selectDate(context);
                        },
                        child: Container(
                          height: 30,
                          width: 105,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: customSmoothGreyColor)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              SvgPicture.asset(
                                'assets/Icons/other_icons/fi-rr-calendar.svg',
                                color: customThemeColor,
                              ),
                              Text(
                                '${selectedDate.toString().substring(0,11)}',
                                style: Theme.of(context).textTheme.subtitle2.copyWith(
                                    fontSize: 12,
                                    color: customThemeColor
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ]
                // _randomHeightWidgets(context),
              ),
            ),
          ];
        },
        // You tab view goes here
        body: Padding(
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom
          ),
          child: Column(
            children: <Widget>[
              ///---tabs
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Container(
                  color:Colors.white,
                  height: 40,
                  width: double.infinity,
                  child: TabBar(
                      onTap: (index){
                        setState(() {});
                      },
                      controller: reviewsPageTabController,
                      unselectedLabelColor: Colors.grey,
                      indicatorColor: Theme.of(context).buttonColor,
                      indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(
                            width: 2.0,
                            color: Theme.of(context).buttonColor,
                          ),
                          insets: EdgeInsets.symmetric(horizontal:0.0)
                      ),
                      labelStyle: Theme.of(context).textTheme.headline5.copyWith(fontSize: 12),
                      labelColor: Theme.of(context).buttonColor,
                      labelPadding: EdgeInsets.all(1),
                      tabs: [
                        Text(
                          'Pending',
                        ),
                        Text(
                          'History',
                        ),
                      ]
                  ),
                ),
              ),
              ///---tab-view
              Expanded(
                child: TabBarView(
                  controller: reviewsPageTabController,
                  children: [
                    ///---first-view
                    ListView(
                      children: [
                        ///---search-bar
                        Padding(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          child: Container(
                            color: Color(0xffF9F9F9),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Theme(
                                data: ThemeData(
                                    primaryColor: Color(0xff363636)
                                ),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                                      border: InputBorder.none,
                                      hintText: 'Search Product',
                                      hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Wrap(
                          children: List.generate(2, (index){
                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: customIconListTileColor
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        ///---order-id-date
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  'Order ID: ',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customThemeColor
                                                  ),
                                                ),
                                                Text(
                                                  'NIC-446622-25',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customDarkGreyColor
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              '25/06/2021',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: customDarkGreyColor
                                              ),
                                            )
                                          ],
                                        ),
                                        ///---order-vendor-status
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    'By ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customLightBlackColor
                                                    ),
                                                  ),
                                                  Text(
                                                    'Luxury house décor',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customRatingStartColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              InkWell(
                                                onTap: (){
                                                  Get.toNamed(PageRoutes.reviewWrite);
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                                                  child: Text(
                                                    'Write a Review',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customRatingStartColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        ///---order-items
                                        SizedBox(height: 8,),
                                        Wrap(
                                            children: List.generate(1, (index){
                                              return Column(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(top: 0),
                                                    child: Container(
                                                      height: 86,
                                                      decoration: BoxDecoration(
                                                          color: Theme.of(context).scaffoldBackgroundColor,
                                                          borderRadius: BorderRadius.circular(10)
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          ///---item-image
                                                          Container(
                                                            height: double.infinity,
                                                            width: 80,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(10),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        'assets/images/featuredImage.png'
                                                                    ),
                                                                    fit: BoxFit.fill
                                                                )
                                                            ),
                                                          ),
                                                          ///---item-detail
                                                          Expanded(
                                                              child: Padding(
                                                                padding: const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Expanded(
                                                                      child: Text(
                                                                        'Modern ladies casual bag',
                                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                                            fontSize: 14,
                                                                            color: customDarkGreyColor
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      '\$150.00',
                                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                                          fontSize: 14,
                                                                          color: customThemeColor
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                          ),
                                                          ///---item-count
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(0, 13, 16, 0),
                                                            child: Align(
                                                              alignment: Alignment.topCenter,
                                                              child: Text(
                                                                'x2',
                                                                style: Theme.of(context).textTheme.headline4.copyWith(
                                                                    color: customDarkGreyColor
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            })
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20,)
                              ],
                            );
                          }),
                        )
                      ],
                    ),
                    ///---second-view
                    ListView(
                      children: [
                        ///---search-bar
                        Padding(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                          child: Container(
                            color: Color(0xffF9F9F9),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Theme(
                                data: ThemeData(
                                    primaryColor: Color(0xff363636)
                                ),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      suffixIcon: Icon(Icons.search,color: customLightGreyColor,),
                                      border: InputBorder.none,
                                      hintText: 'Search Product',
                                      hintStyle: Theme.of(context).textTheme.headline4.copyWith(color: customLightGreyColor)
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Wrap(
                          children: List.generate(2, (index){
                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: customIconListTileColor
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(15, 18, 15, 18),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        ///---order-id-date
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  'Order ID: ',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customThemeColor
                                                  ),
                                                ),
                                                Text(
                                                  'NIC-446622-25',
                                                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                      fontSize: 12,
                                                      color: customDarkGreyColor
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              '25/06/2021',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 12,
                                                  color: customDarkGreyColor
                                              ),
                                            )
                                          ],
                                        ),
                                        ///---order-vendor-status
                                        Padding(
                                          padding: const EdgeInsets.only(top: 20),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    'By ',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customLightBlackColor
                                                    ),
                                                  ),
                                                  Text(
                                                    'Luxury house décor',
                                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                                        fontSize: 12,
                                                        color: customRatingStartColor,
                                                        decoration: TextDecoration.underline
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                'Change Review',
                                                style: Theme.of(context).textTheme.headline4.copyWith(
                                                    fontSize: 12,
                                                    color: customRatingStartColor,
                                                    decoration: TextDecoration.underline
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        ///---order-items
                                        SizedBox(height: 18,),
                                        Wrap(
                                            children: List.generate(1, (index){
                                              return Column(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(top: 0),
                                                    child: Container(
                                                      height: 86,
                                                      decoration: BoxDecoration(
                                                          color: Theme.of(context).scaffoldBackgroundColor,
                                                          borderRadius: BorderRadius.circular(10)
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          ///---item-image
                                                          Container(
                                                            height: double.infinity,
                                                            width: 80,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(10),
                                                                image: DecorationImage(
                                                                    image: AssetImage(
                                                                        'assets/images/featuredImage.png'
                                                                    ),
                                                                    fit: BoxFit.fill
                                                                )
                                                            ),
                                                          ),
                                                          ///---item-detail
                                                          Expanded(
                                                              child: Padding(
                                                                padding: const EdgeInsets.fromLTRB(16, 10, 0, 10),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Expanded(
                                                                      child: Text(
                                                                        'Modern ladies casual bag',
                                                                        style: Theme.of(context).textTheme.headline1.copyWith(
                                                                            fontSize: 14,
                                                                            color: customDarkGreyColor
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      '\$150.00',
                                                                      style: Theme.of(context).textTheme.headline1.copyWith(
                                                                          fontSize: 14,
                                                                          color: customThemeColor
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                          ),
                                                          ///---item-count
                                                          Padding(
                                                            padding: const EdgeInsets.fromLTRB(0, 13, 16, 0),
                                                            child: Align(
                                                              alignment: Alignment.topCenter,
                                                              child: Text(
                                                                'x2',
                                                                style: Theme.of(context).textTheme.headline4.copyWith(
                                                                    color: customDarkGreyColor
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            })
                                        ),
                                        ///---review-section
                                        SizedBox(height: 18,),
                                        Row(
                                          children: [
                                            SvgPicture.asset('assets/Icons/Product_page_icons/profile.svg'),
                                            SizedBox(width: 10,),
                                            Text(
                                              'John Doe',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 10,
                                                  color: customLightGreyColor
                                              ),
                                            ),
                                            SizedBox(width: 10,),
                                            Text(
                                              '(21 May 2021)',
                                              style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                  fontSize: 10,
                                                  color: customLightGreyColor
                                              ),
                                            ),
                                            Expanded(
                                              child: Align(
                                                alignment: Alignment.centerRight,
                                                child:  RatingBar(
                                                  initialRating: 5,
                                                  direction: Axis.horizontal,
                                                  allowHalfRating: true,
                                                  itemCount: 5,
                                                  itemSize: 15,
                                                  ratingWidget: RatingWidget(
                                                    full: Icon(Icons.star,color: customRatingStartColor,),
                                                    half: Icon(Icons.star_half,color: customRatingStartColor),
                                                    empty: Icon(Icons.star_border_purple500_sharp,color: customRatingStartColor),
                                                  ),
                                                  itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                                  onRatingUpdate: (rating) {
                                                    print(rating);
                                                  },
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: Text(
                                            'Nam tempus turpis at metus scelerisque placerat '
                                                'nulla deumantos solicitud felis. Pellentesque diam dolor.',
                                            style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                fontSize: 12,
                                                color: customLightBlackColor),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10),
                                          child: SizedBox(
                                            height: 50,
                                            width: double.infinity,
                                            child: ListView(
                                              scrollDirection: Axis.horizontal,
                                              children: List.generate(3, (index){
                                                return Padding(
                                                  padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                                  child: Container(
                                                    height: 50,
                                                    width: 60,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(5),
                                                    ),
                                                    child: ClipRRect(
                                                      borderRadius: BorderRadius.circular(5),
                                                      child: Image.asset('assets/images/productImage.png',
                                                        fit: BoxFit.fill,),
                                                    ),
                                                  ),
                                                );
                                              }),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20,)
                              ],
                            );
                          }),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}
