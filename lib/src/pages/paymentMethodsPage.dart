import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/components/customAppBar.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class PaymentMethodsPage extends StatefulWidget {
  @override
  _PaymentMethodsPageState createState() => _PaymentMethodsPageState();
}

class _PaymentMethodsPageState extends State<PaymentMethodsPage> {

  bool selectedType1 = false;
  bool selectedType2 = false;

  bool isSwitched1 = false;
  bool isSwitched2 = false;
  List isSwitchedList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    isSwitchedList = [
      isSwitched1,
      isSwitched2
    ];
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        appBar: MyCustomAppBar(
          drawerShow: false,
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///---title
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
                child: Text(
                  'PAYMENT METHODS',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(color: customLightBlackColor),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///---payment-method-type
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: (){
                                  setState(() {
                                    selectedType1 = true;
                                    selectedType2 = false;
                                  });
                                },
                                child: Container(
                                  height: 74,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: selectedType1
                                          ?customThemeColor
                                          :Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 2,
                                          blurRadius: 4,
                                          offset: Offset(0,2)
                                      )]
                                  ),
                                  child: Center(
                                    child: ListTile(
                                      leading: SvgPicture.asset(
                                        'assets/Icons/cart_checkout_icons/Group 385.svg',
                                        color: selectedType1
                                            ?Colors.white
                                            :customThemeColor,
                                      ),
                                      title: Text(
                                        'Credit Card',
                                        style: Theme.of(context).textTheme.headline4.copyWith(
                                            color: selectedType1
                                                ?Colors.white
                                                :customDarkGreyColor
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 15,),
                            Expanded(
                              child: InkWell(
                                onTap: (){
                                  setState(() {
                                    selectedType1 = false;
                                    selectedType2 = true;
                                  });
                                },
                                child: Container(
                                  height: 74,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: selectedType2
                                          ?customThemeColor
                                          :Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 2,
                                          blurRadius: 4,
                                          offset: Offset(0,2)
                                      )]
                                  ),
                                  child: Center(
                                    child: ListTile(
                                      leading: selectedType2
                                          ?SvgPicture.asset(
                                        'assets/Icons/cart_checkout_icons/paypal-white.svg',
                                      )
                                          :SvgPicture.asset(
                                        'assets/Icons/cart_checkout_icons/Group 386.svg',
                                      ),
                                      title: Text(
                                        'PayPal',
                                        style: Theme.of(context).textTheme.headline4.copyWith(
                                            color: selectedType2
                                                ?Colors.white
                                                :customDarkGreyColor
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      ///---current-cards
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 25, 15, 12),
                        child: Text(
                          'Current Cards',
                          style: Theme.of(context).textTheme.headline5.copyWith(
                            color: customDarkBlackColor,
                            fontSize: 18
                          ),
                        ),
                      ),
                      Wrap(
                        children: List.generate(2, (index){
                          return Padding(
                            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                            child: Column(
                              children: [
                                Container(
                                  height: 174,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).scaffoldBackgroundColor,
                                    borderRadius: BorderRadius.circular(10),
                                      boxShadow: [BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 2,
                                          blurRadius: 4,
                                          offset: Offset(0,2)
                                      )]
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(20, 20, 15, 0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            SvgPicture.asset(
                                              'assets/Icons/other_icons/visa.svg'
                                            ),
                                            SizedBox(width: 20,),
                                            Text(
                                              '***** **** 3354',
                                              style: Theme.of(context).textTheme.headline4.copyWith(
                                                fontSize: 12,
                                                color: customLightGreyColor
                                              ),
                                            )
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 12, 0, 5),
                                          child: Container(
                                            height: 1,
                                            width: double.infinity,
                                            color: customSmoothGreyColor,
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Make Default',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline4
                                                  .copyWith(
                                                  fontSize: 12,
                                                  color: customLightBlackColor),
                                            ),
                                            Switch(
                                              value: isSwitchedList[index],
                                              onChanged: (value) {
                                                setState(() {
                                                  isSwitchedList[index] = value;
                                                  print(isSwitchedList[index]);
                                                });
                                              },
                                              activeTrackColor: customSelectorColor,
                                              activeColor: customThemeColor,
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: [
                                              InkWell(
                                                onTap: (){
                                                  setState(() {});
                                                },
                                                child: Container(
                                                  height: 40,
                                                  width: 119,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(5),
                                                      border: Border.all(color: customRatingStartColor),
                                                      color: Colors.white
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'Remove',
                                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                          fontSize: 15,
                                                          color: customRatingStartColor
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: (){
                                                  Get.back();
                                                },
                                                child: Container(
                                                  height: 40,
                                                  width: 119,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(5),
                                                      color: customThemeColor
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'Add New',
                                                      style: Theme.of(context).textTheme.subtitle2.copyWith(
                                                          fontSize: 15,
                                                          color: Colors.white
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 20,)
                              ],
                            ),
                          );
                        }),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
