import 'package:flutter/material.dart';

const Color customThemeColor = Color(0xff1DA1F2);
const Color customSelectorColor = Color(0xffD1ECFD);
const Color customLightBlueColor = Color(0xffE8F5FE);

const Color customDarkGreyColor = Color(0xff747474);
const Color customBorderGreyColor = Color(0xff929294);
const Color customLightGreyColor=Color(0xffA7A7A7);
const Color customSmoothGreyColor=Color(0xffDBDBDB);
const Color customDividerGreyColor=Color(0xffBBBBBB);

const Color customLightBrownColor=Color(0xff332B29);

const Color customDarkBlackColor = Color(0xff1D1F26);
const Color customLightBlackColor = Color(0xff363636);

const Color customVoucherColor1 = Color(0xffF21D80);
const Color customVoucherColor2 = Color(0xffFD8F00);

const Color customRatingStartColor = Color(0xffFD8F00);
const Color customGreenSaleFlagColor = Color(0xff00CB5F);
const Color customIconListTileColor = Color(0xffF9F9F9);
const Color customVendorCardColor=Color(0xffF8F8F8);
const Color customRedColor=Color(0xffDD4B39);
const Color customBlueColor=Color(0xff4267B2);
