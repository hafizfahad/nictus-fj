import 'package:flutter/material.dart';
import 'package:get/get.dart';


//---------error-message-box--------------------
errorMessageBox(String title, String message){
  return Get.defaultDialog(
    titleStyle: TextStyle(color: Colors.red, fontSize: 20),
      title: '$title',
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          '$message',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.grey, fontSize: 14),
        ),
      ),
      cancel: InkWell(
        onTap: () {
          Get.back();
        },
        child: Container(
          width: 70,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(10)),
          child: Center(
            child: Text(
              'OK',
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
          ),
        ),
      ),
      onCancel: () {});
}


//---------success-message-box--------------------
successMessageBox(String title, String message){
  return Get.defaultDialog(
      titleStyle: TextStyle(color: Colors.green, fontSize: 20),
      title: '$title',
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          '$message',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.grey, fontSize: 14),
        ),
      ),
      cancel: InkWell(
        onTap: () {
          Get.back();
        },
        child: Container(
          width: 70,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(10)),
          child: Center(
            child: Text(
              'OK',
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
          ),
        ),
      ),
      onCancel: () {});
}

