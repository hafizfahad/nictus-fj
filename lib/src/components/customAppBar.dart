import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class MyCustomAppBar extends StatelessWidget implements PreferredSizeWidget{
  @override
  Size get preferredSize => Size.fromHeight(55);

  MyCustomAppBar({this.drawerShow,this.transparent});
  final bool drawerShow;
  final bool transparent;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder:(_)=> drawerShow
          ?AppBar(
        elevation: 0,
        backgroundColor: transparent == null
            ?Colors.white
            :Colors.transparent,
        leading: Builder(
          builder: (context) => InkWell(
            onTap: () {
              Scaffold.of(context).openDrawer();
            },
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: SvgPicture.asset(
                'assets/Icons/menu.svg',
                color: transparent == null
                    ?customThemeColor
                    :Colors.white,
              ),
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: SvgPicture.asset(
              'assets/Icons/bell.svg',
              color: transparent == null
                ?customThemeColor
                :Colors.white,
            ),
          ),
          InkWell(
            onTap: (){
              Get.toNamed('/cart');
            },
            child: Stack(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15.0),
                    child: SvgPicture.asset(
                      'assets/Icons/shopping-cart.svg',
                      color: transparent == null
                          ?customThemeColor
                          :Colors.white,
                    ),
                  ),
                ),
                _.cartList.length == 0
                    ? SizedBox()
                    :Positioned(
                  top: 5,
                  right: 8,
                  child: CircleAvatar(
                    backgroundColor: customThemeColor,
                    radius: 10,
                    child: Text(
                      '${_.cartList.length}',style: Theme.of(context).textTheme.subtitle2.copyWith(color: Colors.white),
                    ),
                  )
                )
              ],
            ),
          )
        ],
      )
          :AppBar(
        elevation: 0,
        backgroundColor: transparent == null
            ?Colors.white
            :Colors.transparent,
        leading: InkWell(
            onTap:(){
              Get.back();
            },
            child: Icon(
              Icons.arrow_back,
              color: transparent == null
                  ?customThemeColor
                  :Colors.white,
            )
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: SvgPicture.asset(
              'assets/Icons/bell.svg',
              color: transparent == null
                  ?customThemeColor
                  :Colors.white,
            ),
          ),
          InkWell(
            onTap: (){
              Get.toNamed('/cart');
            },
            child: Stack(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15.0),
                    child: SvgPicture.asset(
                      'assets/Icons/shopping-cart.svg',
                    color: transparent == null
                        ?customThemeColor
                        :Colors.white,
                        ),
                  ),
                ),
                _.cartList.length == 0
                    ? SizedBox()
                    :Positioned(
                    top: 5,
                    right: 8,
                    child: CircleAvatar(
                      backgroundColor: customThemeColor,
                      radius: 10,
                      child: Text(
                        '${_.cartList.length}',style: Theme.of(context).textTheme.subtitle2.copyWith(color: Colors.white),
                      ),
                    )
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
