import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

class CustomGeneralBottomBar extends StatefulWidget {
  const CustomGeneralBottomBar({this.text,this.icon});
  final String text;
  final String icon;

  @override
  _CustomGeneralBottomBarState createState() => _CustomGeneralBottomBarState();
}

class _CustomGeneralBottomBarState extends State<CustomGeneralBottomBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(15.0),
              topLeft: Radius.circular(15.0)),
          color: customThemeColor
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          widget.icon == null
              ?SizedBox()
              :SvgPicture.asset(
            '${widget.icon}',
            color: Colors.white,
          ),
          SizedBox(width: 15,),
          Text(
            '${widget.text}',
            style: Theme.of(context).textTheme.headline1.copyWith(
                fontSize: 16,
                color: Colors.white
            ),
          )
        ],
      ),
    );
  }
}
