import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/utils/colors.dart';

notLoggedAlert(BuildContext context, String title, String message){
  return Get.defaultDialog(
      titleStyle: Theme.of(context).textTheme.headline1.copyWith(
          color: customDarkBlackColor
      ),
      title: '$title',
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          '$message',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle2.copyWith(
            color: customLightBlackColor
          ),
        ),
      ),
      confirm: InkWell(
        onTap: () {
          Navigator.pop(context);
          Get.toNamed(PageRoutes.login);
        },
        child: Container(
          width: 100,
          height: 30,
          decoration: BoxDecoration(
              color: customThemeColor,
              borderRadius: BorderRadius.circular(5)),
          child: Center(
            child: Text(
              'Continue',
              style: Theme.of(context).textTheme.headline1.copyWith(
                  color: Colors.white,
                fontSize: 12
              ),
            ),
          ),
        ),
      ),
      cancel: InkWell(
        onTap: () {
          Get.back();
        },
        child: Container(
          width: 100,
          height: 30,
          decoration: BoxDecoration(
              color: customDarkGreyColor,
              borderRadius: BorderRadius.circular(5)),
          child: Center(
            child: Text(
              'Cancel',
              style: Theme.of(context).textTheme.headline1.copyWith(
                  color: Colors.white,
                fontSize: 12
              ),
            ),
          ),
        ),
      ),
      onCancel: () {});
}
