import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getShopPageProductsModel.dart';
import 'package:multi_vendor_customer/src/models/productFilterModel.dart';
import 'package:multi_vendor_customer/src/services/apiUrls.dart';
import 'package:multi_vendor_customer/src/services/getService.dart';

///-------- get-shop-page-products-data-API-call
getShopPageProductsData(bool responseCheck, Map<String, dynamic> response) {
  if (responseCheck) {
    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetShopPageProductsDataCheck(true);

    getShopPageProductsModel = GetShopPageProductsModel.fromJson(response);

    shopPageProductListForPagination = [];
    getShopPageProductsModel.data.data.forEach((element) {
      shopPageProductListForPagination.add(element);
    });

    print(
        'getShopPageProductsModel ------>> ${getShopPageProductsModel.data.data.length}');
  } else if (!responseCheck && response == null) {
    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);
  }
}

getShopPageProductsDataPagination(
    bool responseCheck, Map<String, dynamic> response) {
  if (responseCheck) {
    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<AppController>().changeGetPaginationProgressCheck(false);

    getShopPageProductsModel = GetShopPageProductsModel.fromJson(response);

    getShopPageProductsModel.data.data.forEach((element) {
      shopPageProductListForPagination.add(element);
    });

    print(
        'getShopPageProductsModel ------>> ${getShopPageProductsModel.data.data.length}');
  } else if (!responseCheck && response == null) {
    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);
  }
}

getMethodForFilterProduct(
    BuildContext context, ProductFilterModel _productFilterModel) {
  ///------get-shop-page-filtered-products-api-call
  getMethod(
      context,
      getFilteredProductsForShopPageApi,
      {
        'category': _productFilterModel.selectedCategorySlug == null
            ? null
            : _productFilterModel.selectedCategorySlug,
        'brands[]': _productFilterModel.selectedBrandList == null
            ? []
            : _productFilterModel.selectedBrandList,
        'attributes[]': _productFilterModel.selectedAttributesList == null
            ? []
            : List.generate(
                _productFilterModel.selectedAttributesList.length,
                (index) => jsonEncode({
                  'attribute_id': _productFilterModel.selectedAttributesList[index]['attribute_id'],
                  'slug': _productFilterModel.selectedAttributesList[index]['slug']
                  }),
                ),
        'sort': _productFilterModel.selectedSorting == null
            ? null
            : jsonEncode({
                'field': _productFilterModel.selectedSorting.sortField,
                'type': _productFilterModel.selectedSorting.sortType
              }),
        'page': _productFilterModel.selectedPage == null
            ? null
            : (_productFilterModel.selectedPage + 1),
        'min_price': _productFilterModel.minPrice == 0
            ? null
            : _productFilterModel.minPrice,
        'max_price': _productFilterModel.maxPrice == 1
            ? null
            : _productFilterModel.maxPrice,
        'search': _productFilterModel.searchName == null
            ? null
            : _productFilterModel.searchName
      },
      false,
      getShopPageProductsData);
}


getMethodForFilterProductForPagination(
    BuildContext context, ProductFilterModel _productFilterModel) {
  ///------get-shop-page-filtered-products-api-call
  getMethod(
      context,
      getFilteredProductsForShopPageApi,
      {
      'category': _productFilterModel.selectedCategorySlug == null
      ? null
          : _productFilterModel.selectedCategorySlug,
      'brands[]': _productFilterModel.selectedBrandList == null
      ? []
          : _productFilterModel.selectedBrandList,
      'attributes[]': _productFilterModel.selectedAttributesList == null
      ? []
          : List.generate(
      _productFilterModel.selectedAttributesList.length,
      (index) => jsonEncode({
      'attribute_id': _productFilterModel.selectedAttributesList[index]['attribute_id'],
      'slug': _productFilterModel.selectedAttributesList[index]['slug']
      }),
      ),
      'sort': _productFilterModel.selectedSorting == null
      ? null
          : jsonEncode({
      'field': _productFilterModel.selectedSorting.sortField,
      'type': _productFilterModel.selectedSorting.sortType
      }),
      'page': _productFilterModel.selectedPage == null
      ? null
          : (_productFilterModel.selectedPage + 1),
      'min_price': _productFilterModel.minPrice == 0
      ? null
          : _productFilterModel.minPrice,
      'max_price': _productFilterModel.maxPrice == 1
      ? null
          : _productFilterModel.maxPrice,
      'search': _productFilterModel.searchName == null
      ? null
          : _productFilterModel.searchName
      },
      false,
      getShopPageProductsDataPagination);
}
