import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getFeaturedProductsModel.dart';


///-------- get-featured-product-data-API-call
getFeaturedProductsData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetFeaturedProductsDataCheck(true);

    getFeaturedProductsModel = FeaturedProductsModel.fromJson(response);
    print('getFeaturedProducts ------>> ${getFeaturedProductsModel.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}