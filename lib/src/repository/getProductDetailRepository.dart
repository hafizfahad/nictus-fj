import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getProductDetailModel.dart';


///-------- get-product-detail-data-API-call
getProductDetailData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    getProductDetailModel = GetProductDetailModel.fromJson(response);

    getProductDetailModel.data.product.attributes.forEach((element) {
      element.values[0].isSelected = true;
      print('--->>${element.values[0].isSelected}');
    });

    Get.find<HomeController>().changeGetProductDetailDataCheck(true);

    print('GetProductDetailModel ------>> ${getProductDetailModel.data.product.name}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}