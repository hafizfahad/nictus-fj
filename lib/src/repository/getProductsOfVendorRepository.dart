import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getProductsOfVendorModel.dart';


///-------- get-product-of-vendor-data-API-call
getProductsOfVendorData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetProductsOfVendorDataCheck(true);

    getProductsOfVendorModel = GetProductsOfVendorModel.fromJson(response);
    print('getProductsOfVendorModel ------>> ${getProductsOfVendorModel.data.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}