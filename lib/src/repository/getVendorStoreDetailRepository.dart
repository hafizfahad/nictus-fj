import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getVendorStoreDetailModel.dart';


///-------- get-vendor-detail-data-API-call
getVendorStoreDetailData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetVendorStoreDetailDataCheck(true);

    getVendorStoreDetailModel = GetVendorStoreDetailModel.fromJson(response);
    print('getProductsOfVendorModel ------>> ${getVendorStoreDetailModel.data.categories.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}