import 'dart:convert';

import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/defaultSettingsModel.dart';
import 'package:multi_vendor_customer/src/models/enviromentModel.dart';

///-------- get-default-settings-API-call
getDefaultSettings(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    defaultSettingsModel = DefaultSettingsModel.fromJson(response);
    print('defaultSettings ------>> ${defaultSettingsModel.data.language.code}');
    box.write('defaultSettings', defaultSettingsModel);
    DefaultSettingsModel temp = box.read('defaultSettings');
    print("defaultSettings-box--->> ${temp.data.language.name}");

    if(!box.hasData('customLanguageSettings') && !box.hasData('customCurrencySettings')){

      print('setCustomLanguage&CurrencySettings');
      box.write('customLanguageSettings', jsonEncode(defaultSettingsModel.data.language));
      box.write('customCurrencySettings', jsonEncode(defaultSettingsModel.data.currency));

      // Language language = Language.fromJson(box.read('customLanguageSettings'));
      Language language = Language.fromJson(jsonDecode(box.read('customLanguageSettings')));
      print("customLanguageSettings-box--->> ${language.name}");

      // Currency currency = Currency.fromJson(box.read('customCurrencySettings'));
      Currency currency = Currency.fromJson(jsonDecode(box.read('customCurrencySettings')));
      print("customCurrencySettings-box--->> ${currency.name}");
    }
    else{
      Language language = Language.fromJson(jsonDecode(box.read('customLanguageSettings')));
      print("customLanguageSettings-box--->> ${language.name}");

      Currency currency = Currency.fromJson(jsonDecode(box.read('customCurrencySettings')));
      print("customCurrencySettings-box--->> ${currency.name}");
    }

  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}