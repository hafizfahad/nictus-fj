import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';


///-------- wishList-data-API-call
addToWishListDataRepo(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);
    Get.find<AppController>().changeLoaderCheck(false);


    Get.snackbar(
        'Success',
        '${response['data']['message']}',
        colorText: Colors.black
    );

    print('addToWishListDataRepo ------>> ${response}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeLoaderCheck(false);
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}