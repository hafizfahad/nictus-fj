import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getLoginDataModel.dart';


///-------- logout-data-API-call
logoutDataRepo(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);
    Get.find<AppController>().changeLoaderCheck(false);

    Get.offAllNamed(PageRoutes.bottomNavBar);

    box.remove('authToken');
    box.remove('userData');
    currentUserModel = new UserModel();

    print('--->>logout');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeLoaderCheck(false);
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}