import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getShopPageDataModel.dart';
import 'package:multi_vendor_customer/src/models/getShopPageProductsModel.dart';
import 'package:multi_vendor_customer/src/pages/shopFiltersPage.dart';

///-------- get-shop-page-data-API-call
getShopPageData(bool responseCheck, Map<String, dynamic> response) {
  if (responseCheck) {
    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetShopPageDataCheck(true);

    getShopPageDataModel = GetShopPageDataModel.fromJson(response);

    ///-categories-list-for-selection
    categoryFilterColorSelectorList =
        List.generate(getShopPageDataModel.data.categories.length, (index) {
      return ColorChangeSelectorClass(
        id: getShopPageDataModel.data.categories[index].id,
        name: getShopPageDataModel.data.categories[index].name,
        slug: getShopPageDataModel.data.categories[index].slug,
        isSelected: false,
      );
    });

    ///-brands-list-for-selection
    brandFilterColorSelectorList =
        List.generate(getShopPageDataModel.data.brands.length, (index) {
      return ColorChangeSelectorClass(
        id: getShopPageDataModel.data.brands[index].id,
        name: getShopPageDataModel.data.brands[index].name,
        slug: getShopPageDataModel.data.brands[index].name,
        isSelected: false,
      );
    });

    print(
        'getShopPageDataModel ------>> ${getShopPageDataModel.data.categories.length}');
  } else if (!responseCheck && response == null) {
    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);
  }
}
