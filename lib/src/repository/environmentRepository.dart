import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/enviromentModel.dart';


///-------- get-environment-API-call
getEnvironment(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    environmentModel = EnvironmentModel.fromJson(response);
    print('environment ------>> ${environmentModel.environment}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}