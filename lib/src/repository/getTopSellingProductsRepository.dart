import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getTopSellingProductsModel.dart';


///-------- get-top-selling-data-API-call
getTopSellingProductsData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetTopSellingProductsDataCheck(true);

    getTopSellingProductsModel = GetTopSellingProductsModel.fromJson(response);
    print('GetTopSellingProductsModel ------>> ${getTopSellingProductsModel.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}