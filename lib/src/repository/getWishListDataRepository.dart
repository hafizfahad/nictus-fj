import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getWishListProductsModel.dart';


///-------- get-wish-list-product-data-API-call
getWishListProductsData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetWishListDataCheckDataCheck(true);

    getWishListProductsModel = GetWishListProductsModel.fromJson(response);

    wishListProductListForPagination = [];
    getWishListProductsModel.data.data.forEach((element) {
      wishListProductListForPagination.add(element);
    });

    print('getWishListProductsModel ------>> ${getWishListProductsModel.data.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}

getWishListProductsDataForPagination(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<AppController>().changeGetPaginationProgressCheck(false);

    getWishListProductsModel = GetWishListProductsModel.fromJson(response);

    getWishListProductsModel.data.data.forEach((element) {
      wishListProductListForPagination.add(element);
    });

    print('getWishListProductsModelPagination ------>> ${wishListProductListForPagination.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}