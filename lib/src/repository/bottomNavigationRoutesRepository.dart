import 'package:flutter/material.dart';
import 'package:multi_vendor_customer/src/pages/categoriesPage.dart';
import 'package:multi_vendor_customer/src/pages/homeModules/home1/homePage1.dart';
import 'package:multi_vendor_customer/src/pages/shopPage.dart';
import 'package:multi_vendor_customer/src/pages/userProfilePage.dart';
import 'package:multi_vendor_customer/src/pages/wishListPage.dart';

homeBottomNavigationRepository(
  int index,
  BuildContext context,
) {
  switch (index) {
    case 0:
      {
        return HomePage();
      }
      break;
    case 1:
      {
        return ShopPage();
      }
      break;
    case 2:
      {
        return CategoriesPage();
      }
      break;
    case 3:
      {
        return WishListPage();
      }
      break;
    case 4:
      {
        return UserProfilePage();
      }
      break;
    default:
      {
        return Scaffold(
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        );
      }
  }
}
