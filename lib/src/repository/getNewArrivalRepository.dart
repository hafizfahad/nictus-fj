import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getNewArrivalProductsModal.dart';


///-------- get-new-arrival-data-API-call
getNewArrivalData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetNewArrivalDataCheck(true);

    getNewArrivalModal = NewArrivalModal.fromJson(response);
    print('getNewArrivalData ------>> ${getNewArrivalModal.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}