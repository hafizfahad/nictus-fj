import 'dart:convert';

import 'package:get/get.dart';
import 'package:multi_vendor_customer/routeGenerator.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getLoginDataModel.dart';


///-------- login-data-API-call
loginDataRepo(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);
    Get.find<AppController>().changeLoaderCheck(false);

    getLoginDataModel = GetLoginDataModel.fromJson(response);

    box.write('authToken', getLoginDataModel.data.token);
    box.write('userData', jsonEncode(getLoginDataModel.data.user));

    Get.offAllNamed(PageRoutes.bottomNavBar);

    print('getLoginDataModel ------>> ${getLoginDataModel.data.token}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeLoaderCheck(false);
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}