import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getCategoryWiseProductsModel.dart';


///-------- get-category-Wise-data-API-call
getCategoryWiseProductsData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){
    categoryWiseProductsTabList = [];
    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetCategoryWiseProductsDataCheck(true);

    getCategoryWiseProductsModel = GetCategoryWiseProductsModel.fromJson(response);
    print('GetCategoryWiseProductsModel ------>> ${getCategoryWiseProductsModel.data.allCategories.length}');

    categoryWiseProductsTabList.add(Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'All Categories',
          ),
        ],
      ),
    ));
    getCategoryWiseProductsModel.data.otherCats.forEach((element) {
      categoryWiseProductsTabList.add(Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '${element.name}',
            ),
          ],
        ),
      ));
    });

  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}