import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getTopReviewedProductsModel.dart';


///-------- get-top-reviewed-data-API-call
getTopReviewedProductsData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetTopReviewedProductsDataCheck(true);

    getTopReviewedProductsModel = GetTopReviewedProductsModel.fromJson(response);
    print('GetTopReviewedProductsModel ------>> ${getTopReviewedProductsModel.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}