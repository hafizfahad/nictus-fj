import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/getFeaturedVendorsModel.dart';


///-------- get-featured-vendors-data-API-call
getFeaturedVendorsProductsData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetFeaturedVendorsProductsDataCheck(true);

    getFeaturedVendorsModel = GetFeaturedVendorsModel.fromJson(response);
    print('GetFeaturedVendorsModel ------>> ${getFeaturedVendorsModel.data.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}