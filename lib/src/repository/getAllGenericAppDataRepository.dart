import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/genericAppDataModel.dart';


///-------- get-app-generic-data-API-call
getAllGenericAppData(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetAllGenericAppDataCheck(true);

    genericAppDataModel = GetGenericAppDataModel.fromJson(response);
    print('genericAppDataModel ------>> ${genericAppDataModel.data.sliderImages.length}');
  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}