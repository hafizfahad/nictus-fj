import 'package:get/get.dart';
import 'package:multi_vendor_customer/src/controllers/appController.dart';
import 'package:multi_vendor_customer/src/controllers/homeController.dart';
import 'package:multi_vendor_customer/src/data/globalData.dart';
import 'package:multi_vendor_customer/src/models/enviromentModel.dart';
import 'package:multi_vendor_customer/src/models/getAllCategoriesModel.dart';


///-------- get-all-categories-API-call
getAllCategories(bool responseCheck, Map<String, dynamic> response){

  if(responseCheck){

    Get.find<AppController>().changeServerErrorCheck(false);

    Get.find<HomeController>().changeGetAllCategoriesCheck(true);

    getAllCategoriesModel = GetAllCategoriesModel.fromJson(response);
    print('all-categories ------>> ${getAllCategoriesModel.data.length}');

  }
  else if(!responseCheck && response == null){

    print('Exception........................');
    Get.find<AppController>().changeServerErrorCheck(true);

  }
}